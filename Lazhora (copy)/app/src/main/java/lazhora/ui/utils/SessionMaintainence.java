package lazhora.ui.utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;

/**
 * Created by Arivu on 10-12-2015.
 */
public class SessionMaintainence {

    public static final String PREF_NAME = "lazhora";
    public static final String KEY_CUS_NAME = "cl_cus_name";
    public static final String KEY_CUS_CONTACT = "cl_cus_mobile";
    private static final String KEY_IP = "ip";
    public static final String KEY_PHNUM1 = "phnum1";
    public static final String KEY_PHNUM2 = "phnum2";
    private static final String KEY_ADDRESS = "address";
    private static final String KEY_EMPLOYMENT = "employment";
    private static final String KEY_STATUS = "status";
    private static final String KEY_SCREEN_ID = "screen_id";
    private static final String KEY_CATEGORY = "category";

    private static final String KEY_ZHORA_POINTS = "zhora_points";
    private static final String KEY_CARTLIST_COUNT = "cart_list_count";
    private static final String KEY_PAGE_ID = "page_id";
    private static final String KEY_ZHORA_POINTS_DISCOUNT = "zhora_points_discount";

    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    Context context;
    int PRIVATE_MODE = 0;
    private static SessionMaintainence sessionMaintainance;

    public SessionMaintainence(Context context) {
        this.context = context;
        preferences = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = preferences.edit();
    }

    public static void init(Context context) {
        if (sessionMaintainance == null) {
            sessionMaintainance = new SessionMaintainence(context);
        }
    }

    public static SessionMaintainence getInstance() {
        return sessionMaintainance;
    }

    public String getIp() {

        String userdetail1 = "http://ec2-54-148-22-97.us-west-2.compute.amazonaws.com/";
        return userdetail1;
    }

    public String getCusName() {
        return preferences.getString(KEY_CUS_NAME, "");
    }

    public void setCusName(String screenId) {
        editor.putString(KEY_CUS_NAME, screenId);

        editor.commit();
    }

    public String getCusMobile() {
        return preferences.getString(KEY_PHNUM1, "");
    }

    public void setCusMobile(String screenId) {
        editor.putString(KEY_PHNUM1, screenId);

        editor.commit();
    }

    public boolean getStatus() {
        return preferences.getBoolean(KEY_STATUS, false);
    }

    public void setStatus(boolean status) {
        editor.putBoolean(KEY_STATUS, status);

        editor.commit();
    }

    public String getScreenId() {
        return preferences.getString(KEY_SCREEN_ID, "");
    }

    public void setScreenId(String screenId) {
        editor.putString(KEY_SCREEN_ID, screenId);

        editor.commit();
    }

    public String getCategory() {
        return preferences.getString(KEY_CATEGORY, "");
    }

    public void setCategory(String category) {
        editor.putString(KEY_CATEGORY, category);

        editor.commit();
    }

    public void setCustomerInfo(String name,
                                String phnum1,
                                String phnum2,
                                String address,
                                String employment) {
        editor.putString(KEY_CUS_NAME, name);
        editor.putString(KEY_PHNUM1, phnum1);
        editor.putString(KEY_PHNUM2, phnum2);
        editor.putString(KEY_ADDRESS, address);
        editor.putString(KEY_EMPLOYMENT, employment);

        editor.commit();
    }

    public HashMap<String, String> getCustomerInfo() {
        HashMap<String, String> cus_datas = new HashMap<>();
        cus_datas.put(KEY_CUS_NAME, preferences.getString(KEY_CUS_NAME, ""));
        cus_datas.put(KEY_PHNUM1, preferences.getString(KEY_PHNUM1, ""));
        cus_datas.put(KEY_PHNUM2, preferences.getString(KEY_PHNUM2, ""));
        cus_datas.put(KEY_ADDRESS, preferences.getString(KEY_ADDRESS, ""));
        cus_datas.put(KEY_EMPLOYMENT, preferences.getString(KEY_EMPLOYMENT, ""));

        return cus_datas;
    }

    public void clearSession() {
        editor.clear();
        editor.commit();
    }

    public void setZhoraPoints(int points) {
        editor.putInt(KEY_ZHORA_POINTS, points);

        editor.commit();
    }

    public int getZhoraPoints() {
        return preferences.getInt(KEY_ZHORA_POINTS, 0);
    }

    public void setCartListCount(int cartListCount) {
        editor.putInt(KEY_CARTLIST_COUNT, cartListCount);

        editor.commit();
    }

    public int getCartListCount() {
        return preferences.getInt(KEY_CARTLIST_COUNT, 0);
    }

    public void setZhoraPointsDiscount(int points) {
        editor.putInt(KEY_ZHORA_POINTS_DISCOUNT, points);

        editor.commit();
    }

    public int getZhoraPointsDiscount() {
        return preferences.getInt(KEY_ZHORA_POINTS_DISCOUNT, 0);
    }

    public void setPageID(String pageID) {
        editor.putString(KEY_PAGE_ID, pageID);

        editor.commit();
    }

    public String getPageID() {
        return preferences.getString(KEY_PAGE_ID, "");
    }


}
