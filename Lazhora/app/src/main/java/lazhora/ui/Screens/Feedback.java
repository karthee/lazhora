package lazhora.ui.Screens;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import lazhora.R;
import lazhora.ui.api.AddFeedBackApi;
import lazhora.ui.utils.Font;
import lazhora.ui.utils.NetworkConnectionVerification;
import lazhora.ui.utils.SessionMaintainence;

/**
 * Created by Arivu on 26-09-2016.
 */
public class Feedback extends AppCompatActivity {

    /**
     * The Feedback range.
     */
    RatingBar feedbackRange;
    /**
     * The Feedback submit.
     */
    Button feedbackSubmit;

    /**
     * The Font.
     */
    Font font;

    /**
     * The Title.
     */
    TextView title, /**
     * The Description.
     */
    description;
    /**
     * The Submit.
     */
    Button submit;
    private float vrating;

    /**
     * The Sm.
     */
    SessionMaintainence sm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        font = new Font(Feedback.this);
        sm  = SessionMaintainence.getInstance();


        SpannableString pageTitle = new SpannableString(getSupportActionBar().getTitle());
        pageTitle.setSpan(font.robotoMedium, 0, pageTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        getSupportActionBar().setTitle(pageTitle);
        submit = (Button) findViewById(R.id.feedback_submit);

        if (sm.getScreenId().equals("HomeActivity")) {
            submit.setText("Submit & Exit");
        } else {
            submit.setText("SUBMIT");
        }

        final NetworkConnectionVerification ncv = new NetworkConnectionVerification(Feedback.this);

        title = (TextView) findViewById(R.id.feedback_title);
        description = (TextView) findViewById(R.id.feedback_description);


        title.setTypeface(font.robotoRegular);
        description.setTypeface(font.robotoRegular);

        submit.setTypeface(font.robotoMedium);

        feedbackRange = (RatingBar) findViewById(R.id.feedback_range);
        LayerDrawable stars = (LayerDrawable) feedbackRange.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(getResources().getColor(
                R.color.colorPrimary),
                PorterDuff.Mode.SRC_ATOP);
        feedbackRange.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                InputMethodManager ipmm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                ipmm.hideSoftInputFromWindow(feedbackRange.getWindowToken(), 0);
                vrating = rating;
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String vtitle = title.getText().toString().trim();
                String vdesc = description.getText().toString().trim();
                if (vtitle.equals("") || vdesc.equals("")) {
                    Toast.makeText(Feedback.this, "Please fill the details",
                            Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        if (ncv.isNetworkAvailable()) {
                            new AddFeedBackApi(Feedback.this).execute(vtitle, vdesc,
                                    String.valueOf(vrating));

                        } else {
                            Toast.makeText(Feedback.this, "Please check for internet connection",
                                    Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Toast.makeText(Feedback.this, "Your feedback has been " +
                            "submitted", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
//        Intent i = new Intent(Feedback.this, HomeActivity.class);
//        startActivity(i);
        finish();
    }
}
