package lazhora.ui.Screens.home;

import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import butterknife.BindView;
import butterknife.ButterKnife;
import lazhora.R;
import lazhora.ui.Screens.ProductDetails;
import lazhora.ui.data.Item;

import static android.content.ContentValues.TAG;

/**
 * Created by arivuventures on 4/4/17.
 */

class ProductItemOnGridView extends HomeActivityViewHolder implements View.OnClickListener {

    @BindView(R.id.itemname)
    TextView itemName;

    @BindView(R.id.itemdesc)
    TextView itemDesc;

    @BindView(R.id.itemimage)
    ImageView itemImage;

    Context context;

    public ProductItemOnGridView(View rootView) {
        super(rootView);

        ButterKnife.bind(this, rootView);

        context = rootView.getContext();
        rootView.setOnClickListener(this);

    }

    public void bind(final Item item) {

        itemName.setText(Html.fromHtml(item.name.trim()));
        itemDesc.setText(item.desc);
        try {
            String pic = item.imageid;

            int resID = context.getResources().getIdentifier(pic.substring(
                    0, pic.lastIndexOf(".")),
                    "drawable", context.getPackageName());

            Glide.with(context).load("").thumbnail(0.5f)
                    .crossFade()
                    .placeholder(context.getResources().getDrawable(resID))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(itemImage);
        } catch (Exception e) {
            Log.e(TAG, item.imageid + "getView: " + e.toString());
        }
    }

    @Override
    public void onClick(View v) {

        String product_title = ((TextView) v.findViewById(R.id.itemname)).getText().toString();

        Log.e("gridview", "onItemClick: " + product_title);

        Intent i = new Intent(context, ProductDetails.class);
        i.putExtra("productname", product_title);
        context.startActivity(i);
    }
}
