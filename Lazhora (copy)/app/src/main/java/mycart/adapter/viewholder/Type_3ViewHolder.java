package mycart.adapter.viewholder;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import lazhora.R;
import mycart.MyCartPOJO;
import mycart.adapter.listener.DeleteProductListener;
import mycart.utils.SystemUtils;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * Created by Arivu on 21/03/17.
 */

public class Type_3ViewHolder extends MyViewHolder {

    @BindView(R.id.cart_pro_title)
    TextView productTitle;

    @BindView(R.id.cart_pro_price)
    TextView productPrice;

    @BindView(R.id.quantity2)
    TextView productQuantity;

    @BindView(R.id.increase)
    ImageView increase;

    @BindView(R.id.decrease)
    ImageView decrease;

    @BindView(R.id.cart_delete)
    ImageView close;

    @BindView(R.id.cart_pro_image)
    ImageView productImage;

    int quantity;

    Context context;

    public Type_3ViewHolder(View rootView) {
        super(rootView);

        ButterKnife.bind(this, rootView);

        context = rootView.getContext();
        productQuantity.setTag(View.generateViewId());
        increase.setTag(View.generateViewId());
        decrease.setTag(View.generateViewId());
        close.setTag(View.generateViewId());
    }

    public void bind(final MyCartPOJO myCartPOJO, final DeleteProductListener deletelistener) {


        productTitle.setText(myCartPOJO.getProductTitle());
        productImage.setImageDrawable(myCartPOJO.getProductImagePath());

        final String cat = myCartPOJO.getProductCategory();

        if (cat.equals("CREAMS") || cat.equals("OTHER")) {
            if (myCartPOJO.getQuantity2() > 5) {
                quantity--;
                if (quantity == 5) {
                    decrease.setAlpha((float) .54);
                    productQuantity.setAlpha((float) .54);
                    decrease.setClickable(false);
                }
            }
        } else {
            if (myCartPOJO.getQuantity2() > 0) {
                quantity--;
                if (quantity == 0) {
                    decrease.setAlpha((float) .54);
                    productQuantity.setAlpha((float) .54);
                    decrease.setClickable(false);
                }
            }
        }

        if (myCartPOJO.getProductPrice() == 0) {
            productPrice.setText("--");
        } else {
            productPrice.setText(String.valueOf(myCartPOJO.getProductPrice()));
        }
        productQuantity.setText(String.valueOf(myCartPOJO.getQuantity2()));

        final int actualPrice = myCartPOJO.getProductPrice() / myCartPOJO.getQuantity2();

        final View footerView = ((LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE))
                                        .inflate(R.layout.content_mycart_lv_footer, null);

        increase.setTag(R.id.quantity2, productQuantity);
        increase.setTag(R.id.cart_pro_price, productPrice);
        increase.setTag(R.id.decrease, decrease);
        increase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView productQuantity = (TextView) view.getTag(R.id.quantity2);
                TextView productPrice = (TextView) view.getTag(R.id.cart_pro_price);
                ImageView decrease = (ImageView) view.getTag(R.id.decrease);

                int price = 0;
                int quantity = Integer.parseInt(productQuantity.getText().toString());

                quantity++;

                decrease.setAlpha((float) .99);
                decrease.setClickable(true);
                productQuantity.setAlpha((float) .99);
                productQuantity.setText(String.valueOf(quantity));

                if (myCartPOJO.getProductPrice() != 0) {
                    price = actualPrice * quantity;
                    productPrice.setText(Integer.toString(price));

                    TextView overallPrice = (TextView) footerView.findViewById(R.id.overall_price);

                    int oPrice = Integer.parseInt(overallPrice.getText().toString());
                    overallPrice.setText(Integer.toString(oPrice + price));
                }
            }
        });

        decrease.setTag(R.id.quantity2, productQuantity);
        decrease.setTag(R.id.cart_pro_price, productPrice);
        decrease.setTag(R.id.decrease, decrease);
        decrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView productQuantity = (TextView) view.getTag(R.id.quantity2);
                TextView productPrice = (TextView) view.getTag(R.id.cart_pro_price);
                ImageView decrease = (ImageView) view.getTag(R.id.decrease);

                int price = 0;
                int quantity = Integer.parseInt(productQuantity.getText().toString());

                String category = myCartPOJO.getProductCategory();

                if (category.equals("CREAMS") || category.equals("OTHER")) {
                    if (quantity > 5) {
                        quantity--;
                        if (quantity == 5) {
                            decrease.setAlpha((float) .54);
                            productQuantity.setAlpha((float) .54);
                            decrease.setClickable(false);
                        }
                    }
                } else {
                    if (quantity > 0) {
                        quantity--;
                        if (quantity == 0) {
                            decrease.setAlpha((float) .54);
                            productQuantity.setAlpha((float) .54);
                            decrease.setClickable(false);
                        }
                    }
                }

                productQuantity.setText(String.valueOf(quantity));
                if (myCartPOJO.getProductPrice() != 0) {
                    price = actualPrice * quantity;
                    productPrice.setText(Integer.toString(price));

                    TextView overallPrice = (TextView) footerView.findViewById(R.id.overall_price);

                    int oPrice = Integer.parseInt(overallPrice.getText().toString());
                    overallPrice.setText(Integer.toString(oPrice + price));
                }
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setCancelable(false);
                final String title = (String) productTitle.getText();
                builder.setMessage("Sure to remove \"" + title + "\" from your cart?");
                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int id) {
//                                context.startActivity(new Intent(context, HomeActivity.class));
//                                ((Activity) context).finish();

                        String string = SystemUtils.getData(context);

                        try {
                            JSONArray resultSet = new JSONArray(string);
                            for (int i = 0; i < resultSet.length(); i++) {
                                JSONObject results = resultSet.getJSONObject(i);

//                                        String title = (String) productTitle.getText();

                                if (results.getString("title").equals(title)) {
                                    resultSet.remove(i);
                                }
                            }
                            SystemUtils.saveData(context, "mycart.json", resultSet.toString());
                            deletelistener.deleteProduct(myCartPOJO);
                        } catch (JSONException e) {
                            Log.e("jsonException", e.toString());
                        }



                        dialogInterface.dismiss();
                    }
                });

                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        dialogInterface.dismiss();
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();
                dialog.getButton(dialog.BUTTON_NEGATIVE).setTextColor(
                        context.getResources().getColor(R.color.colorDelete));
                dialog.getButton(dialog.BUTTON_POSITIVE).setTextColor(
                        context.getResources().getColor(R.color.colorPrimary));

            }
        });


    }
}
