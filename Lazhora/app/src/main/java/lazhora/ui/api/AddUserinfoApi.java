package lazhora.ui.api;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONObject;

import lazhora.ui.Screens.HomeActivity;
import lazhora.ui.utils.CustomProgressDialog;
import lazhora.ui.utils.JsonparserObject;
import lazhora.ui.utils.SessionMaintainence;

/**
 * Created by Karthee on 25/01/16.
 */
public class AddUserinfoApi extends AsyncTask<String, String, JSONObject> {


    /**
     * The constant KEY_OUTPUT_STATUS.
     */
// output keys for userinfo api
    public static final String KEY_OUTPUT_STATUS = "status";
    /**
     * The constant KEY_OUTPUT_DATA.
     */
    public static final String KEY_OUTPUT_DATA = "data";
    /**
     * The constant add_userinfo.
     */
    public static String add_userinfo = "add_user_detail";
    /**
     * The Json.
     */
    JSONObject json;
    private CustomProgressDialog progressBar;
    private boolean status;
    private String data;
    private Context context;
    private SessionMaintainence session;
    private String username, phnum1, phnum2, location, employment;

    /**
     * Instantiates a new Add userinfo api.
     *
     * @param context the context
     */
    public AddUserinfoApi(Context context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        /*
         *    do things before doInBackground() code runs
         *    such as preparing and showing a Dialog or ProgressBar
        */
        progressBar = new CustomProgressDialog(context, "Loading...");
        session = SessionMaintainence.getInstance();

    }

    @Override
    protected JSONObject doInBackground(String... f_url) {
        JsonparserObject jparser = JsonparserObject.getInstance();

        try {

            username = f_url[0];
            phnum1 = f_url[1];
            phnum2 = f_url[2];
            location = f_url[3];
            employment = f_url[4];

            JSONObject jobj = new JSONObject();

            jobj.put("name", f_url[0]);
            jobj.put("phone_one", f_url[1]);
            jobj.put("phone_two", f_url[2]);
            jobj.put("address", f_url[3]);
            jobj.put("employment", f_url[4]);

            json = jparser.makeHttpRequest(session.getIp() + add_userinfo,
                    "POST",
                    jobj.toString());

        } catch (Exception e) {
            Log.e("OnUserinfo", e.toString());
        }

        return json;
    }

    @Override
    protected void onPostExecute(JSONObject json) {
        /*
         *    do something with data here
         *    display it or send to mainactivity
         *    close any dialogs/ProgressBars/etc...
        */

        try {
            status = json.getBoolean(KEY_OUTPUT_STATUS);
        } catch (Exception e) {
            Toast.makeText(context, "Network Error. Please try again", Toast.LENGTH_SHORT).show();

            Log.e("OnUserInfo", e.toString());
        }
        if (status) {
            session.setCustomerInfo(username, phnum1, phnum2,
                    location, employment);
            session.setStatus(true);

            Intent i = new Intent(context, HomeActivity.class);
            context.startActivity(i);
            ((Activity) context).finish();
        } else {
            Toast.makeText(context, "Please check for internet connection.", Toast.LENGTH_SHORT).show();
        }
        // dismiss the dialog after the file was downloaded
        try {
            progressBar.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
