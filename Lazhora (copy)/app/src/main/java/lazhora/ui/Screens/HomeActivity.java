package lazhora.ui.Screens;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.SQLException;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.Button;
import android.widget.FilterQueryProvider;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import lazhora.R;
import lazhora.R.id;
import lazhora.presenter.ProductsListImplementation;
import lazhora.ui.Screens.home.HomeActivityRVAdapter;
import lazhora.ui.data.JsonDataCopy;
import lazhora.ui.db.DBManager;
import lazhora.ui.utils.AppRater;
import lazhora.ui.utils.CustomProgressDialog;
import lazhora.ui.utils.Font;
import lazhora.ui.utils.SearchFeedResultsAdaptor;
import lazhora.ui.utils.SessionMaintainence;


public class HomeActivity extends AppCompatActivity implements
        View.OnClickListener,
        NavigationView.OnNavigationItemSelectedListener,
        SearchView.OnQueryTextListener,
        SearchView.OnSuggestionListener, FilterQueryProvider,
        ProductsListImplementation.ProductsListListener {

    public static String[] columns = {"product_title"};
    DrawerLayout drawer;
    ActionBarDrawerToggle toggle;
    NavigationView navigationProfileView;
    NavigationView navigationCategoryView;
    View navHeader;
    ImageView settings;

    SessionMaintainence sessionmanager;
    Font font;
    Cursor matrixCursor;
    private SearchView searchView;
    private CursorAdapter mSearchViewAdapter;
    private NavigationView categoriesNavigationView;
    private Menu c;
    private Toolbar toolbar;
    private TextView tvCartList;
    private TextView tvZhoraPoints;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        ButterKnife.bind(this);

        verifyForLatestVersion();

        initViews();

        if (savedInstanceState == null) {
            Fragment newFragment = new ItemlistFragment();
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.add(id.container, newFragment).commit();
        }

    }

    private void initViews() {
        font = new Font(this);
        sessionmanager = SessionMaintainence.getInstance();
        toolbar = (Toolbar) findViewById(id.toolbar);

        setSupportActionBar(toolbar);

        SpannableString homeTitle;

        if (sessionmanager.getCategory().equals("")) {
            homeTitle = new SpannableString(getSupportActionBar().getTitle());
        } else {
            homeTitle = new SpannableString(sessionmanager.getCategory());
        }

        homeTitle.setSpan(font.robotoMedium, 0, homeTitle.length(),
                Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        getSupportActionBar().setTitle(homeTitle);

        AppRater.app_launched(this);

        navigationProfileView = (NavigationView) findViewById(id.nav_profile_view);
        navigationCategoryView = (NavigationView) findViewById(id.nav_category_view);

        if (sessionmanager.getStatus()) {
            showAlertDialog(getString(R.string.futurealert));
            sessionmanager.setStatus(false);
        }

        //navigation header view
        View header = navigationProfileView.getHeaderView(0);
        TextView username = (TextView) header.findViewById(id.user_name);
        username.setText(sessionmanager.getCustomerInfo().get(SessionMaintainence.KEY_CUS_NAME));

        Menu profileMenus = navigationProfileView.getMenu();

        for (int i = 0; i < profileMenus.size(); i++) {
            MenuItem item = profileMenus.getItem(i);

            SubMenu subMenu = item.getSubMenu();
            if (subMenu != null && subMenu.size() > 0) {
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);
                }
            }

            applyFontToMenuItem(item);
        }

        navHeader = navigationProfileView.getHeaderView(0);
        settings = (ImageView) navHeader.findViewById(id.settings);


        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(HomeActivity.this, Settings.class);
                startActivity(i);
            }
        });


        drawer = (DrawerLayout) findViewById(id.drawer_layout);
        toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);

        drawer.setDrawerListener(toggle);

        NavigationView profileNavigationView = (NavigationView) findViewById(id.nav_profile_view);
        profileNavigationView.setNavigationItemSelectedListener(this);

        categoriesNavigationView = (NavigationView) findViewById(id.nav_category_view);

        categoriesNavigationView.setNavigationItemSelectedListener(this);

        Menu m = profileNavigationView.getMenu();

        c = categoriesNavigationView.getMenu();
        new LoadData().execute();

        MenuItem youitem = m.findItem(id.action_you);
        SpannableString s = new SpannableString(youitem.getTitle());
        s.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorPrimaryDark)),
                0, s.length(), 0);
        youitem.setTitle(s);

        MenuItem lazhoraItem = m.findItem(id.action_lazhora);
        SpannableString s1 = new SpannableString(lazhoraItem.getTitle());
        s1.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorPrimaryDark)),
                0, s1.length(), 0);
        lazhoraItem.setTitle(s1);

        MenuItem cat = c.findItem(id.action_categories);
        SpannableString s2 = new SpannableString(cat.getTitle());
        s2.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorPrimaryDark)),
                0, s2.length(), 0);
        cat.setTitle(s2);

        LinearLayout zhoraPoints = (LinearLayout) m.findItem(id.nav_lpoints).getActionView();
        LinearLayout cartList = (LinearLayout) m.findItem(id.nav_mycart).getActionView();

        tvZhoraPoints = (TextView) zhoraPoints.findViewById(id.counterView);
        tvCartList = (TextView) cartList.findViewById(id.counterView);

        if (sessionmanager.getZhoraPoints() != 0) {
            Log.e("drawer_zhora_points", String.valueOf(sessionmanager.getZhoraPoints()));
            tvZhoraPoints.setText(String.valueOf(sessionmanager.getZhoraPoints()));
        } else {
            tvZhoraPoints.setText("500");
        }

        /*Log.e("drawer_cart_count", String.valueOf(sessionmanager.getCartListCount()));
        if (sessionmanager.getCartListCount() != 0) {
            tvCartList.setVisibility(View.VISIBLE);
            tvCartList.setText(String.valueOf(sessionmanager.getCartListCount()));
        } else {
            tvCartList.setVisibility(View.GONE);
        }*/

        String result = getData(this);

        try {
            if (result != null) {
                JSONArray resultSet = new JSONArray(result);

                if (resultSet.length() != 0) {
                    tvCartList.setVisibility(View.VISIBLE);
                    tvCartList.setText(String.valueOf(resultSet.length()));
                } else {
                    tvCartList.setVisibility(View.GONE);
                }
            } else {
                tvCartList.setVisibility(View.GONE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        drawer.closeDrawer(GravityCompat.START);

        if (sessionmanager.getZhoraPoints() != 0) {
            Log.e("drawer_zhora_points", String.valueOf(sessionmanager.getZhoraPoints()));
            tvZhoraPoints.setText(String.valueOf(sessionmanager.getZhoraPoints()));
        } else {
            tvZhoraPoints.setText("500");
        }

        /*Log.e("drawer_cart_count", String.valueOf(sessionmanager.getCartListCount()));
        if (sessionmanager.getCartListCount() != 0) {
            tvCartList.setVisibility(View.VISIBLE);
            tvCartList.setText(String.valueOf(sessionmanager.getCartListCount()));
        } else {
            tvCartList.setVisibility(View.GONE);
        }*/

        String result = getData(this);

        try {
            if (result != null) {
                JSONArray resultSet = new JSONArray(result);

                if (resultSet.length() != 0) {
                    tvCartList.setVisibility(View.VISIBLE);
                    tvCartList.setText(String.valueOf(resultSet.length()));
                } else {
                    tvCartList.setVisibility(View.GONE);
                }
            } else {
                tvCartList.setVisibility(View.GONE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void applyFontToMenuItem(MenuItem mi) {
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(font.robotoMedium, 0, mNewTitle.length(),
                Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }

    @Override
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // Sync the toggle state after onRestoreInstanceState has occurred.
        toggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        toggle.onConfigurationChanged(newConfig);
    }

    @Override
    public Cursor runQuery(CharSequence constraint) {

//        ProductsPresenter presenter = new ProductsListImplementation(this);
//        presenter.getFilteredResult(constraint);

        return getSerchResult(constraint);
    }

    @Nullable
    private Cursor getSerchResult(CharSequence constraint) {
        Cursor c1 = null;

        try {
            DBManager myDbHelper = new DBManager(this);

            try {
                myDbHelper.openDataBase();
            } catch (Exception ioe) {
                throw new Error("Unable to create database");
            }

            try {
                c1 = myDbHelper.getSearchProducts(String.valueOf(constraint));
                Log.e("product", String.valueOf(c1.getCount()));
            } catch (Exception e) {
                Log.e("product", e.toString());
            }
            myDbHelper.close();
        } catch (Exception e) {
            Log.e("product", e.toString());
        }
        return c1;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.dashboard, menu);

        MenuItem searchItem = menu.findItem(id.action_search);

        searchView = (SearchView) searchItem.getActionView();

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setQueryHint("Search item");
        searchView.setOnQueryTextListener(this);
        searchView.setOnSuggestionListener(this);

        try {
            mSearchViewAdapter =
                    new SearchFeedResultsAdaptor(this,
                            R.layout.search_suggestion, matrixCursor, columns, null, -1000);

            searchView.setSuggestionsAdapter(mSearchViewAdapter);

            mSearchViewAdapter.setFilterQueryProvider(this);

        } catch (Exception e) {
            Log.e("CategoryFragment", e.toString());
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int id = item.getItemId();

        switch (id) {
            case R.id.action_search:
                break;
            case R.id.action_categories:
                drawer.openDrawer(GravityCompat.END);
                break;
            case R.id.action_cart:
                startActivity(new Intent(this, MyCart.class));
//                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(final MenuItem item) {

        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Log.e("nav_aboutus", "onNavigationItemSelected: ");
        Fragment newFragment;
        FragmentTransaction ft;
        //String tag = item.getActionView().getTag(0).toString();

        switch (id) {
            case R.id.nav_home:
                drawer.closeDrawer(GravityCompat.START);
                break;

            case R.id.nav_lpoints:
                showAlertDialog(getString(R.string.futurealert));

                break;

            case R.id.nav_mycart:
                startActivity(new Intent(this, MyCart.class));
//                finish();

                break;

            case R.id.nav_orderhistory:
                startActivity(new Intent(this, OrderHistory.class));
//                finish();

                break;

            case R.id.nav_payments:
                startActivity(new Intent(this, Payments.class));
//                finish();
                break;

            case R.id.nav_aboutus:
                Intent aboutUs = new Intent(this, AboutUs.class);
                startActivity(aboutUs);
//                finish();
                break;

            case R.id.nav_contactus:
                Intent contact = new Intent(this, ContactUs.class);
                startActivity(contact);
//                finish();
                break;

            case R.id.nav_feedback:
                Intent feedback = new Intent(this, Feedback.class);
                startActivity(feedback);
//                finish();
                break;

            case R.id.nav_privacy:
                showPrivacyDialog(getString(R.string.privacypolicy));

                break;

            case R.id.nav_profile_view:
                drawer.closeDrawer(GravityCompat.START);
                break;

            case R.id.nav_category_view:
                drawer.closeDrawer(GravityCompat.END);
                break;


            default:
                if (item.getTitle().toString().equals(getString(R.string.allcategory))) {
                    filterByCategory("");

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            String actualTitle = "All Products";

                            SpannableString homeTitle = new SpannableString(actualTitle);
                            homeTitle.setSpan(font.robotoMedium, 0, homeTitle.length(),
                                    Spannable.SPAN_INCLUSIVE_INCLUSIVE);

                            getSupportActionBar().setTitle(homeTitle);
                        }
                    });
                } else {
                    filterByCategory(item.getTitle().toString());

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            String catTitle = item.getTitle().toString();
                            String actualTitle = catTitle.substring(0, 1).toUpperCase() +
                                    catTitle.substring(1).toLowerCase();

                            SpannableString homeTitle = new SpannableString(actualTitle);
                            homeTitle.setSpan(font.robotoMedium, 0, homeTitle.length(),
                                    Spannable.SPAN_INCLUSIVE_INCLUSIVE);

                            getSupportActionBar().setTitle(homeTitle);
                        }
                    });
                }
                break;
        }

        return true;
    }

    private void filterByCategory(final String category) {
        drawer.closeDrawer(GravityCompat.END);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Fragment newFragment;
                FragmentTransaction ft;

                newFragment = new ItemlistFragment();
                ft = getSupportFragmentManager().beginTransaction();

                sessionmanager.setCategory(category);
                ft.replace(id.container, newFragment).commit();
            }
        });

    }

    public void showAlertDialog(String message) {
        AlertDialog alert = null;
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();

        View dialogView = inflater.inflate(R.layout.gift_layout, null);
        alertDialog.setView(dialogView);
        alert = alertDialog.create();

        TextView title = (TextView) dialogView.findViewById(id.dialogtitle);
        TextView msg = (TextView) dialogView.findViewById(id.dialogmessage);
        Button gotit = (Button) dialogView.findViewById(id.gotit);

        final AlertDialog finalAlert = alert;
        gotit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finalAlert.dismiss();
            }
        });

        Resources res = getResources();
        String titletext = String.format(res.getString(R.string.title_gift),
                sessionmanager.getCustomerInfo().get(SessionMaintainence.KEY_CUS_NAME)).trim();
        title.setText(titletext);
        title.setTypeface(font.robotoMedium);
        title.setAlpha((float) .87);

        msg.setTypeface(font.robotoRegular);

        // Showing Alert Message
        alert.show();

    }

    public void showPrivacyDialog(String message) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        // Setting Dialog Title
        alertDialog.setTitle("Policy");
        alertDialog.setIcon(getResources().getDrawable(R.drawable.fill_41));

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting OK Button
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        AlertDialog alert = alertDialog.create();
        // Showing Alert Message
        alert.show();

        TextView msg = (TextView) alert.findViewById(android.R.id.message);
        msg.setTypeface(font.robotoRegular);
        msg.setAlpha((float) .87);

        Button pbutton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        pbutton.setTextColor(getResources().getColor(R.color.colorPrimary));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        } else {
            sessionmanager.setCategory("");
            showExitDialog("Please take a moment to rate it. Thanks for your support!");
        }
    }

    private void showExitDialog(String s) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        // Setting Dialog Message
        alertDialog.setMessage(s);

        // Setting Yes Button
        alertDialog.setPositiveButton("Rate it!", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent feedback = new Intent(HomeActivity.this, Feedback.class);
                sessionmanager.setScreenId("HomeActivity");
                HomeActivity.this.startActivity(feedback);
            }
        });

        // Setting No Button
        alertDialog.setNegativeButton("Dismiss", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

            }
        });

        alertDialog.setNeutralButton("Exit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
                System.exit(0);
            }
        });

        // Showing Alert Message
        AlertDialog alert = alertDialog.create();
        alert.show();

        TextView msg = (TextView) alert.findViewById(android.R.id.message);
        msg.setTypeface(font.robotoMedium);
        msg.setAlpha((float) .84);

        Button pbutton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        pbutton.setTextColor(getResources().getColor(R.color.colorPrimary));
        Button nbutton = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
        nbutton.setTextColor(getResources().getColor(R.color.colorPrimary));
        Button neutralbutton = alert.getButton(DialogInterface.BUTTON_NEUTRAL);
        neutralbutton.setTextColor(getResources().getColor(R.color.colorPrimary));

    }


    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.action_sort:
                Intent intentSort = new Intent(this, SortActivity.class);
                startActivity(intentSort);
                overridePendingTransition(R.anim.slide_down, R.anim.slide_up);
                break;
            case R.id.action_filter:
                Intent intentfilter = new Intent(this, FilterActivity
                        .class);
                startActivity(intentfilter);
                overridePendingTransition(R.anim.slide_down, R.anim.slide_up);

                break;
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    @Override
    public boolean onSuggestionSelect(int position) {
        return false;
    }

    @Override
    public boolean onSuggestionClick(int position) {
        try {
            Cursor cursor = (Cursor) searchView.getSuggestionsAdapter().getItem(position);
            String q = cursor.getString(2);

            searchView.setQuery(q, false);
            searchView.clearFocus();
            String product_title = cursor.getString(2);

            Log.e("gridview", "onItemClick: " + product_title);

            Intent i = new Intent(this, ProductDetails.class);
            i.putExtra("productname", product_title);
            startActivity(i);
//            finish();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private void createList() {
        try {
            DBManager myDbHelper = new DBManager(this);

            myDbHelper.openDataBase();
            try {
                JSONObject products = new JSONObject(JsonDataCopy.jsondata);
                JSONArray productsjsonarray = products.getJSONArray("data");
                for (int i = 0; i < productsjsonarray.length(); i++) {
                    JSONObject jobj = productsjsonarray.getJSONObject(i);

                    Object priceObj = jobj.get("price");
                    String price = null;

                    if (priceObj instanceof String) {
                        price = (String) priceObj;
                        Log.e("price@adding_String", jobj.getString("product_title") + ", " + price);
                    } else if (priceObj instanceof JSONArray) {
                        StringBuilder priceBuilder = new StringBuilder();
                        JSONArray priceArr = (JSONArray) priceObj;

                        for (int j = 0; j < priceArr.length(); j++) {
                            if (j == 0) {
                                priceBuilder.append(priceArr.getString(0));
                            } else {
                                priceBuilder.append("," + priceArr.getString(j));
                            }
                        }
                        price = String.valueOf(priceBuilder);

                        Log.e("price@adding_Array", jobj.getString("product_title") + ", " + price);
                    }

                    myDbHelper.addProducts(String.valueOf(
                            jobj.getInt("product_id")),
                            jobj.getString("product_title"),
                            jobj.getString("product_desc"),
                            price.trim(),
                            jobj.getString("tag"),
                            jobj.getString("picture"),
                            jobj.getString("category"),
                            jobj.getString("picture_name"));
                }
            } catch (JSONException e) {
                Log.e("json", "createList: " + e);
            }

            myDbHelper.close();
        } catch (SQLException e) {
            Log.e("sql", "createList: " + e);
        }
    }

    public class LoadData extends AsyncTask<String, String, JSONObject> {

        // output keys for enquire api
        public static final String KEY_OUTPUT_STATUS = "status";
        public static final String KEY_OUTPUT_DATA = "message";
        public String enquire = "add_enquiry";
        JSONObject json;
        private CustomProgressDialog progressBar;
        private boolean status;
        private String data;
        private Context context;
        private SessionMaintainence session;
        private Font font;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        /*
         *    do things before doInBackground() code runs
         *    such as preparing and showing a Dialog or ProgressBar
        */
            progressBar = new CustomProgressDialog(HomeActivity.this, "loading...");
            session = SessionMaintainence.getInstance();
            font = new Font(HomeActivity.this);


        }

        @Override
        protected JSONObject doInBackground(String... f_url) {

            try {
                try {
                    createList();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

//stuff that updates ui
                            DBManager myDbHelper;
                            myDbHelper = new DBManager(HomeActivity.this);

                            try {
                                myDbHelper.openDataBase();
                            } catch (Exception ioe) {
                                throw new Error("Unable to create database");
                            }

                            try {
                                List categories = new ArrayList();
                                categories = myDbHelper.getCategories();
                                c.add(getString(R.string.allcategory)).setCheckable(true);
                                for (int i = 0; i < categories.size(); i++) {
                                    c.add(categories.get(i).toString()).setCheckable(true);
                                    Log.e("category", "doInBackground: " + categories.get(i));
                                }
                            } catch (Exception e) {
                                Log.e("category", e.toString());
                            }
                            myDbHelper.close();
                            // TODO : uncomment for category filter to be get vivible
                            Menu categoryMenus = navigationCategoryView.getMenu();

                            for (int i = 0; i < categoryMenus.size(); i++) {
                                MenuItem item = categoryMenus.getItem(i);

                                SubMenu subMenu = item.getSubMenu();
                                if (subMenu != null && subMenu.size() > 0) {
                                    for (int j = 0; j < subMenu.size(); j++) {
                                        MenuItem subMenuItem = subMenu.getItem(j);
                                        applyFontToMenuItem(subMenuItem);
                                    }
                                }

                                applyFontToMenuItem(item);
                            }
                        }
                    });
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    Log.e("category", e.toString());
                }

            } catch (Exception e) {
                Log.e("OnEnquire", e.toString());

            }

            return json;
        }

        @Override
        protected void onPostExecute(JSONObject json) {
        /*
         *    do something with data here
         *    display it or send to mainactivity
         *    close any dialogs/ProgressBars/etc...
        */

            // dismiss the dialog after the file was downloaded
            try {
                progressBar.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static String getData(Context context) {
        try {
            File f = new File(context.getFilesDir().getPath() + "/" + "mycart.json");
            //check whether file exists
            FileInputStream is = new FileInputStream(f);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();

            Log.e("Read Success@mycart", new String(buffer, "UTF-8"));
            return new String(buffer, "UTF-8");
        } catch (IOException e) {
            Log.e("File not exist@mycart", e.getLocalizedMessage());
            return null;
        }
    }

    String currentVersion, latestVersion;

    private void verifyForLatestVersion() {
        PackageManager pm = this.getPackageManager();
        PackageInfo pInfo = null;

        try {
            pInfo = pm.getPackageInfo(this.getPackageName(), 0);

        } catch (PackageManager.NameNotFoundException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        currentVersion = pInfo.versionName;
        Log.e("currentVersion", currentVersion);

        new AppLatestVersionUpdation().execute();

    }

    public class AppLatestVersionUpdation extends AsyncTask<String, String, JSONObject> {

        Dialog dialog;
        private ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

           /* progressDialog = new ProgressDialog(getApplicationContext());
            progressDialog.show();*/
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            try {
//                It retrieves the latest version by scraping the content of current version from play store at runtime
                Document doc = Jsoup.
                        connect("https://play.google.com/store/apps/details?id=lazhora.app").get();
                latestVersion = doc.getElementsByAttributeValue
                        ("itemprop", "softwareVersion").first().text();

                Log.e("latestVersion", latestVersion);

            } catch (Exception e) {
                e.printStackTrace();

            }

            return new JSONObject();
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {

//            progressDialog.dismiss();

            if (latestVersion != null) {
                if (!currentVersion.equalsIgnoreCase(latestVersion)) {
                    if (!isFinishing()) { //This would help to prevent Error : BinderProxy@45d459c0 is not valid; is your activity running? error
                        showUpdateDialog();
                    }
                }
            }

            super.onPostExecute(jsonObject);
        }

        private void showUpdateDialog() {
            final AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
            builder.setTitle("A New Update is Available");
            builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse
                            ("market://details?id=lazhora.app")));
                    dialog.dismiss();
                }
            });

            builder.setCancelable(false);
            dialog = builder.show();
        }
    }
}
