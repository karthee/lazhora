package lazhora.ui.Screens;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.appyvet.rangebar.RangeBar;

import lazhora.R;

/**
 * Created by Arivu on 22-09-2016.
 */

public class FilterActivity extends AppCompatActivity {

    RangeBar rangeBar;
    TextView lowestPrice, highestPrice;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        rangeBar = (RangeBar) findViewById(R.id.rangebar);

        lowestPrice = (TextView) findViewById(R.id.lowest_price);
        highestPrice = (TextView) findViewById(R.id.highest_price);

        rangeBar.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onRangeChangeListener(RangeBar rangeBar,
                                              int leftPinIndex,
                                              int rightPinIndex,
                                              String leftPinValue,
                                              String rightPinValue) {
                Log.e("lowest", String.valueOf(leftPinIndex));
                Log.e("higest", String.valueOf(rightPinIndex));

                Log.e("lowestVal", leftPinValue);
                Log.e("higestVal", rightPinValue);

                lowestPrice.setText("Rs. " + leftPinValue);
                highestPrice.setText("Rs. " + rightPinValue);

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
