package lazhora.ui.Screens;


import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import lazhora.R;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.pressImeActionButton;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withClassName;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;

/**
 * The type User information test 2.
 */
@LargeTest
@RunWith(AndroidJUnit4.class)
public class UserInformationTest2 {

    /**
     * The M activity test rule.
     */
    @Rule
    public ActivityTestRule<UserInformation> mActivityTestRule = new ActivityTestRule<>(UserInformation.class);

    private static Matcher<View> childAtPosition(
                                                        final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                               && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }

    /**
     * User information test 2.
     */
    @Test
    public void userInformationTest2() {
        ViewInteraction materialEditText = onView(allOf(withId(R.id.user_name), isDisplayed()));
        materialEditText.perform(replaceText("Karthee"), closeSoftKeyboard());

        ViewInteraction materialEditText2 = onView(
                allOf(withId(R.id.user_phone1), isDisplayed()));
        materialEditText2.perform(replaceText("9876543210"), closeSoftKeyboard());

        ViewInteraction materialEditText3 = onView(
                allOf(withId(R.id.user_phone2), isDisplayed()));
        materialEditText3.perform(replaceText("9876621635"), closeSoftKeyboard());

        ViewInteraction materialEditText4 = onView(
                allOf(withId(R.id.user_location), isDisplayed()));
        materialEditText4.perform(replaceText("Velache"), closeSoftKeyboard());

        ViewInteraction materialEditText5 = onView(
                allOf(withId(R.id.user_location), withText("Velache"), isDisplayed()));
        materialEditText5.perform(pressImeActionButton());

        ViewInteraction materialEditText6 = onView(
                allOf(withId(R.id.user_employment_status), isDisplayed()));
        materialEditText6.perform(replaceText("Develo"), closeSoftKeyboard());

        ViewInteraction materialEditText7 = onView(
                allOf(withId(R.id.user_employment_status), withText("Develo"), isDisplayed()));
        materialEditText7.perform(pressImeActionButton());

        ViewInteraction appCompatButton = onView(
                allOf(withId(R.id.user_submit), withText("SUBMIT"), isDisplayed()));
        appCompatButton.perform(click());

        ViewInteraction appCompatButton2 = onView(
                allOf(withId(android.R.id.button1), withText("OK"),
                        withParent(allOf(withClassName(is("android.widget.LinearLayout")),
                                withParent(withClassName(is("android.widget.LinearLayout"))))),
                        isDisplayed()));
        appCompatButton2.perform(click());

        ViewInteraction cardView = onView(
                allOf(childAtPosition(
                        withId(R.id.itemlist),
                        1),
                        isDisplayed()));
        cardView.perform(click());

        ViewInteraction appCompatImageButton = onView(
                allOf(withContentDescription("Navigate up"),
                        withParent(allOf(withId(R.id.toolbar),
                                withParent(withId(R.id.collapsing_toolbar)))),
                        isDisplayed()));
        appCompatImageButton.perform(click());

        ViewInteraction cardView2 = onView(
                allOf(childAtPosition(
                        withId(R.id.itemlist),
                        0),
                        isDisplayed()));
        cardView2.perform(click());

    }
}
