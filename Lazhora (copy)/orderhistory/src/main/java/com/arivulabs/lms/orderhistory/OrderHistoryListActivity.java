package com.arivulabs.lms.orderhistory;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.arivulabs.lms.mycart.CompleteOrder;
import com.arivulabs.lms.mycart.MyCartAdapter;
import com.arivulabs.lms.mycart.MyCartPOJO;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * Created by arivuventures on 30/1/17.
 */

public class OrderHistoryListActivity extends RelativeLayout {

    private static String TAG = "OrderHistoryListActivity";

    private Context context;
    private ArrayList<OrderHistoryPOJO> orderHistoryList = new ArrayList<OrderHistoryPOJO>();

    OrderHistoryAdapter orderHistoryAdapter;
    OrderHistoryPOJO orderHistoryPOJO;

    private RecyclerView recyclerView;
    private TextView emptyTxtMsg;
    private TextView orderMsg;

    public OrderHistoryListActivity(Context context) {
        super(context);
    }

    public OrderHistoryListActivity(Context context, ArrayList<OrderHistoryPOJO> lists) {
        super(context);
        this.context = context;
        this.orderHistoryList = lists;
        init(context, this.orderHistoryList);
    }

    private void init(Context context) {
        inflate(context, R.layout.activity_order_history, this);

    }

    private void init(final Context context, ArrayList<OrderHistoryPOJO> items) {
        inflate(context, R.layout.activity_order_history, this);

        orderMsg = (TextView) findViewById(R.id.order_msg);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        emptyTxtMsg = (TextView) findViewById(android.R.id.empty);

        orderHistoryAdapter = new OrderHistoryAdapter(context, orderHistoryList, new OnItemClickListener() {
            @Override
            public void onItemClick(OrderHistoryPOJO orderHistoryPOJO) {
                /*getContext().startActivity(new Intent(context, OrderDetailsActivity.class).
                        putExtra("order_item", orderHistoryPOJO));*/
//                ((Activity) getContext()).finish();
            }
        });

        orderHistoryAdapter.notifyDataSetChanged();

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(orderHistoryAdapter);

        if (this.orderHistoryList.size() != 0) {
            orderMsg.setVisibility(VISIBLE);
            recyclerView.setVisibility(VISIBLE);
            emptyTxtMsg.setVisibility(GONE);

            Log.e("MyCartArray: ", String.valueOf(this.orderHistoryList));
            recyclerView.setAdapter(orderHistoryAdapter);

            View footerView = ((LayoutInflater) getContext().getSystemService(LAYOUT_INFLATER_SERVICE)).
                    inflate(com.arivulabs.lms.mycart.R.layout.content_mycart_lv_footer, null);

        } else {
            orderMsg.setVisibility(GONE);
            recyclerView.setVisibility(GONE);
            emptyTxtMsg.setVisibility(VISIBLE);
        }
    }
}
