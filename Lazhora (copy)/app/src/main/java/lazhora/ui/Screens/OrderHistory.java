package lazhora.ui.Screens;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import lazhora.ui.utils.SessionMaintainence;
import orderhistory.OrderHistoryListActivity;
import orderhistory.OrderHistoryPOJO;

/**
 * Created by arivuventures on 30/1/17.
 */

public class OrderHistory extends AppCompatActivity {

    OrderHistoryPOJO orderHistoryPOJO;
    private ArrayList<OrderHistoryPOJO> orderHistoryList = new ArrayList<>();

    SessionMaintainence sm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sm = new SessionMaintainence(this);

        String string = getData(this, "orderhistory.json");

        JSONArray results;

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Log.e("sdf", simpleDateFormat.format(calendar.getTime()));

        try {
            if (string != null) {
                results = new JSONArray(string);
                Log.e("results_orders_size", String.valueOf(results.length()));
                Log.e("results_orders_history", String.valueOf(results));

                for (int i = 0; i < results.length(); i++) {
                    JSONObject order = results.getJSONObject(i);

                    int orderId = order.getInt("order_id");
                    Log.e("order_id", String.valueOf(orderId));

                    int price = 0, discount = 0;

                    String date = order.getString("date");

                    JSONArray order_content = order.getJSONArray("order_content");
                    price = order.getInt("order_price");
                    discount = order.getInt("order_discount");


                    /*for (int j = 0; j < order_content.length(); j++) {
                        JSONObject content = order_content.getJSONObject(j);
                        price = content.getInt("order_price");
                    }*/

                    orderHistoryPOJO = new OrderHistoryPOJO(String.valueOf(orderId),
                            date,
                            price,
                            discount);
                    orderHistoryList.add(orderHistoryPOJO);

                    Log.e("Order_item", String.valueOf(i));
                }
            }
        } catch (JSONException e) {
            Log.e("completeOrderOnClick", e.toString());
        }

        OrderHistoryListActivity orderHistoryListActivity =
                new OrderHistoryListActivity(this, orderHistoryList);
        setContentView(orderHistoryListActivity);

        getSupportActionBar().setTitle("Order History");
    }

    public static String getData(Context context, String fileName) {
        try {
            File f = new File(context.getFilesDir().getPath() + "/" + fileName);
            //check whether file exists
            FileInputStream is = new FileInputStream(f);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();

            Log.e("Read Success", new String(buffer, "UTF-8"));
            return new String(buffer, "UTF-8");
        } catch (IOException e) {
            Log.e("File not exist", e.getLocalizedMessage());
            return null;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

//        startActivity(new Intent(this, HomeActivity.class));
        finish();

    }
}
