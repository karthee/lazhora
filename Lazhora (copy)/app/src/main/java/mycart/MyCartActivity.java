package mycart;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import lazhora.R;
import lazhora.presenter.MyCartListImplementation;
import lazhora.presenter.MyCartListPresenter;

import static android.view.View.VISIBLE;

public class MyCartActivity extends AppCompatActivity implements
        MyCartListImplementation.MyCartListListener {

    List<MyCartPOJO> cartList = new ArrayList<>();
    private TextView activityMsg;
    private ListView cartListView;

    MyCartAdapter myCartAdapter;
    MyCartPOJO myCartPOJO;

    final ArrayList<String> s = new ArrayList<String>();
    final ArrayList<String> w = new ArrayList<String>();
    final ArrayList<Integer> q = new ArrayList<Integer>();

    String[] sArray, wArray;
    int[] qArray;

    int type3quantity = 0;
    int resID = 0;
    private RelativeLayout emptyListViewMsg;
    private RelativeLayout layoutMsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*setContentView(R.layout.activity_my_cart);*/

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        layoutMsg = (RelativeLayout) findViewById(R.id.msg);

        emptyListViewMsg = (RelativeLayout) findViewById(android.R.id.empty);

        MyCartListPresenter presenter = new MyCartListImplementation(this);
        presenter.getMyCartList();

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
//        startActivity(new Intent(this, getParentActivityIntent().getClass()));
        finish();
    }

    public static String getData(Context context, String fileName) {
        try {
            File f = new File(context.getFilesDir().getPath() + "/" + fileName);
            //check whether file exists
            FileInputStream is = new FileInputStream(f);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();

            Log.e("Read Success", new String(buffer, "UTF-8"));
            return new String(buffer, "UTF-8");
        } catch (IOException e) {
            Log.e("File not exist", e.getLocalizedMessage());
            return null;
        }
    }

    @Override
    public void getCartProductList(List<MyCartPOJO> listasString) {

        MyCartListActivity myCartListActivity = new MyCartListActivity(this, listasString);
        setContentView(myCartListActivity);

    }

    @Override
    public void emptyList() {
        Log.e("emptylist: ", "called");
        this.layoutMsg.setVisibility(View.GONE);
        emptyListViewMsg.setVisibility(VISIBLE);
    }

}
