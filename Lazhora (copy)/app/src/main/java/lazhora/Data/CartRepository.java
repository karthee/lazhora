package lazhora.Data;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import lazhora.ui.MyApplication;
import mycart.MyCartPOJO;

import static java.util.Collections.EMPTY_LIST;

/**
 * Created by Karthee on 17/03/17.
 */

public class CartRepository implements MyCartRepository {


    private List<MyCartPOJO> cartList = new ArrayList<>();
    private int resID;
    private String[] sArray;
    private String[] wArray;
    private int[] qArray;
    final ArrayList<String> s = new ArrayList<String>();
    final ArrayList<String> w = new ArrayList<String>();
    final ArrayList<Integer> q = new ArrayList<Integer>();
    private MyCartPOJO myCartPOJO;

    public CartRepository() {
    }


    @Override
    public List<MyCartPOJO> getMyCartListfromModel() {
        try {
            File f = new File(MyApplication.getContext().getFilesDir().getPath() + "/" +
                    "mycart.json");
            //check whether file exists
            FileInputStream is = new FileInputStream(f);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();

            Log.e("Read Success@mycart", new String(buffer, "UTF-8"));

//            arrayList =new Gson().fromJson(new String(buffer, "UTF-8"), new TypeToken<Collection<MyCartPOJO>>() {
//            }.getType());
            parsing(new String(buffer, "UTF-8"));
            return cartList;
        } catch (IOException e) {
            Log.e("File not exist@mycart", e.getLocalizedMessage());
            return null;
        }
    }

    private List<MyCartPOJO> parsing(String string) {
        JSONArray resultSet = null;
        try {
            if (string != null) {
                resultSet = new JSONArray(string);

                Log.e("resultSet_myCart", String.valueOf(resultSet.length()));

                for (int i = 0; i < resultSet.length(); i++) {
                    JSONObject results = resultSet.getJSONObject(i);

                    int type = results.getInt("type");
                    Log.e("type", String.valueOf(type));

                    String title = results.getString("title");
                    String category = results.getString("category");
                    resID = results.getInt("picture");

                    Log.e("resID", String.valueOf(resID));

                    int price = Integer.parseInt(results.getString("overall_price"));

                    Log.e("overall_price", String.valueOf(price));

                    JSONArray requirements;

                    if (type == 1) {
                        sArray = new String[0];
                        wArray = new String[0];
                        qArray = new int[0];

                        ArrayList<String> sArr = null;
                        ArrayList<String> wArr = null;

                        requirements = results.getJSONArray("requirements");

                        for (int j = 0; j < requirements.length(); j++) {

                            JSONObject reqObjects = requirements.getJSONObject(j);

                            s.add(reqObjects.getString("strength"));
                            w.add(reqObjects.getString("weight"));
                            q.add(reqObjects.getInt("quantity"));

                            sArr = new ArrayList<>();
                            wArr = new ArrayList<>();

                            for (String s1 : s) {
                                Log.e("s1", s1);
                                sArr.add(s1);
                            }

                            sArray = new String[sArr.size()];
                            for (int k = 0; k < sArray.length; k++) {
                                sArray[k] = sArr.get(k);
                                Log.e("sArray[" + k + "]", sArray[k]);
                            }

                            for (String w1 : w) {
                                Log.e("w1", w1);
                                wArr.add(w1);
                            }
                            wArray = new String[wArr.size()];
                            for (int k = 0; k < wArray.length; k++) {
                                wArray[k] = wArr.get(k);
                                Log.e("wArray[" + k + "]", wArray[k]);
                            }

                            qArray = new int[q.size()];
                            for (int k = 0; k < qArray.length; k++) {
                                qArray[k] = q.get(k).intValue();
                            }
                            Log.e("qArray", String.valueOf(qArray));
                        }

                        myCartPOJO = new MyCartPOJO(title,
                                MyApplication.getContext().getResources().getDrawable(resID),
                                price,
                                category,
                                sArray,
                                wArray,
                                qArray, type);
                        cartList.add(myCartPOJO);

                        s.clear();
                        w.clear();
                        q.clear();

                    } else if (type == 2) {
                        wArray = new String[0];
                        qArray = new int[0];

                        ArrayList<String> wArr = null;

                        requirements = results.getJSONArray("requirements");

                        for (int j = 0; j < requirements.length(); j++) {

                            JSONObject reqObjects = requirements.getJSONObject(j);

                            w.add(reqObjects.getString("weight"));
                            q.add(reqObjects.getInt("quantity"));

                            wArr = new ArrayList<>();

                            for (String w1 : w) {
                                wArr.add(w1);
                            }
                            wArray = new String[wArr.size()];
                            for (int k = 0; k < wArray.length; k++) {
                                wArray[k] = wArr.get(k);
                            }
                            Log.e("wArray", String.valueOf(wArray));

                            qArray = new int[q.size()];
                            for (int k = 0; k < qArray.length; k++) {
                                qArray[k] = q.get(k).intValue();
                            }
                            Log.e("qArray", String.valueOf(qArray));
                        }

                        myCartPOJO = new MyCartPOJO(title,
                                MyApplication.getContext().getResources().getDrawable(resID),
                                price,
                                category,
                                wArray,
                                qArray, type);
                        cartList.add(myCartPOJO);

                        w.clear();
                        q.clear();

                    } else if (type == 3) {
                        results.getInt("quantity");
                        results.getInt("price");
                        results.getInt("overall_price");

                        myCartPOJO = new MyCartPOJO(
                                title,
                                MyApplication.getContext().getResources().getDrawable(resID),
                                price,
                                category,
                                results.getInt("quantity"));
                        cartList.add(myCartPOJO);
                    }


                    Log.e("mycart_added_item", String.valueOf(i));
                }
            }
            return cartList;
        } catch (Exception e) {
            Log.e("resultSetException", e.toString());
        }
        return EMPTY_LIST;

    }

}
