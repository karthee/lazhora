package lazhora.ui.utils;

import android.content.Context;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.logging.Logger;

public class JsonparserObject {


    private static InputStream is = null;
    private static JSONObject jObj = null;
    private static String json = "";
    public int errorcode;
    Logger logger;
    private SessionMaintainence session;
    private Context mycontext;

    static JsonparserObject jsonparserObject;


    private JsonparserObject(Context context) {
        this.mycontext = context;
        session = SessionMaintainence.getInstance();

    }

    // function get json from url
    // by making HTTP POST or GET mehtod
    //	@TargetApi(Build.VERSION_CODES.KITKAT)
    public JSONObject makeHttpRequest(String url, String method,
                                      String params) {
        // Making HTTP request
        try {
           /* logger.addRecordToLog("API LINK: " + url);
            logger.addRecordToLog("Input Param: " + params);*/
            // check for request method
            if (method.equals("POST")) {
                // request method is POST
                // defaultHttpClient
                HttpParams httpParameters = new BasicHttpParams();
                    /*// Set the timeout in milliseconds until a connection is established.
                    // The default_img value is zero, that means the timeout is not used.
					int timeoutConnection = 4000;
					HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);*/
                // Set the default_img socket timeout (SO_TIMEOUT)
                // in milliseconds which is the timeout for waiting for data.
                int timeoutSocket = 6000;
                HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);

                DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
                // DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(url);

                StringEntity se = new StringEntity(params);
                se.setContentType("application/json;charset=UTF-8");
                se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
                        "application/json;charset=UTF-8"));
                httpPost.setEntity(se);

                HttpResponse httpResponse = httpClient.execute(httpPost);
                HttpEntity httpEntity = httpResponse.getEntity();
                //Log.e("POST ", EntityUtils.toString(httpEntity));

                errorcode = httpResponse.getStatusLine().getStatusCode();
                Log.e("error code", String.valueOf(errorcode));
                is = httpEntity.getContent();

            } else if (method.equals("GET")) {
                // request method is GET
                HttpParams httpParameters = new BasicHttpParams();
                /*	// Set the timeout in milliseconds until a connection is established.
                    // The default_img value is zero, that means the timeout is not used.
					int timeoutConnection = 4000;
					HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);*/
                // Set the default_img socket timeout (SO_TIMEOUT)
                // in milliseconds which is the timeout for waiting for data.
                int timeoutSocket = 6000;
                HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
                DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);

                //  DefaultHttpClient httpClient = new DefaultHttpClient();
                if (params.equals("")) {
                } else {
                    String paramString = "";
                    url += "/" + params;
                }

                Log.e("api", url);

                HttpGet httpGet = new HttpGet(url);

                HttpResponse httpResponse = httpClient.execute(httpGet);

                errorcode = httpResponse.getStatusLine().getStatusCode();

                HttpEntity httpEntity = httpResponse.getEntity();
                is = httpEntity.getContent();
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            if (is != null) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "utf-8"));
                StringBuilder sb = new StringBuilder();
                String line;

                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }

                is.close();
                int n = sb.lastIndexOf(">");
                json = sb.toString().substring(n + 1, sb.toString().length());
            }
            Log.e("json file", "json " + json);
          /*  if (!json.equals("")) {
                logger.addRecordToLog("Output params: " + json);
            }*/

        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());

        }

        // try parse the string to a JSON object
        try {
            jObj = new JSONObject(json);

        } catch (JSONException e) {

            Log.e("JSON Parser", "Error parsing data " + e.toString());
            Log.e("Buffer Error", "json " + json);
        }

        // return JSON String
        return jObj;

    }

    public static void init(Context context) {

        if (jsonparserObject == null) {
            jsonparserObject = new JsonparserObject(context);
        }
    }

    public static JsonparserObject getInstance() {
        return jsonparserObject;
    }
}
