package lazhora.ui.Screens.home;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import lazhora.R;
import lazhora.ui.Screens.HomeActivity;
import lazhora.ui.data.Item;
import mycart.MyCartPOJO;

/**
 * Created by arivuventures on 4/4/17.
 */

public class HomeActivityRVAdapter extends RecyclerView.Adapter<HomeActivityViewHolder> {

    private List<Item> itemlist;

    @Override
    public HomeActivityViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_row, parent, false);

        return new ProductItemOnGridView(view);
    }

    @Override
    public void onBindViewHolder(HomeActivityViewHolder holder, int position) {

        ProductItemOnGridView viewHolder = (ProductItemOnGridView) holder;
        viewHolder.bind(itemlist.get(position));
    }

    @Override
    public int getItemCount() {
        return itemlist.size();
    }

    public void setItemlist(List<Item> itemlist) {
        this.itemlist = itemlist;
        notifyDataSetChanged();
    }
}
