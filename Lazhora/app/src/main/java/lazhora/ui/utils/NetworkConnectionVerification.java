package lazhora.ui.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

/**
 * Created by Arivu on 24-10-2016.
 */
public class NetworkConnectionVerification {

    /**
     * The Context.
     */
    Context context;
    /**
     * The Connected.
     */
    boolean connected = false;

    /**
     * Instantiates a new Network connection verification.
     *
     * @param context the context
     */
    public NetworkConnectionVerification(Context context) {
        this.context = context;
    }

    /**
     * Is network available boolean.
     *
     * @return the boolean
     */
    public boolean isNetworkAvailable() {
        try {
            ConnectivityManager connectivityManager
                    = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            connected = activeNetworkInfo != null && activeNetworkInfo.isConnected();
        } catch (Exception e) {
            Log.e("", e.toString());
        }

        return connected;
    }
}
