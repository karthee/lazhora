package mycart.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import lazhora.R;
import mycart.MyCartPOJO;
import mycart.adapter.listener.DeleteProductListener;
import mycart.adapter.listener.FooterListener;
import mycart.adapter.viewholder.FooterViewHolder;
import mycart.adapter.viewholder.Type_1and2ViewHolder;
import mycart.adapter.viewholder.Type_3ViewHolder;
import mycart.adapter.viewholder.MyViewHolder;

/**
 * Created by Karthee on 21/03/17.
 */

public class MyCartRecAdapter extends RecyclerView.Adapter<MyViewHolder> {

    private static final int ISQTY = 1;
    private static final int ISNOQTY = 0;
    private static final int FOOTER_VIEW = 9;
    List<MyCartPOJO> cartList;
    private DeleteProductListener deletelistener;
    private FooterListener footerListener;

    public MyCartRecAdapter(DeleteProductListener listener,FooterListener footerlistener) {

        this.deletelistener = listener;
        this.footerListener = footerlistener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v;
        if (viewType == ISNOQTY) {

            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_mycart_listitem2,
                    parent, false);
            return new Type_3ViewHolder(v);
        }
        if (viewType == ISQTY) {

            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_mycart_listitem,
                    parent, false);
            return new Type_1and2ViewHolder(v);
        }
        if (viewType == FOOTER_VIEW) {

            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.content_mycart_lv_footer,
                    parent, false);
            return new FooterViewHolder(v);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        if (holder.getItemViewType() == ISNOQTY) {
            Type_3ViewHolder viewHolder = (Type_3ViewHolder) holder;
            viewHolder.bind(cartList.get(position), deletelistener);
        } else if (holder.getItemViewType() == ISQTY) {
            Type_1and2ViewHolder viewHolder = (Type_1and2ViewHolder) holder;
            viewHolder.bind(cartList.get(position), position, deletelistener);
        } else if (holder.getItemViewType() == FOOTER_VIEW) {
            FooterViewHolder viewHolder = (FooterViewHolder) holder;
            viewHolder.bind(cartList,footerListener);
        }
    }

    @Override
    public int getItemCount() {
        return cartList.size() + 1;
    }

    public void setCartList(List<MyCartPOJO> cartList) {
        this.cartList = cartList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == cartList.size()) {
            // This is where we'll add footer.
            return FOOTER_VIEW;
        }

        return cartList.get(position).getQuantity2() == 0 ? ISQTY : ISNOQTY;
    }
}
