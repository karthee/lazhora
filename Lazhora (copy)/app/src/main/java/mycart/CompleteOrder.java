package mycart;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import lazhora.BuildConfig;
import lazhora.R;
import lazhora.ui.Screens.OrderHistory;
import lazhora.ui.api.EnquireApi;
import lazhora.ui.utils.*;
import lazhora.ui.utils.NetworkConnectionVerification;

/**
 * Created by arivuventures on 10/1/17.
 */

public class CompleteOrder extends AppCompatActivity {

    Button reviewCart, completeOrder;
    LinearLayout cartItems;

    ArrayList<MyCartPOJO> cartList = new ArrayList<MyCartPOJO>();

//    static String fileName = "mycart.json"

    JSONArray resultSet;

    int overallPrice = 0;
    TextView totalPrice;

    String name = null, phone = null;
    SessionMaintainence sm;

    int points = 0, coupon_points;
    private TextView payableAmount;
    private TextView appliedZhoraPoints;

    private lazhora.ui.utils.NetworkConnectionVerification ncv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_checkout);

        getSupportActionBar().setTitle("Checkout");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sm = new SessionMaintainence(this);

        ncv = new NetworkConnectionVerification(this);

        final Calendar calendar = Calendar.getInstance();
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Log.e("sdf", simpleDateFormat.format(calendar.getTime()));

        Intent intent = getIntent();
        name = intent.getStringExtra("name");
        phone = intent.getStringExtra("phone");
        coupon_points = intent.getIntExtra("coupon_points", 0);
        Log.e("name_phone", name + ", " + phone);

        reviewCart = (Button) findViewById(R.id.review_cart);
        completeOrder = (Button) findViewById(R.id.complete_order);

        cartItems = (LinearLayout) findViewById(R.id.cart_items);

        totalPrice = (TextView) findViewById(R.id.order_price);
        appliedZhoraPoints = (TextView) findViewById(R.id.zhora_points_applied);
        payableAmount = (TextView) findViewById(R.id.order_payable_amount);

        appliedZhoraPoints.setText(String.valueOf(coupon_points));

        Intent i2 = getIntent();
        cartList = i2.getParcelableArrayListExtra("cart_list");

        Log.e("onCreate: ", String.valueOf(cartList));

        String string = getData(getApplicationContext(), "mycart.json");

        try {
            resultSet = new JSONArray(string);
            Log.e("resultSet_completOrder", String.valueOf(resultSet.length()));

            for (int i = 0; i < resultSet.length(); i++) {
                JSONObject results = resultSet.getJSONObject(i);

                LinearLayout.LayoutParams paramsLL = new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT);

                LinearLayout.LayoutParams paramsTV1 = new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f);

                LinearLayout.LayoutParams paramsTV2 = new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 5.0f);

                LinearLayout itemsLayout = new LinearLayout(this);

                itemsLayout.setLayoutParams(paramsLL);
                itemsLayout.setOrientation(LinearLayout.HORIZONTAL);

                TextView productTitle = new TextView(this);
                TextView productPrice = new TextView(this);

                productTitle.setText(results.getString("title"));
                productTitle.setLayoutParams(paramsTV1);
                productTitle.setTextColor(getResources().getColor(R.color.colorPrimary));

                productPrice.setCompoundDrawablesWithIntrinsicBounds(
                        getResources().getDrawable(R.drawable.rupee_black),
                        null, null, null);
                productPrice.setCompoundDrawablePadding(4);
                productPrice.setGravity(Gravity.RIGHT);
                productPrice.setTextColor(Color.BLACK);
                Log.e("overall_price", String.valueOf(results.getInt("overall_price")));
                if (results.getInt("overall_price") == 0) {
                    productPrice.setText("0");
                } else {
                    productPrice.setText(String.valueOf(results.getInt("overall_price")));
                    overallPrice += results.getInt("overall_price");
                }
                productPrice.setTypeface(Typeface.DEFAULT_BOLD);
                productPrice.setLayoutParams(paramsTV2);
                productPrice.setAlpha((float) .87);

                itemsLayout.addView(productTitle);
                itemsLayout.addView(productPrice);
                cartItems.addView(itemsLayout);

            }

            totalPrice.setText(Integer.toString(overallPrice));

            overallPrice -= coupon_points;
            payableAmount.setText(Integer.toString(overallPrice));

        } catch (JSONException e) {
            Log.e("JSONException: ", e.toString());
        }

        reviewCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        if (overallPrice != 0) {
            if (overallPrice <= 1000) {
                points = sm.getZhoraPoints();
                points += 0;
            } else if (overallPrice <= 2000) {
                points = sm.getZhoraPoints();
                points += 100;
            } else if (overallPrice <= 3000) {
                points = sm.getZhoraPoints();
                points += 250;
            } else if (overallPrice <= 5000) {
                points = sm.getZhoraPoints();
                points += 450;
            } else if (overallPrice <= 7500) {
                points = sm.getZhoraPoints();
                points += 650;
            } else if (overallPrice <= 10000) {
                points = sm.getZhoraPoints();
                points += 750;
            } else if (overallPrice <= 15000) {
                points = sm.getZhoraPoints();
                points += 1000;
            } else if (overallPrice > 15000) {
                points = sm.getZhoraPoints();
                points += 0;
            }
        } else {
            points = sm.getZhoraPoints();
            points += 0;

        }

        completeOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("points_added", String.valueOf(points));
                sm.setZhoraPoints(points);

                String string = getData(getApplicationContext(), "orderhistory.json");

                JSONArray resultsHistory = null;
                JSONObject orderContent = new JSONObject();

                try {
                    if (string != null) {
                        resultsHistory = new JSONArray(string);

                        for (int i = 0; i < resultsHistory.length(); i++) {
                            JSONObject content = resultsHistory.getJSONObject(i);

                            int orderId = content.getInt("order_id");
                            orderId++;

                            orderContent.put("order_id", orderId);
                            orderContent.put("order_content", resultSet);
                            orderContent.put("order_price", overallPrice);
                        }
                    } else {
                        resultsHistory = new JSONArray();
                        orderContent.put("order_id", 101);
                        orderContent.put("order_content", resultSet);
                        orderContent.put("order_price", overallPrice);
                    }

                    orderContent.put("order_discount", sm.getZhoraPointsDiscount());
                    orderContent.put("date", simpleDateFormat.format(calendar.getTime()).toString());
                    resultsHistory.put(orderContent);


                    Log.e("resultSet_OH", String.valueOf(resultsHistory));
                    saveData(getApplicationContext(), "orderhistory.json", resultsHistory.toString());

                    Log.e("BCUrl", BuildConfig.url);

                    sm.setPageID("CompleteOrder");

                    try {
                        if (ncv.isNetworkAvailable()) {

                            Log.e("order_enquiry", BuildConfig.url + "add_enquire");

                            new EnquireApi(CompleteOrder.this).execute(resultSet.toString());
                        } else {
                            Toast.makeText(CompleteOrder.this, "Please check for internet connection.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        Log.e("CO_enquire_api", e.toString());
                    }

//                    new OnSubmitOrders().execute(resultSet.toString());

                    File f = new File(getApplicationContext().getFilesDir().getPath() + "/" + "mycart.json");
                    f.delete();

                    startActivity(new Intent(CompleteOrder.this, OrderHistory.class));
                    finish();
                } catch (JSONException e) {
                    Log.e("completeOrderOnClick", e.toString());
                }
            }
        });
    }

    class OnSubmitOrders extends AsyncTask<String, Void, Void> {

        final ProgressDialog mProgressDialog = new ProgressDialog(CompleteOrder.this);

        @Override
        protected void onPreExecute() {
            mProgressDialog.setMessage("Your order's been submitted...");
            mProgressDialog.show();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... strings) {
            try {
                RequestQueue requestQueue = Volley.newRequestQueue(CompleteOrder.this);
                String url = SessionMaintainence.getInstance().getIp() + "add_enquiry";

                Log.e("order_enquiry: ", url);
                final String orders = strings[0];

                StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.e("onResponse", response);

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.e("onErrorResponse", String.valueOf(error));
                            }
                        }) {
                    @Override
                    protected Map<String, String> getParams() {
                        Map<String, String> params = new HashMap<String, String>();
                        params.put("product", "Pumpkin" + ". " + orders);
                        params.put("name", name);
                        params.put("phone_one", phone);
                        params.put("phone_teo", "");

                        return params;
                    }
                };

                requestQueue.add(stringRequest);
            } catch (Exception e) {
                Log.e("onClick: ", e.toString());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            mProgressDialog.dismiss();
            super.onPostExecute(aVoid);
        }
    }

    public static void saveData(Context context, String fileName, String mJsonResponse) {
        try {
            FileWriter file = new FileWriter(context.getFilesDir().getPath() + "/" + fileName);
            file.write(mJsonResponse);
            file.flush();
            file.close();
            Log.e("Write Success_OH.", String.valueOf(file));

        } catch (IOException e) {
            Log.e("Error in Writing_OH.", e.getLocalizedMessage());
        }
    }

    public static String getData(Context context, String fileName) {
        try {
            File f = new File(context.getFilesDir().getPath() + "/" + fileName);
            //check whether file exists
            FileInputStream is = new FileInputStream(f);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();

            Log.e("Read Success_OH.", new String(buffer, "UTF-8"));
            return new String(buffer, "UTF-8");
        } catch (IOException e) {
            Log.e("File not exist_OH.", e.getLocalizedMessage());
            return null;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        startActivity(new Intent(this, MyCartActivity.class));
        finish();
    }
}
