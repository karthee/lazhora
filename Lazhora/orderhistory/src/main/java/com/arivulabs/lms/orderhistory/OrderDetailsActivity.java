package com.arivulabs.lms.orderhistory;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by arivuventures on 11/1/17.
 */

public class OrderDetailsActivity extends AppCompatActivity {

    LinearLayout orderList;
    TextView orderID;
    TextView orderPrice;
    TextView overallOrders;
    TextView orderDate;

    int orderId;
    int price = 0;

    String date;

    boolean isDetailsClicked = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_order_details);

        getSupportActionBar().setTitle("Order History");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        orderList = (LinearLayout) findViewById(R.id.order_items);
        orderID = (TextView) findViewById(R.id.order_id);
        orderPrice = (TextView) findViewById(R.id.order_price);
        orderDate = (TextView) findViewById(R.id.order_date);
        overallOrders = (TextView) findViewById(R.id.overall_orders);

        String string = getData(this, "orderhistory.json");

        JSONArray results;

        try {
            if (string != null) {
                results = new JSONArray(string);
                Log.e("resulst_orderhistory", String.valueOf(results));
                Log.e("order_size", String.valueOf(results.length()));

                for (int i = 0; i < results.length(); i++) {
                    JSONObject order = results.getJSONObject(i);

                    orderId = order.getInt("order_id");

                    Log.e("currentOrderId", String.valueOf(orderId));

                    Log.e("onCreateOrderID", getIntent().getStringExtra("order_id"));
                    int intentOrderId = Integer.parseInt(getIntent().getStringExtra("order_id"));

                    orderID.setText("#" + Integer.toString(intentOrderId));

                    if (intentOrderId == orderId) {
                        date = order.getString("date");
                        price = order.getInt("order_price");

                        JSONArray order_content = order.getJSONArray("order_content");
                        Log.e("onCurrentContent", String.valueOf(order_content));

                        Log.e("content_size", String.valueOf(order_content.length()));
                        overallOrders.setText(Integer.toString(order_content.length()));

                        for (int j = 0; j < order_content.length(); j++) {
                            JSONObject content = order_content.getJSONObject(j);
//                            price = content.getInt("overall_price");

                            String title = content.getString("title");
                            int type = content.getInt("type");

                            String overallPrice = content.getString("overall_price");

                            LinearLayout.LayoutParams paramsLL = new LinearLayout.LayoutParams(
                                    ViewGroup.LayoutParams.MATCH_PARENT,
                                    ViewGroup.LayoutParams.WRAP_CONTENT);

                            LinearLayout.LayoutParams paramsLL2 = new LinearLayout.LayoutParams(
                                    0,
                                    ViewGroup.LayoutParams.WRAP_CONTENT, 6.0f);

                            LinearLayout.LayoutParams paramsTV1 = new LinearLayout.LayoutParams(
                                    ViewGroup.LayoutParams.WRAP_CONTENT,
                                    ViewGroup.LayoutParams.WRAP_CONTENT);

                            LinearLayout.LayoutParams paramsTV2 = new LinearLayout.LayoutParams(
                                    0,
                                    ViewGroup.LayoutParams.WRAP_CONTENT, 1.5f);
                            paramsTV2.gravity = Gravity.TOP;

                            LinearLayout itemsLayout = new LinearLayout(this);

                            itemsLayout.setLayoutParams(paramsLL);
                            itemsLayout.setPadding(4, 4, 4, 6);
                            itemsLayout.setOrientation(LinearLayout.HORIZONTAL);

                            LinearLayout itemsLayout2 = new LinearLayout(this);

                            itemsLayout2.setLayoutParams(paramsLL2);
                            itemsLayout2.setOrientation(LinearLayout.VERTICAL);

                            TextView productTitle = new TextView(this);
                            final TextView details = new TextView(this);

                            TextView productPrice = new TextView(this);

                            productTitle.setText(title);
                            productTitle.setLayoutParams(paramsTV1);
                            productTitle.setGravity(Gravity.LEFT);
                            productTitle.setTextColor(getResources().getColor(R.color.colorPrimary));

                            final LinearLayout layoutSpec = (LinearLayout) View.inflate(this,
                                    R.layout.layout_specifications, null);
                            layoutSpec.setVisibility(View.GONE);

                            details.setText("Show Details");
                            details.setLayoutParams(paramsTV1);
                            details.setGravity(Gravity.LEFT);
                            details.setTextColor(getResources().getColor(android.R.color.holo_blue_dark));
                            details.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    if (!isDetailsClicked) {
                                        layoutSpec.setVisibility(View.VISIBLE);
                                        isDetailsClicked = true;
                                        details.setText("Hide Details");
                                    } else {
                                        layoutSpec.setVisibility(View.GONE);
                                        isDetailsClicked = false;
                                        details.setText("Show Details");
                                    }
                                }
                            });

                            if (type == 1) {
                                JSONArray requirements = content.getJSONArray("requirements");

                                for (int k = 0; k < requirements.length(); k++) {
                                    JSONObject spec = requirements.getJSONObject(k);

                                    final LinearLayout itemSpec = (LinearLayout) View.inflate(this,
                                            R.layout.item_product_specification, null);

                                    TextView iStrength, iWeight, iQuantity;

                                    iStrength = (TextView) itemSpec.findViewById(R.id.item_strength);
                                    iWeight = (TextView) itemSpec.findViewById(R.id.item_weight);
                                    iQuantity = (TextView) itemSpec.findViewById(R.id.item_quantity);

                                    Log.e("order_req_contents", spec.getString("strength") +
                                            spec.getString("weight") + spec.getString("quantity"));

                                    iStrength.setText(spec.getString("strength"));
                                    iWeight.setText(spec.getString("weight"));
                                    iQuantity.setText(spec.getString("quantity"));

                                    layoutSpec.addView(itemSpec);
                                }
                            } else if (type == 2) {
                                JSONArray requirements = content.getJSONArray("requirements");

                                for (int k = 0; k < requirements.length(); k++) {
                                    JSONObject spec = requirements.getJSONObject(k);

                                    final LinearLayout itemSpec = (LinearLayout) View.inflate(this,
                                            R.layout.item_product_specification, null);

                                    TextView tStrength, iStrength, iWeight, iQuantity;

                                    tStrength = (TextView) layoutSpec.findViewById(R.id.title_strength);
                                    iStrength = (TextView) itemSpec.findViewById(R.id.item_strength);
                                    iWeight = (TextView) itemSpec.findViewById(R.id.item_weight);
                                    iQuantity = (TextView) itemSpec.findViewById(R.id.item_quantity);

                                    tStrength.setVisibility(View.GONE);
                                    iStrength.setVisibility(View.GONE);
                                    iWeight.setText(spec.getString("weight"));
                                    iQuantity.setText(spec.getString("quantity"));

                                    layoutSpec.addView(itemSpec);
                                }
                            } else if (type == 3) {
                                final LinearLayout itemSpec = (LinearLayout) View.inflate(this,
                                        R.layout.item_product_specification, null);

                                TextView tStrength, tWeight, iStrength, iWeight, iQuantity;

                                iStrength = (TextView) itemSpec.findViewById(R.id.item_strength);
                                tStrength = (TextView) layoutSpec.findViewById(R.id.title_strength);
                                iWeight = (TextView) itemSpec.findViewById(R.id.item_weight);
                                tWeight = (TextView) layoutSpec.findViewById(R.id.title_weight);
                                iQuantity = (TextView) itemSpec.findViewById(R.id.item_quantity);

                                tStrength.setVisibility(View.GONE);
                                iStrength.setVisibility(View.GONE);
                                tWeight.setVisibility(View.GONE);
                                iWeight.setVisibility(View.GONE);
                                iQuantity.setText(Integer.toString(content.getInt("quantity")));

                                layoutSpec.addView(itemSpec);
                            }

                            itemsLayout2.addView(productTitle);
                            itemsLayout2.addView(details);
                            itemsLayout2.addView(layoutSpec);

                            productPrice.setCompoundDrawablesWithIntrinsicBounds(
                                    getResources().getDrawable(com.arivulabs.lms.mycart.R.drawable.rupee_black),
                                    null, null, null);
                            productPrice.setCompoundDrawablePadding(4);
                            productPrice.setText(overallPrice);
                            productPrice.setLayoutParams(paramsTV2);
                            productPrice.setGravity(Gravity.RIGHT);
                            productPrice.setAlpha((float) .87);
                            productPrice.setTypeface(Typeface.DEFAULT_BOLD);
                            productPrice.setTextColor(getResources().getColor(android.R.color.black));

//                            price += Integer.parseInt(overallPrice);

                            itemsLayout.addView(itemsLayout2);
                            itemsLayout.addView(productPrice);

                            orderList.addView(itemsLayout);

                            Log.e("readed_item", String.valueOf(j));
                        }
                    }
                }
            }
        } catch (JSONException e) {
            Log.e("OrderHistoryOnCreate", e.toString());
        }

        orderDate.setText(date);
        orderPrice.setText(Integer.toString(price));

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
//        startActivity(new Intent(this, OrderHistoryActivity.class));
        finish();
    }

    public static String getData(Context context, String fileName) {
        try {
            File f = new File(context.getFilesDir().getPath() + "/" + fileName);
            //check whether file exists
            FileInputStream is = new FileInputStream(f);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();

            Log.e("Read Success", new String(buffer, "UTF-8"));
            return new String(buffer, "UTF-8");
        } catch (IOException e) {
            Log.e("File not exist", e.getLocalizedMessage());
            return null;
        }
    }
}
