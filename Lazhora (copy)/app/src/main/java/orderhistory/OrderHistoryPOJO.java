package orderhistory;

import java.io.Serializable;

/**
 * Created by arivuventures on 11/1/17.
 */

public class OrderHistoryPOJO implements Serializable {

    public OrderHistoryPOJO() {
    }

    public OrderHistoryPOJO(String orderId,
                            String orderDate,
                            int orderPrice, int orderDiscount) {
        setOrderId(orderId);
        setOrderDate(orderDate);
        setOrderPrice(orderPrice);
        setOrderDiscount(orderDiscount);
    }

    String orderId;
    String orderDate;
    int orderPrice;

    int orderDiscount;

    public int getOrderPrice() {
        return orderPrice;
    }

    public void setOrderPrice(int orderPrice) {
        this.orderPrice = orderPrice;
    }

    public int getOrderDiscount() {
        return this.orderDiscount;
    }

    public void setOrderDiscount(int orderDiscount) {
        this.orderDiscount = orderDiscount;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }
}
