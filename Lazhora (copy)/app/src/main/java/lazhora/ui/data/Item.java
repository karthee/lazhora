package lazhora.ui.data;

public class Item {
    public String name;
    public String imageid;
    public String desc;
    public String category;
    public String price;
    public String tag;
    public String pic_name;

    public Item(String name, String desc, String imageid, String price, String tag, String pic_name) {
        this.name = name;
        this.imageid = imageid;
        this.desc = desc;
        this.price = price;
        this.tag = tag;
        this.pic_name = pic_name;
    }

    public Item(String name, String desc, String category, String imageid, String price, String tag, String pic_name) {
        this.name = name;
        this.imageid = imageid;
        this.desc = desc;
        this.category = category;
        this.price = price;
        this.tag = tag;
        this.pic_name = pic_name;
    }
}