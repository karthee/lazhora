package lazhora.ui.Screens.presenter;

/**
 * Created by Karthee on 19/01/17.
 */
public interface ProductDetailsPresenter {

    /**
     * Enquire.
     *
     * @param proName the pro name
     */
    public void enquire(String proName);
}
