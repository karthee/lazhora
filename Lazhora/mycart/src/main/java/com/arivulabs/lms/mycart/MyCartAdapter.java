package com.arivulabs.lms.mycart;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.widget.ViewDragHelper;
import android.support.v7.widget.AppCompatSpinner;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static android.content.ContentValues.TAG;
import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * Created by arivuventures on 6/1/17.
 */

public class MyCartAdapter extends ArrayAdapter<MyCartPOJO> {

    private static String TAG = "MyCartAdapter";

    private Context context;
    private ArrayList<MyCartPOJO> cartList = new ArrayList<MyCartPOJO>();
    private LayoutInflater layoutInflater;

    MyCartPOJO myCartPOJO = null;

    ViewHolder viewHolder = new ViewHolder();
    ViewHolder2 viewHolder2 = new ViewHolder2();

    String title, proName, category;
    ImageView removeItem;

    LinearLayout items;

    //    @BindView(R.id.proDescLayout2)
    LinearLayout proDescLayout2;
    List<HashMap<String, Object>> row = new ArrayList<HashMap<String, Object>>();
    List<HashMap<String, Object>> row2 = new ArrayList<HashMap<String, Object>>();

    //    @BindView(R.id.default_content2)
    LinearLayout default_content2;
    LinearLayout dynamicContent;

    final ArrayList<String> s = new ArrayList<String>();
    final ArrayList<String> w = new ArrayList<String>();
    final ArrayList<Integer> q = new ArrayList<Integer>();

    int resID = 0;
    int quantity = 0;
    int proPrice;
    String[] prices;
    StringBuilder strengths, packs, custom_packs, sizes;

    public MyCartAdapter(Context context, int resource) {
        super(context, resource);
    }

    public MyCartAdapter(Context context, ArrayList<MyCartPOJO> cartList) {
        super(context, 0, cartList);
        this.context = context;
        this.cartList = cartList;

        layoutInflater = (LayoutInflater) this.context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public MyCartPOJO getItem(int position) {
        return cartList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getViewTypeCount() {
        return getCount();
    }

    @Override
    public int getItemViewType(int position) {
        return (getItem(position).getQuantity2() == 0) ? 1 : 0;
//        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

//        final ViewHolder viewHolder = new ViewHolder();
//        final ViewHolder2 viewHolder2 = new ViewHolder2();

        View rootView = convertView;

        Log.e("view_position", String.valueOf(position));
        int type = getItemViewType(position);

        Log.e("type", String.valueOf(type));

        if (rootView == null) {
            switch (type) {
                case 0:
                    rootView = layoutInflater.inflate(R.layout.content_mycart_listitem2, parent, false);

                    viewHolder2.productTitle = (TextView) rootView.findViewById(R.id.cart_pro_title);
                    viewHolder2.productPrice = (TextView) rootView.findViewById(R.id.cart_pro_price);
                    viewHolder2.productQuantity = (TextView) rootView.findViewById(R.id.quantity2);
                    viewHolder2.productQuantity.setTag(View.generateViewId());

                    viewHolder2.productImage = (ImageView) rootView.findViewById(R.id.cart_pro_image);

                    viewHolder2.increase = (ImageView) rootView.findViewById(R.id.increase);
                    viewHolder2.increase.setTag(View.generateViewId());

                    viewHolder2.decrease = (ImageView) rootView.findViewById(R.id.decrease);
                    viewHolder2.decrease.setTag(View.generateViewId());

                    viewHolder2.close = (ImageView) rootView.findViewById(R.id.cart_delete);
                    viewHolder2.close.setTag(View.generateViewId());

                    rootView.setTag(viewHolder2);

                    break;
                case 1:
                    rootView = layoutInflater.inflate(R.layout.content_mycart_listitem, parent, false);

                    viewHolder.productTitle = (TextView) rootView.findViewById(R.id.cart_pro_title);
                    viewHolder.productPrice = (TextView) rootView.findViewById(R.id.cart_pro_price);

                    viewHolder.productImage = (ImageView) rootView.findViewById(R.id.cart_pro_image);
                    viewHolder.close = (ImageView) rootView.findViewById(R.id.cart_delete);
                    viewHolder.close.setTag(View.generateViewId());

                    viewHolder.strength = (LinearLayout) rootView.findViewById(R.id.strength);

                    viewHolder.contentStrength = (LinearLayout) rootView.findViewById(R.id.content_strength);
                    viewHolder.contentWeight = (LinearLayout) rootView.findViewById(R.id.content_weight);
                    viewHolder.contentQuantity = (LinearLayout) rootView.findViewById(R.id.content_quantity);

                    viewHolder.change = (TextView) rootView.findViewById(R.id.cart_modify);
                    viewHolder.change.setTag(View.generateViewId());

                    rootView.setTag(viewHolder);

                    break;
            }
        } else {
            switch (type) {
                case 0:
                    viewHolder2 = (ViewHolder2) rootView.getTag();

                    break;
                case 1:
                    viewHolder = (ViewHolder) rootView.getTag();

                    break;
            }
        }

//        myCartPOJO = null;
        myCartPOJO = getItem(position);
        Log.e("getView_title", String.valueOf(myCartPOJO.getProductTitle()));

        proName = myCartPOJO.getProductTitle();
        Log.e("getView_ProductName", proName);

        switch (type) {
            case 0:

                viewHolder2.productTitle.setText(myCartPOJO.getProductTitle());
                viewHolder2.productImage.setImageDrawable(myCartPOJO.getProductImagePath());

                String cat = myCartPOJO.getProductCategory();

                if (cat.equals("CREAMS") || cat.equals("OTHER")) {
                    if (myCartPOJO.getQuantity2() > 5) {
                        quantity--;
                        if (quantity == 5) {
                            viewHolder2.decrease.setAlpha((float) .54);
                            viewHolder2.productQuantity.setAlpha((float) .54);
                            viewHolder2.decrease.setClickable(false);
                        }
                    }
                } else {
                    if (myCartPOJO.getQuantity2() > 0) {
                        quantity--;
                        if (quantity == 0) {
                            viewHolder2.decrease.setAlpha((float) .54);
                            viewHolder2.productQuantity.setAlpha((float) .54);
                            viewHolder2.decrease.setClickable(false);
                        }
                    }
                }

                if (myCartPOJO.getProductPrice() == 0) {
                    viewHolder2.productPrice.setText("--");
                } else {
                    viewHolder2.productPrice.setText(String.valueOf(myCartPOJO.getProductPrice()));
                }
                viewHolder2.productQuantity.setText(String.valueOf(myCartPOJO.getQuantity2()));

                final int actualPrice = myCartPOJO.getProductPrice() / myCartPOJO.getQuantity2();

                final View footerView = ((LayoutInflater) getContext().
                        getSystemService(LAYOUT_INFLATER_SERVICE)).
                        inflate(R.layout.content_mycart_lv_footer, null);

                viewHolder2.increase.setTag(R.id.quantity2, viewHolder2.productQuantity);
                viewHolder2.increase.setTag(R.id.cart_pro_price, viewHolder2.productPrice);
                viewHolder2.increase.setTag(R.id.decrease, viewHolder2.decrease);
                viewHolder2.increase.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        TextView productQuantity = (TextView) view.getTag(R.id.quantity2);
                        TextView productPrice = (TextView) view.getTag(R.id.cart_pro_price);
                        ImageView decrease = (ImageView) view.getTag(R.id.decrease);

                        int price = 0;
                        int quantity = Integer.parseInt(productQuantity.
                                getText().toString());

                        quantity++;

                        decrease.setAlpha((float) .99);
                        decrease.setClickable(true);
                        productQuantity.setAlpha((float) .99);
                        productQuantity.setText(String.valueOf(quantity));

                        if (myCartPOJO.getProductPrice() != 0) {
                            price = actualPrice * quantity;
                            productPrice.setText(Integer.toString(price));

                            final TextView overallPrice = (TextView) footerView.findViewById(R.id.overall_price);

                            int oPrice = Integer.parseInt(overallPrice.getText().toString());
                            overallPrice.setText(Integer.toString(oPrice + price));
                        }
                    }
                });

                viewHolder2.decrease.setTag(R.id.quantity2, viewHolder2.productQuantity);
                viewHolder2.decrease.setTag(R.id.cart_pro_price, viewHolder2.productPrice);
                viewHolder2.decrease.setTag(R.id.decrease, viewHolder2.decrease);
                viewHolder2.decrease.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        TextView productQuantity = (TextView) view.getTag(R.id.quantity2);
                        TextView productPrice = (TextView) view.getTag(R.id.cart_pro_price);
                        ImageView decrease = (ImageView) view.getTag(R.id.decrease);

                        int price = 0;
                        int quantity = Integer.parseInt(productQuantity.
                                getText().toString());

                        String category = myCartPOJO.getProductCategory();

                        if (category.equals("CREAMS") || category.equals("OTHER")) {
                            if (quantity > 5) {
                                quantity--;
                                if (quantity == 5) {
                                    decrease.setAlpha((float) .54);
                                    productQuantity.setAlpha((float) .54);
                                    decrease.setClickable(false);
                                }
                            }
                        } else {
                            if (quantity > 0) {
                                quantity--;
                                if (quantity == 0) {
                                    decrease.setAlpha((float) .54);
                                    productQuantity.setAlpha((float) .54);
                                    decrease.setClickable(false);
                                }
                            }
                        }

                        productQuantity.setText(String.valueOf(quantity));
                        if (myCartPOJO.getProductPrice() != 0) {
                            price = actualPrice * quantity;
                            productPrice.setText(Integer.toString(price));

                            final TextView overallPrice = (TextView) footerView.findViewById(R.id.overall_price);

                            int oPrice = Integer.parseInt(overallPrice.getText().toString());
                            overallPrice.setText(Integer.toString(oPrice + price));
                        }
                    }
                });

                viewHolder2.close.setOnClickListener(new View.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onClick(View view) {

                        String string = getData(getContext());

                        try {
                            JSONArray resultSet = new JSONArray(string);
                            for (int i = 0; i < resultSet.length(); i++) {
                                JSONObject results = resultSet.getJSONObject(i);

                                String title = (String) viewHolder2.productTitle.getText();

                                if (results.getString("title").equals(title)) {
                                    resultSet.remove(i);
                                }
                            }
                            saveData(getContext(), "mycart.json", resultSet.toString());
                        } catch (JSONException e) {
                            Log.e("jsonException", e.toString());
                        }

                        try {
                            Log.e("onClosePos", String.valueOf(position));
                            cartList.remove(position);
                            notifyDataSetChanged();
                        } catch (Exception e) {
                            Log.e("cartListException: ", e.toString());
                        }
                    }
                });

                break;
            case 1:

                viewHolder.productTitle.setText(myCartPOJO.getProductTitle());
                viewHolder.productImage.setImageDrawable(myCartPOJO.getProductImagePath());

                if (myCartPOJO.getProductPrice() == 0) {
                    viewHolder.productPrice.setText("--");
                } else {
                    viewHolder.productPrice.setText(String.valueOf(myCartPOJO.getProductPrice()));
                }

                viewHolder.change.setTag(position);
                viewHolder.change.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.e("clickedViewTag", String.valueOf(view.getTag()));

                        String string = getData(getContext());

                        JSONArray resultSet = null;

                        try {
                            resultSet = new JSONArray(string);

                            for (int i = 0; i < resultSet.length(); i++) {
                                JSONObject results = resultSet.getJSONObject(i);

                                if (results.getString("title").equals(proName)) {

                                    row.clear();
                                    row2.clear();

                                    resID = results.getInt("picture");

                                    int type = results.getInt("type");

                                    if (type == 1) {
                                        JSONArray requirements = results.getJSONArray("requirements");

                                        for (int j = 0; j < requirements.length(); j++) {
                                            JSONObject reqObj = requirements.getJSONObject(j);

                                        /*Log.e("strength", String.valueOf(reqObj.getString("strength")));
                                        if (j == 0) {
                                            strengths.append(reqObj.getString("strength"));
                                        } else {
                                            strengths.append("," + reqObj.getString("strength"));
                                        }*/


                                            HashMap<String, Object> rowData = new HashMap<String, Object>();

                                            rowData.put("layout_id", position);

                                            Log.e("onClick_changeDialog", reqObj.getString("strength"));

                                            rowData.put("strength", reqObj.getString("strength"));
                                            rowData.put("weight", reqObj.getString("weight"));
                                            rowData.put("quantity", reqObj.getString("quantity"));

                                            row.add(rowData);
                                            Log.e("rowData1", String.valueOf(row));
                                        }
                                    } else if (type == 2) {
                                        JSONArray requirements = results.getJSONArray("requirements");

                                        for (int j = 0; j < requirements.length(); j++) {
                                            JSONObject reqObj = requirements.getJSONObject(j);

                                            HashMap<String, Object> rowData = new HashMap<String, Object>();

                                            rowData.put("layout_id", position);
                                            rowData.put("weight", reqObj.getString("weight"));
                                            rowData.put("quantity", reqObj.getString("quantity"));

                                            row.add(rowData);
                                            Log.e("rowData2", String.valueOf(row));
                                        }
                                    } /*else if (type == 3) {

                                    }*/
                                }
                            }
                        } catch (JSONException e) {
                            Log.e("jsonException", e.toString());
                        }

                        category = myCartPOJO.getProductCategory();

                        if (category.equals("CHEMICAL PEEL")) {
                            Log.e("ope_proName", "name is ... " + proName);
                            Log.e("ope_category", "category is ... " + category);

                            try {
                                JSONObject products = new JSONObject(JsonDataCopy.jsondata);
                                JSONArray productsjsonarray = products.getJSONArray("data");
                                for (int i = 0; i < productsjsonarray.length(); i++) {
                                    JSONObject jobj = productsjsonarray.getJSONObject(i);

                                    if (jobj.getString("product_title").equals(proName)) {
                                        Log.e("ope_product_title", jobj.getString("product_title"));

                                        Object strengthObj = jobj.get("strength");

                                        if (strengthObj instanceof String) {
                                            strengths = new StringBuilder();

                                            strengths.append(strengthObj);
                                        } else if (strengthObj instanceof JSONArray) {
                                            JSONArray strength = (JSONArray) strengthObj;

                                            strengths = new StringBuilder();

                                            for (int j = 0; j < strength.length(); j++) {
                                                Log.e("strength_items", String.valueOf(strength.get(j)));
                                                if (j == 0) {
                                                    strengths.append(strength.get(j).toString());
                                                } else {
                                                    strengths.append("," + strength.get(j).toString());
                                                }
                                            }
                                        }

                                        Object packObj = jobj.get("pack");

                                        if (packObj instanceof String) {
                                            packs = new StringBuilder();

                                            packs.append(packObj);
                                        } else if (packObj instanceof JSONArray) {
                                            JSONArray pack = (JSONArray) packObj;

                                            packs = new StringBuilder();

                                            for (int j = 0; j < pack.length(); j++) {
                                                if (j == 0) {
                                                    packs.append(pack.get(j).toString());
                                                } else {
                                                    packs.append("," + pack.get(j).toString());
                                                }
                                            }
                                        }


                                        Object cusPackObj = jobj.get("custom_pack");

                                        if (cusPackObj instanceof JSONArray) {
                                            JSONArray custom_pack = (JSONArray) cusPackObj;

                                            custom_packs = new StringBuilder();

                                            for (int j = 0; j < custom_pack.length(); j++) {
                                                if (j == 0) {
                                                    custom_packs.append(custom_pack.get(j).toString());
                                                } else {
                                                    custom_packs.append("," + custom_pack.get(j).toString());
                                                }
                                            }
                                        }
                                    }
                                }
                            } catch (JSONException e) {
                                Log.e("chemical_peel excep", e.toString());
                            }
                        } else if (category.equals("SERUMS - SKIN CARE") || category.equals("SERUMS - HAIR CARE")) {
                            Log.e("ope_category", category);

                            try {
                                JSONObject products = new JSONObject(JsonDataCopy.jsondata);
                                JSONArray productsjsonarray = products.getJSONArray("data");
                                for (int i = 0; i < productsjsonarray.length(); i++) {
                                    JSONObject jobj = productsjsonarray.getJSONObject(i);

                                    if (jobj.getString("product_title").equals(proName)) {

                                        Object pack = jobj.get("pack");

                                        if (pack instanceof String) {
                                        } else if (pack instanceof JSONArray) {


                                            packs = new StringBuilder();

                                            JSONArray weight = (JSONArray) pack;

                                            for (int j = 0; j < weight.length(); j++) {
                                                if (j == 0) {
                                                    packs.append(weight.get(j).toString());
                                                } else {
                                                    packs.append("," + weight.get(j).toString());
                                                }
                                            }
                                        }

                                        Object priceObj = jobj.get("price");
                                        String price = null;

                                        if (priceObj instanceof String) {
                                            price = (String) priceObj;
                                        } else if (priceObj instanceof JSONArray) {
                                            StringBuilder priceBuilder = new StringBuilder();
                                            JSONArray priceArr = (JSONArray) priceObj;

                                            for (int j = 0; j < priceArr.length(); j++) {
                                                if (j == 0) {
                                                    priceBuilder.append(priceArr.getString(j));
                                                } else {
                                                    priceBuilder.append("," + priceArr.getString(j));
                                                }
                                            }
                                            price = String.valueOf(priceBuilder);

                                        }

                                        prices = price.split(",");
                                    }
                                }
                            } catch (JSONException e) {
                                Log.e("skin & hair excep", e.toString());
                            }
                        } else if (category.equals("CREAMS") || category.equals("OTHER")) {
                            Log.e("ope_category", category);
                            try {
                                JSONObject products = new JSONObject(JsonDataCopy.jsondata);
                                JSONArray productsjsonarray = products.getJSONArray("data");
                                for (int i = 0; i < productsjsonarray.length(); i++) {
                                    JSONObject jobj = productsjsonarray.getJSONObject(i);

                                    if (jobj.getString("product_title").equals(proName)) {

                                        Object pack = jobj.get("pack");

                                        if (!pack.toString().equals("NA")) {
                                            if (pack instanceof String) {
                                            } else if (pack instanceof JSONArray) {

                                                packs = new StringBuilder();

                                                JSONArray weight = (JSONArray) pack;

                                                for (int j = 0; j < weight.length(); j++) {
                                                    if (j == 0) {
                                                        packs.append(weight.get(j).toString());
                                                    } else {
                                                        packs.append("," + weight.get(j).toString());
                                                    }
                                                }
                                            }
                                        } else {
                                        }

                                        Object priceObj = jobj.get("price");
                                        String price = null;

                                        if (priceObj instanceof String) {
                                            price = (String) priceObj;
                                        } else if (priceObj instanceof JSONArray) {
                                            StringBuilder priceBuilder = new StringBuilder();
                                            JSONArray priceArr = (JSONArray) priceObj;

                                            for (int j = 0; j < priceArr.length(); j++) {
                                                if (j == 0) {
                                                    priceBuilder.append(priceArr.getString(j));
                                                } else {
                                                    priceBuilder.append("," + priceArr.getString(j));
                                                }
                                            }
                                            price = String.valueOf(priceBuilder);

                                        }

                                        prices = price.split(",");
                                    }
                                }
                            } catch (JSONException e) {
                                Log.e("creams and others excep", e.toString());
                            }
                        } else if (category.equals("ROLLERS")) {
                            Log.e("ope_category", category);
                            try {
                                JSONObject products = new JSONObject(JsonDataCopy.jsondata);
                                JSONArray productsjsonarray = products.getJSONArray("data");
                                for (int i = 0; i < productsjsonarray.length(); i++) {
                                    JSONObject jobj = productsjsonarray.getJSONObject(i);

                                    if (jobj.getString("product_title").equals(proName)) {

                                        Object sizeObj = jobj.get("sizes");

                                        if (!sizeObj.toString().equals("NA")) {
                                            if (sizeObj instanceof String) {

                                                sizes = new StringBuilder();
                                                sizes.append(sizeObj);
                                            } else if (sizeObj instanceof JSONArray) {
                                                JSONArray size = (JSONArray) sizeObj;


                                                sizes = new StringBuilder();

                                                for (int j = 0; j < size.length(); j++) {
                                                    if (j == 0) {
                                                        sizes.append(size.get(j).toString());
                                                    } else {
                                                        sizes.append("," + size.get(j).toString());
                                                    }
                                                }
                                            }
                                        } else {
                                        }

                                        Object priceObj = jobj.get("price");
                                        String price = null;

                                        if (priceObj instanceof String) {
                                            price = (String) priceObj;
                                        } else if (priceObj instanceof JSONArray) {
                                            StringBuilder priceBuilder = new StringBuilder();
                                            JSONArray priceArr = (JSONArray) priceObj;

                                            for (int j = 0; j < priceArr.length(); j++) {
                                                if (j == 0) {
                                                    priceBuilder.append(priceArr.getString(j));
                                                } else {
                                                    priceBuilder.append("," + priceArr.getString(j));
                                                }
                                            }
                                            price = String.valueOf(priceBuilder);

                                        }

                                        prices = price.split(",");
                                    }
                                }
                            } catch (JSONException e) {
                                Log.e("rollers excep", e.toString());
                            }
                        } else if (category.equals("MASK")) {
                            Log.e("ope_category", category);
                        }

                        title = myCartPOJO.getProductTitle();

                        changeDialog((Integer) view.getTag());
//                        changeDialog(position);

                        notifyDataSetChanged();
                    }
                });

                viewHolder.close.setOnClickListener(new View.OnClickListener() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onClick(View view) {
//                        Toast.makeText(getContext(), "Close Clicked1", Toast.LENGTH_SHORT).show();

                        String string = getData(getContext());

                        try {
                            JSONArray resultSet = new JSONArray(string);
                            for (int i = 0; i < resultSet.length(); i++) {
                                JSONObject results = resultSet.getJSONObject(i);

                                String title = (String) viewHolder.productTitle.getText();

                                if (results.getString("title").equals(title)) {
                                    resultSet.remove(i);
                                    Log.e("resultSet_removed", String.valueOf(resultSet));
                                }
                            }

                            saveData(getContext(), "mycart.json", resultSet.toString());
                        } catch (JSONException e) {
                            Log.e("jsonException", e.toString());
                        }

                        try {
                            Log.e("onClosePos", String.valueOf(position));
                            cartList.remove(position);
                            MyCartAdapter.this.notifyDataSetChanged();
                        } catch (Exception e) {
                            Log.e("cartListException: ", e.toString());
                        }

                        notifyDataSetChanged();
                    }
                });

                if (myCartPOJO.getStrength() != null) {
                    viewHolder.strength.setVisibility(View.VISIBLE);
                    viewHolder.contentStrength.setVisibility(View.VISIBLE);

                    if (viewHolder.contentStrength.getChildCount() > 0 &&
                            viewHolder.contentWeight.getChildCount() > 0 &&
                            viewHolder.contentQuantity.getChildCount() > 0) {
                        viewHolder.contentStrength.removeAllViews();
                        viewHolder.contentWeight.removeAllViews();
                        viewHolder.contentQuantity.removeAllViews();
                    }

                    for (String strength : myCartPOJO.getStrength()) {
                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT);

                        TextView content = new TextView(getContext());

                        content.setLayoutParams(params);
                        content.setPadding(4, 4, 4, 4);
                        content.setGravity(Gravity.CENTER);
                        content.setText(strength);
                        content.setTextColor(Color.BLACK);
                        content.setAlpha((float) .87);

                        viewHolder.contentStrength.addView(content);
                    }
                } else {
                    viewHolder.strength.setVisibility(View.GONE);
                    if (viewHolder.contentWeight.getChildCount() > 0 &&
                            viewHolder.contentQuantity.getChildCount() > 0) {
                        viewHolder.contentWeight.removeAllViews();
                        viewHolder.contentQuantity.removeAllViews();
                    }
                }

                for (String weight : myCartPOJO.getWeight()) {
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT);

                    TextView content = new TextView(getContext());

                    content.setLayoutParams(params);
                    content.setPadding(4, 4, 4, 4);
                    content.setGravity(Gravity.CENTER);
                    content.setText(weight);
                    content.setTextColor(Color.BLACK);
                    content.setAlpha((float) .87);

                    viewHolder.contentWeight.addView(content);
                }

                for (int quantity : myCartPOJO.getQuantity()) {
                    //viewHolder.contentStrength;
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT);

                    TextView content = new TextView(getContext());

                    content.setLayoutParams(params);
                    content.setPadding(4, 4, 4, 4);
                    content.setGravity(Gravity.CENTER);
                    content.setText(String.valueOf(quantity));
                    content.setTextColor(Color.BLACK);
                    content.setAlpha((float) .87);

                    viewHolder.contentQuantity.addView(content);
                }

                break;
        }

        return rootView;
    }

    class ViewHolder {
        TextView productTitle, productPrice, change;
        ImageView productImage, close;
        LinearLayout strength, contentStrength, contentWeight, contentQuantity;
    }

    class ViewHolder2 {
        TextView productTitle, productPrice, productQuantity;
        ImageView productImage, increase, decrease, close;
    }

    public static void saveData(Context context, String fileName, String mJsonResponse) {
        try {
            FileWriter file = new FileWriter(context.getFilesDir().getPath() + "/" + fileName);
            file.write(mJsonResponse);
            file.flush();
            file.close();
            Log.e("Write Success_OH.", String.valueOf(file));

        } catch (IOException e) {
            Log.e("Error in Writing_OH.", e.getLocalizedMessage());
        }
    }

    public static String getData(Context context) {
        try {
            File f = new File(context.getFilesDir().getPath() + "/" + "mycart.json");
            //check whether file exists
            FileInputStream is = new FileInputStream(f);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();

            Log.e("Read Success", new String(buffer, "UTF-8"));
            return new String(buffer, "UTF-8");
        } catch (IOException e) {
            Log.e("File not exist", e.getLocalizedMessage());
            return null;
        }
    }

    void changeDialog(final int position) {
        final Dialog dialog = new Dialog(getContext()/*, android.R.style.Theme_Holo_Light_Dialog*/);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_change);

        DisplayMetrics metrics = getContext().getResources().getDisplayMetrics();
        final int screenWidth = (int) (metrics.widthPixels * 0.99);

        dialog.getWindow().setLayout(screenWidth, ViewGroup.LayoutParams.WRAP_CONTENT);

        //---------------
        items = (LinearLayout) dialog.findViewById(R.id.items);
        Button done = (Button) dialog.findViewById(R.id.done);
        done.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View view) {
                dialog.dismiss();

                String title = (String) viewHolder.productTitle.getText();

                try {
                    String string = getData(getContext());

                    Log.e("title_remove", proName);
                    JSONArray resultSet = new JSONArray(string);
                    for (int i = 0; i < resultSet.length(); i++) {
                        JSONObject results = resultSet.getJSONObject(i);
                        if (results.getString("title").equals(proName)) {
                            resultSet.remove(i);
                        }
                    }
                    saveData(getContext(), "mycart.json", resultSet.toString());
                    String afterRemoved = getData(getContext());
                    Log.e("aft_removed ", afterRemoved);
                } catch (JSONException e) {
                    Log.e("jsonException", e.toString());
                }

                String[] sArray = new String[0];
                String[] wArray = new String[0];
                int[] qArray = new int[0];

                if (myCartPOJO.getStrength() != null) {
                    viewHolder.strength.setVisibility(View.VISIBLE);
                    viewHolder.contentStrength.setVisibility(View.VISIBLE);

                    if (viewHolder.contentStrength.getChildCount() > 0 &&
                            viewHolder.contentWeight.getChildCount() > 0 &&
                            viewHolder.contentQuantity.getChildCount() > 0) {
                        viewHolder.contentStrength.removeAllViews();
                        viewHolder.contentWeight.removeAllViews();
                        viewHolder.contentQuantity.removeAllViews();
                    }

                    for (int i = 0; i < row.size(); i++) {
                        HashMap<String, Object> roww = row.get(i);
                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT);

                        TextView content = new TextView(getContext());

                        content.setLayoutParams(params);
                        content.setPadding(4, 4, 4, 4);
                        content.setGravity(Gravity.CENTER);
                        content.setText(roww.get("strength").toString());
                        content.setTextColor(Color.BLACK);
                        content.setAlpha((float) .87);

                        viewHolder.contentStrength.addView(content);

                        String strength = roww.get("strength").toString();
                        s.add(strength);
                    }
                } else {
                    viewHolder.strength.setVisibility(View.GONE);
                    if (viewHolder.contentWeight.getChildCount() > 0 &&
                            viewHolder.contentQuantity.getChildCount() > 0) {
                        viewHolder.contentWeight.removeAllViews();
                        viewHolder.contentQuantity.removeAllViews();
                    }
                }

                for (int i = 0; i < row.size(); i++) {
                    HashMap<String, Object> roww = row.get(i);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT);

                    TextView content = new TextView(getContext());

                    content.setLayoutParams(params);
                    content.setPadding(4, 4, 4, 4);
                    content.setGravity(Gravity.CENTER);
                    content.setText(roww.get("weight").toString());
                    content.setTextColor(Color.BLACK);
                    content.setAlpha((float) .87);

                    viewHolder.contentWeight.addView(content);

                    String weight = roww.get("weight").toString();
                    w.add(weight);
                }

                for (int i = 0; i < row.size(); i++) {
                    HashMap<String, Object> roww = row.get(i);
                    //viewHolder.contentStrength;
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT);

                    TextView content = new TextView(getContext());

                    content.setLayoutParams(params);
                    content.setPadding(4, 4, 4, 4);
                    content.setGravity(Gravity.CENTER);
                    content.setText(roww.get("quantity").toString());
                    content.setTextColor(Color.BLACK);
                    content.setAlpha((float) .87);

                    viewHolder.contentQuantity.addView(content);

                    String quantity = (String) roww.get("quantity");
                    q.add(Integer.valueOf(quantity));
                }


                if (quantity != 0) {
                    String string = getData(getContext());
                    JSONArray resultSet;
                    try {
                        if (string != null) {
                            resultSet = new JSONArray(string);
                        } else {
                            resultSet = new JSONArray();
                        }

                        JSONObject values = new JSONObject();

                        values.put("quantity", quantity);
                        values.put("price", proPrice);
                        int overallPrice = proPrice * quantity;
                        values.put("overall_price", overallPrice);
                        values.put("type", 3);
                        values.put("picture", resID);
                        values.put("category", category);
                        values.put("title", title);

                        resultSet.put(values);

                        Log.e("resultSet_myCart", String.valueOf(resultSet));

                        saveData(getContext(), "mycart.json", resultSet.toString());
                        row.clear();
                    } catch (JSONException e) {
                        Log.e("Exception", e.toString());
                    }
                } else {

                    ArrayList<String> sArr;
                    ArrayList<String> wArr = null;

                    if (category.equals("CHEMICAL PEEL")) {
                        sArr = new ArrayList<>();
                        wArr = new ArrayList<>();

                        for (String s1 : s) {
                            sArr.add(s1);
                        }

                        sArray = new String[sArr.size()];
                        for (int i = 0; i < sArray.length; i++) {
                            sArray[i] = sArr.get(i);
                        }

                        for (String w1 : w) {
                            wArr.add(w1);
                        }

                        wArray = new String[wArr.size()];
                        for (int i = 0; i < wArray.length; i++) {
                            wArray[i] = wArr.get(i);
                        }
                    } else if (category.equals("SERUMS - SKIN CARE")
                            || category.equals("SERUMS - HAIR CARE")
                            || category.equals("CREAMS")
                            || category.equals("ROLLERS")) {
                        wArr = new ArrayList<>();

                        for (String w1 : w) {
                            wArr.add(w1);
                        }

                        wArray = new String[wArr.size()];
                        for (int i = 0; i < wArray.length; i++) {
                            wArray[i] = wArr.get(i);
                        }
                    }

                    qArray = new int[q.size()];
                    for (int i = 0; i < qArray.length; i++) {
                        qArray[i] = q.get(i).intValue();
                    }

                    JSONArray resultSet;
                    try {
                        String string = getData(getContext());

                        if (string != null) {
                            resultSet = new JSONArray(string);
                        } else {
                            resultSet = new JSONArray();
                        }

                        JSONObject values = new JSONObject();

                        JSONArray content = new JSONArray();

                        int overallPrice = 0;

                        for (int i = 0; i < row.size(); i++) {
                            HashMap<String, Object> roww = row.get(i);

                            Log.e("strength()", String.valueOf(roww.get("strength")));
                            JSONObject contentValue = new JSONObject();

                            if (category.equals("CHEMICAL PEEL")) {
                                contentValue.put("strength", roww.get("strength"));
                                contentValue.put("weight", roww.get("weight"));
                                contentValue.put("quantity", roww.get("quantity"));
                                contentValue.put("price", proPrice);
                                overallPrice += proPrice;
                            } else if (category.equals("SERUMS - SKIN CARE")
                                    || category.equals("SERUMS - HAIR CARE")
                                    || category.equals("CREAMS")
                                    || category.equals("ROLLERS")) {
                                contentValue.put("weight", roww.get("weight"));
                                contentValue.put("quantity", roww.get("quantity"));
                                contentValue.put("price", proPrice);
                                overallPrice += proPrice;
                            }
                            content.put(contentValue);
                        }
                        values.put("requirements", content);
                        values.put("overall_price", overallPrice);

                        if (category.equals("CHEMICAL PEEL")) {
                            values.put("type", 1);
                        } else if (category.equals("SERUMS - SKIN CARE")
                                || category.equals("SERUMS - HAIR CARE")
                                || category.equals("CREAMS")
                                || category.equals("ROLLERS")) {
                            values.put("type", 2);
                        }
                        values.put("picture", resID);
                        values.put("category", category);
                        values.put("title", title);

                        resultSet.put(values);

                        Log.e("resultSet", String.valueOf(resultSet));

                        saveData(getContext(), "mycart.json", resultSet.toString());
                        row.clear();

                    } catch (JSONException e) {
                        Log.e("Exception", e.toString());
                    }
                }

                context.startActivity(new Intent(getContext(), MyCartActivity.class));
                ((Activity) getContext()).finish();
            }
        });

        Button cancel = (Button) dialog.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                row.clear();
                row2.clear();

                dialog.dismiss();

            }
        });

        Log.e("row_size", String.valueOf(row.size()));
        if (row.size() != 0) {

            for (HashMap<String, Object> roww : row) {
                row2.add(roww);
            }
            row.clear();
            createRow(row2);
            addButtonView();

        } else {
            createRow();
            addButtonView();
        }

        dialog.show();
    }

    public void addButtonView() {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                130, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.CENTER_HORIZONTAL;
        params.setMargins(0, 4, 0, 8);

        final TextView addButton = new TextView(getContext());
        addButton.setTag("dialog_item_add");
        addButton.setLayoutParams(params);
        addButton.setBackground(getContext().getResources().getDrawable(R.drawable.add_bg));
        addButton.setGravity(Gravity.CENTER);
        addButton.setPadding(8, 8, 8, 8);
        addButton.setText("ADD");
        addButton.setTextColor(getContext().getResources().getColor(R.color.colorPrimary));
        addButton.setTextSize(14);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addButton.setVisibility(View.GONE);
                removeItem.setVisibility(View.VISIBLE);

                createRow();
                addButtonView();
            }
        });

        items.addView(addButton);
    }

    public void createRow(final List<HashMap<String, Object>> rowItems) {

        int lastItem = rowItems.size() - 1;
        Log.e("lastItem", String.valueOf(lastItem));

        int curItem = 0;

        for (HashMap<String, Object> roww : rowItems) {
            final RelativeLayout itemDialogLO = (RelativeLayout) View.inflate(getContext(),
                    R.layout.item_dialog, null);
            itemDialogLO.setId(View.generateViewId());

            LinearLayout topLO = (LinearLayout) itemDialogLO.findViewById(R.id.top);
            topLO.setId(View.generateViewId());

            LinearLayout strenghtLO = (LinearLayout) itemDialogLO.findViewById(R.id.strength_layout);
            LinearLayout weightLO = (LinearLayout) itemDialogLO.findViewById(R.id.weight_layout);
            LinearLayout quantityLO = (LinearLayout) itemDialogLO.findViewById(R.id.quantity_layout);
            final LinearLayout priceLO = (LinearLayout) itemDialogLO.findViewById(R.id.price_layout);

            final AppCompatSpinner strengthSpinner = (AppCompatSpinner) strenghtLO.findViewById(R.id.strength_spinner);
            final AppCompatSpinner weightSpinner = (AppCompatSpinner) weightLO.findViewById(R.id.weight_spinner);
            final TextView quantityText = (TextView) quantityLO.findViewById(R.id.quantity_text);
            final TextView priceText = (TextView) priceLO.findViewById(R.id.price_text);

            ImageView increaseQuantity = (ImageView) quantityLO.findViewById(R.id.increase);
            ImageView decreaseQuantity = (ImageView) quantityLO.findViewById(R.id.decrease);

            if (category.equals("CHEMICAL PEEL")) {
                strenghtLO.setVisibility(View.VISIBLE);
                weightLO.setVisibility(View.VISIBLE);
                priceLO.setVisibility(View.GONE);

                String[] strArray = this.strengths.toString().split(",");

                List<String> strengths = new ArrayList<String>();
                for (String s : strArray) {
                    strengths.add(s);
                }

                ArrayAdapter<String> strengthAdapter = new ArrayAdapter<String>(getContext(),
                        android.R.layout.simple_spinner_item, strengths);

                strengthAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                strengthSpinner.setDropDownWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
                strengthSpinner.setAdapter(strengthAdapter);

                int i = 0;
                for (String strength : strengths) {
                    if (roww.get("strength").equals(strength)) {
                        int spinnerPosition = strengthAdapter.getPosition(strength);
//                        weightSpinner.setPrompt(strengthAdapter.getItem(j));
                        weightSpinner.setSelection(spinnerPosition);
                        i++;
                    }
                }

                final List<String> weights = new ArrayList<String>();

                strengthSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                        weights.clear();

                        if (strengthSpinner.getSelectedItem().equals("Custom")) {

                            if (custom_packs.toString().contains(",")) {
                                String[] pacArray = custom_packs.toString().split(",");

                                for (String s : pacArray) {
                                    weights.add(s);
                                }
                            } else {
                                weights.add(custom_packs.toString());
                            }
                        } else {

                            if (packs.toString().contains(",")) {
                                String[] pacArray = packs.toString().split(",");

                                for (String s : pacArray) {
                                    weights.add(s);
                                }
                            } else {
                                weights.add(packs.toString());
                            }
                        }

                        for (int i = 0; i < row.size(); i++) {

                            HashMap<String, Object> roww = row.get(i);

                            int id = (int) roww.get("layout_id");

                            if (id == itemDialogLO.getId()) {
                                roww.put("strength", strengthSpinner.getSelectedItem());
                                roww.put("weight", weightSpinner.getSelectedItem());
                                roww.put("quantity", quantityText.getText());
                            }
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

                if (packs.toString().contains(",")) {
                    String[] pacArray = packs.toString().split(",");

                    for (String s : pacArray) {
                        weights.add(s);
                    }
                } else {
                    weights.add(packs.toString());
                }

                ArrayAdapter<String> weightAdapter = new ArrayAdapter<String>(getContext(),
                        android.R.layout.simple_spinner_item, weights);

                weightAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                weightSpinner.setDropDownWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
                weightSpinner.setAdapter(weightAdapter);

                int j = 0;
                for (String weight : weights) {
                    if (roww.get("weight").equals(weight)) {
                        int spinnerPosition = strengthAdapter.getPosition(weight);
//                        weightSpinner.setPrompt(strengthAdapter.getItem(j));
                        weightSpinner.setSelection(spinnerPosition);
                        i++;
                    }
                }

                weightSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                        for (int i = 0; i < row.size(); i++) {

                            HashMap<String, Object> roww = row.get(i);

                            int id = (int) roww.get("layout_id");

                            if (id == itemDialogLO.getId()) {
                                roww.put("strength", strengthSpinner.getSelectedItem());
                                roww.put("weight", weightSpinner.getSelectedItem());
                                roww.put("quantity", quantityText.getText());
                            }
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

                HashMap<String, Object> rowData = new HashMap<String, Object>();

                rowData.put("layout_id", itemDialogLO.getId());
//                Log.e("rowLayoutId: ", String.valueOf(rowLayout.getId()));

                rowData.put("strength", strengthSpinner.getSelectedItem());
                rowData.put("weight", weightSpinner.getSelectedItem());
                rowData.put("quantity", quantityText.getText());

                row.add(rowData);

            } else if (category.equals("SERUMS - SKIN CARE")
                    || category.equals("SERUMS - HAIR CARE")
                    || category.equals("CREAMS")
                    || category.equals("OTHER")) {
                strenghtLO.setVisibility(View.GONE);
                weightLO.setVisibility(View.VISIBLE);
//              priceLO.setVisibility(View.VISIBLE);

                String[] pacArray = this.packs.toString().split(",");

                List<String> weights = new ArrayList<String>();
                for (String s : pacArray) {
                    weights.add(s);
                }

                ArrayAdapter<String> weightAdapter = new ArrayAdapter<String>(getContext(),
                        android.R.layout.simple_spinner_item, weights);

                weightAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                weightSpinner.setDropDownWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
                weightSpinner.setAdapter(weightAdapter);
                weightSpinner.setPrompt((CharSequence) roww.get("weight"));
                weightSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {

                        if (prices[position].equals("NA")) {
                            priceLO.setVisibility(View.VISIBLE);
                            priceText.setText("--");
                        } else {
                            priceLO.setVisibility(View.VISIBLE);
                            priceText.setText(prices[position]);
                        }

                        for (int i = 0; i < row.size(); i++) {

                            HashMap<String, Object> roww = row.get(i);

                            int id = (int) roww.get("layout_id");

                            if (id == itemDialogLO.getId()) {
                                roww.put("strength", strengthSpinner.getSelectedItem());
                                roww.put("weight", weightSpinner.getSelectedItem());
                                roww.put("quantity", quantityText.getText());
                                if (prices[position].equals("NA")) {
                                    roww.put("price", proPrice);
                                } else {
                                    roww.put("price", prices[position]);
                                }
                            }
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

                HashMap<String, Object> rowData = new HashMap<String, Object>();

                rowData.put("layout_id", itemDialogLO.getId());
//                Log.e("rowLayoutId: ", String.valueOf(rowLayout.getId()));

                rowData.put("weight", weightSpinner.getSelectedItem());
                rowData.put("quantity", quantityText.getText());

                row.add(rowData);
            } else if (category.equals("ROLLERS")) {
                strenghtLO.setVisibility(View.GONE);
                weightLO.setVisibility(View.VISIBLE);
//              priceLO.setVisibility(View.VISIBLE);

                String[] pacArray = this.sizes.toString().split(",");

                List<String> weights = new ArrayList<String>();
                for (String s : pacArray) {
                    weights.add(s);
                }

                ArrayAdapter<String> weightAdapter = new ArrayAdapter<String>(getContext(),
                        android.R.layout.simple_spinner_item, weights);

                weightAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                weightSpinner.setDropDownWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
                weightSpinner.setAdapter(weightAdapter);
                weightSpinner.setPrompt((CharSequence) roww.get("weight"));
                weightSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {

                        if (prices[position].equals("NA")) {
                            priceLO.setVisibility(View.VISIBLE);
                            priceText.setText("--");
                        } else {
                            priceLO.setVisibility(View.VISIBLE);
                            priceText.setText(prices[position]);
                        }

                        for (int i = 0; i < row.size(); i++) {

                            HashMap<String, Object> roww = row.get(i);

                            int id = (int) roww.get("layout_id");

                            if (id == itemDialogLO.getId()) {
                                roww.put("strength", strengthSpinner.getSelectedItem());
                                roww.put("weight", weightSpinner.getSelectedItem());
                                roww.put("quantity", quantityText.getText());
                                if (prices[position].equals("NA")) {
                                    roww.put("price", proPrice);
                                } else {
                                    roww.put("price", prices[position]);
                                }
                            }
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

                HashMap<String, Object> rowData = new HashMap<String, Object>();

                rowData.put("layout_id", itemDialogLO.getId());
//                Log.e("rowLayoutId: ", String.valueOf(rowLayout.getId()));

                rowData.put("weight", weightSpinner.getSelectedItem());
                rowData.put("quantity", quantityText.getText());

                row.add(rowData);
            }

            quantityText.setText((CharSequence) roww.get("quantity"));

            increaseQuantity.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int quantity = Integer.parseInt(quantityText.
                            getText().toString());

                    quantity++;
                    quantityText.setText(String.valueOf(quantity));

                    for (int i = 0; i < row.size(); i++) {

                        HashMap<String, Object> roww = row.get(i);

                        int id = (int) roww.get("layout_id");

                        if (id == itemDialogLO.getId()) {
                            roww.put("strength", strengthSpinner.getSelectedItem());
                            roww.put("weight", weightSpinner.getSelectedItem());
                            roww.put("quantity", quantityText.getText());
                        }
                    }
                }
            });

            decreaseQuantity.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int quantity = Integer.parseInt(quantityText.
                            getText().toString());

                    if (quantity > 0) {
                        quantity--;
                    }
                    quantityText.setText(String.valueOf(quantity));

                    for (int i = 0; i < row.size(); i++) {

                        HashMap<String, Object> roww = row.get(i);

                        int id = (int) roww.get("layout_id");

                        if (id == itemDialogLO.getId()) {
                            roww.put("strength", strengthSpinner.getSelectedItem());
                            roww.put("weight", weightSpinner.getSelectedItem());
                            roww.put("quantity", quantityText.getText());
                        }
                    }
                }
            });

//            addItem = (Button) itemDialogLO.findViewById(R.id.dialog_item_add);
            RelativeLayout removeItemLO = (RelativeLayout) itemDialogLO.findViewById(R.id.remove_layout);
            removeItem = (ImageView) itemDialogLO.findViewById(R.id.dialog_item_remove);

            verifyDialogListSize();

            if (curItem == lastItem) {
                removeItem.setVisibility(View.GONE);

            } else {
                removeItem.setVisibility(View.VISIBLE);
                curItem++;
            }

            removeItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    for (int i = 0; i < row.size(); i++) {
                        if (row.size() == 1) {
                            removeItem.setVisibility(View.GONE);
                        } else {
                            removeItem.setVisibility(View.VISIBLE);
                            HashMap<String, Object> roww = row.get(i);

                            int id = (int) roww.get("layout_id");

                            if (id == itemDialogLO.getId()) {
                                row.remove(i);

                                items.removeView(itemDialogLO);
                            }

                            verifyDialogListSize();
                        }
                    }
                }
            });

            items.addView(itemDialogLO);
        }
    }

    public void createRow() {
        try {
            final RelativeLayout itemDialogLO = (RelativeLayout) View.inflate(getContext(),
                    R.layout.item_dialog, null);
            itemDialogLO.setId(View.generateViewId());

            LinearLayout topLO = (LinearLayout) itemDialogLO.findViewById(R.id.top);
            topLO.setId(View.generateViewId());

            LinearLayout strenghtLO = (LinearLayout) itemDialogLO.findViewById(R.id.strength_layout);
            LinearLayout weightLO = (LinearLayout) itemDialogLO.findViewById(R.id.weight_layout);
            LinearLayout quantityLO = (LinearLayout) itemDialogLO.findViewById(R.id.quantity_layout);
            final LinearLayout priceLO = (LinearLayout) itemDialogLO.findViewById(R.id.price_layout);

            final AppCompatSpinner strengthSpinner = (AppCompatSpinner) strenghtLO.findViewById(R.id.strength_spinner);
            final AppCompatSpinner weightSpinner = (AppCompatSpinner) weightLO.findViewById(R.id.weight_spinner);
            final TextView quantityText = (TextView) quantityLO.findViewById(R.id.quantity_text);
            final TextView priceText = (TextView) priceLO.findViewById(R.id.price_text);

            ImageView increaseQuantity = (ImageView) quantityLO.findViewById(R.id.increase);
            ImageView decreaseQuantity = (ImageView) quantityLO.findViewById(R.id.decrease);

            if (category.equals("CHEMICAL PEEL")) {
                strenghtLO.setVisibility(View.VISIBLE);
                weightLO.setVisibility(View.VISIBLE);
                priceLO.setVisibility(View.GONE);

                String[] strArray = this.strengths.toString().split(",");

                List<String> strengths = new ArrayList<String>();
                for (String s : strArray) {
                    strengths.add(s);
                }

                ArrayAdapter<String> strengthAdapter = new ArrayAdapter<String>(getContext(),
                        android.R.layout.simple_spinner_item, strengths);

                strengthAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                strengthSpinner.setDropDownWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
                strengthSpinner.setAdapter(strengthAdapter);

                final List<String> weights = new ArrayList<String>();

                strengthSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                        weights.clear();

                        if (strengthSpinner.getSelectedItem().equals("Custom")) {

                            if (custom_packs.toString().contains(",")) {
                                String[] pacArray = custom_packs.toString().split(",");

                                for (String s : pacArray) {
                                    weights.add(s);
                                }
                            } else {
                                weights.add(custom_packs.toString());
                            }
                        } else {
                            if (packs.toString().contains(",")) {
                                String[] pacArray = packs.toString().split(",");

                                for (String s : pacArray) {
                                    weights.add(s);
                                }
                            } else {
                                weights.add(packs.toString());
                            }
                        }

                        for (int i = 0; i < row.size(); i++) {

                            HashMap<String, Object> roww = row.get(i);

                            int id = (int) roww.get("layout_id");

                            if (id == itemDialogLO.getId()) {
                                roww.put("strength", adapterView.getItemAtPosition(position));
                                roww.put("weight", weightSpinner.getSelectedItem());
                                roww.put("quantity", quantityText.getText());
                            }
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

                if (packs.toString().contains(",")) {
                    String[] pacArray = packs.toString().split(",");

                    for (String s : pacArray) {
                        weights.add(s);
                    }
                } else {
                    weights.add(packs.toString());
                }

                final ArrayAdapter<String> weightAdapter = new ArrayAdapter<String>(getContext(),
                        android.R.layout.simple_spinner_item, weights);

                weightAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                weightSpinner.setDropDownWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
                weightSpinner.setAdapter(weightAdapter);
//            weightSpinner.setPrompt(weightAdapter.getItem(0));
                weightSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                        weightSpinner.setPrompt((CharSequence) weightSpinner.getSelectedItem());

                        for (int i = 0; i < row.size(); i++) {

                            HashMap<String, Object> roww = row.get(i);

                            int id = (int) roww.get("layout_id");

                            if (id == itemDialogLO.getId()) {
                                roww.put("strength", strengthSpinner.getSelectedItem());
                                roww.put("weight", adapterView.getItemAtPosition(position));
                                roww.put("quantity", quantityText.getText());
                            }
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

                HashMap<String, Object> rowData = new HashMap<String, Object>();

                rowData.put("layout_id", itemDialogLO.getId());

                rowData.put("strength", strengthSpinner.getSelectedItem());
                rowData.put("weight", weightSpinner.getSelectedItem());
                rowData.put("quantity", quantityText.getText());

                row.add(rowData);
                Log.e("rowData", String.valueOf(row));

            } else if (category.equals("SERUMS - SKIN CARE")
                    || category.equals("SERUMS - HAIR CARE")
                    || category.equals("CREAMS")) {
                strenghtLO.setVisibility(View.GONE);
                weightLO.setVisibility(View.VISIBLE);

                String[] pacArray = packs.toString().split(",");

                List<String> weights = new ArrayList<String>();
                for (String s : pacArray) {
                    weights.add(s);
                }

                ArrayAdapter<String> weightAdapter = new ArrayAdapter<String>(getContext(),
                        android.R.layout.simple_spinner_item, weights);

                weightAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                weightSpinner.setDropDownWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
                weightSpinner.setAdapter(weightAdapter);
                weightSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {

                        if (prices[position].equals("NA")) {
                            priceLO.setVisibility(View.VISIBLE);
                            priceText.setText("--");
                        } else {
                            priceLO.setVisibility(View.VISIBLE);
                            priceText.setText(prices[position]);
                        }

                        for (int i = 0; i < row.size(); i++) {

                            HashMap<String, Object> roww = row.get(i);

                            int id = (int) roww.get("layout_id");

                            if (id == itemDialogLO.getId()) {
                                roww.put("strength", strengthSpinner.getSelectedItem());
                                roww.put("weight", weightSpinner.getSelectedItem());
                                roww.put("quantity", quantityText.getText());

                                if (prices[position].equals("NA")) {
                                    roww.put("price", proPrice);
                                } else {
                                    Log.e("priceTag", prices[position]);
                                    roww.put("price", prices[position]);
                                }
                            }
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

                HashMap<String, Object> rowData = new HashMap<String, Object>();

                rowData.put("layout_id", itemDialogLO.getId());

                Log.e("onClick: ", weightSpinner.getSelectedItem().toString());

                rowData.put("weight", weightSpinner.getSelectedItem());
                rowData.put("quantity", quantityText.getText());

                row.add(rowData);
                Log.e("rowData", String.valueOf(row));
            } else if (category.equals("ROLLERS")) {
                strenghtLO.setVisibility(View.GONE);
                weightLO.setVisibility(View.VISIBLE);
//              priceLO.setVisibility(View.VISIBLE);

                String[] pacArray = this.sizes.toString().split(",");

                List<String> weights = new ArrayList<String>();
                for (String s : pacArray) {
                    weights.add(s);
                }

                ArrayAdapter<String> weightAdapter = new ArrayAdapter<String>(getContext(),
                        android.R.layout.simple_spinner_item, weights);

                weightAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                weightSpinner.setDropDownWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
                weightSpinner.setAdapter(weightAdapter);
                weightSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                        try {
                            if (prices[position].equals("NA")) {
                                priceLO.setVisibility(View.VISIBLE);
                                priceText.setText("--");
                            } else {
                                priceLO.setVisibility(View.VISIBLE);
                                priceText.setText(prices[position]);
                            }

                            for (int i = 0; i < row.size(); i++) {

                                HashMap<String, Object> roww = row.get(i);

                                int id = (int) roww.get("layout_id");

                                if (id == itemDialogLO.getId()) {
                                    roww.put("strength", strengthSpinner.getSelectedItem());
                                    roww.put("weight", weightSpinner.getSelectedItem());
                                    roww.put("quantity", quantityText.getText());
                                    if (prices[position].equals("NA")) {
                                        roww.put("price", proPrice);
                                    } else {
                                        roww.put("price", prices[position]);
                                    }
                                }
                            }
                        } catch (Exception e) {
                            Log.e("onItemSelected_weight", e.toString());
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

                HashMap<String, Object> rowData = new HashMap<String, Object>();

                rowData.put("layout_id", itemDialogLO.getId());
//                Log.e("rowLayoutId: ", String.valueOf(rowLayout.getId()));

                rowData.put("weight", weightSpinner.getSelectedItem());
                rowData.put("quantity", quantityText.getText());

                row.add(rowData);
                Log.e("rowData", String.valueOf(row));
            }

            quantityText.setText("1");

            increaseQuantity.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int quantity = Integer.parseInt(quantityText.
                            getText().toString());

                    quantity++;
                    quantityText.setText(String.valueOf(quantity));

                    for (int i = 0; i < row.size(); i++) {

                        HashMap<String, Object> roww = row.get(i);

                        int id = (int) roww.get("layout_id");

                        if (id == itemDialogLO.getId()) {
                            roww.put("strength", strengthSpinner.getSelectedItem());
                            roww.put("weight", weightSpinner.getSelectedItem());
                            roww.put("quantity", quantityText.getText());
                        }
                    }
                }
            });

            decreaseQuantity.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int quantity = Integer.parseInt(quantityText.
                            getText().toString());

                    if (quantity > 0) {
                        quantity--;
                    }
                    quantityText.setText(String.valueOf(quantity));

                    for (int i = 0; i < row.size(); i++) {

                        HashMap<String, Object> roww = row.get(i);

                        int id = (int) roww.get("layout_id");

                        if (id == itemDialogLO.getId()) {
                            roww.put("strength", strengthSpinner.getSelectedItem());
                            roww.put("weight", weightSpinner.getSelectedItem());
                            roww.put("quantity", quantityText.getText());
                        }
                    }
                }
            });

            RelativeLayout removeItemLO = (RelativeLayout) itemDialogLO.findViewById(R.id.remove_layout);
            removeItem = (ImageView) itemDialogLO.findViewById(R.id.dialog_item_remove);

            verifyDialogListSize();

            removeItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    int lastItem = row.size() - 1;

                    for (int i = 0; i < row.size(); i++) {
                        if (row.size() == 1) {
                            removeItem.setVisibility(View.GONE);
                        } else {
                            removeItem.setVisibility(View.VISIBLE);
                            HashMap<String, Object> roww = row.get(i);

                            int id = (int) roww.get("layout_id");

                            if (id == itemDialogLO.getId()) {
                                row.remove(i);

                                items.removeView(itemDialogLO);
                            }

                            verifyDialogListSize();
                        }
                    }
                }
            });

            items.addView(itemDialogLO);
        } catch (Exception e) {
            Log.e("createRow: ", e.toString());
        }
    }

    void verifyDialogListSize() {

        if (row.size() == 1) {
            removeItem.setVisibility(View.GONE);
        } else {
            removeItem.setVisibility(View.VISIBLE);
        }
    }
}
