package lazhora.ui.Screens;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutionException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Optional;
import lazhora.R;
import lazhora.ui.Screens.presenter.ProductDetailsPresenter;
import lazhora.ui.Screens.presenter.ProductDetailsPresenterImplementation;
import lazhora.ui.api.EnquireApi;
import lazhora.ui.data.Item;
import lazhora.ui.data.JsonDataCopy;
import lazhora.ui.db.DBManager;
import lazhora.ui.utils.CustomProgressDialog;
import lazhora.ui.utils.Font;
import lazhora.ui.utils.NetworkConnectionVerification;

import static android.content.ContentValues.TAG;

/**
 * Created by Arivu on 26-09-2016.
 */
public class ProductDetails extends AppCompatActivity implements
        ProductDetailsPresenterImplementation.ProductDetailsListener {

    /**
     * The Font.
     */
    Font font;

    /**
     * The Product title.
     */
//    @BindView(R.id.product_title)
    TextView productTitle;

    /**
     * The Product price.
     */
//    @BindView(R.id.product_price)
    TextView productPrice;
    // TextView productTag;
    // TextView productTags;

    /**
     * The Product desc title.
     */
//    @BindView(R.id.product_desc_title)
    TextView productDescTitle;

    /**
     * The Product desc.
     */
//    @BindView(R.id.product_desc)
    WebView productDesc;

    /**
     * The Appbar.
     */
//    @BindView(R.id.appbar)
    AppBarLayout appbar;

    /**
     * The Collapsing toolbar layout.
     */
//    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbarLayout;

    /**
     * The Product image.
     */
//    @BindView(R.id.imageView)
    ImageView productImage;
    /**
     * The Productimagename.
     */
//    @BindView(R.id.imageViewname)
    ImageView productimagename;
    /**
     * The Image pack.
     */
//    @BindView(R.id.imagePack)
    ImageView imagePack;

    private String proName;

    /**
     * The Taglayout.
     */
//    @BindView(R.id.taglayout)
    LinearLayout taglayout;

    private NetworkConnectionVerification ncv;

    /**
     * The Buying options.
     */
//    @BindView(R.id.buying_options)
    Button buyingOptions;

    /**
     * The Items.
     */
    LinearLayout items;

    /**
     * The Pro desc layout.
     */
//    @BindView(R.id.proDescLayout)
    LinearLayout proDescLayout;

    /**
     * The Pro desc layout 2.
     */
//    @BindView(R.id.proDescLayout2)
    LinearLayout proDescLayout2;
    /**
     * The Row.
     */
    List<HashMap<String, Object>> row = new ArrayList<HashMap<String, Object>>();
    /**
     * The Row 2.
     */
    List<HashMap<String, Object>> row2 = new ArrayList<HashMap<String, Object>>();

    /**
     * The Default content 2.
     */
//    @BindView(R.id.default_content2)
    LinearLayout default_content2;
    /**
     * The Dynamic content.
     */
    LinearLayout dynamicContent;

    /**
     * The Toolbarview.
     */
//    @BindView(R.id.toolbar)
    Toolbar toolbarview;

    private CustomProgressDialog progressBar;

    /**
     * The Remove item.
     */
    ImageView removeItem;

    /**
     * The Category.
     */
    String category = "";
    /**
     * The Res id.
     */
    int resID = 0;
    /**
     * The Quantity.
     */
    int quantity = 0;
    /**
     * The Pro price.
     */
    int proPrice;
    /**
     * The Prices.
     */
    String[] prices;

    /**
     * The Strengths.
     */
    StringBuilder strengths, /**
     * The Packs.
     */
    packs, /**
     * The Custom packs.
     */
    custom_packs, /**
     * The Sizes.
     */
    sizes;
    /**
     * The Relative layout.
     */
    RelativeLayout relativeLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);

//        ButterKnife.bind(this);

        productTitle = (TextView) findViewById(R.id.product_title);
        productPrice = (TextView) findViewById(R.id.product_price);

        productDesc = (WebView) findViewById(R.id.product_desc);
        productDescTitle = (TextView) findViewById(R.id.product_desc_title);

        appbar = (AppBarLayout) findViewById(R.id.appbar);
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);

        productImage = (ImageView) findViewById(R.id.imageView);
        productimagename = (ImageView) findViewById(R.id.imageViewname);
        imagePack = (ImageView) findViewById(R.id.imagePack);

        taglayout = (LinearLayout) findViewById(R.id.taglayout);
        buyingOptions = (Button) findViewById(R.id.buying_options);
        proDescLayout = (LinearLayout) findViewById(R.id.proDescLayout);
        proDescLayout2 = (LinearLayout) findViewById(R.id.proDescLayout2);

        default_content2 = (LinearLayout) findViewById(R.id.default_content2);

        toolbarview = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbarview);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        toolbarTextAppearance();

        font = new Font(ProductDetails.this);

        SpannableString pageTitle = new SpannableString(getSupportActionBar().getTitle());
        pageTitle.setSpan(font.robotoMedium, 0, pageTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        getSupportActionBar().setTitle(pageTitle);

        collapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(android.R.color.transparent)); //
        collapsingToolbarLayout.setCollapsedTitleTextColor(getResources().getColor(R.color.colorAccent)); //Color of your

        //enable JavaScript ***
        productDesc.getSettings().setJavaScriptEnabled(true);
        productDesc.getSettings().setLoadWithOverviewMode(true);

        String product_title = getIntent().getStringExtra("productname");
        Log.e("product_details", "onCreate: " + product_title);

        ncv = new NetworkConnectionVerification(ProductDetails.this);

        /*try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/

        new OnLoad().execute(product_title);

        try {
            if (product_title.contains("Mask")) {
                imagePack.setVisibility(View.VISIBLE);
            } else {
                imagePack.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            Log.e("onCreate: ", e.toString());
        }

        productPrice.setTypeface(font.robotoMedium);
        productDescTitle.setTypeface(font.robotoMedium);

    }

    private void toolbarTextAppearance() {
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.collapsedappbar);
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.expandedappbar);
        // colloapsing toolbar listener
        appbar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener
                () {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (verticalOffset == -collapsingToolbarLayout.getHeight() +
                        appBarLayout.getHeight()) {

                    collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppBarPlus1);

                    final Drawable upArrow = getResources().getDrawable(R.drawable.back_black);
                    getSupportActionBar().setHomeAsUpIndicator(upArrow);
                } else if (verticalOffset < -collapsingToolbarLayout.getHeight() +
                        appBarLayout.getHeight()) {

                    final Drawable upArrow = getResources().getDrawable(R.drawable.back_white);
                    getSupportActionBar().setHomeAsUpIndicator(upArrow);
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
//        Intent i = new Intent(ProductDetails.this, HomeActivity.class);
//        startActivity(i);
        row.clear();
        finish();
    }

    /**
     * On enquire.
     *
     * @param view the view
     */
    public void onEnquire(View view) {
        try {
            /*ProductDetailsPresenter presenter = new ProductDetailsPresenterImplementation(this);
            presenter.enquire(proName);*/

            if (ncv.isNetworkAvailable()) {
                new EnquireApi(ProductDetails.this).execute(proName);
            } else {
                Toast.makeText(ProductDetails.this, "Please check for internet connection.",
                        Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * The S.
     */
    final ArrayList<String> s = new ArrayList<String>();
    /**
     * The W.
     */
    final ArrayList<String> w = new ArrayList<String>();
    /**
     * The Q.
     */
    final ArrayList<Integer> q = new ArrayList<Integer>();

    /**
     * On buying options.
     *
     * @param view the view
     */
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void onBuyingOptions(View view) {

        String[] sArray = new String[0];
        String[] wArray = new String[0];
        int[] qArray = new int[0];

        String buyText = buyingOptions.getText().toString();

        if (buyText.equalsIgnoreCase("BUYING OPTIONS")) {

            buyingOptionDialog();

        } else if (buyText.equalsIgnoreCase("ADD TO CART")) {
            RelativeLayout relativeLayout = (RelativeLayout) View.inflate(this,
                    R.layout.layout_without_specification1, null);
            LinearLayout middleLayout = (LinearLayout) relativeLayout.findViewById(R.id.middleLayout);

            proDescLayout2.removeView(this.relativeLayout);
            proDescLayout2.addView(relativeLayout);

//            String string = "";
            String string = getData(getApplicationContext());
            Log.e("string", "content is... " + string);

//            Toast.makeText(ProductDetails.this, "Added to cart", Toast.LENGTH_SHORT).show();

            if (quantity != 0) {
                JSONArray resultSet;
                try {
                    if (string != null) {
                        resultSet = new JSONArray(string);
                    } else {
                        resultSet = new JSONArray();
                    }

                    JSONObject values = new JSONObject();

                    values.put("quantity", quantity);
                    values.put("price", proPrice);
                    int overallPrice = proPrice * quantity;
                    values.put("overall_price", overallPrice);
                    values.put("type", 3);
                    values.put("picture", resID);
                    values.put("category", category);
                    values.put("title", getSupportActionBar().getTitle());

                    resultSet.put(values);

                    Log.e("resultSet_myCart", String.valueOf(resultSet));

                    saveData(getApplicationContext(), resultSet.toString());
                    row.clear();
                } catch (JSONException e) {
                    Log.e("Exception", e.toString());
                }
            } else {

                ArrayList<String> sArr;
                ArrayList<String> wArr = null;

                if (category.equals("CHEMICAL PEEL")) {
                    sArr = new ArrayList<>();
                    wArr = new ArrayList<>();

                    for (String s1 : s) {
                        sArr.add(s1);
                    }

                    sArray = new String[sArr.size()];
                    for (int i = 0; i < sArray.length; i++) {
                        sArray[i] = sArr.get(i);
                        Log.e("sArray[i]", sArray[i]);
                    }

                    for (String w1 : w) {
                        wArr.add(w1);
                    }

                    wArray = new String[wArr.size()];
                    for (int i = 0; i < wArray.length; i++) {
                        wArray[i] = wArr.get(i);
                    }
                } else if (category.equals("SERUMS - SKIN CARE")
                        || category.equals("SERUMS - HAIR CARE")
                        || category.equals("CREAMS")
                        || category.equals("ROLLERS")) {
                    wArr = new ArrayList<>();

                    for (String w1 : w) {
                        wArr.add(w1);
                    }

                    wArray = new String[wArr.size()];
                    for (int i = 0; i < wArray.length; i++) {
                        wArray[i] = wArr.get(i);
                    }
                } /*else if (category.equals("Other")) {

                }*/

                qArray = new int[q.size()];
                for (int i = 0; i < qArray.length; i++) {
                    qArray[i] = q.get(i).intValue();
                }

                JSONArray resultSet;
                try {
                    if (string != null) {
                        resultSet = new JSONArray(string);
                    } else {
                        resultSet = new JSONArray();
                    }

                    JSONObject values = new JSONObject();

                    JSONArray content = new JSONArray();

                    int overallPrice = 0;

                    for (int i = 0; i < row.size(); i++) {
                        HashMap<String, Object> roww = row.get(i);

                        Log.e("strength()", String.valueOf(roww.get("strength")));
                        JSONObject contentValue = new JSONObject();

                        if (category.equals("CHEMICAL PEEL")) {
                            contentValue.put("strength", roww.get("strength"));
                            contentValue.put("weight", roww.get("weight"));
                            contentValue.put("quantity", roww.get("quantity"));
                            contentValue.put("price", proPrice);
                            overallPrice += proPrice;
                        } else if (category.equals("SERUMS - SKIN CARE")
                                || category.equals("SERUMS - HAIR CARE")
                                || category.equals("CREAMS")
                                || category.equals("ROLLERS")) {
                            contentValue.put("weight", roww.get("weight"));
                            contentValue.put("quantity", roww.get("quantity"));
                            contentValue.put("price", proPrice);
                            overallPrice += proPrice;
                        }
                        content.put(contentValue);
                    }
                    values.put("requirements", content);
                    values.put("overall_price", overallPrice);

                    if (category.equals("CHEMICAL PEEL")) {
                        values.put("type", 1);
                    } else if (category.equals("SERUMS - SKIN CARE")
                            || category.equals("SERUMS - HAIR CARE")
                            || category.equals("CREAMS")
                            || category.equals("ROLLERS")) {
                        values.put("type", 2);
                    }

                    values.put("picture", resID);
                    values.put("category", category);
                    values.put("title", getSupportActionBar().getTitle());

                    resultSet.put(values);

                    Log.e("resultSet", String.valueOf(resultSet));

                    saveData(getApplicationContext(), resultSet.toString());
                    row.clear();

                } catch (JSONException e) {
                    Log.e("Exception", e.toString());
                }
            }

            buyingOptions.setText("MY CART");
            buyingOptions.setCompoundDrawablesWithIntrinsicBounds(
                    getResources().getDrawable(R.drawable.cart_white),
                    null, null, null);
            buyingOptions.setCompoundDrawablePadding(4);
        } else if (buyText.equalsIgnoreCase("MY CART")) {

            getSupportActionBar().getTitle();
            Log.e("sArray", sArray.toString());
            startActivity(new Intent(ProductDetails.this, MyCart.class)/*.
                    putExtra("product_title", productTitle.getText()).
                    putExtra("strength", s).
                    putExtra("weight", wArray).
                    putExtra("quantity", q)*/);
            finish();
        }
    }

    /**
     * Buying option dialog.
     */
    public void buyingOptionDialog() {

        final Dialog dialog = new Dialog(this/*, android.R.style.Theme_Holo_Light_Dialog*/);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_buying_options);

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        final int screenWidth = (int) (metrics.widthPixels * 0.99);

        dialog.getWindow().setLayout(screenWidth, ViewGroup.LayoutParams.WRAP_CONTENT);

        //---------------
        ButterKnife.bind(this, dialog);
        items = (LinearLayout) dialog.findViewById(R.id.items);
        Button done = (Button) dialog.findViewById(R.id.done);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

                TextView selectedRequirements = (TextView) findViewById(R.id.selected_requirements);
                TextView change = (TextView) findViewById(R.id.change);

                LinearLayout default_content = (LinearLayout) findViewById(R.id.default_content);

                selectedRequirements.setText("Selected Requirements");
                selectedRequirements.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        null, null, null, null
                );
                change.setVisibility(View.VISIBLE);
                change.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dynamicContent.removeAllViews();

                        buyingOptionDialog();
                    }
                });

                default_content.setVisibility(View.GONE);

                LinearLayout defaultTitle = (LinearLayout) findViewById(R.id.default_title);
                defaultTitle.setVisibility(View.VISIBLE);

                TextView titleStrength = (TextView) defaultTitle.findViewById(R.id.title_strength);
                TextView titlePack = (TextView) defaultTitle.findViewById(R.id.title_pack);

                dynamicContent = (LinearLayout) findViewById(R.id.dynamic_content);
                dynamicContent.setVisibility(View.VISIBLE);

                for (int i = 0; i < row.size(); i++) {
                    HashMap<String, Object> roww = row.get(i);

                    Log.e("rowLayoutIdMap: ", String.valueOf(roww.get("layout_id")));
                    int id = (int) roww.get("layout_id");

                    LinearLayout linearLayout = (LinearLayout) View.inflate(ProductDetails.this,
                            R.layout.item_product_specification, null);

                    TextView strengthItem = (TextView) linearLayout.findViewById(R.id.item_strength);

                    if (category.equals("CHEMICAL PEEL")) {
                        titleStrength.setVisibility(View.VISIBLE);

                        strengthItem.setVisibility(View.VISIBLE);
                        String strength = roww.get("strength").toString();
                        strengthItem.setText(strength);

                        s.add(strength);
                        Log.e("s", String.valueOf(s));

                        TextView wealthItem = (TextView) linearLayout.findViewById(R.id.item_weight);
                        String weight = roww.get("weight").toString();
                        wealthItem.setText(weight);

                        w.add(weight);

                        TextView quantityItem = (TextView) linearLayout.findViewById(R.id.item_quantity);
                        String quantity = (String) roww.get("quantity");
                        quantityItem.setText(quantity);

                        q.add(Integer.valueOf(quantity));
                    } else if (category.equals("SERUMS - SKIN CARE")
                            || category.equals("SERUMS - HAIR CARE")
                            || category.equals("CREAMS")
                            || category.equals("ROLLERS")) {

                        titleStrength.setVisibility(View.GONE);

                        if (category.equals("ROLLERS")) {
                            titlePack.setText("Size");
                        } else {
                            titlePack.setText("Pack");
                        }

                        strengthItem.setVisibility(View.GONE);

                        TextView wealthItem = (TextView) linearLayout.findViewById(R.id.item_weight);
                        String weight = roww.get("weight").toString();
                        wealthItem.setText(weight);

                        w.add(weight);

                        TextView quantityItem = (TextView) linearLayout.findViewById(R.id.item_quantity);
                        String quantity = (String) roww.get("quantity");
                        quantityItem.setText(quantity);

                        q.add(Integer.valueOf(quantity));
                    }

                    dynamicContent.addView(linearLayout);
                }

                buyingOptions.setText("ADD TO CART");

                buyingOptions.setCompoundDrawablesWithIntrinsicBounds(
                        getResources().getDrawable(R.drawable.cart_add),
                        null, null, null);
                buyingOptions.setCompoundDrawablePadding(4);
            }
        });

        Button cancel = (Button) dialog.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                row.clear();

               /* buyingOptions.setText("BUYING OPTIONS");
                buyingOptions.setCompoundDrawablesWithIntrinsicBounds(
                        getResources().getDrawable(R.drawable.cart_white),
                        null, null, null);*/
            }
        });

        if (row.size() != 0) {
            Log.e("row_size", String.valueOf(row.size()));

            for (HashMap<String, Object> roww : row) {
                row2.add(roww);
            }
            Log.e("lRowBefore", String.valueOf(row2.size()));
            row.clear();
            Log.e("lRowAfter", String.valueOf(row2.size()));
            createRow(row2);
            addButtonView();

        } else {
            createRow();
            addButtonView();
        }

        dialog.show();
    }

    /**
     * Add button view.
     */
    public void addButtonView() {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                130, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.CENTER_HORIZONTAL;
        params.setMargins(0, 4, 0, 8);

        final TextView addButton = new TextView(this);
        addButton.setTag("dialog_item_add");
        addButton.setLayoutParams(params);
        addButton.setBackground(getResources().getDrawable(R.drawable.add_bg));
        addButton.setGravity(Gravity.CENTER);
        addButton.setPadding(8, 8, 8, 8);
        addButton.setText("ADD");
        addButton.setTextColor(getResources().getColor(R.color.colorPrimary));
        addButton.setTextSize(14);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addButton.setVisibility(View.GONE);
                Log.e("row_size@add", String.valueOf(row.size()));
                removeItem.setVisibility(View.VISIBLE);

                Log.e("createRow", String.valueOf(row));

                createRow();
                addButtonView();
            }
        });

        items.addView(addButton);
    }

    /**
     * Create row.
     *
     * @param rowItems the row items
     */
    public void createRow(final List<HashMap<String, Object>> rowItems) {

        int lastItem = rowItems.size() - 1;
        Log.e("lastItem", String.valueOf(lastItem));

        int curItem = 0;

        for (HashMap<String, Object> roww : rowItems) {
            final RelativeLayout itemDialogLO = (RelativeLayout) View.inflate(this,
                    R.layout.item_dialog, null);
            itemDialogLO.setId(View.generateViewId());

            LinearLayout topLO = (LinearLayout) itemDialogLO.findViewById(R.id.top);
            topLO.setId(View.generateViewId());

            LinearLayout strenghtLO = (LinearLayout) itemDialogLO.findViewById(R.id.strength_layout);
            LinearLayout weightLO = (LinearLayout) itemDialogLO.findViewById(R.id.weight_layout);
            LinearLayout quantityLO = (LinearLayout) itemDialogLO.findViewById(R.id.quantity_layout);
            final LinearLayout priceLO = (LinearLayout) itemDialogLO.findViewById(R.id.price_layout);

            final AppCompatSpinner strengthSpinner = (AppCompatSpinner) strenghtLO.findViewById(R.id.strength_spinner);
            final AppCompatSpinner weightSpinner = (AppCompatSpinner) weightLO.findViewById(R.id.weight_spinner);
            final TextView quantityText = (TextView) quantityLO.findViewById(R.id.quantity_text);
            final TextView priceText = (TextView) priceLO.findViewById(R.id.price_text);

            ImageView increaseQuantity = (ImageView) quantityLO.findViewById(R.id.increase);
            ImageView decreaseQuantity = (ImageView) quantityLO.findViewById(R.id.decrease);

            if (category.equals("CHEMICAL PEEL")) {
                strenghtLO.setVisibility(View.VISIBLE);
                weightLO.setVisibility(View.VISIBLE);
                priceLO.setVisibility(View.GONE);

                String[] strArray = this.strengths.toString().split(",");

                List<String> strengths = new ArrayList<String>();
                for (String s : strArray) {
                    strengths.add(s);
                }

                ArrayAdapter<String> strengthAdapter = new ArrayAdapter<String>(this,
                        android.R.layout.simple_spinner_item, strengths);

                strengthAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                strengthSpinner.setDropDownWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
                strengthSpinner.setAdapter(strengthAdapter);
                Log.e("change_strength", String.valueOf(roww.get("strength")));

                int i = 0;
                for (String strength : strengths) {
                    if (roww.get("strength").equals(strength)) {
                        int spinnerPosition = strengthAdapter.getPosition(strength);
                        Log.e("spinnerPosition", String.valueOf(spinnerPosition));
//                        weightSpinner.setPrompt(strengthAdapter.getItem(j));
                        weightSpinner.setSelection(spinnerPosition);
                        i++;
                    }
                }

                final List<String> weights = new ArrayList<String>();

                strengthSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                        weights.clear();

                        if (strengthSpinner.getSelectedItem().equals("Custom")) {
                            Log.e("custom_packs", custom_packs.toString());

                            if (custom_packs.toString().contains(",")) {
                                String[] pacArray = custom_packs.toString().split(",");

                                for (String s : pacArray) {
                                    weights.add(s);
                                }
                            } else {
                                weights.add(custom_packs.toString());
                            }
                        } else {

                            Log.e("packs", packs.toString());

                            if (packs.toString().contains(",")) {
                                String[] pacArray = packs.toString().split(",");

                                for (String s : pacArray) {
                                    weights.add(s);
                                }
                            } else {
                                weights.add(packs.toString());
                            }
                        }

                        for (int i = 0; i < row.size(); i++) {

                            HashMap<String, Object> roww = row.get(i);

                            int id = (int) roww.get("layout_id");

                            if (id == itemDialogLO.getId()) {
                                roww.put("strength", strengthSpinner.getSelectedItem());
                                roww.put("weight", weightSpinner.getSelectedItem());
                                roww.put("quantity", quantityText.getText());
                            }
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

                Log.e("packs", packs.toString());

                if (packs.toString().contains(",")) {
                    String[] pacArray = packs.toString().split(",");

                    for (String s : pacArray) {
                        weights.add(s);
                    }
                } else {
                    weights.add(packs.toString());
                }

                ArrayAdapter<String> weightAdapter = new ArrayAdapter<String>(this,
                        android.R.layout.simple_spinner_item, weights);

                weightAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                weightSpinner.setDropDownWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
                weightSpinner.setAdapter(weightAdapter);
                Log.e("change_weight: ", String.valueOf(roww.get("weight")));

                int j = 0;
                for (String weight : weights) {
                    if (roww.get("weight").equals(weight)) {
                        int spinnerPosition = strengthAdapter.getPosition(weight);
//                        weightSpinner.setPrompt(strengthAdapter.getItem(j));
                        weightSpinner.setSelection(spinnerPosition);
                        i++;
                    }
                }

                weightSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                        for (int i = 0; i < row.size(); i++) {

                            HashMap<String, Object> roww = row.get(i);

                            int id = (int) roww.get("layout_id");

                            if (id == itemDialogLO.getId()) {
                                roww.put("strength", strengthSpinner.getSelectedItem());
                                roww.put("weight", weightSpinner.getSelectedItem());
                                roww.put("quantity", quantityText.getText());
                            }
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

                HashMap<String, Object> rowData = new HashMap<String, Object>();

                rowData.put("layout_id", itemDialogLO.getId());
//                Log.e("rowLayoutId: ", String.valueOf(rowLayout.getId()));

                Log.e("onClick: ", strengthSpinner.getSelectedItem().toString());

                rowData.put("strength", strengthSpinner.getSelectedItem());
                rowData.put("weight", weightSpinner.getSelectedItem());
                rowData.put("quantity", quantityText.getText());

                row.add(rowData);
                Log.e("rowData", String.valueOf(row));

            } else if (category.equals("SERUMS - SKIN CARE")
                    || category.equals("SERUMS - HAIR CARE")
                    || category.equals("CREAMS")
                    || category.equals("OTHER")) {
                strenghtLO.setVisibility(View.GONE);
                weightLO.setVisibility(View.VISIBLE);
//              priceLO.setVisibility(View.VISIBLE);

                String[] pacArray = this.packs.toString().split(",");

                List<String> weights = new ArrayList<String>();
                for (String s : pacArray) {
                    weights.add(s);
                }

                ArrayAdapter<String> weightAdapter = new ArrayAdapter<String>(this,
                        android.R.layout.simple_spinner_item, weights);

                weightAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                weightSpinner.setDropDownWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
                weightSpinner.setAdapter(weightAdapter);
                weightSpinner.setPrompt((CharSequence) roww.get("weight"));
                weightSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {

                        if (prices[position].equals("NA")) {
                            priceLO.setVisibility(View.VISIBLE);
                            priceText.setText("--");
                        } else {
                            priceLO.setVisibility(View.VISIBLE);
                            priceText.setText(prices[position]);
                        }

                        for (int i = 0; i < row.size(); i++) {

                            HashMap<String, Object> roww = row.get(i);

                            int id = (int) roww.get("layout_id");

                            if (id == itemDialogLO.getId()) {
                                roww.put("strength", strengthSpinner.getSelectedItem());
                                roww.put("weight", weightSpinner.getSelectedItem());
                                roww.put("quantity", quantityText.getText());
                                if (prices[position].equals("NA")) {
                                    roww.put("price", proPrice);
                                } else {
                                    roww.put("price", prices[position]);
                                }
                            }
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

                HashMap<String, Object> rowData = new HashMap<String, Object>();

                rowData.put("layout_id", itemDialogLO.getId());
//                Log.e("rowLayoutId: ", String.valueOf(rowLayout.getId()));

                Log.e("onClick: ", weightSpinner.getSelectedItem().toString());

                rowData.put("weight", weightSpinner.getSelectedItem());
                rowData.put("quantity", quantityText.getText());

                row.add(rowData);
            } else if (category.equals("ROLLERS")) {
                strenghtLO.setVisibility(View.GONE);
                weightLO.setVisibility(View.VISIBLE);
//              priceLO.setVisibility(View.VISIBLE);

                String[] pacArray = this.sizes.toString().split(",");

                List<String> weights = new ArrayList<String>();
                for (String s : pacArray) {
                    weights.add(s);
                }

                ArrayAdapter<String> weightAdapter = new ArrayAdapter<String>(this,
                        android.R.layout.simple_spinner_item, weights);

                weightAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                weightSpinner.setDropDownWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
                weightSpinner.setAdapter(weightAdapter);
                weightSpinner.setPrompt((CharSequence) roww.get("weight"));
                weightSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {

                        if (prices[position].equals("NA")) {
                            priceLO.setVisibility(View.VISIBLE);
                            priceText.setText("--");
                        } else {
                            priceLO.setVisibility(View.VISIBLE);
                            priceText.setText(prices[position]);
                        }

                        for (int i = 0; i < row.size(); i++) {

                            HashMap<String, Object> roww = row.get(i);

                            int id = (int) roww.get("layout_id");

                            if (id == itemDialogLO.getId()) {
                                roww.put("strength", strengthSpinner.getSelectedItem());
                                roww.put("weight", weightSpinner.getSelectedItem());
                                roww.put("quantity", quantityText.getText());
                                if (prices[position].equals("NA")) {
                                    roww.put("price", proPrice);
                                } else {
                                    roww.put("price", prices[position]);
                                }
                            }
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

                HashMap<String, Object> rowData = new HashMap<String, Object>();

                rowData.put("layout_id", itemDialogLO.getId());
//                Log.e("rowLayoutId: ", String.valueOf(rowLayout.getId()));

                Log.e("onClick: ", weightSpinner.getSelectedItem().toString());

                rowData.put("weight", weightSpinner.getSelectedItem());
                rowData.put("quantity", quantityText.getText());

                row.add(rowData);
            }

            quantityText.setText((CharSequence) roww.get("quantity"));

            increaseQuantity.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int quantity = Integer.parseInt(quantityText.
                            getText().toString());

                    quantity++;
                    quantityText.setText(String.valueOf(quantity));

                    for (int i = 0; i < row.size(); i++) {

                        HashMap<String, Object> roww = row.get(i);

                        int id = (int) roww.get("layout_id");

                        if (id == itemDialogLO.getId()) {
                            roww.put("strength", strengthSpinner.getSelectedItem());
                            roww.put("weight", weightSpinner.getSelectedItem());
                            roww.put("quantity", quantityText.getText());
                        }
                    }
                }
            });

            decreaseQuantity.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int quantity = Integer.parseInt(quantityText.
                            getText().toString());

                    if (quantity > 0) {
                        quantity--;
                    }
                    quantityText.setText(String.valueOf(quantity));

                    for (int i = 0; i < row.size(); i++) {

                        HashMap<String, Object> roww = row.get(i);

                        int id = (int) roww.get("layout_id");

                        if (id == itemDialogLO.getId()) {
                            roww.put("strength", strengthSpinner.getSelectedItem());
                            roww.put("weight", weightSpinner.getSelectedItem());
                            roww.put("quantity", quantityText.getText());
                        }
                    }
                }
            });

//            addItem = (Button) itemDialogLO.findViewById(R.id.dialog_item_add);
            RelativeLayout removeItemLO = (RelativeLayout) itemDialogLO.findViewById(R.id.remove_layout);
            removeItem = (ImageView) itemDialogLO.findViewById(R.id.dialog_item_remove);

            verifyDialogListSize();

            if (curItem == lastItem) {
                Log.e("curItem", String.valueOf(curItem));

                /*addItem.setVisibility(View.VISIBLE);
                addItem.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.e("row_size@add", String.valueOf(row.size()));
                        Log.e("layoutID@add", String.valueOf(itemDialogLO.getId()));
                        addItem.setVisibility(View.GONE);
                        removeItem.setVisibility(View.VISIBLE);

                *//*if (row.size() == 1) {
                    removeItem.setVisibility(View.VISIBLE);
                } else {
                    removeItem.setVisibility(View.VISIBLE);
                }*//*

                        Log.e("createRow ", String.valueOf(row));

                        createRow();
                    }
                });*/
            } else {
                Log.e("curItem", String.valueOf(curItem));
                removeItem.setVisibility(View.VISIBLE);
                curItem++;
            }

            removeItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    for (int i = 0; i < row.size(); i++) {
                        if (row.size() == 1) {
                            removeItem.setVisibility(View.GONE);
                        } else {
                            removeItem.setVisibility(View.VISIBLE);
                            HashMap<String, Object> roww = row.get(i);

                            Log.e("rowLayoutIdMap: ", String.valueOf(roww.get("layout_id")));
                            int id = (int) roww.get("layout_id");

                            if (id == itemDialogLO.getId()) {
                                row.remove(i);

                                items.removeView(itemDialogLO);
                            }

                            verifyDialogListSize();
                        }
                    }
                    Log.e("row_size@remove", String.valueOf(row.size()));
                }
            });

            items.addView(itemDialogLO);

            /*LinearLayout.LayoutParams addParams = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
            );

            Button add = new Button(this);
            add.setLayoutParams(addParams);*/
        }
    }

    /**
     * Create row.
     */
    public void createRow() {
        final RelativeLayout itemDialogLO = (RelativeLayout) View.inflate(this,
                R.layout.item_dialog, null);
        itemDialogLO.setId(View.generateViewId());

        LinearLayout topLO = (LinearLayout) itemDialogLO.findViewById(R.id.top);
        topLO.setId(View.generateViewId());

        LinearLayout strenghtLO = (LinearLayout) itemDialogLO.findViewById(R.id.strength_layout);
        LinearLayout weightLO = (LinearLayout) itemDialogLO.findViewById(R.id.weight_layout);
        LinearLayout quantityLO = (LinearLayout) itemDialogLO.findViewById(R.id.quantity_layout);
        final LinearLayout priceLO = (LinearLayout) itemDialogLO.findViewById(R.id.price_layout);

        final AppCompatSpinner strengthSpinner = (AppCompatSpinner) strenghtLO.findViewById(R.id.strength_spinner);
        final AppCompatSpinner weightSpinner = (AppCompatSpinner) weightLO.findViewById(R.id.weight_spinner);
        final TextView quantityText = (TextView) quantityLO.findViewById(R.id.quantity_text);
        final TextView priceText = (TextView) priceLO.findViewById(R.id.price_text);

        ImageView increaseQuantity = (ImageView) quantityLO.findViewById(R.id.increase);
        ImageView decreaseQuantity = (ImageView) quantityLO.findViewById(R.id.decrease);

        if (category.equals("CHEMICAL PEEL")) {
            strenghtLO.setVisibility(View.VISIBLE);
            weightLO.setVisibility(View.VISIBLE);
            priceLO.setVisibility(View.GONE);

            String[] strArray = this.strengths.toString().split(",");

            List<String> strengths = new ArrayList<String>();
            for (String s : strArray) {
                strengths.add(s);
            }

            ArrayAdapter<String> strengthAdapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_spinner_item, strengths);

            strengthAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            strengthSpinner.setDropDownWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
            strengthSpinner.setAdapter(strengthAdapter);

            final List<String> weights = new ArrayList<String>();

            strengthSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                    weights.clear();

                    if (strengthSpinner.getSelectedItem().equals("Custom")) {
                        Log.e("custom_packs", custom_packs.toString());

                        if (custom_packs.toString().contains(",")) {
                            String[] pacArray = custom_packs.toString().split(",");

                            for (String s : pacArray) {
                                weights.add(s);
                            }
                        } else {
                            weights.add(custom_packs.toString());
                        }
                    } else {

                        Log.e("packs", packs.toString());

                        if (packs.toString().contains(",")) {
                            String[] pacArray = packs.toString().split(",");

                            for (String s : pacArray) {
                                weights.add(s);
                            }
                        } else {
                            weights.add(packs.toString());
                        }
                    }

                    for (int i = 0; i < row.size(); i++) {

                        HashMap<String, Object> roww = row.get(i);

                        int id = (int) roww.get("layout_id");

                        if (id == itemDialogLO.getId()) {
                            roww.put("strength", adapterView.getItemAtPosition(position));
                            roww.put("weight", weightSpinner.getSelectedItem());
                            roww.put("quantity", quantityText.getText());
                        }
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            Log.e("packs", packs.toString());

            if (packs.toString().contains(",")) {
                String[] pacArray = packs.toString().split(",");

                for (String s : pacArray) {
                    weights.add(s);
                }
            } else {
                weights.add(packs.toString());
            }

            final ArrayAdapter<String> weightAdapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_spinner_item, weights);

            weightAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            weightSpinner.setDropDownWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
            weightSpinner.setAdapter(weightAdapter);
//            weightSpinner.setPrompt(weightAdapter.getItem(0));
            weightSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                    weightSpinner.setPrompt((CharSequence) weightSpinner.getSelectedItem());

                    for (int i = 0; i < row.size(); i++) {

                        HashMap<String, Object> roww = row.get(i);

                        int id = (int) roww.get("layout_id");

                        if (id == itemDialogLO.getId()) {
                            roww.put("strength", strengthSpinner.getSelectedItem());
                            roww.put("weight", adapterView.getItemAtPosition(position));
                            roww.put("quantity", quantityText.getText());
                        }
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            HashMap<String, Object> rowData = new HashMap<String, Object>();

            rowData.put("layout_id", itemDialogLO.getId());
//                Log.e("rowLayoutId: ", String.valueOf(rowLayout.getId()));

            Log.e("onClick: ", strengthSpinner.getSelectedItem().toString());

            rowData.put("strength", strengthSpinner.getSelectedItem());
            rowData.put("weight", weightSpinner.getSelectedItem());
            rowData.put("quantity", quantityText.getText());

            row.add(rowData);
            Log.e("rowData", String.valueOf(row));

        } else if (category.equals("SERUMS - SKIN CARE")
                || category.equals("SERUMS - HAIR CARE")
                || category.equals("CREAMS")) {
            strenghtLO.setVisibility(View.GONE);
            weightLO.setVisibility(View.VISIBLE);

            String[] pacArray = this.packs.toString().split(",");

            List<String> weights = new ArrayList<String>();
            for (String s : pacArray) {
                weights.add(s);
            }

            ArrayAdapter<String> weightAdapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_spinner_item, weights);

            weightAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            weightSpinner.setDropDownWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
            weightSpinner.setAdapter(weightAdapter);
            weightSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {

                    if (prices[position].equals("NA")) {
                        priceLO.setVisibility(View.VISIBLE);
                        priceText.setText("--");
                    } else {
                        priceLO.setVisibility(View.VISIBLE);
                        priceText.setText(prices[position]);
                    }

                    for (int i = 0; i < row.size(); i++) {

                        HashMap<String, Object> roww = row.get(i);

                        int id = (int) roww.get("layout_id");

                        if (id == itemDialogLO.getId()) {
                            roww.put("strength", strengthSpinner.getSelectedItem());
                            roww.put("weight", weightSpinner.getSelectedItem());
                            roww.put("quantity", quantityText.getText());

                            if (prices[position].equals("NA")) {
                                roww.put("price", proPrice);
                            } else {
                                Log.e("priceTag", prices[position]);
                                roww.put("price", prices[position]);
                            }
                        }
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            HashMap<String, Object> rowData = new HashMap<String, Object>();

            rowData.put("layout_id", itemDialogLO.getId());
//                Log.e("rowLayoutId: ", String.valueOf(rowLayout.getId()));

            Log.e("onClick: ", weightSpinner.getSelectedItem().toString());

            rowData.put("weight", weightSpinner.getSelectedItem());
            rowData.put("quantity", quantityText.getText());

            row.add(rowData);
        } else if (category.equals("ROLLERS")) {
            strenghtLO.setVisibility(View.GONE);
            weightLO.setVisibility(View.VISIBLE);
//              priceLO.setVisibility(View.VISIBLE);

            String[] pacArray = this.sizes.toString().split(",");

            List<String> weights = new ArrayList<String>();
            for (String s : pacArray) {
                weights.add(s);
            }

            ArrayAdapter<String> weightAdapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_spinner_item, weights);

            weightAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            weightSpinner.setDropDownWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
            weightSpinner.setAdapter(weightAdapter);
            weightSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                    try {
                        if (prices[position].equals("NA")) {
                            priceLO.setVisibility(View.VISIBLE);
                            priceText.setText("--");
                        } else {
                            priceLO.setVisibility(View.VISIBLE);
                            priceText.setText(prices[position]);
                        }

                        for (int i = 0; i < row.size(); i++) {

                            HashMap<String, Object> roww = row.get(i);

                            int id = (int) roww.get("layout_id");

                            if (id == itemDialogLO.getId()) {
                                roww.put("strength", strengthSpinner.getSelectedItem());
                                roww.put("weight", weightSpinner.getSelectedItem());
                                roww.put("quantity", quantityText.getText());
                                if (prices[position].equals("NA")) {
                                    roww.put("price", proPrice);
                                } else {
                                    roww.put("price", prices[position]);
                                }
                            }
                        }
                    } catch (Exception e) {
                        Log.e("onItemSelected_weight", e.toString());
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            HashMap<String, Object> rowData = new HashMap<String, Object>();

            rowData.put("layout_id", itemDialogLO.getId());
//                Log.e("rowLayoutId: ", String.valueOf(rowLayout.getId()));

            Log.e("onClick: ", weightSpinner.getSelectedItem().toString());

            rowData.put("weight", weightSpinner.getSelectedItem());
            rowData.put("quantity", quantityText.getText());

            row.add(rowData);
        }

        quantityText.setText("1");

        increaseQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int quantity = Integer.parseInt(quantityText.
                        getText().toString());

                quantity++;
                quantityText.setText(String.valueOf(quantity));

                for (int i = 0; i < row.size(); i++) {

                    HashMap<String, Object> roww = row.get(i);

                    int id = (int) roww.get("layout_id");

                    if (id == itemDialogLO.getId()) {
                        roww.put("strength", strengthSpinner.getSelectedItem());
                        roww.put("weight", weightSpinner.getSelectedItem());
                        roww.put("quantity", quantityText.getText());
                    }
                }
            }
        });

        decreaseQuantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int quantity = Integer.parseInt(quantityText.
                        getText().toString());

                if (quantity > 0) {
                    quantity--;
                }
                quantityText.setText(String.valueOf(quantity));

                for (int i = 0; i < row.size(); i++) {

                    HashMap<String, Object> roww = row.get(i);

                    int id = (int) roww.get("layout_id");

                    if (id == itemDialogLO.getId()) {
                        roww.put("strength", strengthSpinner.getSelectedItem());
                        roww.put("weight", weightSpinner.getSelectedItem());
                        roww.put("quantity", quantityText.getText());
                    }
                }
            }
        });

//        addItem = (Button) itemDialogLO.findViewById(R.id.dialog_item_add);
        RelativeLayout removeItemLO = (RelativeLayout) itemDialogLO.findViewById(R.id.remove_layout);
        removeItem = (ImageView) itemDialogLO.findViewById(R.id.dialog_item_remove);

        verifyDialogListSize();

        /*addItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("row_size@add", String.valueOf(row.size()));
                Log.e("layoutID@add", String.valueOf(itemDialogLO.getId()));
                removeItem.setVisibility(View.VISIBLE);

                *//*if (row.size() == 1) {
                    removeItem.setVisibility(View.VISIBLE);
                } else {
                    removeItem.setVisibility(View.VISIBLE);
                }*//*

                Log.e("createRow ", String.valueOf(row));

                createRow();
            }
        });*/

        removeItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int lastItem = row.size() - 1;

                for (int i = 0; i < row.size(); i++) {
                    if (row.size() == 1) {
                        removeItem.setVisibility(View.GONE);
                    } else {
                        removeItem.setVisibility(View.VISIBLE);
                        HashMap<String, Object> roww = row.get(i);

                        Log.e("rowLayoutIdMap: ", String.valueOf(roww.get("layout_id")));
                        int id = (int) roww.get("layout_id");

                        if (id == itemDialogLO.getId()) {
                            row.remove(i);

                            items.removeView(itemDialogLO);
                        }

                        verifyDialogListSize();
                    }
                }
                Log.e("row_size@remove", String.valueOf(row.size()));
            }
        });

        items.addView(itemDialogLO);
    }

    /**
     * The File name.
     */
    static String fileName = "mycart.json";

    /**
     * Save data.
     *
     * @param context       the context
     * @param mJsonResponse the m json response
     */
    public static void saveData(Context context, String mJsonResponse) {
        try {
            FileWriter file = new FileWriter(context.getFilesDir().getPath() + "/" + fileName);
            file.write(mJsonResponse);
            file.flush();
            file.close();
            Log.e("Write Success", String.valueOf(file));

        } catch (IOException e) {
            Log.e("Error in Writing", e.getLocalizedMessage());
        }
    }

    /**
     * Gets data.
     *
     * @param context the context
     * @return the data
     */
    public static String getData(Context context) {
        try {
            File f = new File(context.getFilesDir().getPath() + "/" + fileName);
            //check whether file exists
            FileInputStream is = new FileInputStream(f);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();

            Log.e("Read Success", new String(buffer, "UTF-8"));
            return new String(buffer, "UTF-8");
        } catch (IOException e) {
            Log.e("File not exist", e.getLocalizedMessage());
            return null;
        }
    }

    /**
     * Verify dialog list size.
     */
    void verifyDialogListSize() {
        Log.e("row_size", String.valueOf(row.size()));

        if (row.size() == 1) {
            removeItem.setVisibility(View.GONE);
        } else {
            removeItem.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Gets screen width.
     *
     * @param activity the activity
     * @return the screen width
     */
    public static int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    }


    @Override
    public void success(JSONObject json) {
        showAlertDialog("Your feedback has been submitted");

    }

    @Override
    public void failure() {
        Toast.makeText(this, "Network Error. Please try again", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void showProgress() {
        progressBar = new CustomProgressDialog(this, "Your request is being sent...");

    }

    @Override
    public void hideProgress() {
        progressBar.dismiss();
    }

    /**
     * The type On load.
     */
    class OnLoad extends AsyncTask<String, String, String> {

        /**
         * The Product details.
         */
        Item product_details;
        /**
         * The Pdialog.
         */
        CustomProgressDialog pdialog;

        /**
         * The Tags.
         */
        String[] tags;

        @Override
        protected void onPreExecute() {

            pdialog = new CustomProgressDialog(ProductDetails.this, "Fetching " +
                    "Details...");

            pdialog.show();

            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                DBManager db = new DBManager(ProductDetails.this);
                db.openDataBase();
                product_details = db.getProductDetails(params[0]);
                Log.e(TAG, "doInBackground: " + product_details);
                db.close();

                category = product_details.category;

                Log.e("product_category", "category is... " + product_details.category);
                Log.e("product_price", "value is... " + product_details.price);

                if (product_details.price.contains(",")) {
                    prices = product_details.price.split(",");
                } else if (product_details.price.equals("NA")) {
                    proPrice = 0;
                } else {
                    proPrice = Integer.parseInt(product_details.price);
                }

                Log.e("proNameOnPost", "name is... " + product_details.name);
                proName = product_details.name;
                Log.e("proNameOnPost1", "name is... " + proName);

                // productDesc.setText(product_details.desc);
                // productTags.setText(product_details.tag);
                Log.e("tags", product_details.tag);
                tags = product_details.tag.split(",");
                Log.e("tags", String.valueOf(tags));

            } catch (Exception e) {
                Log.e("doInBackground", e.toString());
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            pdialog.dismiss();
            super.onPostExecute(s);
            productTitle.setText("");
            //      productDesc.setText(product_details.desc);
            try {
                String pic = product_details.imageid;
                resID = getResources().getIdentifier(pic.substring(0, pic.lastIndexOf(".")),
                        "drawable", getPackageName());
                Log.e("resID_ProductDetails", String.valueOf(resID));
                productImage.setImageDrawable(getResources().getDrawable(resID));

            } catch (Exception e) {
                Log.e("imageid", e.toString());
            }
            try {

                String pic = product_details.pic_name;
                int res_ID = getResources().getIdentifier(pic.substring(0, pic.lastIndexOf(".")),
                        "drawable", getPackageName());
                productimagename.setImageDrawable(getResources().getDrawable(res_ID));

            } catch (Exception e) {
                Log.e("pic_name", e.toString());
            }

            Log.e("opeCategory: ", category);
            Log.e("opeproName", "name is ... " + proName);

            if (category.equals("CHEMICAL PEEL")) {
                Log.e("ope_proName", "name is ... " + proName);
                Log.e("ope_category", "category is ... " + category);

                try {
                    JSONObject products = new JSONObject(JsonDataCopy.jsondata);
                    JSONArray productsjsonarray = products.getJSONArray("data");
                    for (int i = 0; i < productsjsonarray.length(); i++) {
                        JSONObject jobj = productsjsonarray.getJSONObject(i);

                        if (jobj.getString("product_title").equals(proName)) {
                            Log.e("ope_product_title", jobj.getString("product_title"));

                            Object strengthObj = jobj.get("strength");

                            if (strengthObj instanceof String) {
                                strengths = new StringBuilder();

                                strengths.append(strengthObj);
                            } else if (strengthObj instanceof JSONArray) {
                                JSONArray strength = (JSONArray) strengthObj;

                                strengths = new StringBuilder();

                                for (int j = 0; j < strength.length(); j++) {
                                    Log.e("strength_items", String.valueOf(strength.get(j)));
                                    if (j == 0) {
                                        strengths.append(strength.get(j).toString());
                                    } else {
                                        strengths.append("," + strength.get(j).toString());
                                    }
                                }
                            }

                            Object packObj = jobj.get("pack");

                            if (packObj instanceof String) {
                                packs = new StringBuilder();

                                packs.append(packObj);
                            } else if (packObj instanceof JSONArray) {
                                JSONArray pack = (JSONArray) packObj;

                                packs = new StringBuilder();

                                for (int j = 0; j < pack.length(); j++) {
                                    if (j == 0) {
                                        packs.append(pack.get(j).toString());
                                    } else {
                                        packs.append("," + pack.get(j).toString());
                                    }
                                }
                            }

                            Log.e("ope_strengths&packs", strengths + " ; " + packs);

                            if (strengths.toString().equals("NA") && packs.toString().equals("NA")) {
                                layoutWithoutSpecification();
                            } else {
                                relativeLayout = (RelativeLayout) View.inflate(ProductDetails.this,
                                        R.layout.layout_with_specification, null);
                                LinearLayout middleLayout = (LinearLayout) relativeLayout.findViewById(R.id.middleLayout);

                                proDescLayout2.addView(relativeLayout);

                                buyingOptions.setText("BUYING OPTIONS");
                            }

                            Object cusPackObj = jobj.get("custom_pack");

                            if (cusPackObj instanceof JSONArray) {
                                JSONArray custom_pack = (JSONArray) cusPackObj;

                                custom_packs = new StringBuilder();

                                for (int j = 0; j < custom_pack.length(); j++) {
                                    if (j == 0) {
                                        custom_packs.append(custom_pack.get(j).toString());
                                    } else {
                                        custom_packs.append("," + custom_pack.get(j).toString());
                                    }
                                }
                            }
                        }
                    }
                } catch (JSONException e) {
                    Log.e("chemical_peel excep", e.toString());
                }
            } else if (category.equals("SERUMS - SKIN CARE") || category.equals("SERUMS - HAIR CARE")) {
                Log.e("ope_category", category);

                try {
                    JSONObject products = new JSONObject(JsonDataCopy.jsondata);
                    JSONArray productsjsonarray = products.getJSONArray("data");
                    for (int i = 0; i < productsjsonarray.length(); i++) {
                        JSONObject jobj = productsjsonarray.getJSONObject(i);

                        if (jobj.getString("product_title").equals(proName)) {
                            Log.e("ope_product_title", jobj.getString("product_title"));
                            Object pack = jobj.get("pack");

                            if (pack instanceof String) {
                                layoutWithoutSpecification();
                            } else if (pack instanceof JSONArray) {

                                relativeLayout = (RelativeLayout) View.inflate(ProductDetails.this,
                                        R.layout.layout_with_specification, null);
                                proDescLayout2.addView(relativeLayout);

                                buyingOptions.setText("BUYING OPTIONS");

                                packs = new StringBuilder();

                                JSONArray weight = (JSONArray) pack;

                                for (int j = 0; j < weight.length(); j++) {
                                    if (j == 0) {
                                        packs.append(weight.get(j).toString());
                                    } else {
                                        packs.append("," + weight.get(j).toString());
                                    }
                                }
                            }
                        }
                    }
                } catch (JSONException e) {
                    Log.e("skin & hair excep", e.toString());
                }
            } else if (category.equals("CREAMS") || category.equals("OTHER")) {
                Log.e("ope_category", category);
                try {
                    JSONObject products = new JSONObject(JsonDataCopy.jsondata);
                    JSONArray productsjsonarray = products.getJSONArray("data");
                    for (int i = 0; i < productsjsonarray.length(); i++) {
                        JSONObject jobj = productsjsonarray.getJSONObject(i);

//                        Log.e("proTitle", jobj.getString("product_title"));
                        if (jobj.getString("product_title").equals(proName)) {
                            Log.e("proTitle", jobj.getString("product_title"));

                            Object pack = jobj.get("pack");

                            if (!pack.toString().equals("NA")) {
                                if (pack instanceof String) {
                                    layoutWithoutSpecification5();
                                } else if (pack instanceof JSONArray) {

                                    relativeLayout = (RelativeLayout) View.inflate(ProductDetails.this,
                                            R.layout.layout_with_specification, null);
                                    proDescLayout2.addView(relativeLayout);

                                    buyingOptions.setText("BUYING OPTIONS");

                                    packs = new StringBuilder();

                                    JSONArray weight = (JSONArray) pack;

                                    for (int j = 0; j < weight.length(); j++) {
                                        if (j == 0) {
                                            packs.append(weight.get(j).toString());
                                        } else {
                                            packs.append("," + weight.get(j).toString());
                                        }
                                    }
                                }
                            } else {
                                layoutWithoutSpecification();
                            }
                        }
                    }
                } catch (JSONException e) {
                    Log.e("creams and others excep", e.toString());
                }
            } else if (category.equals("ROLLERS")) {
                Log.e("ope_category", category);
                try {
                    JSONObject products = new JSONObject(JsonDataCopy.jsondata);
                    JSONArray productsjsonarray = products.getJSONArray("data");
                    for (int i = 0; i < productsjsonarray.length(); i++) {
                        JSONObject jobj = productsjsonarray.getJSONObject(i);

                        if (jobj.getString("product_title").equals(proName)) {

                            Object sizeObj = jobj.get("sizes");

                            if (!sizeObj.toString().equals("NA")) {
                                if (sizeObj instanceof String) {
                                    layoutWithoutSpecification();

                                    sizes = new StringBuilder();
                                    sizes.append(sizeObj);
                                } else if (sizeObj instanceof JSONArray) {
                                    JSONArray size = (JSONArray) sizeObj;

                                    relativeLayout = (RelativeLayout) View.inflate(ProductDetails.this,
                                            R.layout.layout_with_specification, null);
                                    LinearLayout middleLayout = (LinearLayout) relativeLayout.findViewById(R.id.middleLayout);
                                    proDescLayout2.addView(relativeLayout);

                                    buyingOptions.setText("BUYING OPTIONS");

                                    sizes = new StringBuilder();

                                    for (int j = 0; j < size.length(); j++) {
                                        if (j == 0) {
                                            sizes.append(size.get(j).toString());
                                        } else {
                                            sizes.append("," + size.get(j).toString());
                                        }
                                    }
                                }
                            } else {
                                layoutWithoutSpecification();
                            }
                        }
                    }
                } catch (JSONException e) {
                    Log.e("rollers excep", e.toString());
                }
            } else if (category.equals("MASK")) {
                Log.e("ope_category", category);
                layoutWithoutSpecification();
            }

            //productPrice.setText(product_details.price);
            //productDesc.setText(Html.fromHtml(product_details.desc));
            productDesc.clearView();
            try {
                productDesc.loadDataWithBaseURL(null, product_details.desc, "text/html", "utf-8", null);
            } catch (Exception e) {
                e.printStackTrace();
            }
            productDesc.reload();

            try {
                productDesc.loadData(product_details.desc, "text/html; charset=utf-8", "UTF-8");
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                for (String tag : tags) {
                    Log.e("tag", tag);
                    TextView t = new TextView(getApplicationContext());
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        t.setBackground(getResources().getDrawable(R.drawable.tag_bg));
                        //t.setTextAppearance(Typeface.ITALIC);
                    }
                    LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.WRAP_CONTENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT);
                    llp.setMargins(16, 0, 0, 0); // llp.setMargins(left, top, right, bottom);
                    t.setLayoutParams(llp);
                    t.setAlpha((float) .87);
                    t.setText(tag.trim());
                    t.setTextColor(Color.WHITE);
                    t.setTextSize(14);

                    //t.setTextAppearance(Typeface.ITALIC);

                    taglayout.addView(t);
                }
            } catch (Exception e) {
                Log.e("onPostExecute", e.toString());
            }

            Resources res = getResources();
            proName = String.format(res.getString(R.string.title_product), product_details.name).trim();

            getSupportActionBar().setTitle(proName);
            collapsingToolbarLayout.setTitle(proName);


        }
    }

    /**
     * Layout without specification.
     */
    void layoutWithoutSpecification() {
        relativeLayout = (RelativeLayout) View.inflate(ProductDetails.this,
                R.layout.layout_without_specifications, null);

        LinearLayout topLO = (LinearLayout) relativeLayout.findViewById(R.id.top);
        TextView priceText = (TextView) topLO.findViewById(R.id.price_text);
        if (proPrice == 0) {
            priceText.setText("--");
        } else {
            priceText.setText(Integer.toString(proPrice));
        }

        ImageView increase = (ImageView) relativeLayout.findViewById(R.id.increase);
        ImageView decrease = (ImageView) relativeLayout.findViewById(R.id.decrease);
        final TextView quantityText = (TextView) relativeLayout.findViewById(R.id.quantity);

        quantityText.setText("1");

        quantity = Integer.parseInt(quantityText.getText().toString());

        increase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                quantity = Integer.parseInt(quantityText.getText().toString());

                quantity++;
                quantityText.setAlpha((float) .99);
                quantityText.setText(String.valueOf(quantity));

            }
        });

        decrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                quantity = Integer.parseInt(quantityText.getText().toString());

                if (quantity != 1) {
                    quantity--;
                    if (quantity == 1) {
                        quantityText.setAlpha((float) .54);
                    }
                    quantityText.setText(String.valueOf(quantity));
                }
            }
        });

        proDescLayout2.addView(relativeLayout);
        buyingOptions.setText("ADD TO CART");
    }

    /**
     * Layout without specification 5.
     */
    void layoutWithoutSpecification5() {
        relativeLayout = (RelativeLayout) View.inflate(ProductDetails.this,
                R.layout.layout_without_specifications, null);

        LinearLayout topLO = (LinearLayout) relativeLayout.findViewById(R.id.top);
        TextView priceText = (TextView) topLO.findViewById(R.id.price_text);
        if (proPrice == 0) {
            priceText.setText("--");
        } else {
            priceText.setText(Integer.toString(proPrice));
        }

        ImageView increase = (ImageView) relativeLayout.findViewById(R.id.increase);
        final ImageView decrease = (ImageView) relativeLayout.findViewById(R.id.decrease);
        final TextView quantityText = (TextView) relativeLayout.findViewById(R.id.quantity);

        decrease.setAlpha((float) .54);
        quantityText.setAlpha((float) .54);
        decrease.setClickable(false);
        quantityText.setText("5");

        quantity = Integer.parseInt(quantityText.getText().toString());

        increase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                quantity = Integer.parseInt(quantityText.getText().toString());

                quantity++;
                quantityText.setAlpha((float) .99);
                quantityText.setText(String.valueOf(quantity));
                decrease.setAlpha((float) .99);
                decrease.setClickable(true);
            }
        });

        decrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                quantity = Integer.parseInt(quantityText.getText().toString());

                if (quantity > 5) {
                    quantity--;
                    if (quantity == 5) {
                        decrease.setAlpha((float) .54);
                        quantityText.setAlpha((float) .54);
                        decrease.setClickable(false);
                    }
                    quantityText.setText(String.valueOf(quantity));
                }
            }
        });

        proDescLayout2.addView(relativeLayout);
        buyingOptions.setText("ADD TO CART");
    }

    /**
     * Show alert dialog.
     *
     * @param message the message
     */
    public void showAlertDialog(String message) {
        android.support.v7.app.AlertDialog alert = null;
        final android.support.v7.app.AlertDialog.Builder alertDialog = new android.support.v7.app.AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();

        View dialogView = inflater.inflate(R.layout.gift_layout, null);
        alertDialog.setView(dialogView);
        alert = alertDialog.create();

        TextView title = (TextView) dialogView.findViewById(R.id.dialogtitle);
        TextView msg = (TextView) dialogView.findViewById(R.id.dialogmessage);

        ImageView gift = (ImageView) dialogView.findViewById(R.id.gift);
        gift.setVisibility(View.GONE);

        title.setText("Thanks for your interest!");
        title.setTypeface(font.robotoMedium);
        title.setAlpha((float) .87);
        msg.setText("We will get in touch with you shortly.");
        msg.setTypeface(font.robotoRegular);

        Button gotit = (Button) dialogView.findViewById(R.id.gotit);

        final android.support.v7.app.AlertDialog finalAlert = alert;
        gotit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finalAlert.dismiss();
            }
        });

        // Showing Alert Message
        alert.show();
    }
}
