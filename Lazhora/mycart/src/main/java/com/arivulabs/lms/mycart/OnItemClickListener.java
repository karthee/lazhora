package com.arivulabs.lms.mycart;

/**
 * Created by arivuventures on 11/1/17.
 */

public interface OnItemClickListener {

    void onItemClick(MyCartPOJO myCartPOJO);
}
