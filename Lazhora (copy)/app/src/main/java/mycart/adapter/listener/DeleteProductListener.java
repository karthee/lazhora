package mycart.adapter.listener;

import mycart.MyCartPOJO;

/**
 * Created by Karthee on 21/03/17.
 */

public interface DeleteProductListener {
    void deleteProduct(MyCartPOJO myCartPOJO);
}
