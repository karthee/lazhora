package lazhora.ui.Screens;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.MenuItem;
import android.widget.RadioButton;
import android.widget.TextView;

import lazhora.R;
import lazhora.ui.utils.Font;

/**
 * Created by Arivu on 26-09-2016.
 */
public class Settings extends AppCompatActivity {

    /**
     * The Image quality.
     */
    TextView imageQuality, /**
     * The Read license.
     */
    readLicense;
    /**
     * The Low iq.
     */
    RadioButton lowIq, /**
     * The Med iq.
     */
    medIq, /**
     * The High iq.
     */
    highIq;
    /**
     * The Font.
     */
    Font font;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        font = new Font(Settings.this);

        SpannableString pageTitle = new SpannableString(getSupportActionBar().getTitle());
        pageTitle.setSpan(font.robotoMedium, 0, pageTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        getSupportActionBar().setTitle(pageTitle);

        imageQuality = (TextView) findViewById(R.id.settings_iq);
        readLicense = (TextView) findViewById(R.id.settings_read_license);

        lowIq = (RadioButton) findViewById(R.id.low_iq);
        medIq = (RadioButton) findViewById(R.id.med_iq);
        highIq = (RadioButton) findViewById(R.id.high_iq);

        imageQuality.setTypeface(font.robotoRegular);
        readLicense.setTypeface(font.robotoRegular);

        lowIq.setTypeface(font.robotoRegular);
        medIq.setTypeface(font.robotoRegular);
        highIq.setTypeface(font.robotoRegular);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
