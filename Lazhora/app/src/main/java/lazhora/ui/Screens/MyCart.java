package lazhora.ui.Screens;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;

import com.arivulabs.lms.mycart.MyCartAdapter;
import com.arivulabs.lms.mycart.MyCartListActivity;
import com.arivulabs.lms.mycart.MyCartPOJO;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import lazhora.R;
import lazhora.ui.utils.SessionMaintainence;

/**
 * Created by arivuventures on 11/1/17.
 */
public class MyCart extends AppCompatActivity {

    /**
     * The My cart pojo.
     */
    MyCartPOJO myCartPOJO;
    private ArrayList<MyCartPOJO> cartList = new ArrayList<>();

    /**
     * The S.
     */
    final ArrayList<String> s = new ArrayList<String>();
    /**
     * The W.
     */
    final ArrayList<String> w = new ArrayList<String>();
    /**
     * The Q.
     */
    final ArrayList<Integer> q = new ArrayList<Integer>();

    /**
     * The S array.
     */
    String[] sArray, /**
     * The W array.
     */
    wArray;
    /**
     * The Q array.
     */
    int[] qArray;

    /**
     * The Type 3 quantity.
     */
    int type3quantity = 0;
    /**
     * The Res id.
     */
    int resID = 0;

    /**
     * The Sm.
     */
    SessionMaintainence sm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sm = SessionMaintainence.getInstance();

        String string = getData(getApplicationContext());
        JSONArray resultSet = null;
        try {
            if (string != null) {
                resultSet = new JSONArray(string);

                Log.e("resultSet_myCart", String.valueOf(resultSet.length()));

                for (int i = 0; i < resultSet.length(); i++) {
                    JSONObject results = resultSet.getJSONObject(i);

                    int type = results.getInt("type");
                    Log.e("type", String.valueOf(type));

                    String title = results.getString("title");
                    String category = results.getString("category");
                    resID = results.getInt("picture");

                    Log.e("resID", String.valueOf(resID));

                    int price = Integer.parseInt(results.getString("overall_price"));

                    Log.e("overall_price", String.valueOf(price));

                    JSONArray requirements;

                    if (type == 1) {
                        sArray = new String[0];
                        wArray = new String[0];
                        qArray = new int[0];

                        ArrayList<String> sArr = null;
                        ArrayList<String> wArr = null;

                        requirements = results.getJSONArray("requirements");

                        for (int j = 0; j < requirements.length(); j++) {

                            JSONObject reqObjects = requirements.getJSONObject(j);

                            s.add(reqObjects.getString("strength"));
                            w.add(reqObjects.getString("weight"));
                            q.add(reqObjects.getInt("quantity"));

                            sArr = new ArrayList<>();
                            wArr = new ArrayList<>();

                            for (String s1 : s) {
                                Log.e("s1", s1);
                                sArr.add(s1);
                            }

                            sArray = new String[sArr.size()];
                            for (int k = 0; k < sArray.length; k++) {
                                sArray[k] = sArr.get(k);
                                Log.e("sArray[" + k + "]", sArray[k]);
                            }

                            for (String w1 : w) {
                                Log.e("w1", w1);
                                wArr.add(w1);
                            }
                            wArray = new String[wArr.size()];
                            for (int k = 0; k < wArray.length; k++) {
                                wArray[k] = wArr.get(k);
                                Log.e("wArray[" + k + "]", wArray[k]);
                            }

                            qArray = new int[q.size()];
                            for (int k = 0; k < qArray.length; k++) {
                                qArray[k] = q.get(k).intValue();
                            }
                            Log.e("qArray", String.valueOf(qArray));
                        }

                        myCartPOJO = new MyCartPOJO(
                                title,
                                getResources().getDrawable(resID),
                                price,
                                category,
                                sArray,
                                wArray,
                                qArray);
                        cartList.add(myCartPOJO);

                        s.clear();
                        w.clear();
                        q.clear();

                    } else if (type == 2) {
                        wArray = new String[0];
                        qArray = new int[0];

                        ArrayList<String> wArr = null;

                        requirements = results.getJSONArray("requirements");

                        for (int j = 0; j < requirements.length(); j++) {

                            JSONObject reqObjects = requirements.getJSONObject(j);

                            w.add(reqObjects.getString("weight"));
                            q.add(reqObjects.getInt("quantity"));

                            wArr = new ArrayList<>();

                            for (String w1 : w) {
                                wArr.add(w1);
                            }
                            wArray = new String[wArr.size()];
                            for (int k = 0; k < wArray.length; k++) {
                                wArray[k] = wArr.get(k);
                            }
                            Log.e("wArray", String.valueOf(wArray));

                            qArray = new int[q.size()];
                            for (int k = 0; k < qArray.length; k++) {
                                qArray[k] = q.get(k).intValue();
                            }
                            Log.e("qArray", String.valueOf(qArray));
                        }

                        myCartPOJO = new MyCartPOJO(
                                title,
                                getResources().getDrawable(resID),
                                price,
                                category,
                                wArray,
                                qArray);
                        cartList.add(myCartPOJO);

                        w.clear();
                        q.clear();

                    } else if (type == 3) {
                        results.getInt("quantity");
                        results.getInt("price");
                        results.getInt("overall_price");

                        myCartPOJO = new MyCartPOJO(
                                title,
                                getResources().getDrawable(resID),
                                price,
                                category,
                                results.getInt("quantity"));
                        cartList.add(myCartPOJO);
                    }

                    Log.e("mycart_added_item", String.valueOf(i));
                }
            }
        } catch (Exception e) {
            Log.e("resultSetException", e.toString());
        }

        MyCartListActivity myCartListActivity = new MyCartListActivity(this, cartList,
                sm.getCusName(), sm.getCusMobile());
        setContentView(myCartListActivity);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
//        startActivity(new Intent(this, HomeActivity.class));
        finish();
    }

    /**
     * Gets data.
     *
     * @param context the context
     * @return the data
     */
    public static String getData(Context context) {
        try {
            File f = new File(context.getFilesDir().getPath() + "/" + "mycart.json");
            //check whether file exists
            FileInputStream is = new FileInputStream(f);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();

            Log.e("Read Success@mycart", new String(buffer, "UTF-8"));
            return new String(buffer, "UTF-8");
        } catch (IOException e) {
            Log.e("File not exist@mycart", e.getLocalizedMessage());
            return null;
        }
    }
}
