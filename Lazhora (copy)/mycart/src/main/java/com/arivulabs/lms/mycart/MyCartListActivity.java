package com.arivulabs.lms.mycart;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * Created by arivuventures on 21/12/16.
 */

public class MyCartListActivity extends RelativeLayout {

    private static String TAG = "MyCartListActivity";

    private static String name = null, phone = null;
    private Context context;

    private RelativeLayout layoutMsg;
    private RelativeLayout emptyListViewMsg;
    private ListView cartListView;

    MyCartAdapter myCartAdapter;
    MyCartPOJO myCartPOJO;

    private ArrayList<MyCartPOJO> list = new ArrayList<MyCartPOJO>();

    public MyCartListActivity(Context context) {
        super(context);
        this.context = context;
        init(context);
    }

    public MyCartListActivity(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init(context);
    }

    public MyCartListActivity(Context context, ArrayList<MyCartPOJO> lists) {
        super(context);
        this.context = context;
        this.list = lists;
        init(context, this.list);
    }

    public MyCartListActivity(Context context, ArrayList<MyCartPOJO> lists,
                              String name, String phone) {
        super(context);
        this.context = context;
        this.list = lists;
        init(context, this.list);
        this.name = name;
        this.phone = phone;
    }

    private void init(Context context) {
        inflate(context, R.layout.activity_my_cart, this);

    }

    private void init(final Context context, ArrayList<MyCartPOJO> items) {
        inflate(context, R.layout.activity_my_cart, this);

        cartListView = (ListView) findViewById(R.id.mycart_list);
        cartListView.setPadding(8, 8, 8, 8);
        cartListView.setDivider(null);

        layoutMsg = (RelativeLayout) findViewById(R.id.msg);
        emptyListViewMsg = (RelativeLayout) findViewById(android.R.id.empty);

        Button browseProducts = (Button) emptyListViewMsg.findViewById(R.id.browse_products);
        browseProducts.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ((Activity) getContext()).finish();
            }
        });

        myCartAdapter = new MyCartAdapter(context, items);

        cartListView.setAdapter(null);
        Log.e(TAG, "al_size: " + this.list.size());

        if (this.list.size() != 0) {
            Log.e("MyCartArray: ", String.valueOf(this.list));
            cartListView.setAdapter(myCartAdapter);
            myCartAdapter.notifyDataSetChanged();

            View footerView = ((LayoutInflater) getContext().getSystemService(LAYOUT_INFLATER_SERVICE)).
                    inflate(R.layout.content_mycart_lv_footer, null);
            cartListView.addFooterView(footerView);

            Button apply, checkout;
            final EditText zhoraPoints;
            TextView usablePoints, availablePoints;

            zhoraPoints = (EditText) footerView.findViewById(R.id.zhoro_points);
            usablePoints = (TextView) footerView.findViewById(R.id.max_usable_points);
            availablePoints = (TextView) footerView.findViewById(R.id.available_points);

            apply = (Button) footerView.findViewById(R.id.apply_zhora_points);
            checkout = (Button) footerView.findViewById(R.id.checkout);

            final TextView overallPrice = (TextView) footerView.findViewById(R.id.overall_price);

            int price = 0;

            for (MyCartPOJO cartPrice : list) {
                price += cartPrice.getProductPrice();
            }

            //String pri = String.valueOf(price);
            Log.e("overall_price", String.valueOf(price));

            overallPrice.setText(String.valueOf(price));

            int oaPrice = Integer.parseInt(overallPrice.getText().toString());

            if (oaPrice != 0) {
                if (oaPrice <= 1000) {
                    usablePoints.setText("50");
                    usablePoints.setCompoundDrawablesRelativeWithIntrinsicBounds(
                            getResources().getDrawable(R.drawable.help_green),
                            null, null, null
                    );
                    zhoraPoints.setHint("50");
                } else if (oaPrice <= 2000) {
                    usablePoints.setText("50");
                    usablePoints.setCompoundDrawablesRelativeWithIntrinsicBounds(
                            getResources().getDrawable(R.drawable.help_green),
                            null, null, null
                    );
                    zhoraPoints.setHint("50");
                } else if (oaPrice <= 3000) {
                    usablePoints.setText("100");
                    usablePoints.setCompoundDrawablesRelativeWithIntrinsicBounds(
                            getResources().getDrawable(R.drawable.help_green),
                            null, null, null
                    );
                    zhoraPoints.setHint("100");
                } else if (oaPrice <= 5000) {
                    usablePoints.setText("200");
                    usablePoints.setCompoundDrawablesRelativeWithIntrinsicBounds(
                            getResources().getDrawable(R.drawable.help_green),
                            null, null, null
                    );
                    zhoraPoints.setHint("200");
                } else if (oaPrice <= 7500) {
                    usablePoints.setText("500");
                    usablePoints.setCompoundDrawablesRelativeWithIntrinsicBounds(
                            getResources().getDrawable(R.drawable.help_green),
                            null, null, null
                    );
                    zhoraPoints.setHint("500");
                } else if (oaPrice <= 10000) {
                    usablePoints.setText("500");
                    usablePoints.setCompoundDrawablesRelativeWithIntrinsicBounds(
                            getResources().getDrawable(R.drawable.help_green),
                            null, null, null
                    );
                    zhoraPoints.setHint("500");
                } else if (oaPrice <= 15000) {
                    usablePoints.setText("750");
                    usablePoints.setCompoundDrawablesRelativeWithIntrinsicBounds(
                            getResources().getDrawable(R.drawable.help_green),
                            null, null, null
                    );
                    zhoraPoints.setHint("750");
                } else if (oaPrice > 15000) {
                    int pricePercentage = oaPrice * 25 / 100;
                    usablePoints.setText(Integer.toString(pricePercentage));
                    usablePoints.setCompoundDrawablesRelativeWithIntrinsicBounds(
                            getResources().getDrawable(R.drawable.help_green),
                            null, null, null
                    );
                    zhoraPoints.setHint(Integer.toString(pricePercentage));
                }
            } else {
                usablePoints.setText("0");
                usablePoints.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        getResources().getDrawable(R.drawable.help_green),
                        null, null, null
                );
                zhoraPoints.setHint("0");
            }

            apply.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    int oaPrice, zPoints;

                    try {
                        oaPrice = Integer.parseInt(overallPrice.getText().toString());
                        zPoints = Integer.parseInt(zhoraPoints.getText().toString());

                        if (zPoints > 50) {
                            Toast.makeText(context, "Enter points below 50", Toast.LENGTH_SHORT).show();
                        } else {
                            oaPrice -= zPoints;
                            overallPrice.setText(Integer.toString(oaPrice));
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "Zhora Points NullPointer" + e.toString());
                    }
                }
            });

            checkout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    Toast.makeText(getContext(), "Checkout clicked.", Toast.LENGTH_SHORT).show();

                    /*Bundle carts = new Bundle();
                    carts.putSerializable("cart_list", cartList);*/

                    getContext().startActivity(new Intent(getContext(), CompleteOrder.class).
                            putExtra("name", name).putExtra("phone", phone));
                    ((Activity) getContext()).finish();
                }
            });

        } else {
            layoutMsg.setVisibility(View.GONE);
        }
        cartListView.setEmptyView(emptyListViewMsg);
        cartListView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

}
