package orderhistory;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import lazhora.R;

import static lazhora.ui.MyApplication.getApplication;
import static lazhora.ui.MyApplication.getContext;

/**
 * Created by arivuventures on 11/1/17.
 */

public class OrderHistoryAdapter extends
        RecyclerView.Adapter<OrderHistoryAdapter.ViewHolder> {

    private Context context;
    private List<OrderHistoryPOJO> orderList;
    private final OnItemClickListener listener;

    public OrderHistoryAdapter() {
        listener = null;
    }

    public OrderHistoryAdapter(Context context,
                               List<OrderHistoryPOJO> orderList,
                               OnItemClickListener listener) {
        this.context = context;
        this.orderList = orderList;
        this.listener = listener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView orderID, orderDate, orderPrice, orderDiscount;

        public ViewHolder(View itemView) {
            super(itemView);
            orderID = (TextView) itemView.findViewById(R.id.order_id);
            orderDate = (TextView) itemView.findViewById(R.id.order_date);
            orderPrice = (TextView) itemView.findViewById(R.id.order_price);
//            orderDiscount = (TextView) itemView.findViewById(R.id.order_discount);
        }

        public void bind(final OrderHistoryPOJO orderHistoryPOJO,
                         final OnItemClickListener listener) {

        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.content_orderhistory_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final OrderHistoryPOJO orderHistoryPOJO = orderList.get(position);

        holder.orderID.setText("#" + orderHistoryPOJO.getOrderId());
        holder.orderDate.setText(orderHistoryPOJO.getOrderDate());
        holder.orderPrice.setText(String.valueOf(orderHistoryPOJO.getOrderPrice()));
//        holder.orderDiscount.setText(String.valueOf(orderHistoryPOJO.getOrderDiscount()));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(orderHistoryPOJO);

            }
        });
    }

    @Override
    public int getItemCount() {
        return orderList.size();
    }
}
