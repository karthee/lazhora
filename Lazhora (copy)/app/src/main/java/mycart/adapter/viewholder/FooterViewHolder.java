package mycart.adapter.viewholder;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.io.Serializable;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import lazhora.R;
import lazhora.ui.utils.SessionMaintainence;
import mycart.CompleteOrder;
import mycart.MyCartPOJO;
import mycart.adapter.listener.FooterListener;

/**
 * Created by Arivu on 22/03/17.
 */

public class FooterViewHolder extends MyViewHolder {

    private final SessionMaintainence sm;

    @BindView(R.id.checkout)
    Button checkout;

    @BindView(R.id.max_usable_points)
    TextView usablePoints;

    @BindView(R.id.available_points)
    TextView availablePoints;

    @BindView(R.id.overall_price)
    TextView overallAmount;

    @BindView(R.id.payable_amount)
    TextView payableAmount;

    @BindView(R.id.apply_points_check)
    CheckBox applyCoupon;

    Context context;
    private List<MyCartPOJO> cartList;
    private FooterListener footerListener;
    private int oaPrice;
    private int points = 0;
    int price = 0, currentPoints;

    public FooterViewHolder(View v) {
        super(v);
        ButterKnife.bind(this, v);
        context = v.getContext();
        sm = SessionMaintainence.getInstance();

    }

    public void bind(final List<MyCartPOJO> cartList, FooterListener footerListener) {
        this.cartList = cartList;
        this.footerListener = footerListener;

        currentPoints = sm.getZhoraPoints();
        Log.e("currPoints", String.valueOf(currentPoints));

        if (cartList.size() != 0) {

            for (MyCartPOJO cartPrice : cartList) {
                price += cartPrice.getProductPrice();
            }

            //String pri = String.valueOf(price);
            Log.e("overall_price", String.valueOf(price));

            overallAmount.setText(String.valueOf(price));
            payableAmount.setText(String.valueOf(price));

            oaPrice = Integer.parseInt(overallAmount.getText().toString());

            usablePoints.setCompoundDrawablesRelativeWithIntrinsicBounds(
                    null, null,
                    context.getResources().getDrawable(R.drawable.help_green),
                    null
            );

            if (oaPrice != 0) {
                if (oaPrice <= 1000) {
                    points = 50;
                    usablePoints.setText("50");

                } else if (oaPrice <= 2000) {
                    points = 50;
                    usablePoints.setText("50");

                } else if (oaPrice <= 3000) {
                    points = 100;
                    usablePoints.setText("100");

                } else if (oaPrice <= 5000) {
                    points = 200;
                    usablePoints.setText("200");

                } else if (oaPrice <= 7500) {
                    points = 500;
                    usablePoints.setText("500");

                } else if (oaPrice <= 10000) {
                    points = 500;
                    usablePoints.setText("500");

                } else if (oaPrice <= 15000) {
                    points = 750;
                    usablePoints.setText("750");

                } else if (oaPrice > 15000) {
                    points = oaPrice * 25 / 100;
                    usablePoints.setText(Integer.toString(points));

                }
            } else {
                points = 0;
                usablePoints.setText("0");

                applyCoupon.setClickable(false);
                applyCoupon.setAlpha((float) .50);
            }

            applyCoupon.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (compoundButton.isChecked()) {

                        if (oaPrice > 15000) {
                            currentPoints = currentPoints - 0;
                            Log.e("points_reducted", String.valueOf(currentPoints));
                            sm.setZhoraPoints(currentPoints);
                            sm.setZhoraPointsDiscount(points);

                        } else {

                            currentPoints -= points;
                            sm.setZhoraPoints(currentPoints);
                            sm.setZhoraPointsDiscount(points);
                            Log.e("points_reducted", String.valueOf(currentPoints));
                        }

                        applyCoupon.setText("You saved cash worth " + String.valueOf(points));

                        oaPrice = Integer.parseInt(overallAmount.getText().toString());

                        oaPrice -= points;
                        payableAmount.setText(String.valueOf(oaPrice));


                    } else if (!compoundButton.isChecked()) {
                        int currentPoints = sm.getZhoraPoints();
                        currentPoints += points;

                        sm.setZhoraPoints(currentPoints);

                        applyCoupon.setText("Apply coupon points");

                        payableAmount.setText(String.valueOf(price));
                    }
                }
            });

            checkout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    /*Bundle carts = new Bundle();
                    carts.putSerializable("cart_list", cartList);*/

                    Intent i = new Intent(context, CompleteOrder.class);
                    i.putExtra("name", sm.getCusName()).putExtra("phone", sm.getCusMobile());

                    if (applyCoupon.isChecked()) {
                        i.putExtra("coupon_points", points);
                    } else {
                        i.putExtra("coupon_points", 0);
                    }

                    context.startActivity(i);
                    ((Activity) context).finish();
                }
            });

        } else {
            footerListener.emptylist();
            itemView.setVisibility(View.GONE);
        }
    }
}
