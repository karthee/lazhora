package orderhistory;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import lazhora.R;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * Created by arivuventures on 30/1/17.
 */

public class OrderHistoryListActivity extends RelativeLayout {

    private static String TAG = "OrderHistoryListActivity";

    private Context context;
    private ArrayList<OrderHistoryPOJO> orderHistoryList = new ArrayList<OrderHistoryPOJO>();

    OrderHistoryAdapter orderHistoryAdapter;
    OrderHistoryPOJO orderHistoryPOJO;

    private RecyclerView recyclerView;
    private TextView emptyTxtMsg;
    private TextView orderMsg;

    public OrderHistoryListActivity(Context context) {
        super(context);
    }

    public OrderHistoryListActivity(Context context, ArrayList<OrderHistoryPOJO> lists) {
        super(context);
        this.context = context;
        this.orderHistoryList = lists;
        init(context, this.orderHistoryList);
    }

    private void init(Context context) {
        inflate(context, R.layout.activity_order_history, this);

    }

    private void init(final Context context, ArrayList<OrderHistoryPOJO> items) {
        inflate(context, R.layout.activity_order_history, this);

        orderMsg = (TextView) findViewById(R.id.order_msg);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        emptyTxtMsg = (TextView) findViewById(android.R.id.empty);

        orderHistoryAdapter = new OrderHistoryAdapter(context, orderHistoryList,
                new OnItemClickListener() {
            @Override
            public void onItemClick(OrderHistoryPOJO orderHistoryPOJO) {

                Log.e("orderIDToBeSend", orderHistoryPOJO.getOrderId());
                Intent i = new Intent(context, OrderDetailsActivity.class);
                i.putExtra("order_id", orderHistoryPOJO.getOrderId());

                context.startActivity(i);
                ((Activity) context).finish();
            }
        });

        orderHistoryAdapter.notifyDataSetChanged();

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(orderHistoryAdapter);

        if (this.orderHistoryList.size() != 0) {
            orderMsg.setVisibility(VISIBLE);
            recyclerView.setVisibility(VISIBLE);
            emptyTxtMsg.setVisibility(GONE);

            Log.e("MyCartArray: ", String.valueOf(this.orderHistoryList));
            recyclerView.setAdapter(orderHistoryAdapter);

            View footerView = ((LayoutInflater) getContext().getSystemService(LAYOUT_INFLATER_SERVICE)).
                    inflate(R.layout.content_mycart_lv_footer, null);

        } else {
            orderMsg.setVisibility(GONE);
            recyclerView.setVisibility(GONE);
            emptyTxtMsg.setVisibility(VISIBLE);
        }
    }
}
