package lazhora.ui.api;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import lazhora.BuildConfig;
import lazhora.R;
import lazhora.ui.utils.CustomProgressDialog;
import lazhora.ui.utils.Font;
import lazhora.ui.utils.JsonparserObject;
import lazhora.ui.utils.SessionMaintainence;

/**
 * Created by Karthee on 25/01/16.
 */
public class EnquireApi extends AsyncTask<String, String, JSONObject> {

    // output keys for enquire api
    public static final String KEY_OUTPUT_STATUS = "status";
    public static final String KEY_OUTPUT_DATA = "message";
    public static String enquire = "add_enquiry";
    JSONObject json;
    private CustomProgressDialog progressBar;
    private boolean status;
    private String data;
    private Context context;
    private SessionMaintainence session;
    private Font font;

    public EnquireApi(Context context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        /*
         *    do things before doInBackground() code runs
         *    such as preparing and showing a Dialog or ProgressBar
        */
        progressBar = new CustomProgressDialog(context, "Your request is being sent...");
        session = SessionMaintainence.getInstance();
        font = new Font(context);

    }

    @Override
    protected JSONObject doInBackground(String... f_url) {
        JsonparserObject jparser = JsonparserObject.getInstance();

        try {
            JSONObject jobj = new JSONObject();

            jobj.put("product", f_url[0]);
            jobj.put("name", session.getCustomerInfo().get(SessionMaintainence.KEY_CUS_NAME));
            jobj.put("phone_one", session.getCustomerInfo().get(SessionMaintainence.KEY_PHNUM1));
            jobj.put("phone_teo", session.getCustomerInfo().get(SessionMaintainence.KEY_PHNUM2));

//            json = jparser.makeHttpRequest(session.getIp() + enquire,

            Log.e("order_enquiry", BuildConfig.url + enquire);

            json = jparser.makeHttpRequest(BuildConfig.url + enquire,
                    "POST",
                    jobj.toString());

        } catch (Exception e) {
            Log.e("OnEnquire", e.toString());

        }

        return json;
    }

    @Override
    protected void onPostExecute(JSONObject json) {
        /*
         *    do something with data here
         *    display it or send to mainactivity
         *    close any dialogs/ProgressBars/etc...
        */

        // dismiss the dialog after the file was downloaded
        try {
            progressBar.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            status = json.getBoolean(KEY_OUTPUT_STATUS);
            data = json.getString(KEY_OUTPUT_DATA);

        } catch (Exception e) {
            Log.e("OnEnquireStatus", e.toString());
//            Toast.makeText(context, "Network Error. Please try again", Toast.LENGTH_SHORT).show();

        }

        if (status) {
            if(!session.getPageID().equals("CompleteOrder")){
                showAlertDialog("Your enquiry has been submitted.");
            }

        } else {
            Toast.makeText(context, "Network Error. Please try again", Toast.LENGTH_SHORT).show();

        }
    }

    public void showAlertDialog(String message) {
        android.support.v7.app.AlertDialog alert = null;
        final android.support.v7.app.AlertDialog.Builder alertDialog =
                new android.support.v7.app.AlertDialog.Builder(context);
        LayoutInflater inflater = ((Activity)context).getLayoutInflater();

        View dialogView = inflater.inflate(R.layout.gift_layout, null);
        alertDialog.setView(dialogView);
        alert = alertDialog.create();

        TextView title = (TextView) dialogView.findViewById(R.id.dialogtitle);
        TextView msg = (TextView) dialogView.findViewById(R.id.dialogmessage);

        ImageView gift = (ImageView) dialogView.findViewById(R.id.gift);
        gift.setVisibility(View.GONE);

        title.setText("Thanks for your interest!");
        title.setTypeface(font.robotoMedium);
        title.setAlpha((float) .87);
        msg.setText("We will get in touch with you shortly.");
        msg.setTypeface(font.robotoRegular);

        Button gotit = (Button) dialogView.findViewById(R.id.gotit);

        final android.support.v7.app.AlertDialog finalAlert = alert;
        gotit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finalAlert.dismiss();
            }
        });

        // Showing Alert Message
        alert.show();
    }
}
