package com.arivulabs.lms.orderhistory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by arivuventures on 11/1/17.
 */

public class OrderHistoryPOJO implements Serializable {

    public OrderHistoryPOJO() {
    }

    public OrderHistoryPOJO(String orderId,
                            String orderDate,
                            int orderPrice) {
        setOrderId(orderId);
        setOrderDate(orderDate);
        setOrderPrice(orderPrice);
    }

    String orderId;
    String orderDate;
    int orderPrice;


    public int getOrderPrice() {
        return orderPrice;
    }

    public void setOrderPrice(int orderPrice) {
        this.orderPrice = orderPrice;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }
}
