package lazhora.views;

/**
 * Created by Karthee on 17/03/17.
 */

public interface ProductsPresenter {
    void getFilteredResult(CharSequence constraint);
}
