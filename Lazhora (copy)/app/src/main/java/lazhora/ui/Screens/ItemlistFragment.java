package lazhora.ui.Screens;

import android.content.Intent;
import android.database.SQLException;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import lazhora.R;
import lazhora.ui.Screens.home.HomeActivityRVAdapter;
import lazhora.ui.data.Item;
import lazhora.ui.data.JsonData;
import lazhora.ui.data.JsonDataCopy;
import lazhora.ui.db.DBManager;
import lazhora.ui.utils.CustomAdapter;
import lazhora.ui.utils.SessionMaintainence;

/**
 * A placeholder fragment containing a simple view.
 */
public class ItemlistFragment extends Fragment {

    @BindView(R.id.rv_home)
    RecyclerView listItem;

    HomeActivityRVAdapter homeActivityRVAdapter;

    private List<Item> itemlist;
    SessionMaintainence session;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);

        ButterKnife.bind(this, rootView);

        listItem.setLayoutManager(new GridLayoutManager(getActivity(), 2));

        itemlist = new ArrayList<>();
        session = SessionMaintainence.getInstance();
        createList();

        homeActivityRVAdapter = new HomeActivityRVAdapter();

        listItem.setAdapter(homeActivityRVAdapter);

        homeActivityRVAdapter.setItemlist(itemlist);

        return rootView;
    }

    private void createList() {
        try {
            final DBManager myDbHelper = new DBManager(getActivity());

            myDbHelper.openDataBase();
            try {
                JSONObject products = new JSONObject(JsonDataCopy.jsondata);
                JSONArray productsjsonarray = products.getJSONArray("data");
                for (int i = 0; i < productsjsonarray.length(); i++) {
                    JSONObject jobj = productsjsonarray.getJSONObject(i);

                    Object priceObj = jobj.get("price");
                    String price = null;

                    if (priceObj instanceof String) {
                        price = (String) priceObj;
                        Log.e("price@adding_String", jobj.getString("product_title") + ", " + price);
                    } else if (priceObj instanceof JSONArray) {
                        StringBuilder priceBuilder = new StringBuilder();
                        JSONArray priceArr = (JSONArray) priceObj;

                        for (int j = 0; j < priceArr.length(); j++) {
                            if (j == 0) {
                                priceBuilder.append(priceArr.getString(j));
                            } else {
                                priceBuilder.append("," + priceArr.getString(j));
                            }
                        }
                        price = String.valueOf(priceBuilder);

                        Log.e("price@adding_Array", jobj.getString("product_title") + ", " + price);
                    }

                    myDbHelper.addProducts(String.valueOf(
                            jobj.getInt("product_id")),
                            jobj.getString("product_title"),
                            jobj.getString("product_desc"),
                            price.trim(),
                            jobj.getString("tag"),
                            jobj.getString("picture"),
                            jobj.getString("category"),
                            jobj.getString("picture_name"));
                }
            } catch (JSONException e) {
                Log.e("json", "createList: " + e.toString());
            }

            String category = "";
            try {
                category = session.getCategory();
                Log.e("category:  ", "session" + session.getCategory());
            } catch (Exception e) {
                e.printStackTrace();

            }
            itemlist = myDbHelper.getProductsByCategory(category);
            myDbHelper.close();
        } catch (Exception e) {
            Log.e("sql", "category: " + e.toString());
        }

    }
}
