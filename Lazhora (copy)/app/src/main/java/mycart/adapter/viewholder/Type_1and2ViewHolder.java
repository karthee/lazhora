package mycart.adapter.viewholder;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import lazhora.R;
import mycart.JsonDataCopy;
import mycart.MyCartActivity;
import mycart.MyCartPOJO;
import mycart.adapter.listener.DeleteProductListener;
import mycart.utils.SystemUtils;

import static android.content.ContentValues.TAG;

/**
 * Created by Karthee on 21/03/17.
 */

public class Type_1and2ViewHolder extends MyViewHolder {

    private final Context context;
    @BindView(R.id.cart_pro_title)
    TextView productTitle;

    @BindView(R.id.cart_pro_price)
    TextView productPrice;

    @BindView(R.id.cart_modify)
    TextView change;

    @BindView(R.id.cart_delete)
    ImageView close;

    @BindView(R.id.cart_pro_image)
    ImageView productImage;

    @BindView(R.id.strength)
    LinearLayout strength;

    @BindView(R.id.content_strength)
    LinearLayout contentStrength;

    @BindView(R.id.content_weight)
    LinearLayout contentWeight;

    @BindView(R.id.content_quantity)
    LinearLayout contentQuantity;

    String title;
    String proName;
    private MyCartPOJO myCartPOJO;
    private int position;
    private DeleteProductListener deletelistener;
    String category;


    List<HashMap<String, Object>> row = new ArrayList<HashMap<String, Object>>();
    List<HashMap<String, Object>> row2 = new ArrayList<HashMap<String, Object>>();
    private StringBuilder strengths;
    private StringBuilder packs;
    private StringBuilder custom_packs;
    private String[] prices;
    private StringBuilder sizes;
    private LinearLayout items;

    int resID;
    int quantity;
    int proPrice;
    private ImageView removeItem;

    final List<String> s = new ArrayList<>();
    final List<String> w = new ArrayList<String>();
    final List<Integer> q = new ArrayList<Integer>();


    public Type_1and2ViewHolder(View v) {
        super(v);

        ButterKnife.bind(this, v);

        context = v.getContext();
        close.setTag(View.generateViewId());
        change.setTag(View.generateViewId());
    }

    public void bind(final MyCartPOJO myCartPOJO, final int position, final DeleteProductListener deletelistener) {


        proName = myCartPOJO.getProductTitle();
        this.myCartPOJO = myCartPOJO;
        this.position = position;
        this.deletelistener = deletelistener;

        productTitle.setText(myCartPOJO.getProductTitle());
        productImage.setImageDrawable(myCartPOJO.getProductImagePath());

        if (myCartPOJO.getProductPrice() == 0) {
            productPrice.setText("--");
        } else {
            productPrice.setText(String.valueOf(myCartPOJO.getProductPrice()));
        }

        change.setTag(position);
        change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                String string = SystemUtils.getData(context);
                Log.e("clickedViewTag", string);

                JSONArray resultSet = null;

                try {
                    resultSet = new JSONArray(string);

                    for (int i = 0; i < resultSet.length(); i++) {
                        JSONObject results = resultSet.getJSONObject(i);

                        if (results.getString("title").equals(proName)) {

                            row.clear();
                            row2.clear();

                            resID = results.getInt("picture");

                            int type = results.getInt("type");

                            if (type == 1) {
                                JSONArray requirements = results.getJSONArray("requirements");

                                for (int j = 0; j < requirements.length(); j++) {
                                    JSONObject reqObj = requirements.getJSONObject(j);


                                    HashMap<String, Object> rowData = new HashMap<String, Object>();

                                    rowData.put("layout_id", position);

                                    Log.e("onClick_changeDialog", reqObj.getString("strength"));

                                    rowData.put("strength", reqObj.getString("strength"));
                                    rowData.put("weight", reqObj.getString("weight"));
                                    rowData.put("quantity", reqObj.getString("quantity"));

                                    row.add(rowData);
                                    Log.e("rowData1", String.valueOf(row));
                                }
                            } else if (type == 2) {
                                JSONArray requirements = results.getJSONArray("requirements");

                                for (int j = 0; j < requirements.length(); j++) {
                                    JSONObject reqObj = requirements.getJSONObject(j);

                                    HashMap<String, Object> rowData = new HashMap<String, Object>();

                                    rowData.put("layout_id", position);
                                    rowData.put("weight", reqObj.getString("weight"));
                                    rowData.put("quantity", reqObj.getString("quantity"));

                                    row.add(rowData);
                                    Log.e("rowData2", String.valueOf(row));
                                }
                            } /*else if (type == 3) {

                                    }*/
                        }
                    }
                } catch (JSONException e) {
                    Log.e("jsonException", e.toString());
                }

                category = myCartPOJO.getProductCategory();

                if (category.equals("CHEMICAL PEEL")) {
                    Log.e("ope_proName", "name is ... " + proName);
                    Log.e("ope_category", "category is ... " + category);

                    try {
                        JSONObject products = new JSONObject(JsonDataCopy.jsondata);
                        JSONArray productsjsonarray = products.getJSONArray("data");
                        for (int i = 0; i < productsjsonarray.length(); i++) {
                            JSONObject jobj = productsjsonarray.getJSONObject(i);

                            if (jobj.getString("product_title").equals(proName)) {
                                Log.e("ope_product_title", jobj.getString("product_title"));

                                Object strengthObj = jobj.get("strength");

                                if (strengthObj instanceof String) {
                                    strengths = new StringBuilder();

                                    strengths.append(strengthObj);
                                } else if (strengthObj instanceof JSONArray) {
                                    JSONArray strength = (JSONArray) strengthObj;

                                    strengths = new StringBuilder();

                                    for (int j = 0; j < strength.length(); j++) {
                                        Log.e("strength_items", String.valueOf(strength.get(j)));
                                        if (j == 0) {
                                            strengths.append(strength.get(0));
                                        } else {
                                            strengths.append("," + strength.get(j));
                                        }
                                    }
                                }

                                Object packObj = jobj.get("pack");

                                if (packObj instanceof String) {
                                    packs = new StringBuilder();

                                    packs.append(packObj);
                                } else if (packObj instanceof JSONArray) {
                                    JSONArray pack = (JSONArray) packObj;

                                    packs = new StringBuilder();

                                    for (int j = 0; j < pack.length(); j++) {
                                        if (j == 0) {
                                            packs.append(pack.get(0));
                                        } else {
                                            packs.append("," + pack.get(j));
                                        }
                                    }
                                }

                                Object cusPackObj = jobj.get("custom_pack");

                                if (cusPackObj instanceof JSONArray) {
                                    JSONArray custom_pack = (JSONArray) cusPackObj;

                                    custom_packs = new StringBuilder();

                                    for (int j = 0; j < custom_pack.length(); j++) {
                                        if (j == 0) {
                                            custom_packs.append(custom_pack.get(0));
                                        } else {
                                            custom_packs.append("," + custom_pack.get(j));
                                        }
                                    }
                                }
                            }
                        }
                    } catch (JSONException e) {
                        Log.e("chemical_peel excep", e.toString());
                    }
                } else if (category.equals("SERUMS - SKIN CARE") || category.equals("SERUMS - HAIR CARE")) {
                    Log.e("ope_category", category);

                    try {
                        JSONObject products = new JSONObject(JsonDataCopy.jsondata);
                        JSONArray productsjsonarray = products.getJSONArray("data");
                        for (int i = 0; i < productsjsonarray.length(); i++) {
                            JSONObject jobj = productsjsonarray.getJSONObject(i);

                            if (jobj.getString("product_title").equals(proName)) {

                                Object pack = jobj.get("pack");

                                if (pack instanceof String) {
                                } else if (pack instanceof JSONArray) {


                                    packs = new StringBuilder();

                                    JSONArray weight = (JSONArray) pack;

                                    for (int j = 0; j < weight.length(); j++) {
                                        if (j == 0) {
                                            packs.append(weight.get(0));
                                        } else {
                                            packs.append("," + weight.get(j));
                                        }
                                    }
                                }

                                Object priceObj = jobj.get("price");
                                String price = null;

                                if (priceObj instanceof String) {
                                    price = (String) priceObj;
                                } else if (priceObj instanceof JSONArray) {
                                    StringBuilder priceBuilder = new StringBuilder();
                                    JSONArray priceArr = (JSONArray) priceObj;

                                    for (int j = 0; j < priceArr.length(); j++) {
                                        if (j == 0) {
                                            priceBuilder.append(priceArr.getString(0));
                                        } else {
                                            priceBuilder.append("," + priceArr.getString(j));
                                        }
                                    }
                                    price = String.valueOf(priceBuilder);

                                }

                                prices = price.split(",");
                            }
                        }
                    } catch (JSONException e) {
                        Log.e("skin & hair excep", e.toString());
                    }
                } else if (category.equals("CREAMS") || category.equals("OTHER")) {
                    Log.e("ope_category", category);
                    try {
                        JSONObject products = new JSONObject(JsonDataCopy.jsondata);
                        JSONArray productsjsonarray = products.getJSONArray("data");
                        for (int i = 0; i < productsjsonarray.length(); i++) {
                            JSONObject jobj = productsjsonarray.getJSONObject(i);

                            if (jobj.getString("product_title").equals(proName)) {

                                Object pack = jobj.get("pack");

                                if (!pack.toString().equals("NA")) {
                                    if (pack instanceof String) {
                                    } else if (pack instanceof JSONArray) {

                                        packs = new StringBuilder();

                                        JSONArray weight = (JSONArray) pack;

                                        for (int j = 0; j < weight.length(); j++) {
                                            if (j == 0) {
                                                packs.append(weight.get(0));
                                            } else {
                                                packs.append("," + weight.get(j));
                                            }
                                        }
                                    }
                                } else {
                                }

                                Object priceObj = jobj.get("price");
                                String price = null;

                                if (priceObj instanceof String) {
                                    price = (String) priceObj;
                                } else if (priceObj instanceof JSONArray) {
                                    StringBuilder priceBuilder = new StringBuilder();
                                    JSONArray priceArr = (JSONArray) priceObj;

                                    for (int j = 0; j < priceArr.length(); j++) {
                                        if (j == 0) {
                                            priceBuilder.append(priceArr.getString(0));
                                        } else {
                                            priceBuilder.append("," + priceArr.getString(j));
                                        }
                                    }
                                    price = String.valueOf(priceBuilder);

                                }

                                prices = price.split(",");
                            }
                        }
                    } catch (JSONException e) {
                        Log.e("creams and others excep", e.toString());
                    }
                } else if (category.equals("ROLLERS")) {
                    Log.e("ope_category", category);
                    try {
                        JSONObject products = new JSONObject(JsonDataCopy.jsondata);
                        JSONArray productsjsonarray = products.getJSONArray("data");
                        for (int i = 0; i < productsjsonarray.length(); i++) {
                            JSONObject jobj = productsjsonarray.getJSONObject(i);

                            if (jobj.getString("product_title").equals(proName)) {

                                Object sizeObj = jobj.get("sizes");

                                if (!sizeObj.toString().equals("NA")) {
                                    if (sizeObj instanceof String) {

                                        sizes = new StringBuilder();
                                        sizes.append(sizeObj);
                                    } else if (sizeObj instanceof JSONArray) {
                                        JSONArray size = (JSONArray) sizeObj;


                                        sizes = new StringBuilder();

                                        for (int j = 0; j < size.length(); j++) {
                                            if (j == 0) {
                                                sizes.append(size.get(0));
                                            } else {
                                                sizes.append("," + size.get(j));
                                            }
                                        }
                                    }
                                } else {
                                }

                                Object priceObj = jobj.get("price");
                                String price = null;

                                if (priceObj instanceof String) {
                                    price = (String) priceObj;
                                } else if (priceObj instanceof JSONArray) {
                                    StringBuilder priceBuilder = new StringBuilder();
                                    JSONArray priceArr = (JSONArray) priceObj;

                                    for (int j = 0; j < priceArr.length(); j++) {
                                        if (j == 0) {
                                            priceBuilder.append(priceArr.getString(0));
                                        } else {
                                            priceBuilder.append("," + priceArr.getString(j));
                                        }
                                    }
                                    price = String.valueOf(priceBuilder);

                                }

                                prices = price.split(",");
                                Log.e(TAG, "Roller Prices: " + price);
                            }
                        }
                    } catch (JSONException e) {
                        Log.e("rollers excep", e.toString());
                    }
                } else if (category.equals("MASK")) {
                    Log.e("ope_category", category);
                }

                title = myCartPOJO.getProductTitle();

                changeDialog(myCartPOJO);
//                        changeDialog(position);

                //    notifyDataSetChanged();
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View view) {
//                        Toast.makeText(getContext(), "Close Clicked1", Toast.LENGTH_SHORT).show();

                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setCancelable(false);
                final String title = (String) productTitle.getText();
                builder.setMessage("Want to remove \"" + title + "\" from your cart?");

                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int id) {
                        String string = SystemUtils.getData(context);

                        try {
                            JSONArray resultSet = new JSONArray(string);
                            for (int i = 0; i < resultSet.length(); i++) {
                                JSONObject results = resultSet.getJSONObject(i);

                                if (results.getString("title").equals(title)) {
                                    resultSet.remove(i);
                                    Log.e("resultSet_removed", String.valueOf(resultSet));
                                }
                            }

                            SystemUtils.saveData(context, "mycart.json", resultSet.toString());
                        } catch (JSONException e) {
                            Log.e("jsonException", e.toString());
                        }

                        try {
                            Log.e("onClosePos", String.valueOf(position));
                            //  cartList.remove(position);
                            //   MyCartAdapter.this.notifyDataSetChanged();
                        } catch (Exception e) {
                            Log.e("cartListException: ", e.toString());
                        }

                        //   notifyDataSetChanged();
                        deletelistener.deleteProduct(myCartPOJO);
                        dialogInterface.dismiss();
                    }
                });

                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        dialogInterface.dismiss();
                    }
                });


                AlertDialog dialog = builder.create();
                dialog.show();
                dialog.getButton(dialog.BUTTON_NEGATIVE).setTextColor(
                        context.getResources().getColor(R.color.colorDelete));
                dialog.getButton(dialog.BUTTON_POSITIVE).setTextColor(
                        context.getResources().getColor(R.color.colorPrimary));


            }
        });

        if (myCartPOJO.getStrength() != null) {
            strength.setVisibility(View.VISIBLE);
            contentStrength.setVisibility(View.VISIBLE);

            if (contentStrength.getChildCount() > 0 &&
                        contentWeight.getChildCount() > 0 &&
                        contentQuantity.getChildCount() > 0) {
                contentStrength.removeAllViews();
                contentWeight.removeAllViews();
                contentQuantity.removeAllViews();
            }

            for (String strength : myCartPOJO.getStrength()) {
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                                                                                        ViewGroup.LayoutParams.WRAP_CONTENT);

                TextView content = new TextView(context);

                content.setLayoutParams(params);
                content.setPadding(4, 4, 4, 4);
                content.setGravity(Gravity.CENTER);
                content.setText(strength);
                content.setTextColor(Color.BLACK);
                content.setAlpha((float) .87);

                contentStrength.addView(content);
            }
        } else {
            strength.setVisibility(View.GONE);
            if (contentWeight.getChildCount() > 0 &&
                        contentQuantity.getChildCount() > 0) {
                contentWeight.removeAllViews();
                contentQuantity.removeAllViews();
            }
        }

        for (String weight : myCartPOJO.getWeight()) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                                                                                    ViewGroup.LayoutParams.WRAP_CONTENT);

            TextView content = new TextView(context);

            content.setLayoutParams(params);
            content.setPadding(4, 4, 4, 4);
            content.setGravity(Gravity.CENTER);
            content.setText(weight);
            content.setTextColor(Color.BLACK);
            content.setAlpha((float) .87);

            contentWeight.addView(content);
        }

        for (int quantity : myCartPOJO.getQuantity()) {
            //contentStrength;
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                                                                                    LinearLayout.LayoutParams.WRAP_CONTENT);

            TextView content = new TextView(context);

            content.setLayoutParams(params);
            content.setPadding(4, 4, 4, 4);
            content.setGravity(Gravity.CENTER);
            content.setText(String.valueOf(quantity));
            content.setTextColor(Color.BLACK);
            content.setAlpha((float) .87);

            contentQuantity.addView(content);
        }
    }

    void changeDialog(final MyCartPOJO myCartPOJO) {
        final Dialog dialog = new Dialog(context/*, android.R.style.Theme_Holo_Light_Dialog*/);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_change);

        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.99);

        dialog.getWindow().setLayout(screenWidth, ViewGroup.LayoutParams.WRAP_CONTENT);

        //---------------
        items = (LinearLayout) dialog.findViewById(R.id.items);
        Button done = (Button) dialog.findViewById(R.id.done);
        done.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View view) {
                dialog.dismiss();

                String title = (String) productTitle.getText();

                try {
                    String string = SystemUtils.getData(context);

                    Log.e("title_remove", proName);
                    JSONArray resultSet = new JSONArray(string);
                    for (int i = 0; i < resultSet.length(); i++) {
                        JSONObject results = resultSet.getJSONObject(i);
                        if (results.getString("title").equals(proName)) {
                            resultSet.remove(i);
                        }
                    }
                    SystemUtils.saveData(context, "mycart.json", resultSet.toString());
                    String afterRemoved = SystemUtils.getData(context);
                    Log.e("aft_removed ", afterRemoved);
                } catch (JSONException e) {
                    Log.e("jsonException", e.toString());
                }

                String[] sArray = new String[0];
                String[] wArray = new String[0];
                int[] qArray = new int[0];

                if (myCartPOJO.getStrength() != null) {
                    strength.setVisibility(View.VISIBLE);
                    contentStrength.setVisibility(View.VISIBLE);

                    if (contentStrength.getChildCount() > 0 &&
                                contentWeight.getChildCount() > 0 &&
                                contentQuantity.getChildCount() > 0) {
                        contentStrength.removeAllViews();
                        contentWeight.removeAllViews();
                        contentQuantity.removeAllViews();
                    }

                    for (int i = 0; i < row.size(); i++) {
                        HashMap<String, Object> roww = row.get(i);
                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                                ViewGroup.LayoutParams.MATCH_PARENT,
                                ViewGroup.LayoutParams.WRAP_CONTENT);
                        TextView content = new TextView(context);
                        content.setLayoutParams(params);
                        content.setPadding(4, 4, 4, 4);
                        content.setGravity(Gravity.CENTER);
                        content.setText(roww.get("strength").toString());
                        content.setTextColor(Color.BLACK);
                        content.setAlpha((float) .87);

                        contentStrength.addView(content);

                        String strength = roww.get("strength").toString();
                        s.add(strength);
                    }
                } else {
                    strength.setVisibility(View.GONE);
                    if (contentWeight.getChildCount() > 0 &&
                                contentQuantity.getChildCount() > 0) {
                        contentWeight.removeAllViews();
                        contentQuantity.removeAllViews();
                    }
                }

                for (int i = 0; i < row.size(); i++) {
                    HashMap<String, Object> roww = row.get(i);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT);

                    TextView content = new TextView(context);

                    content.setLayoutParams(params);
                    content.setPadding(4, 4, 4, 4);
                    content.setGravity(Gravity.CENTER);
                    content.setText(roww.get("weight").toString());
                    content.setTextColor(Color.BLACK);
                    content.setAlpha((float) .87);

                    contentWeight.addView(content);

                    String weight = roww.get("weight").toString();
                    w.add(weight);
                }

                for (int i = 0; i < row.size(); i++) {
                    Log.e(TAG, "change: " + row.get(i));
                    HashMap<String, Object> roww = row.get(i);
                    //contentStrength;
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT);

                    TextView content = new TextView(context);

                    content.setLayoutParams(params);
                    content.setPadding(4, 4, 4, 4);
                    content.setGravity(Gravity.CENTER);
                    content.setText(roww.get("quantity").toString());
                    content.setTextColor(Color.BLACK);
                    content.setAlpha((float) .87);

                    contentQuantity.addView(content);

                    String quantity = (String) roww.get("quantity");
                    q.add(Integer.valueOf(quantity));
                }


                if (quantity != 0) {
                    String string = SystemUtils.getData(context);
                    JSONArray resultSet;
                    try {
                        if (string != null) {
                            resultSet = new JSONArray(string);
                        } else {
                            resultSet = new JSONArray();
                        }

                        JSONObject values = new JSONObject();

                        values.put("quantity", quantity);
                        values.put("price", proPrice);
                        int overallPrice = proPrice * quantity;
                        values.put("overall_price", overallPrice);
                        values.put("type", 3);
                        values.put("picture", resID);
                        values.put("category", category);
                        values.put("title", title);

                        resultSet.put(values);

                        Log.e("resultSet_myCart", String.valueOf(resultSet));

                        SystemUtils.saveData(context, "mycart.json", resultSet.toString());
                        row.clear();
                    } catch (JSONException e) {
                        Log.e("Exception", e.toString());
                    }
                } else {

                    List<String> sArr;
                    List<String> wArr = null;

                    if (category.equals("CHEMICAL PEEL")) {
                        sArr = new ArrayList<>();
                        wArr = new ArrayList<>();

                        for (String s1 : s) {
                            sArr.add(s1);
                        }

                        sArray = new String[sArr.size()];
                        for (int i = 0; i < sArray.length; i++) {
                            sArray[i] = sArr.get(i);
                        }

                        for (String w1 : w) {
                            wArr.add(w1);
                        }

                        wArray = new String[wArr.size()];
                        for (int i = 0; i < wArray.length; i++) {
                            wArray[i] = wArr.get(i);
                        }
                    } else if (category.equals("SERUMS - SKIN CARE")
                                       || category.equals("SERUMS - HAIR CARE")
                                       || category.equals("CREAMS")
                                       || category.equals("ROLLERS")) {
                        wArr = new ArrayList<>();

                        for (String w1 : w) {
                            wArr.add(w1);
                        }

                        wArray = new String[wArr.size()];
                        for (int i = 0; i < wArray.length; i++) {
                            wArray[i] = wArr.get(i);
                        }
                    }

                    qArray = new int[q.size()];
                    for (int i = 0; i < qArray.length; i++) {
                        qArray[i] = q.get(i).intValue();
                    }

                    JSONArray resultSet;
                    try {
                        String string = SystemUtils.getData(context);

                        if (string != null) {
                            resultSet = new JSONArray(string);
                        } else {
                            resultSet = new JSONArray();
                        }

                        JSONObject values = new JSONObject();

                        JSONArray content = new JSONArray();

                        int netPrice = 0, overallPrice = 0;

                        for (int i = 0; i < row.size(); i++) {
                            HashMap<String, Object> roww = row.get(i);

                            Log.e("strength()", String.valueOf(roww.get("strength")));
                            JSONObject contentValue = new JSONObject();

                            if (category.equals("CHEMICAL PEEL")) {
                                contentValue.put("strength", roww.get("strength"));
                                contentValue.put("weight", roww.get("weight"));
                                contentValue.put("quantity", roww.get("quantity"));
                                contentValue.put("price", proPrice);
                                overallPrice += proPrice;
                            } else if (category.equals("SERUMS - SKIN CARE")
                                               || category.equals("SERUMS - HAIR CARE")
                                               || category.equals("CREAMS")
                                               || category.equals("ROLLERS")) {
                                contentValue.put("weight", roww.get("weight"));
                                contentValue.put("quantity", roww.get("quantity"));
                                contentValue.put("price",
                                        Integer.parseInt(roww.get("price").toString()));

                                int quantity = Integer.parseInt(roww.get("quantity").toString());
//                                netPrice = proPrice * quantity;
                                netPrice = Integer.parseInt(roww.get("price").toString()) * quantity;
                                contentValue.put("net_price", netPrice);

                                overallPrice += netPrice;
                            }
                            content.put(contentValue);
                        }
                        values.put("requirements", content);
                        values.put("overall_price", overallPrice);

                        if (category.equals("CHEMICAL PEEL")) {
                            values.put("type", 1);
                        } else if (category.equals("SERUMS - SKIN CARE")
                                           || category.equals("SERUMS - HAIR CARE")
                                           || category.equals("CREAMS")
                                           || category.equals("ROLLERS")) {
                            values.put("type", 2);
                        }
                        values.put("picture", resID);
                        values.put("category", category);
                        values.put("title", title);

                        resultSet.put(values);

                        Log.e("resultSet", String.valueOf(resultSet));

                        SystemUtils.saveData(context, "mycart.json", resultSet.toString());
                        row.clear();

                    } catch (JSONException e) {
                        Log.e("Exception", e.toString());
                    }
                }

                context.startActivity(new Intent(context, MyCartActivity.class));
                ((Activity) context).finish();
            }
        });

        Button cancel = (Button) dialog.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                row.clear();
                row2.clear();

                dialog.dismiss();

            }
        });


        Log.e("row_size", String.valueOf(row.size()));
        if (row.size() != 0) {

            for (HashMap<String, Object> roww : row) {
                row2.add(roww);
            }
            row.clear();
            createRow(row2);
            addButtonView();

        } else {
            createRow();
            addButtonView();
        }

        dialog.show();
    }

    public void addButtonView() {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                                                                                130, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.CENTER_HORIZONTAL;
        params.setMargins(0, 4, 0, 8);

        final TextView addButton = new TextView(context);
        addButton.setTag("dialog_item_add");
        addButton.setLayoutParams(params);
        addButton.setBackground(context.getResources().getDrawable(R.drawable.add_bg));
        addButton.setGravity(Gravity.CENTER);
        addButton.setPadding(8, 8, 8, 8);
        addButton.setText("ADD");
        addButton.setTextColor(context.getResources().getColor(R.color.colorPrimary));
        addButton.setTextSize(14);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addButton.setVisibility(View.GONE);
                removeItem.setVisibility(View.VISIBLE);

                createRow();
                addButtonView();
            }
        });

        items.addView(addButton);
    }

    public void createRow(List<HashMap<String, Object>> rowItems) {

        int lastItem = rowItems.size() - 1;
        Log.e("lastItem", String.valueOf(lastItem));

        int curItem = 0;

        for (HashMap<String, Object> roww : rowItems) {
            final RelativeLayout itemDialogLO = (RelativeLayout) View.inflate(context,
                    R.layout.item_dialog, null);
            itemDialogLO.setId(View.generateViewId());

            LinearLayout topLO = (LinearLayout) itemDialogLO.findViewById(R.id.top);
            topLO.setId(View.generateViewId());

            LinearLayout strenghtLO = (LinearLayout) itemDialogLO.findViewById(R.id.strength_layout);
            LinearLayout weightLO = (LinearLayout) itemDialogLO.findViewById(R.id.weight_layout);
            LinearLayout quantityLO = (LinearLayout) itemDialogLO.findViewById(R.id.quantity_layout);
            final LinearLayout priceLO = (LinearLayout) itemDialogLO.findViewById(R.id.price_layout);

            final Spinner strengthSpinner = (Spinner) strenghtLO.findViewById(R.id.strength_spinner);
            final Spinner weightSpinner = (Spinner) weightLO.findViewById(R.id.weight_spinner);
            final TextView quantityText = (TextView) quantityLO.findViewById(R.id.quantity_text);
            final TextView priceText = (TextView) priceLO.findViewById(R.id.price_text);
            final TextView netPriceText = (TextView) priceLO.findViewById(R.id.net_price_text);

            ImageView increaseQuantity = (ImageView) quantityLO.findViewById(R.id.increase);
            ImageView decreaseQuantity = (ImageView) quantityLO.findViewById(R.id.decrease);

            if (category.equals("CHEMICAL PEEL")) {
                Log.e(TAG, "createRow: CHEMICAL PEEL");
                strenghtLO.setVisibility(View.VISIBLE);
                weightLO.setVisibility(View.VISIBLE);
                priceLO.setVisibility(View.GONE);

                String[] strArray = this.strengths.toString().split(",");

                List<String> strengths = new ArrayList<String>();
                for (String s : strArray) {
                    strengths.add(s);
                }
                Log.e(TAG, "createRow:strength " + strengths);

                ArrayAdapter<String> strengthAdapter =
                        new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, strengths);

                strengthSpinner.setDropDownWidth(ViewGroup.LayoutParams.MATCH_PARENT);
                strengthSpinner.setAdapter(strengthAdapter);

                int i = 0;
                for (String strength : strengths) {
                    if (roww.get("strength").equals(strength)) {
                        strengthSpinner.setSelection(i);

                        break;
                    }
                    i++;
                }

                final List<String> weights = new ArrayList<String>();

                strengthSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                        weights.clear();

                        if (strengthSpinner.getSelectedItem().equals("Custom")) {

                            if (custom_packs.toString().contains(",")) {
                                String[] pacArray = custom_packs.toString().split(",");

                                for (String s : pacArray) {
                                    weights.add(s);
                                }
                            } else {
                                weights.add(custom_packs.toString());
                            }
                        } else {

                            if (packs.toString().contains(",")) {
                                String[] pacArray = packs.toString().split(",");

                                for (String s : pacArray) {
                                    weights.add(s);
                                }
                            } else {
                                weights.add(packs.toString());
                            }
                        }

                        for (int i = 0; i < row.size(); i++) {

                            HashMap<String, Object> roww = row.get(i);

                            int id = (int) roww.get("layout_id");

                            if (id == itemDialogLO.getId()) {
                                roww.put("strength", strengthSpinner.getSelectedItem());
                                roww.put("weight", weightSpinner.getSelectedItem());
                                roww.put("quantity", quantityText.getText());
                            }
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

                if (packs.toString().contains(",")) {
                    String[] pacArray = packs.toString().split(",");

                    for (String s : pacArray) {
                        weights.add(s);
                    }
                } else {
                    weights.add(packs.toString());
                }

                ArrayAdapter<String> weightAdapter = new ArrayAdapter<String>(context,
                                                                                     android.R.layout.simple_spinner_item, weights);

                weightAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                weightSpinner.setDropDownWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
                weightSpinner.setAdapter(weightAdapter);

                //  weightSpinner.setSelection(1);

                int j = 0;
                for (String weight : weights) {
                    if (roww.get("weight").equals(weight)) {
                        int spinnerPosition = strengthAdapter.getPosition(weight);
//                        weightSpinner.setPrompt(strengthAdapter.getItem(j));
                        Log.e(TAG, "spinner pos: " + (i));
                        Log.e(TAG, "spinner pos: " + weight);

                        weightSpinner.setSelection(i);

                        break;
                    }
                    i++;
                }

                weightSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                        for (int i = 0; i < row.size(); i++) {

                            HashMap<String, Object> roww = row.get(i);

                            int id = (int) roww.get("layout_id");

                            if (id == itemDialogLO.getId()) {
                                roww.put("strength", strengthSpinner.getSelectedItem());
                                roww.put("weight", weightSpinner.getSelectedItem());
                                roww.put("quantity", quantityText.getText());
                            }
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

                HashMap<String, Object> rowData = new HashMap<String, Object>();

                rowData.put("layout_id", itemDialogLO.getId());
//                Log.e("rowLayoutId: ", String.valueOf(rowLayout.getId()));

                rowData.put("strength", strengthSpinner.getSelectedItem());
                rowData.put("weight", weightSpinner.getSelectedItem());
                rowData.put("quantity", quantityText.getText());

                row.add(rowData);

            } else if (category.equals("SERUMS - SKIN CARE")
                               || category.equals("SERUMS - HAIR CARE")
                               || category.equals("CREAMS")
                               || category.equals("OTHER")) {
                Log.e(TAG, "createRow: Others");

                strenghtLO.setVisibility(View.GONE);
                weightLO.setVisibility(View.VISIBLE);
//              priceLO.setVisibility(View.VISIBLE);

                String[] pacArray = this.packs.toString().split(",");

                List<String> weights = new ArrayList<String>();
                for (String s : pacArray) {
                    weights.add(s);
                }

                ArrayAdapter<String> weightAdapter = new ArrayAdapter<String>(context,
                        android.R.layout.simple_spinner_item, weights);

                weightAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                weightSpinner.setDropDownWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
                weightSpinner.setAdapter(weightAdapter);
                int j = 0;
                for (String weight : weights) {
                    if (roww.get("weight").equals(weight)) {
                        int spinnerPosition = weightAdapter.getPosition(weight);
//                        weightSpinner.setPrompt(strengthAdapter.getItem(j));
                        Log.e(TAG, "spinner pos: " + (j - 1));
                        Log.e(TAG, "spinner pos: " + weight);

                        weightSpinner.setSelection(j);

                        break;

                    }
                    j++;
                }
                weightSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int position,
                                               long l) {

                        if (prices[position].equals("NA")) {
                            priceLO.setVisibility(View.VISIBLE);
                            priceText.setText("--");
                            netPriceText.setText("--");
                            proPrice = 0;
                        } else {
                            priceLO.setVisibility(View.VISIBLE);
                            priceText.setText(prices[position]);
                            int quantity = Integer.parseInt(quantityText.getText().toString());
                            int unitPrice = Integer.parseInt(priceText.getText().toString());

                            unitPrice *= quantity;
                            netPriceText.setText(String.valueOf(unitPrice));
                            proPrice = Integer.parseInt(prices[position]);
                        }

                        for (int i = 0; i < row.size(); i++) {

                            HashMap<String, Object> roww = row.get(i);

                            int id = (int) roww.get("layout_id");

                            if (id == itemDialogLO.getId()) {
                                roww.put("strength", strengthSpinner.getSelectedItem());
                                roww.put("weight", weightSpinner.getSelectedItem());
                                roww.put("quantity", quantityText.getText());
                                if (prices[position].equals("NA")) {
                                    roww.put("price", proPrice);
                                } else {
//                                    roww.put("price", prices[position]);

                                    roww.put("price",
                                            Integer.parseInt(netPriceText.getText().toString()) /
                                                    Integer.parseInt(quantityText.getText().toString()));
                                    roww.put("net_price",
                                            Integer.parseInt(netPriceText.getText().toString()));
                                }
                            }
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

                HashMap<String, Object> rowData = new HashMap<String, Object>();

                rowData.put("layout_id", itemDialogLO.getId());
//                Log.e("rowLayoutId: ", String.valueOf(rowLayout.getId()));

                rowData.put("weight", weightSpinner.getSelectedItem());
                rowData.put("quantity", quantityText.getText());

                row.add(rowData);
            } else if (category.equals("ROLLERS")) {
                Log.e(TAG, "createRow: ROLLERS");

                strenghtLO.setVisibility(View.GONE);
                weightLO.setVisibility(View.VISIBLE);
//              priceLO.setVisibility(View.VISIBLE);

                String[] pacArray = this.sizes.toString().split(",");

                List<String> weights = new ArrayList<String>();
                for (String s : pacArray) {
                    weights.add(s);
                }

                ArrayAdapter<String> weightAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, weights);

                weightAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                weightSpinner.setDropDownWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
                weightSpinner.setAdapter(weightAdapter);
                int j = 0;
                for (String weight : weights) {
                    if (roww.get("weight").equals(weight)) {
                        int spinnerPosition = weightAdapter.getPosition(weight);
//                        weightSpinner.setPrompt(strengthAdapter.getItem(j));
                        Log.e(TAG, "spinner pos: " + (j));
                        Log.e(TAG, "spinner pos: " + weight);

                        weightSpinner.setSelection(j);

                        break;

                    }
                    j++;
                }

                weightSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {

                        Log.e(TAG, position + "onItemSelected: " + prices.length);
                        try {
                            if (prices[position].equals("NA")) {
                                priceLO.setVisibility(View.VISIBLE);
                                priceText.setText("--");
                                netPriceText.setText("--");
                                proPrice = 0;
                            } else {
                                priceLO.setVisibility(View.VISIBLE);
                                priceText.setText(prices[position]);
                                int quantity = Integer.parseInt(quantityText.getText().toString());
                                int unitPrice = Integer.parseInt(priceText.getText().toString());

                                unitPrice *= quantity;
                                netPriceText.setText(String.valueOf(unitPrice));
                                proPrice = Integer.parseInt(prices[position]);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        for (int i = 0; i < row.size(); i++) {

                            HashMap<String, Object> roww = row.get(i);

                            int id = (int) roww.get("layout_id");

                            if (id == itemDialogLO.getId()) {
                                roww.put("strength", strengthSpinner.getSelectedItem());
                                roww.put("weight", weightSpinner.getSelectedItem());
                                roww.put("quantity", quantityText.getText());
                                try {
                                    if (prices[position].equals("NA")) {
                                        roww.put("price", proPrice);
                                    } else {
                                        roww.put("price",
                                                Integer.parseInt(netPriceText.getText().toString()) /
                                                        Integer.parseInt(quantityText.getText().toString()));
                                        roww.put("net_price",
                                                Integer.parseInt(netPriceText.getText().toString()));
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

                HashMap<String, Object> rowData = new HashMap<String, Object>();

                rowData.put("layout_id", itemDialogLO.getId());
//                Log.e("rowLayoutId: ", String.valueOf(rowLayout.getId()));

                rowData.put("weight", weightSpinner.getSelectedItem());
                rowData.put("quantity", quantityText.getText());

                row.add(rowData);
            }

            Log.e(TAG, "createRow:s " + (CharSequence) roww.get("quantity"));
            quantityText.setText((CharSequence) roww.get("quantity"));

            increaseQuantity.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int quantity = Integer.parseInt(quantityText.
                                                                        getText().toString());

                    quantity++;
                    quantityText.setText(String.valueOf(quantity));

                    for (int i = 0; i < row.size(); i++) {

                        HashMap<String, Object> roww = row.get(i);

                        int id = (int) roww.get("layout_id");

                        if (id == itemDialogLO.getId()) {
                            roww.put("strength", strengthSpinner.getSelectedItem());
                            roww.put("weight", weightSpinner.getSelectedItem());
                            roww.put("quantity", quantityText.getText());
                        }
                    }

                    if (proPrice != 0) {
                        int unitPrice = Integer.parseInt(priceText.getText().toString());

                        unitPrice *= quantity;
                        netPriceText.setText(String.valueOf(unitPrice));
                    }
                }
            });

            decreaseQuantity.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int quantity = Integer.parseInt(quantityText.
                                                                        getText().toString());

                    if (quantity > 0) {
                        quantity--;
                    }
                    quantityText.setText(String.valueOf(quantity));

                    for (int i = 0; i < row.size(); i++) {

                        HashMap<String, Object> roww = row.get(i);

                        int id = (int) roww.get("layout_id");

                        if (id == itemDialogLO.getId()) {
                            roww.put("strength", strengthSpinner.getSelectedItem());
                            roww.put("weight", weightSpinner.getSelectedItem());
                            roww.put("quantity", quantityText.getText());
                        }
                    }

                    if (proPrice != 0) {
                        int unitPrice = Integer.parseInt(priceText.getText().toString());

                        unitPrice *= quantity;
                        netPriceText.setText(String.valueOf(unitPrice));
                    }
                }
            });

//            addItem = (Button) itemDialogLO.findViewById(R.id.dialog_item_add);
            RelativeLayout removeItemLO = (RelativeLayout) itemDialogLO.findViewById(R.id.remove_layout);
            removeItem = (ImageView) itemDialogLO.findViewById(R.id.dialog_item_remove);

            verifyDialogListSize();

            if (curItem == lastItem) {
                removeItem.setVisibility(View.GONE);

            } else {
                removeItem.setVisibility(View.VISIBLE);
                curItem++;
            }

            removeItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    for (int i = 0; i < row.size(); i++) {
                        if (row.size() == 1) {
                            removeItem.setVisibility(View.GONE);
                        } else {
                            removeItem.setVisibility(View.VISIBLE);
                            HashMap<String, Object> roww = row.get(i);

                            int id = (int) roww.get("layout_id");

                            if (id == itemDialogLO.getId()) {
                                row.remove(i);

                                items.removeView(itemDialogLO);
                            }

                            verifyDialogListSize();
                        }
                    }
                }
            });

            items.addView(itemDialogLO);
        }
    }

    public void createRow() {
        try {
            final RelativeLayout itemDialogLO = (RelativeLayout) View.inflate(context,
                    R.layout.item_dialog, null);
            itemDialogLO.setId(View.generateViewId());

            LinearLayout topLO = (LinearLayout) itemDialogLO.findViewById(R.id.top);
            topLO.setId(View.generateViewId());

            LinearLayout strenghtLO = (LinearLayout) itemDialogLO.findViewById(R.id.strength_layout);
            LinearLayout weightLO = (LinearLayout) itemDialogLO.findViewById(R.id.weight_layout);
            LinearLayout quantityLO = (LinearLayout) itemDialogLO.findViewById(R.id.quantity_layout);
            final LinearLayout priceLO = (LinearLayout) itemDialogLO.findViewById(R.id.price_layout);

            final Spinner strengthSpinner = (Spinner) strenghtLO.findViewById(R.id.strength_spinner);
            final Spinner weightSpinner = (Spinner) weightLO.findViewById(R.id.weight_spinner);
            final TextView quantityText = (TextView) quantityLO.findViewById(R.id.quantity_text);
            final TextView priceText = (TextView) priceLO.findViewById(R.id.price_text);
            final TextView netPriceText = (TextView) priceLO.findViewById(R.id.net_price_text);

            ImageView increaseQuantity = (ImageView) quantityLO.findViewById(R.id.increase);
            ImageView decreaseQuantity = (ImageView) quantityLO.findViewById(R.id.decrease);

            if (category.equals("CHEMICAL PEEL")) {
                strenghtLO.setVisibility(View.VISIBLE);
                weightLO.setVisibility(View.VISIBLE);
                priceLO.setVisibility(View.GONE);

                String[] strArray = this.strengths.toString().split(",");

                List<String> strengths = new ArrayList<String>();
                for (String s : strArray) {
                    strengths.add(s);
                }

                ArrayAdapter<String> strengthAdapter = new ArrayAdapter<String>(context,
                                                                                       android.R.layout.simple_spinner_item, strengths);

                strengthAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                strengthSpinner.setDropDownWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
                strengthSpinner.setAdapter(strengthAdapter);

                final List<String> weights = new ArrayList<String>();

                strengthSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                        weights.clear();

                        if (strengthSpinner.getSelectedItem().equals("Custom")) {

                            if (custom_packs.toString().contains(",")) {
                                String[] pacArray = custom_packs.toString().split(",");

                                for (String s : pacArray) {
                                    weights.add(s);
                                }
                            } else {
                                weights.add(custom_packs.toString());
                            }
                        } else {
                            if (packs.toString().contains(",")) {
                                String[] pacArray = packs.toString().split(",");

                                for (String s : pacArray) {
                                    weights.add(s);
                                }
                            } else {
                                weights.add(packs.toString());
                            }
                        }

                        for (int i = 0; i < row.size(); i++) {

                            HashMap<String, Object> roww = row.get(i);

                            int id = (int) roww.get("layout_id");

                            if (id == itemDialogLO.getId()) {
                                roww.put("strength", adapterView.getItemAtPosition(position));
                                roww.put("weight", weightSpinner.getSelectedItem());
                                roww.put("quantity", quantityText.getText());
                            }
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

                if (packs.toString().contains(",")) {
                    String[] pacArray = packs.toString().split(",");

                    for (String s : pacArray) {
                        weights.add(s);
                    }
                } else {
                    weights.add(packs.toString());
                }

                ArrayAdapter<String> weightAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, weights);

                weightAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                weightSpinner.setDropDownWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
                weightSpinner.setAdapter(weightAdapter);
//            weightSpinner.setPrompt(weightAdapter.getItem(0));
                weightSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                        weightSpinner.setPrompt((CharSequence) weightSpinner.getSelectedItem());

                        for (int i = 0; i < row.size(); i++) {

                            HashMap<String, Object> roww = row.get(i);

                            int id = (int) roww.get("layout_id");

                            if (id == itemDialogLO.getId()) {
                                roww.put("strength", strengthSpinner.getSelectedItem());
                                roww.put("weight", adapterView.getItemAtPosition(position));
                                roww.put("quantity", quantityText.getText());
                            }
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

                HashMap<String, Object> rowData = new HashMap<String, Object>();

                rowData.put("layout_id", itemDialogLO.getId());

                rowData.put("strength", strengthSpinner.getSelectedItem());
                rowData.put("weight", weightSpinner.getSelectedItem());
                rowData.put("quantity", quantityText.getText());

                row.add(rowData);
                Log.e("rowData", String.valueOf(row));

            } else if (category.equals("SERUMS - SKIN CARE")
                               || category.equals("SERUMS - HAIR CARE")
                               || category.equals("CREAMS")) {
                strenghtLO.setVisibility(View.GONE);
                weightLO.setVisibility(View.VISIBLE);

                String[] pacArray = packs.toString().split(",");

                List<String> weights = new ArrayList<String>();
                for (String s : pacArray) {
                    weights.add(s);
                }

                ArrayAdapter<String> weightAdapter = new ArrayAdapter<String>(context,
                                                                                     android.R.layout.simple_spinner_item, weights);

                weightAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                weightSpinner.setDropDownWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
                weightSpinner.setAdapter(weightAdapter);
                weightSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {

                        if (prices[position].equals("NA")) {
                            priceLO.setVisibility(View.VISIBLE);
                            priceText.setText("--");
                            netPriceText.setText("--");
                            proPrice = 0;
                        } else {
                            priceLO.setVisibility(View.VISIBLE);
                            priceText.setText(prices[position]);
                            int quantity = Integer.parseInt(quantityText.getText().toString());
                            int unitPrice = Integer.parseInt(priceText.getText().toString());

                            unitPrice *= quantity;
                            netPriceText.setText(String.valueOf(unitPrice));
                            proPrice = Integer.parseInt(prices[position]);
                        }

                        for (int i = 0; i < row.size(); i++) {

                            HashMap<String, Object> roww = row.get(i);

                            int id = (int) roww.get("layout_id");

                            if (id == itemDialogLO.getId()) {
                                roww.put("strength", strengthSpinner.getSelectedItem());
                                roww.put("weight", weightSpinner.getSelectedItem());
                                roww.put("quantity", quantityText.getText());

                            if (prices[position].equals("NA")) {
                                roww.put("price", proPrice);//
                            } else {
//                                    roww.put("price", prices[position]);
                                roww.put("price",
                                        Integer.parseInt(netPriceText.getText().toString()) /
                                                Integer.parseInt(quantityText.getText().toString()));
                                roww.put("net_price",
                                        Integer.parseInt(netPriceText.getText().toString()));
                            }
                        }
                    }
                }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

                HashMap<String, Object> rowData = new HashMap<String, Object>();

                rowData.put("layout_id", itemDialogLO.getId());

                Log.e("onClick: ", weightSpinner.getSelectedItem().toString());

                rowData.put("weight", weightSpinner.getSelectedItem());
                rowData.put("quantity", quantityText.getText());

                row.add(rowData);
                Log.e("rowData", String.valueOf(row));
            } else if (category.equals("ROLLERS")) {
                strenghtLO.setVisibility(View.GONE);
                weightLO.setVisibility(View.VISIBLE);
//              priceLO.setVisibility(View.VISIBLE);

                String[] pacArray = this.sizes.toString().split(",");

                List<String> weights = new ArrayList<String>();
                for (String s : pacArray) {
                    weights.add(s);
                }

                ArrayAdapter<String> weightAdapter = new ArrayAdapter<String>(context,
                                                                                     android.R.layout.simple_spinner_item, weights);

                weightAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                weightSpinner.setDropDownWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
                weightSpinner.setAdapter(weightAdapter);
                weightSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                        try {
                            if (prices[position].equals("NA")) {
                                priceLO.setVisibility(View.VISIBLE);
                                priceText.setText("--");
                                netPriceText.setText("--");
                                proPrice = 0;
                            } else {
                                priceLO.setVisibility(View.VISIBLE);
                                priceText.setText(prices[position]);
                                int quantity = Integer.parseInt(quantityText.getText().toString());
                                int unitPrice = Integer.parseInt(priceText.getText().toString());

                                unitPrice *= quantity;
                                netPriceText.setText(String.valueOf(unitPrice));
                                proPrice = Integer.parseInt(prices[position]);
                            }

                            for (int i = 0; i < row.size(); i++) {

                                HashMap<String, Object> roww = row.get(i);

                                int id = (int) roww.get("layout_id");

                            if (id == itemDialogLO.getId()) {
                                roww.put("strength", strengthSpinner.getSelectedItem());
                                roww.put("weight", weightSpinner.getSelectedItem());
                                roww.put("quantity", quantityText.getText());
                                if (prices[position].equals("NA")) {
                                    roww.put("price", proPrice);//
                                } else {
//                                    roww.put("price", prices[position]);
                                    roww.put("price",
                                            Integer.parseInt(netPriceText.getText().toString()) /
                                                    Integer.parseInt(quantityText.getText().toString()));
                                    roww.put("net_price",
                                            Integer.parseInt(netPriceText.getText().toString()));
                                }
                            }
                        }
                    } catch (Exception e) {
                        Log.e("onItemSelected_weight", e.toString());
                    }
                }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

                HashMap<String, Object> rowData = new HashMap<String, Object>();

                rowData.put("layout_id", itemDialogLO.getId());
//                Log.e("rowLayoutId: ", String.valueOf(rowLayout.getId()));

                rowData.put("weight", weightSpinner.getSelectedItem());
                rowData.put("quantity", quantityText.getText());

                row.add(rowData);
                Log.e("rowData", String.valueOf(row));
            }

            quantityText.setText("1");

            increaseQuantity.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int quantity = Integer.parseInt(quantityText.
                                                                        getText().toString());

                    quantity++;
                    quantityText.setText(String.valueOf(quantity));

                    for (int i = 0; i < row.size(); i++) {

                        HashMap<String, Object> roww = row.get(i);

                        int id = (int) roww.get("layout_id");

                        if (id == itemDialogLO.getId()) {
                            roww.put("strength", strengthSpinner.getSelectedItem());
                            roww.put("weight", weightSpinner.getSelectedItem());
                            roww.put("quantity", quantityText.getText());
                        }
                    }

                    if (proPrice != 0) {
                        int unitPrice = Integer.parseInt(priceText.getText().toString());

                        unitPrice *= quantity;
                        netPriceText.setText(String.valueOf(unitPrice));
                    }
                }
            });

            decreaseQuantity.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int quantity = Integer.parseInt(quantityText.getText().toString());

                    if (quantity > 0) {
                        quantity--;
                    }
                    quantityText.setText(String.valueOf(quantity));

                    for (int i = 0; i < row.size(); i++) {

                        HashMap<String, Object> roww = row.get(i);

                        int id = (int) roww.get("layout_id");

                        if (id == itemDialogLO.getId()) {
                            roww.put("strength", strengthSpinner.getSelectedItem());
                            roww.put("weight", weightSpinner.getSelectedItem());
                            roww.put("quantity", quantityText.getText());
                        }
                    }

                    if (proPrice != 0) {
                        int unitPrice = Integer.parseInt(priceText.getText().toString());

                        unitPrice *= quantity;
                        netPriceText.setText(String.valueOf(unitPrice));
                    }
                }
            });

            RelativeLayout removeItemLO = (RelativeLayout) itemDialogLO.findViewById(R.id.remove_layout);
            removeItem = (ImageView) itemDialogLO.findViewById(R.id.dialog_item_remove);

            verifyDialogListSize();

            removeItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    int lastItem = row.size() - 1;

                    for (int i = 0; i < row.size(); i++) {
                        if (row.size() == 1) {
                            removeItem.setVisibility(View.GONE);
                        } else {
                            removeItem.setVisibility(View.VISIBLE);
                            HashMap<String, Object> roww = row.get(i);

                            int id = (int) roww.get("layout_id");

                            if (id == itemDialogLO.getId()) {
                                row.remove(i);

                                items.removeView(itemDialogLO);
                            }

                            verifyDialogListSize();
                        }
                    }
                }
            });

            items.addView(itemDialogLO);
        } catch (Exception e) {
            Log.e("createRow: ", e.toString());
        }
    }

    void verifyDialogListSize() {

        if (row.size() > 1) {
            removeItem.setVisibility(View.VISIBLE);
        } else {
            removeItem.setVisibility(View.GONE);
        }
    }

}
