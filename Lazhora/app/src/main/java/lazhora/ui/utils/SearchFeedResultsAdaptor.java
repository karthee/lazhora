package lazhora.ui.utils;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.View;
import android.widget.TextView;

import lazhora.R;

import static lazhora.ui.db.DBManager.KEY_PRODUCT_TAG;


/**
 * The type Search feed results adaptor.
 */
public class SearchFeedResultsAdaptor extends SimpleCursorAdapter {
    private static final String tag = SearchFeedResultsAdaptor.class.getName();
    private Context context = null;

    /**
     * Instantiates a new Search feed results adaptor.
     *
     * @param context the context
     * @param layout  the layout
     * @param c       the c
     * @param from    the from
     * @param to      the to
     * @param flags   the flags
     */
    public SearchFeedResultsAdaptor(Context context, int layout, Cursor c, String[] from, int[] to, int flags) {
        super(context, layout, c, from, to, flags);
        this.context = context;
    }


    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView title = (TextView) view.findViewById(R.id.textView3);
        title.setText(cursor.getString(2));
        TextView desc = (TextView) view.findViewById(R.id.desc);
        desc.setText(cursor.getString(cursor.getColumnIndex(KEY_PRODUCT_TAG)));


    }
}