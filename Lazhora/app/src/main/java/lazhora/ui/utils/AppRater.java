package lazhora.ui.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Button;

import lazhora.R;
import lazhora.ui.Screens.Feedback;

/**
 * The type App rater.
 */
public class AppRater {
    private final static String APP_TITLE = "Lazhora";// App Name
    private final static String APP_PNAME = "lazhora.app";// Package Name

    private final static int DAYS_UNTIL_PROMPT = 3;//Min number of days
    private final static int LAUNCHES_UNTIL_PROMPT = 3;//Min number of launches

    /**
     * App launched.
     *
     * @param mContext the m context
     */
    public static void app_launched(Context mContext) {
        SharedPreferences prefs = mContext.getSharedPreferences("apprater", 0);
        if (prefs.getBoolean("dontshowagain", false)) {
            return;
        }

        SharedPreferences.Editor editor = prefs.edit();

        // Increment launch counter
        long launch_count = prefs.getLong("launch_count", 0) + 1;
        editor.putLong("launch_count", launch_count);

        // Get date of first launch
        Long date_firstLaunch = prefs.getLong("date_firstlaunch", 0);
        if (date_firstLaunch == 0) {
            date_firstLaunch = System.currentTimeMillis();
            editor.putLong("date_firstlaunch", date_firstLaunch);
        }

        // Wait at least n days before opening
        if (launch_count >= LAUNCHES_UNTIL_PROMPT) {
            Log.e("rate dialog", String.valueOf(System.currentTimeMillis())+" >= "+date_firstLaunch +" + "+
                                                                       (DAYS_UNTIL_PROMPT * 24 * 60 * 60 * 1000));
            if (System.currentTimeMillis() >= date_firstLaunch +
                                                      (DAYS_UNTIL_PROMPT * 24 * 60 * 60 * 1000)) {
                showRateDialog(mContext, editor);
                editor.putLong("date_firstlaunch", System.currentTimeMillis());

            }
        }

        editor.commit();
    }

    /**
     * Show rate dialog.
     *
     * @param mContext the m context
     * @param editor   the editor
     */
    public static void showRateDialog(final Context mContext, final SharedPreferences.Editor editor) {




//        Button b3 = new Button(mContext);
//        b3.setText("No, thanks");
//        b3.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                if (editor != null) {
//                    editor.putBoolean("dontshowagain", true);
//                    editor.commit();
//                }
//            }
//        });

        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);

        // Setting Dialog Title
//        alertDialog.setTitle(title);

        // Setting Dialog Message
        alertDialog.setMessage("If you enjoy using " + APP_TITLE + ", please take a moment to rate it. Thanks for your support!");

        // Setting alert dialog icon
        //	alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);

        // Setting Yes Button
        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent feedback = new Intent(mContext, Feedback.class);
                mContext.startActivity(feedback);
                ((Activity) mContext).finish();

            }
        });


        // Setting No Button
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });


        // Showing Alert Message
        AlertDialog alert = alertDialog.create();
        alert.show();
        Button pbutton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        pbutton.setTextColor(mContext.getResources().getColor(R.color.colorPrimary));
        Button nbutton = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
        nbutton.setTextColor(mContext.getResources().getColor(R.color.colorPrimary));
        Button neubutton = alert.getButton(DialogInterface.BUTTON_NEUTRAL);
        neubutton.setTextColor(mContext.getResources().getColor(R.color.colorPrimary));
    }
}