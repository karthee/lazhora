package mycart.utils;

import android.content.Context;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by Karthee on 21/03/17.
 */

public class SystemUtils {
    public static void saveData(Context context, String fileName, String mJsonResponse) {
        try {
            FileWriter file = new FileWriter(context.getFilesDir().getPath() + "/" + fileName);
            file.write(mJsonResponse);
            file.flush();
            file.close();
            Log.e("Write Success_OH.", String.valueOf(file));

        } catch (IOException e) {
            Log.e("Error in Writing_OH.", e.getLocalizedMessage());
        }
    }

    public static String getData(Context context) {
        try {
            File f = new File(context.getFilesDir().getPath() + "/" + "mycart.json");
            //check whether file exists
            FileInputStream is = new FileInputStream(f);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();

            Log.e("Read Success", new String(buffer, "UTF-8"));
            return new String(buffer, "UTF-8");
        } catch (IOException e) {
            Log.e("File not exist", e.getLocalizedMessage());
            return null;
        }
    }
}
