package lazhora.ui.Screens;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.MenuItem;
import android.widget.TextView;

import lazhora.R;
import lazhora.ui.utils.Font;


/**
 * Created by Arivu on 27-09-2016.
 */
public class AboutUs extends AppCompatActivity {

    /**
     * The About body.
     */
    TextView aboutBody;
    /**
     * The Font.
     */
    Font font;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aboutus);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        font = new Font(AboutUs.this);

        SpannableString pageTitle = new SpannableString(getSupportActionBar().getTitle());
        pageTitle.setSpan(font.robotoMedium, 0, pageTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        getSupportActionBar().setTitle(pageTitle);

        aboutBody = (TextView) findViewById(R.id.about_body);

        aboutBody.setTypeface(font.robotoRegular);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
//        Intent i = new Intent(AboutUs.this, HomeActivity.class);
//        startActivity(i);
        finish();
    }
}
