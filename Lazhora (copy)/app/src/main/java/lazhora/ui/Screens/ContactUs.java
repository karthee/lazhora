package lazhora.ui.Screens;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import lazhora.R;
import lazhora.ui.utils.Font;

/**
 * Created by Arivu on 26-09-2016.
 */

public class ContactUs extends AppCompatActivity implements OnMapReadyCallback {

    TextView officeLabel, officeNumber, faxLabel, faxNumber, ccLabel1, ccLabel2, ccNumber,
            cpLabel, cpName1, cpName2, cpName3, cpName4, cpNumber1,
            cpNumber2, cpNumber3, cpNumber4, cpVid1,
            addressLabel, addressText, emailLabel, emailId;
    ImageView facebookId, webUrl;
    Font font;
    private GoogleMap mMap;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contactus);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        font = new Font(ContactUs.this);

        SpannableString pageTitle = new SpannableString(getSupportActionBar().getTitle());
        pageTitle.setSpan(font.robotoMedium, 0, pageTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        getSupportActionBar().setTitle(pageTitle);

        officeLabel = (TextView) findViewById(R.id.contact_office_label);
        officeNumber = (TextView) findViewById(R.id.contact_office_number);

        faxLabel = (TextView) findViewById(R.id.contact_fax_label);
        faxNumber = (TextView) findViewById(R.id.contact_fax_number);

        ccLabel1 = (TextView) findViewById(R.id.contact_cc1_label);
        ccLabel2 = (TextView) findViewById(R.id.contact_cc2_label);
        ccNumber = (TextView) findViewById(R.id.contact_cc_number);

        cpLabel = (TextView) findViewById(R.id.contact_cp_label);

        cpName1 = (TextView) findViewById(R.id.contact_cp_name1);
        cpName2 = (TextView) findViewById(R.id.contact_cp_name2);
        cpName3 = (TextView) findViewById(R.id.contact_cp_name3);
        cpName4 = (TextView) findViewById(R.id.contact_cp_name4);

        cpNumber1 = (TextView) findViewById(R.id.contact_cp_no1);
        cpNumber2 = (TextView) findViewById(R.id.contact_cp_no2);
        cpNumber3 = (TextView) findViewById(R.id.contact_cp_no3);
        cpNumber4 = (TextView) findViewById(R.id.contact_cp_no4);
        cpVid1 = (TextView) findViewById(R.id.contact_cp_vid1);
        //cpVid2 = (TextView) findViewById(R.id.contact_cp_vid2);

        addressLabel = (TextView) findViewById(R.id.contact_address_label);
        addressText = (TextView) findViewById(R.id.contact_address_text);

        emailLabel = (TextView) findViewById(R.id.contact_email_label);
        emailId = (TextView) findViewById(R.id.contact_email_id);

        facebookId = (ImageView) findViewById(R.id.contact_facebook_id);
        webUrl = (ImageView) findViewById(R.id.contact_weburl);

        /*//Linkify.addLinks(facebookId, Linkify.ALL);
        //facebookId.setMovementMethod(LinkMovementMethod.getInstance());
        Linkify.addLinks(webUrl, Linkify.ALL);
        webUrl.setMovementMethod(LinkMovementMethod.getInstance());*/

        officeLabel.setTypeface(font.robotoRegular);
        faxLabel.setTypeface(font.robotoRegular);
        ccLabel1.setTypeface(font.robotoRegular);
        cpLabel.setTypeface(font.robotoRegular);
        cpName1.setTypeface(font.robotoRegular);
        cpName2.setTypeface(font.robotoRegular);
        cpName3.setTypeface(font.robotoRegular);
        cpName4.setTypeface(font.robotoRegular);
        addressLabel.setTypeface(font.robotoRegular);
        emailLabel.setTypeface(font.robotoRegular);

        officeNumber.setTypeface(font.robotoMedium);
        faxNumber.setTypeface(font.robotoMedium);
        ccNumber.setTypeface(font.robotoMedium);
        cpNumber1.setTypeface(font.robotoMedium);
        cpNumber2.setTypeface(font.robotoMedium);
        cpNumber3.setTypeface(font.robotoMedium);
        cpNumber4.setTypeface(font.robotoMedium);
        cpVid1.setTypeface(font.robotoMedium);
        //cpVid2.setTypeface(font.robotoMedium);
        addressText.setTypeface(font.robotoMedium);
        emailId.setTypeface(font.robotoMedium);
        //facebookId.setTypeface(font.robotoMedium);
        //webUrl.setTypeface(font.robotoMedium);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        facebookId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse("http://www.facebook.com/lazhora/"));
                startActivity(intent);
            }
        });

        webUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse("http://www.lazhora.in"));
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
//        Intent i = new Intent(ContactUs.this, HomeActivity.class);
//        startActivity(i);
        finish();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LatLng lazhora = new LatLng(12.954719, 80.245119);
        mMap.addMarker(new MarkerOptions().position(lazhora).title("Lazhora Medical Cosmetics")).showInfoWindow();
        mMap.moveCamera(CameraUpdateFactory.newLatLng(lazhora));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(lazhora, 15));
        mMap.getUiSettings().setMapToolbarEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setAllGesturesEnabled(true);

    }
}
