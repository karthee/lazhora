package com.arivulabs.lms.mycart;

/**
 * Created by Arivu on 25-10-2016.
 */

public class JsonDataCopy {
    public static String jsondata = "{\n" +
            "  \"status\": true,\n" +
            "  \"data\": [\n" +
            "    {\n" +
            "      \"product_id\": 1,\n" +
            "      \"product_title\": \"Zhora Glyco Peel\",\n" +
            "      \"tag\": \"aging skin, skin rejuvenation, fine lines\",\n" +
            "      \"product_desc\": \"<h3>Description</h3>\\n\\n<p>Glycolic acid peel (alpha hydroxy acid peel) is a superficial chemical peel that removes the top layer of skin. The glycolic peel process triggers the production of new collagen and elastin and, as a result, erases fine lines and wrinkles, evens out skin discoloration,and improves skin texture.  It is the best-known Alpha-hydroxy acid (AHA) is derived from sugar cane.</p>\\n\\n<h3>Indication</h3>\\n\\n<ul>\\n<li>Aging skin </li>\\n<li>Stimulate collagen production</li>\\n<li>Skin rejuvenation </li>\\n<li>Fine lines</li>\\n<li>Age spots </li>\\n<li>Pigmentation </li>\\n<li>Acne</li>\\n</ul>\\n\\n<h3>Available strengths</h3>\\n\\n<p>20% , 35%, 50% , 70% </p>\",\n" +
            "      \"picture\": \"zhora_glycopeel.png\",\n" +
            "      \"picture_name\": \"zhora_glycopeel_pn.png\",\n" +
            "      \"category\": \"CHEMICAL PEEL\",\n" +
            "      \"strength\": [\n" +
            "        \"20%\",\n" +
            "        \"35%\",\n" +
            "        \"50%\",\n" +
            "        \"70%\"\n" +
            "      ],\n" +
            "      \"pack\": [\n" +
            "        \"30ml\",\n" +
            "        \"50ml\"\n" +
            "      ],\n" +
            "      \"custom_pack\": \"NA\",\n" +
            "      \"minimum_quantity\": \"NA\",\n" +
            "      \"sizes\": \"NA\",\n" +
            "      \"price\": \"NA\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"product_id\": 2,\n" +
            "      \"product_title\": \"Zhora Salic Peel\",\n" +
            "      \"tag\": \"skin texture, acne, oily skin\",\n" +
            "      \"product_desc\": \"<h3>Description</h3>\\n\\n<p>Salicylic acid works by softening keratin, a protein that forms part of the skin structure. This helps to loosen dry scaly skin making it easier to remove. When salicylic acid is used in combination with other medicines it takes off the upper layer of skin allowing the additional medicines to  penetrate more effectively.</p>\\n\\n<h3>Indication</h3>\\n\\n<ul>\\n<li>Salicylic acid exfoliates skin </li>\\n<li>Improve skin texture and color</li>\\n<li>It also helps with acne </li>\\n<li>Oily Skin</li>\\n</ul>\\n\\n<h3>Available strengths</h3>\\n\\n<p>30%, 50% </p>\",\n" +
            "      \"picture\": \"zhora_salicpeel.png\",\n" +
            "      \"picture_name\": \"zhora_salic_peel_pn.png\",\n" +
            "      \"category\": \"CHEMICAL PEEL\",\n" +
            "      \"strength\": [\n" +
            "        \"35%\",\n" +
            "        \"50%\",\n" +
            "        \"Custom\"\n" +
            "      ],\n" +
            "      \"pack\": [\n" +
            "        \"30ml\",\n" +
            "        \"50ml\"\n" +
            "      ],\n" +
            "      \"custom_pack\": [\n" +
            "        \"30ml\",\n" +
            "        \"50ml\"\n" +
            "      ],\n" +
            "      \"minimum_quantity\": \"NA\",\n" +
            "      \"sizes\": \"NA\",\n" +
            "      \"price\": \"NA\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"product_id\": 3,\n" +
            "      \"product_title\": \"Zhora TCA Peel\",\n" +
            "      \"tag\": \"pigmentation\",\n" +
            "      \"product_desc\": \"<h3>Description</h3>\\n\\n<p>It is a very common chemical used to perform medium depth chemical peels. The concentration of the chemical will vary, depending on the desired depth of skin penetration. In general, \\nconcentrations used for such peels vary between 20% and 35%. The procedure results in red, swollen, and crusted skin which peels over the weeks following the procedure. This will help improve the skin texture, tone, and reduce the appearance of fine wrinkles and superficial discoloration</p>\\n\\n<h3>Indication</h3>\\n\\n<p>TCA facial peels are commonly used for</p>\\n\\n<ul>\\n<li>Repair and remove fine facial Lines anywhere on the face!  </li>\\n<li>Removal or decreases any lip creases, crows feet and deep forehead lines  </li>\\n<li>Completely peels &amp; removes dry flaky skin  </li>\\n<li>Corrects acne, pimples, black-heads, and clogged pores by killing the bacteria  </li>\\n<li>Removes freckles and age spots on the face or body</li>\\n<li>Skin lightening and dark spots</li>\\n<li>Removes stretch marks</li>\\n</ul>\\n\\n<h3>Available strengths</h3>\\n\\n<p>15% , 25% , 35%</p>\\n\\n\\n\",\n" +
            "      \"picture\": \"zhora_tca_peel.png\",\n" +
            "      \"picture_name\": \"zhora_tca_peel_pn.png\",\n" +
            "      \"category\": \"CHEMICAL PEEL\",\n" +
            "      \"strength\": [\n" +
            "        \"15%\",\n" +
            "        \"25%\",\n" +
            "        \"35%,Custom\"\n" +
            "      ],\n" +
            "      \"pack\": [\n" +
            "        \"30ml\",\n" +
            "        \"50ml\"\n" +
            "      ],\n" +
            "      \"custom_pack\": [\n" +
            "        \"10ml x5\",\n" +
            "        \"30ml\",\n" +
            "        \"50ml\"\n" +
            "      ],\n" +
            "      \"minimum_quantity\": \"NA\",\n" +
            "      \"sizes\": \"NA\",\n" +
            "      \"price\": \"NA\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"product_id\": 4,\n" +
            "      \"product_title\": \"Fusion Peel Pack\",\n" +
            "      \"tag\": \"under eye peel, melasma peel, active acne, acne scar, brighten peel, dark circle, freckles, pimples, post acne pigmentation, party peel\",\n" +
            "      \"product_desc\": \"<h3>Description</h3>\\n<table style=\\\"border-collapse: collapse;border: 1px solid black;\\\">\\n<thead>\\n<tr>\\n<th style=\\\"text-align:center;border: 1px solid black;\\\">PEELS</th>\\n<th style=\\\"text-align:center;border: 1px solid black;\\\">INGREDIENTS</th>\\n<th style=\\\"text-align:center;border: 1px solid black;\\\">INDICATIONS</th>\\n</tr>\\n</thead>\\n<tbody>\\n<tr>\\n<td style=\\\"text-align:center;border: 1px solid black;\\\">FUSION PEEL -A</td>\\n<td style=\\\"text-align:left;border: 1px solid black;\\\">Lactic 45%+Citric 5%+Pyruvic 5%+Arginine 5%(Hydroalchol Base)</td>\\n<td style=\\\"text-align:left;border: 1px solid black;\\\">Wrinckles,Hyper Pigmentation, Dark Circle,Aging skin</td>\\n</tr>\\n<tr>\\n<td style=\\\"text-align:center;border: 1px solid black;\\\">FUSION PEEL -B</td>\\n<td style=\\\"text-align:left;border: 1px solid black;\\\">Lactic 15% + Kojic 5% + Citric 5%+HQ 2% + Sal 2%</td>\\n<td style=\\\"text-align:left;border: 1px solid black;\\\">Skin Lightening,Melasma,Photo Damaged skin</td>\\n</tr>\\n<tr>\\n<td style=\\\"text-align:center;border: 1px solid black;\\\">FUSION PEEL -C</td>\\n<td style=\\\"text-align:left;border: 1px solid black;\\\">Sal 17%+Lactic 17%+citric 8% + Gly 15%</td>\\n<td style=\\\"text-align:left;border: 1px solid black;\\\">Anti acne,Hyper Pigmentation,Anti Aging,Freckles</td>\\n</tr>\\n<tr>\\n<td style=\\\"text-align:center;border: 1px solid black;\\\">FUSION PEEL -D</td>\\n<td style=\\\"text-align:left;border: 1px solid black;\\\">Sal 20% +Man 10%</td>\\n<td style=\\\"text-align:left;border: 1px solid black;\\\">Stretch Marks,Scaring,inflamatory acne with post inflammatory hyper pigmentation</td>\\n</tr>\\n<tr>\\n<td style=\\\"text-align:center;border: 1px solid black;\\\">FUSION PEEL -E</td>\\n<td style=\\\"text-align:left;border: 1px solid black;\\\">Gly 35% +Kojic 10% +Pyruvic 2% + Lactic 10%</td>\\n<td style=\\\"text-align:left;border: 1px solid black;\\\">Skin Rejuvenation,Superfacial wrinckles</td>\\n</tr>\\n</tbody>\\n</table>\\n<br/>\\n<br/>\\n\\n\\n<table style=\\\"border-collapse: collapse;border: 1px solid black;\\\"\\\">\\n\\t<thead>\\n\\t\\t<tr>\\n\\t\\t\\t<th style=\\\"text-align:center;border: 1px solid black;\\\">FUSION PEELS</th>\\n\\t\\t\\t<th style=\\\"text-align:center;border: 1px solid black;\\\">pH</th>\\n\\t\\t\\t<th style=\\\"text-align:center;border: 1px solid black;\\\">APPLICATION TIME</th>\\n\\t\\t\\t<th style=\\\"text-align:center;border: 1px solid black;\\\">SESSIONS</th>\\n\\t\\t</tr>\\n\\t</thead>\\n\\t<tr>\\n\\t\\t<td style=\\\"text-align:center;border: 1px solid black;\\\">FUSION -A (under Eye Peel)</td>\\n\\t\\t<td style=\\\"text-align:center;border: 1px solid black;\\\">2.7</td>\\n\\t\\t<td rowspan=\\\"5\\\" style=\\\"text-align:center;border: 1px solid black;\\\">5-8 Minutes</td>\\n\\t\\t<td rowspan=\\\"5\\\" style=\\\"text-align:center;border: 1px solid black;\\\">Repeated every 3 weeks.4-6 sessions are suggested. maintenance peel can be done once in 6 months</td>\\n\\t</tr>\\n\\t<tr>\\n\\t\\t<td style=\\\"text-align:center;border: 1px solid black;\\\">FUSION -B (Melasma Peel) </td>\\n\\t\\t<td style=\\\"text-align:center;border: 1px solid black;\\\">2.5</td>\\n\\t</tr>\\n\\t<tr>\\n\\t\\t<td style=\\\"text-align:center;border: 1px solid black;\\\">FUSION -C (Acne)</td>\\n\\t\\t<td style=\\\"text-align:center;border: 1px solid black;\\\">2</td>\\n\\t</tr>\\n\\t<tr>\\n\\t\\t<td style=\\\"text-align:center;border: 1px solid black;\\\">FUSION -D (Acne Scar)</td>\\n\\t\\t<td style=\\\"text-align:center;border: 1px solid black;\\\">2.35</td>\\n\\t</tr>\\n\\t<tr>\\n\\t\\t<td style=\\\"text-align:center;border: 1px solid black;\\\">FUSION -E (Brighten Peel)</td>\\n\\t\\t<td style=\\\"text-align:center;border: 1px solid black;\\\">2.35</td>\\n\\t</tr>\\n\\t<tr>\\n\\t\\t<td colspan=\\\"4\\\" style=\\\"text-align:center;border: 1px solid black;\\\"> Peels can be custom formulated according to the pH requirement</td>\\n\\t</tr>\\n</table>\",\n" +
            "      \"picture\": \"fusion_peels_combo.png\",\n" +
            "      \"picture_name\": \"zhora_fusion_peels_pn.png\",\n" +
            "      \"category\": \"CHEMICAL PEEL\",\n" +
            "      \"strength\": [\n" +
            "        \"FA Peel(Under Eye)\",\n" +
            "        \"FB Peel(Mela Peel)\",\n" +
            "        \"FC Peel(Acne)\",\n" +
            "        \"FD Peel(Scar)\",\n" +
            "        \"FE Peel(Glow Peel)\"\n" +
            "      ],\n" +
            "      \"pack\": [\n" +
            "        \"30ml\",\n" +
            "        \"50ml\"\n" +
            "      ],\n" +
            "      \"custom_pack\": \"NA\",\n" +
            "      \"minimum_quantity\": \"NA\",\n" +
            "      \"sizes\": \"NA\",\n" +
            "      \"price\": \"NA\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"product_id\": 5,\n" +
            "      \"product_title\": \"Glutathione Peel Pack\",\n" +
            "      \"tag\": \"brightening\",\n" +
            "      \"product_desc\": \"<h3>Description</h3>\\n<p>Glutathione Pack is a whitening and Antioxidant treatment, which is especially indicated to brighten the skin, protecting daily from externalaggressions, providing a healthy and youthful skin.</p>\\n<h3>Recommendations</h3>\\n<ul>\\n<li>Brighten the skin</li>\\n<li>Daily protection</li>\\n<li>Reduce wrinkles</li>\\n<li>Revitalizes the skin.</li>\\n</ul>\\n<p><strong>Glutathione Peeling: (6x5ml)</strong></p>\\n<p>Glutathione Peeling is a powerful antioxidant which provides the skin a radiant glow, neutralizes free radicals and improves coetaneous hyperpigmentation, inhibiting melanin synthesis. Reduces melasma, expression lines and wrinkles, eliminating and preventing acne scar.</p>\\n<p><strong>Post Peeling Mask (6x20ml)</strong></p>\\n<p>The active ingredients of Post-Peeling Mask are decongesting, moisturizing and relaxing,\\nproviding revitalizing properties. Ideal to recover the skin. Important after facial peelings.</p>\\n<p><strong>Glutathione Cream (50ml)</strong></p>\\n<p>Glutathione Cream contains an advanced skin whitening component, with a complete combination of antioxidant such as Glutathione, Myrtus, Allium Cepa and Commiphora Myrrha. Reduces darkening of spots on the skin, maintaining a youthful vibrancy. Restores and regenerates damaged skin, preventing dryness and improving skin smoothness.</p>\\n<p><strong>Neutralizing (50ml)</strong></p>\\n<p>Neutralizes the acid e ect, balancing the skins pH after acid peeling treatment. Providing\\na refreshing and comforting  ect.</p>\",\n" +
            "      \"picture\": \"glutathione_peel_pack.png\",\n" +
            "      \"picture_name\": \"glutathione_peel_pack_pn.png\",\n" +
            "      \"category\": \"CHEMICAL PEEL\",\n" +
            "      \"strength\": \"NA\",\n" +
            "      \"pack\": \"NA\",\n" +
            "      \"custom_pack\": \"NA\",\n" +
            "      \"minimum_quantity\": \"NA\",\n" +
            "      \"sizes\": \"NA\",\n" +
            "      \"price\": \"NA\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"product_id\": 6,\n" +
            "      \"product_title\": \"Master Peel-Blue Peel\",\n" +
            "      \"tag\": \"pigmentation\",\n" +
            "      \"product_desc\": \"<h3>Description</h3>\\n\\n<p>Master Peel exfoliating treatments are appropriate for all skin types, conditions and\\nsensitivities. They will effectively treat acne, pigment disorders, aging skin, smoker’s skin \\nand photo-damaged skin. The treatment is ideal for super-sensitive skin types, as it has \\nlittle to no stringing sensation. It targets patients that want gradual lightening \\nbut instant tightening.</p>\\n\\n<h3>Direction for use</h3>\\n\\n<ul>\\n<li>Cleanse the skin thoroughly Rinse.</li>\\n<li>Apply Master Peel  exfoliating treatment to a cotton pad, placing the\\nneck of the bottle directly onto the pad, spacing it to give a sufficient working area \\nto complete a full-face treatment. (About 4-5 spots on the pad.) Apply an even layer \\nto the area of treatment.</li>\\n<li>Allow the first layer to penetrate. Burning is a sensation they feel. Give\\nthe patient a fan. Allow the layer to penetrate. The skin will have a slightly tacky \\nfeeling. Depending on the patient’s feeling apply another coat after five minutes. \\nApply maximum two layers, on sensitive skin and up to three on resilient.\\nYou may apply Vit C Serum at this point.</li>\\n<li>For additional strengthening, hydrating and anti-inflammatory benefits, apply two \\npumps of serum all over the area of treatment. </li>\\n<li>Finish with a mixture of post peel serum / cream + SPF 30 to seal / occlude the \\ntreated area for 3-5 days, 2 times a day.</li>\\n</ul>\\n\\n<h3>Ingredients</h3>\\n\\n<p>Thrichloro acetic acid</p>\\n\\n<ul>\\n<li>20% </li>\\n<li>30% </li>\\n<li>40%</li>\\n</ul>\\n\\n<p>colour</p>\\n\\n<ul>\\n<li>Phytic colour (blue).</li>\\n</ul>\\n\\n\\n\",\n" +
            "      \"picture\": \"master_peel.png\",\n" +
            "      \"picture_name\": \"master_peel_pn.png\",\n" +
            "      \"category\": \"CHEMICAL PEEL\",\n" +
            "      \"strength\": [\n" +
            "        \"20%\",\n" +
            "        \"40%\"\n" +
            "      ],\n" +
            "      \"pack\": \"100ml\",\n" +
            "      \"custom_pack\": \"NA\",\n" +
            "      \"minimum_quantity\": \"NA\",\n" +
            "      \"sizes\": \"NA\",\n" +
            "      \"price\": \"NA\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"product_id\": 7,\n" +
            "      \"product_title\": \"Pumpkin Peel\",\n" +
            "      \"tag\": \"brightening and lightening\",\n" +
            "      \"product_desc\": \"<h3>Description</h3>\\n\\n<p>Pamper and revitalize tired and dull skin with a blend of natural Pumpkin Enzyme and skin resurfacing acids to smooth fine lines and awaken the cellular turnover process. PowerBlended with natural Alpha and Beta Hydroxy acids as well as vital nutrients, the Pumpkin Treatment is a decadent resurfacing facial that promotes Collagen production and unveils firmer, smoother skin.</p>\\n\\n<h3>Function</h3>\\n\\n<p>Stimulates cell turnover as dead skin cells are dislodged and removed.Improvement of skin texture Gives the skin a lighter, brighter appearance Reduces discoloration and pigmentation\\nImprovement of congested pores and acne Reduces appearance of fine lines Antioxidant properties.</p>\",\n" +
            "      \"picture\": \"pumpkin_peel.png\",\n" +
            "      \"picture_name\": \"pumpkin_peels_pn.png\",\n" +
            "      \"category\": \"CHEMICAL PEEL\",\n" +
            "      \"strength\": \"NA\",\n" +
            "      \"pack\": \"NA\",\n" +
            "      \"custom_pack\": \"NA\",\n" +
            "      \"minimum_quantity\": \"NA\",\n" +
            "      \"sizes\": \"NA\",\n" +
            "      \"price\": \"NA\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"product_id\": 8,\n" +
            "      \"product_title\": \"Retigen Peel\",\n" +
            "      \"tag\": \"fine lines, wrinkles\",\n" +
            "      \"product_desc\": \"<h3>Description</h3>\\n\\n<p>Deliver refreshed and purified skin to clients in need of an additional resurfacing boost due to acne and congestion. Disinfect p. acnes bacteria and clear away unwanted whiteheads, blackheads and other blemishes with a Power Blended formula of natural Alpha and Beta Hydroxy acids while promoting healing with with soothing actives. Provide a corrective and clinical strength solution to acneic skin with the Purification Peel.</p>\\n\\n<h3>Function</h3>\\n\\n<ul>\\n<li>Stimulates cell turnover as dead skin cells are dislodged and removed</li>\\n<li>Improvement of congested pores and acne</li>\\n<li>Decreased sebum production</li>\\n<li>Improvement of skin texture</li>\\n<li>Reduces discoloration and pigmentation</li>\\n<li>Reduction of inflammation</li>\\n</ul>\",\n" +
            "      \"picture\": \"retigen_peel.png\",\n" +
            "      \"picture_name\": \"retigen_peel_pn.png\",\n" +
            "      \"category\": \"CHEMICAL PEEL\",\n" +
            "      \"strength\": \"NA\",\n" +
            "      \"pack\": \"NA\",\n" +
            "      \"custom_pack\": \"NA\",\n" +
            "      \"minimum_quantity\": \"NA\",\n" +
            "      \"sizes\": \"NA\",\n" +
            "      \"price\": \"NA\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"product_id\": 9,\n" +
            "      \"product_title\": \"EGF Nano Serum\",\n" +
            "      \"tag\": \"post procedure & rejuvenation, firming, hydrating, healing Process\",\n" +
            "      \"product_desc\": \"<h3>Description</h3>\\n\\n<p>The EGF Serum is a spot treatment focused to aid in collagen boosting.  EGF , known to speed the natural turnover cycle, applies to the skin while Chondroitin Sulfate, a natural element found in the body, is improved upon and enhances the skin health.  The Bisabolol aims to purify and prevent irritation and inflammation. Chlorophyll encourages a healthy glow while Vitamin E provides deep moisture.</p>\\n\\n<h3>Key Functions</h3>\\n\\n<ul>\\n<li>Regenerating</li>\\n<li>Firming</li>\\n<li>Hydrating</li>\\n</ul>\\n\\n<h3>Key Ingredients</h3>\\n\\n<ul>\\n<li>EGF</li>\\n<li>Chondroitin Sulfate</li>\\n<li>Bisabolol</li>\\n<li>Chlorophyll</li>\\n<li>Vitamin E Concentrate</li>\\n</ul>\\n\\n\\n\",\n" +
            "      \"picture\": \"egf_nano_serum.png\",\n" +
            "      \"picture_name\": \"egf_nano_serum_pn.png\",\n" +
            "      \"category\": \"SERUMS - SKIN CARE\",\n" +
            "      \"strength\": \"NA\",\n" +
            "      \"pack\": [\n" +
            "        \"10ml x10\",\n" +
            "        \"30ml\"\n" +
            "      ],\n" +
            "      \"custom_pack\": \"NA\",\n" +
            "      \"minimum_quantity\": \"NA\",\n" +
            "      \"sizes\": \"NA\",\n" +
            "      \"price\": [\n" +
            "        \"NA\",\n" +
            "        \"3200\"\n" +
            "      ]\n" +
            "    },\n" +
            "    {\n" +
            "      \"product_id\": 10,\n" +
            "      \"product_title\": \"Glutathione Ampoules\",\n" +
            "      \"tag\": \"lightening Skin\",\n" +
            "      \"product_desc\": \"<h3>Description</h3>\\n<p>Inhibits melanin synthesis in the reaction of tyrosinase; neutralizes free radicals and maintains  exogenous antioxidants such as vitamins C and E in their reduced (active) forms; Improves cells renewal.</p>\\n<h3>Ingredients</h3>\\n<ul>\\n<li>Aqua</li>\\n<li>Glutathione 20%</li>\\n<li>Sodium Chloride</li>\\n</ul>\\n<h3>Recommended for</h3>\\n<ul>\\n<li>Melasma and hyperpigmentation</li>\\n<li>Photo-aging</li>\\n<li>Sun-spots</li>\\n<li>Prevents sun blemishes</li>\\n</ul>\\n<p><strong>Effects</strong></p>\\n<ul>\\n<li>Restores and regenerates damage skins and prevents dryness of the skin, providing smoothness</li>\\n<li>Lightning skin</li>\\n</ul>\\n<p><strong>Usage</strong></p>\\n<ul>\\n<li>Apply the content of the ampoule in the zone to treat through a massage with circulatory movements, or incorporate the ampoule in a cream base to improve its application</li>\\n<li>Put the content of the ampoule in “mesotherapy” or “virtual mesotherapy” or to elevate results obtained with electrotherapy techniques such as ultrasounds, ionization, NARF or other types of medical devices used in dermatology / cosmetology treatments</li>\\n</ul>\",\n" +
            "      \"picture\": \"glutathione_ampoules.png\",\n" +
            "      \"picture_name\": \"glutathione_ampoules_pn.png\",\n" +
            "      \"category\": \"SERUMS - SKIN CARE\",\n" +
            "      \"strength\": \"NA\",\n" +
            "      \"pack\": \"2ml x20\",\n" +
            "      \"custom_pack\": \"NA\",\n" +
            "      \"minimum_quantity\": \"NA\",\n" +
            "      \"sizes\": \"NA\",\n" +
            "      \"price\": \"10000\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"product_id\": 11,\n" +
            "      \"product_title\": \"Vit C 20% Ampoules\",\n" +
            "      \"tag\": \"pigmentation\",\n" +
            "      \"product_desc\": \"<h3>Description</h3>\\n<p>Anti-oxidant, stimulates production of collagen</p>\\n<h3>Ingredients</h3>\\n<ul>\\n<li>Aqua (water)</li>\\n<li>Ascorbic Acid</li>\\n<li>Sodium Bicarbonate</li>\\n</ul>\\n<h3>Recommended for</h3>\\n<ul>\\n<li>All skin types</li>\\n<li>Mature and hyperpigmentated skins</li>\\n<li>Stretch marks and scars.</li>\\n</ul>\\n<p><strong>Effects</strong></p>\\n<ul>\\n<li>Powerful antioxidant properties - neutralize free radicals protecting the skin</li>\\n<li>Helps reduce appearance of wrinkles ghting the signs of aging</li>\\n<li>Helps strengthen skin, providing a younger and bright appearance and provides comfort</li>\\n</ul>\",\n" +
            "      \"picture\": \"vit_c_20__ampoules.png\",\n" +
            "      \"picture_name\": \"vitamin_c_ampoules_pn.png\",\n" +
            "      \"category\": \"SERUMS - SKIN CARE\",\n" +
            "      \"strength\": \"NA\",\n" +
            "      \"pack\": \"5ml x20\",\n" +
            "      \"custom_pack\": \"NA\",\n" +
            "      \"minimum_quantity\": \"NA\",\n" +
            "      \"sizes\": \"NA\",\n" +
            "      \"price\": \"5000\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"product_id\": 12,\n" +
            "      \"product_title\": \"Gavio - BHA Serum\",\n" +
            "      \"tag\": \"pimples,acne\",\n" +
            "      \"product_desc\": \"<h3></a>Description</h3>\\n<p>Salicylic acid is an oil soluble beta hydroxy acid that is used in skin care.It penetrates pores and can dissolve clogs, making it a very effective acne treatment.  High concentrations can produce a peeling effect which removes damaged tissue. You can find a salicylic acid serum in various concentrations to treat a variety of skin complaints.  A good serum for acne control will usually contain 1 - 2% of salicylic acid.  Concentrations of 5% or more can produce irritation. Gavio BHA Serums are ideal for oily and acne prone skin.</p>\\n<h3>Ingredients</h3>\\n<ul>\\n<li>Salicylic acid</li>\\n<li>Aqueous base glycerin, propylene glycol etc.,</li>\\n<li>Removal of pimples</li>\\n<li>Rejuvenation</li>\\n</ul>\",\n" +
            "      \"picture\": \"gavio_bha_serum.png\",\n" +
            "      \"picture_name\": \"gavio_pn.png\",\n" +
            "      \"category\": \"SERUMS - SKIN CARE\",\n" +
            "      \"strength\": \"NA\",\n" +
            "      \"pack\": [\n" +
            "        \"10ml x10\",\n" +
            "        \"30ml\"\n" +
            "      ],\n" +
            "      \"custom_pack\": \"NA\",\n" +
            "      \"minimum_quantity\": \"NA\",\n" +
            "      \"sizes\": \"NA\",\n" +
            "      \"price\": [\n" +
            "        \"5000\",\n" +
            "        \"1500\"\n" +
            "      ]\n" +
            "    },\n" +
            "    {\n" +
            "      \"product_id\": 13,\n" +
            "      \"product_title\": \"Faina - Whitening Serum\",\n" +
            "      \"tag\": \"brightening,face lightening\",\n" +
            "      \"product_desc\": \"<h3>Description</h3>\\n<p>The Alpha Arbutin Lightening Serum is a stable and extremely effective for skin lighteningtreatment. It quickly promotes lightening of the skin, treats Hyper pigmentation and provides an even tone on all skin types. It is a skin lightening active agents that acts faster and more efficiently than most skin lightening serums</p>\\n<h3>Ingredients</h3>\\n<ul>\\n<li>Arbutin and vitamin C</li>\\n<li>Aqueous base glycerin, propylene glycol etc</li>\\n<li>Skin lightening</li>\\n</ul>\",\n" +
            "      \"picture\": \"faina_whitening_serum.png\",\n" +
            "      \"picture_name\": \"faina_pn.png\",\n" +
            "      \"category\": \"SERUMS - SKIN CARE\",\n" +
            "      \"strength\": \"NA\",\n" +
            "      \"pack\": [\n" +
            "        \"10ml x10\",\n" +
            "        \"30ml\"\n" +
            "      ],\n" +
            "      \"custom_pack\": \"NA\",\n" +
            "      \"minimum_quantity\": \"NA\",\n" +
            "      \"sizes\": \"NA\",\n" +
            "      \"price\": [\n" +
            "        \"5000\",\n" +
            "        \"1500\"\n" +
            "      ]\n" +
            "    },\n" +
            "    {\n" +
            "      \"product_id\": 14,\n" +
            "      \"product_title\": \"Gleam - Under Eye Serum\",\n" +
            "      \"tag\": \"reduce fine lines,dark circles\",\n" +
            "      \"product_desc\": \"<h3>Description</h3>\\n<h3>Ingredients</h3>\\n<p>Resveratrol-Oleyl to lift and firm, Aloevera to brighten, and grape seed Polyphenols to protect skin from environmental aggressors.</p>\\n<h3>Application</h3>\\n<ul>\\n<li>Minimize the appearance of puffiness</li>\\n<li>Dark circles</li>\\n<li>Reduce the fine lines</li>\\n</ul>\",\n" +
            "      \"picture\": \"gleam_under_eye_serum.png\",\n" +
            "      \"picture_name\": \"gleam_pn.png\",\n" +
            "      \"category\": \"SERUMS - SKIN CARE\",\n" +
            "      \"strength\": \"NA\",\n" +
            "      \"pack\": [\n" +
            "        \"10ml x10\",\n" +
            "        \"30ml\"\n" +
            "      ],\n" +
            "      \"custom_pack\": \"NA\",\n" +
            "      \"minimum_quantity\": \"NA\",\n" +
            "      \"sizes\": \"NA\",\n" +
            "      \"price\": [\n" +
            "        \"5000\",\n" +
            "        \"1200\"\n" +
            "      ]\n" +
            "    },\n" +
            "    {\n" +
            "      \"product_id\": 15,\n" +
            "      \"product_title\": \"Catalin - Vit C-15%\",\n" +
            "      \"tag\": \"rejuvenation\",\n" +
            "      \"product_desc\": \"<h3>Description</h3>\\n<h3>Ingredients</h3>\\n<p>AA2G, Hydroxyethyl Cellulose, Propylene Glycol, Butylene Glycol, Disodium EDTA, TEA, Phenoxyethanol, Distilled water</p>\\n<h3>Application :</h3>\\n<ul>\\n<li>Promotes collagen synthesis</li>\\n<li>Reduces skin hyperpigmentation</li>\\n<li>Reduces wrinkles and roughness</li>\\n<li>Suppress melanin synthesis</li>\\n</ul>\\n<h3>Available Strength</h3>\\n<p>5% , 10% , 15%</p>\",\n" +
            "      \"picture\": \"catalin_vit_c_15.png\",\n" +
            "      \"picture_name\": \"catalin_pn.png\",\n" +
            "      \"category\": \"SERUMS - SKIN CARE\",\n" +
            "      \"strength\": \"NA\",\n" +
            "      \"pack\": [\n" +
            "        \"10ml x10\",\n" +
            "        \"30ml\"\n" +
            "      ],\n" +
            "      \"custom_pack\": \"NA\",\n" +
            "      \"minimum_quantity\": \"NA\",\n" +
            "      \"sizes\": \"NA\",\n" +
            "      \"price\": [\n" +
            "        \"4500\",\n" +
            "        \"NA\"\n" +
            "      ]\n" +
            "    },\n" +
            "    {\n" +
            "      \"product_id\": 16,\n" +
            "      \"product_title\": \"Hydra - Hyaluronic acid serum\",\n" +
            "      \"tag\": \"anti ageing,hydration,brightening\",\n" +
            "      \"product_desc\": \"<h3>Description</h3>\\n<p>Hydra Serum is an intensely hydrating serum that rejuvenates skin from the inside out. It improves the tone and appearance of skin by enhancing the skin’s ability to retain moisture. Its light, non-oily texture soothes skin leaving it fresh and soft while smoothing fine lines and wrinkles. HA Serum renews your skin’s suppleness and elasticity</p>\\n<h3>Applications</h3>\\n<ul>\\n<li>Smooth &amp; Soft Skin</li>\\n<li>Wrinkle reduction</li>\\n<li>Skin Rejunevation</li>\\n<li>Moisturization</li>\\n<li>Post-treatment (Chemical peel /Microdermabrasion / Laser)</li>\\n<li>Newly-revealed skin</li>\\n<li>Moisturizer to hydrate and protect</li>\\n</ul>\\n<h3>Ingredients</h3>\\n<p>De-ionized water, 1% Sodium hyaluronate &amp; Appropriate preservative</p>\\n<h3Features</h3>\\n<ul>\\n<li>Natural</li>\\n<li>Fragrance Free</li>\\n<li>Pure-Highest Quality</li>\\n<li>Intense Hydration + Moisture</li>\\n<li>Non-greasy</li>\\n<li>Paraben-free</li>\\n<li>High quality 100% Pure  Hyaluronic Acid</li>\\n</ul>\",\n" +
            "      \"picture\": \"hydra_ha_serum.png\",\n" +
            "      \"picture_name\": \"hydra_ha_serum_pn.png\",\n" +
            "      \"category\": \"SERUMS - SKIN CARE\",\n" +
            "      \"strength\": \"NA\",\n" +
            "      \"pack\": [\n" +
            "        \"10ml x10\",\n" +
            "        \"30ml\"\n" +
            "      ],\n" +
            "      \"custom_pack\": \"NA\",\n" +
            "      \"minimum_quantity\": \"NA\",\n" +
            "      \"sizes\": \"NA\",\n" +
            "      \"price\": [\n" +
            "        \"5000\",\n" +
            "        \"1500\"\n" +
            "      ]\n" +
            "    },\n" +
            "    {\n" +
            "      \"product_id\": 17,\n" +
            "      \"product_title\": \"Valarie - Anti Ageing Serum\",\n" +
            "      \"tag\": \"face lift,tightening\",\n" +
            "      \"product_desc\": \"<h3>Description</h3>\\n<p>Aloe vera and pro vitamin B5</p>\\n<ul>\\n<li>Face lift and tightening.</li>\\n</ul>\",\n" +
            "      \"picture\": \"valerie_anti_ageing_serum.png\",\n" +
            "      \"picture_name\": \"valerie_pn.png\",\n" +
            "      \"category\": \"SERUMS - SKIN CARE\",\n" +
            "      \"strength\": \"NA\",\n" +
            "      \"pack\": [\n" +
            "        \"10ml x10\",\n" +
            "        \"30ml\"\n" +
            "      ],\n" +
            "      \"custom_pack\": \"NA\",\n" +
            "      \"minimum_quantity\": \"NA\",\n" +
            "      \"sizes\": \"NA\",\n" +
            "      \"price\": [\n" +
            "        \"5000\",\n" +
            "        \"1500\"\n" +
            "      ]\n" +
            "    },\n" +
            "    {\n" +
            "      \"product_id\": 18,\n" +
            "      \"product_title\": \"Lotoya - Anti Acne Serum\",\n" +
            "      \"tag\": \"acne,fungal infection\",\n" +
            "      \"product_desc\": \"<h3>Description</h3>\\n<p>Tea tree oil</p>\\n<ul>\\n<li>Acne, fungal infections.</li>\\n</ul>\",\n" +
            "      \"picture\": \"lotoya_anti_acne_serum.png\",\n" +
            "      \"picture_name\": \"lotoya_pn.png\",\n" +
            "      \"category\": \"SERUMS - SKIN CARE\",\n" +
            "      \"strength\": \"NA\",\n" +
            "      \"pack\": [\n" +
            "        \"10ml x10\",\n" +
            "        \"30ml\"\n" +
            "      ],\n" +
            "      \"custom_pack\": \"NA\",\n" +
            "      \"minimum_quantity\": \"NA\",\n" +
            "      \"sizes\": \"NA\",\n" +
            "      \"price\": [\n" +
            "        \"5000\",\n" +
            "        \"1200\"\n" +
            "      ]\n" +
            "    },\n" +
            "    {\n" +
            "      \"product_id\": 19,\n" +
            "      \"product_title\": \"Talia-T1, T2 Serum\",\n" +
            "      \"tag\": \"acne,oily skin,pimples\",\n" +
            "      \"product_desc\": \"<h3>Description</h3>\\n\\n<p>Anti-Control Serum is created an oil balancing system designed to control abnormal or excessive oil production and acne using the synergistic effect of 5 anti-acne</p>\\n\\n<ul>\\n<li>Salicylic Acid</li>\\n<li>Glypoic Complex</li>\\n<li>Azelaic Acid</li>\\n<li>Alium Sativa Extract</li>\\n<li>Allantoin</li>\\n</ul>\\n\\n<h3>Direction for use</h3>\\n\\n<p>After cleaning the face, gently apply T1-Serum (Bio Actives) first , leave to dry for 10 minutes. Avoid Eye area. Then apply T2-Serum (Activator) leave to dry for 10 minutes. Can be washed after 10 to 15 minutes of T-2 Serum application.</p>\",\n" +
            "      \"picture\": \"talia_acne_control_serum.png\",\n" +
            "      \"picture_name\": \"talia_t1_t2_pn.png\",\n" +
            "      \"category\": \"SERUMS - SKIN CARE\",\n" +
            "      \"strength\": \"NA\",\n" +
            "      \"pack\": [\n" +
            "        \"10ml x10\",\n" +
            "        \"30ml\"\n" +
            "      ],\n" +
            "      \"custom_pack\": \"NA\",\n" +
            "      \"minimum_quantity\": \"NA\",\n" +
            "      \"sizes\": \"NA\",\n" +
            "      \"price\": [\n" +
            "        \"5000\",\n" +
            "        \"1200\"\n" +
            "      ]\n" +
            "    },\n" +
            "    {\n" +
            "      \"product_id\": 20,\n" +
            "      \"product_title\": \"StemCello Ampoules\",\n" +
            "      \"tag\": \"anti hair loss(proffesional care with micro needling)\",\n" +
            "      \"product_desc\": \"<h3>Description</h3>\\n<p>StemCello Revitalizing EMortalAnti Hair Loss Ampoules is a meso-cocktail rich in active ingredients(A stem cell activator, DHT blockers, Blood circulation enhancers, Active peptides), vitamins, and minerals, formulated to activate and revitalize stem cells in hair follicles and restore effective micro-circulation of the scalp. The meso-cocktail is injected into the scalp to trigger the signal related to hair growth. This results in healthier-looking hair and scalp. It is an outstanding hair loss formula designed to fight thinning hairs and stimulate re-growth</p>\\n<h3>Benefits</h3>\\n<ul>\\n<li>Help stimulate the cellular metabolism at the root and increase the hair growth rate</li>\\n<li>Enhance the microcirculation at the root and the scalp, thus promoting better nourishment and oxygenation of thinning, weak hair.</li>\\n<li>Effectively acts against the aging of the hair follicles and prolong the duration of the hair anagen phase</li>\\n<li>Enhance hair texture and hydration</li>\\n</ul>\",\n" +
            "      \"picture\": \"stemcello_ampoules.png\",\n" +
            "      \"picture_name\": \"anti_hair_loss_ampoules_pn.png\",\n" +
            "      \"category\": \"SERUMS - HAIR CARE\",\n" +
            "      \"strength\": \"NA\",\n" +
            "      \"pack\": [\n" +
            "        \"10ml x10\",\n" +
            "        \"30ml\"\n" +
            "      ],\n" +
            "      \"custom_pack\": \"NA\",\n" +
            "      \"minimum_quantity\": \"NA\",\n" +
            "      \"sizes\": \"NA\",\n" +
            "      \"price\": [\n" +
            "        \"8000\",\n" +
            "        \"NA\"\n" +
            "      ]\n" +
            "    },\n" +
            "    {\n" +
            "      \"product_id\": 21,\n" +
            "      \"product_title\": \"StemCello Essence\",\n" +
            "      \"tag\": \"anti hair loss(home care production)\",\n" +
            "      \"product_desc\": \"<h3>Description</h3>\\n<p>This unique essence formula is for men and women who have a general thinning of hair on top of the scalp. Hair restoration can be multiplied by stimulation of hair follicle stem cells and dermal papilla cells. This formula is natural, safe and effective treatment to stimulate these cells in hair follicles. It can protect hair loss, promote normal hair growth and rapidly improve existing hair</p>\",\n" +
            "      \"picture\": \"stemcello_essence.png\",\n" +
            "      \"picture_name\": \"anti_hair_loss_essence_pn.png\",\n" +
            "      \"category\": \"SERUMS - HAIR CARE\",\n" +
            "      \"strength\": \"NA\",\n" +
            "      \"pack\": [\n" +
            "        \"10ml x10\",\n" +
            "        \"30ml\"\n" +
            "      ],\n" +
            "      \"custom_pack\": \"NA\",\n" +
            "      \"minimum_quantity\": \"NA\",\n" +
            "      \"sizes\": \"NA\",\n" +
            "      \"price\": [\n" +
            "        \"8000\",\n" +
            "        \"NA\"\n" +
            "      ]\n" +
            "    },\n" +
            "    {\n" +
            "      \"product_id\": 22,\n" +
            "      \"product_title\": \"Vienne - Hair Growth Serum\",\n" +
            "      \"tag\": \"hair growth\",\n" +
            "      \"product_desc\": \"<h3>Description</h3>\\n<p>Vienne is a hair serum with improved properties in its hair protecting action and prevention of hair loss, and the reduction of external effects of androgenic causes including Dandruff and resulting hair loss.</p>\\n<h3>Ingredients</h3>\\n<p>Humulus lupulus, Arnica flower Extract, Aloe vera Extract, Brahmi (Centella Asiatica), Amla (Embelicaofficinalis), Deordorised Allium Sativum, Licorice, Green Tea Extract, Geranium Oil, Licorice, Horsetail</p>\\n<h3>Direction for use</h3>\\n<ul>\\n<li>Take 2 to 3 ml of this serum</li>\\n<li>Wet the root of your hair  and massage your scalp thoroughly</li>\\n<li>Leave for 30 minutes and wash  with mild shampoo.</li>\\n</ul>\",\n" +
            "      \"picture\": \"vienne_hair_growth_serum.png\",\n" +
            "      \"picture_name\": \"vienne_pn.png\",\n" +
            "      \"category\": \"SERUMS - HAIR CARE\",\n" +
            "      \"strength\": \"NA\",\n" +
            "      \"pack\": [\n" +
            "        \"10ml x10\",\n" +
            "        \"30ml\"\n" +
            "      ],\n" +
            "      \"custom_pack\": \"NA\",\n" +
            "      \"minimum_quantity\": \"NA\",\n" +
            "      \"sizes\": \"NA\",\n" +
            "      \"price\": [\n" +
            "        \"5000\",\n" +
            "        \"1200\"\n" +
            "      ]\n" +
            "    },\n" +
            "    {\n" +
            "      \"product_id\": 23,\n" +
            "      \"product_title\": \"Lara - Anti Dandruff serum\",\n" +
            "      \"tag\": \"anti dandruff serum\",\n" +
            "      \"product_desc\": \"<h3>Description</h3>\\n<p>This Anti-Dandruff Serum reduces and prevents dandruff. The all natural formula instantly has an anti-dandruff effect from the first application. The Natural Bio Actives ingredient, condition the hair and scalp to help remove visible dandruff flakes while anti-fungal.</p>\\n<h3>Ingredients</h3>\\n<p>Aloevera, Jojoba, Henna, Hibiscus, calendula extract, climbazol , Chlorhexidine</p>\\n<h3>Direction for use</h3>\\n<ul>\\n<li>Apply this serum evenly on the scalp, gently massage.</li>\\n<li>Leave for 10 minutes and wash with any mild shampoo.</li>\\n</ul>\",\n" +
            "      \"picture\": \"lara_anti_dandruff_serum.png\",\n" +
            "      \"picture_name\": \"lara_pn.png\",\n" +
            "      \"category\": \"SERUMS - HAIR CARE\",\n" +
            "      \"strength\": \"NA\",\n" +
            "      \"pack\": [\n" +
            "        \"10ml x10\",\n" +
            "        \"30ml\"\n" +
            "      ],\n" +
            "      \"custom_pack\": \"NA\",\n" +
            "      \"minimum_quantity\": \"NA\",\n" +
            "      \"sizes\": \"NA\",\n" +
            "      \"price\": [\n" +
            "        \"5000\",\n" +
            "        \"1200\"\n" +
            "      ]\n" +
            "    },\n" +
            "    {\n" +
            "      \"product_id\": 24,\n" +
            "      \"product_title\": \"Parina - Mela White cream\",\n" +
            "      \"tag\": \"melasma,sun burn\",\n" +
            "      \"product_desc\": \"<h3>Description</h3>\\n\\n<p>Mela is a blended formula of kojic acid dipalmitate,glycolic acid, Vit-C &amp; arbutin. It reduces pigmentation,sun spots,skin discoloration &amp; evens skin tone. It works as an exfoliant to remove dead skin cells &amp; help diminish fine lines, wrinkles, pores and other aging signs to improve the appearance and texture of the skin.</p>\\n\\n<h3>Ingredients</h3>\\n\\n<ul>\\n<li>Kojic acid Dipalmitate</li>\\n<li>VIT C Derivative ARBUTIN with AHA</li>\\n</ul>\\n\\n<h3>Application</h3>\\n\\n<ul>\\n<li>Melasma</li>\\n<li>Post-inflammatory hyper-pigmentation</li>\\n<li>Hyper-melanotic conditions</li>\\n<li>Sun burns</li></ul>\",\n" +
            "      \"picture\": \"parina_mela_white_cream.png\",\n" +
            "      \"picture_name\": \"parina_pn.png\",\n" +
            "      \"category\": \"CREAMS\",\n" +
            "      \"strength\": \"NA\",\n" +
            "      \"pack\": \"50g\",\n" +
            "      \"custom_pack\": \"NA\",\n" +
            "      \"minimum_quantity\": \"5\",\n" +
            "      \"sizes\": \"NA\",\n" +
            "      \"price\": \"465\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"product_id\": 25,\n" +
            "      \"product_title\": \"Hagan - Sun Screen Lotion\",\n" +
            "      \"tag\": \"sun protection\",\n" +
            "      \"product_desc\": \"<h3>Description</h3>\\n\\n<p>SPF or Sun Protection Factor, is a measure of how well a sunscreen will protect skin from UVB rays, the kind of radiation that causes sunburn, damages skin, and can contribute to skin cancer.</p>\\n\\n<h3>Ingredients</h3>\\n\\n<ul>\\n<li>Purified Water</li>\\n<li>OctylMethoxy Cinnamate</li>\\n<li>Stearic  Acid</li>\\n<li>Avobenzone</li>\\n<li>Benzophenone 3</li>\\n<li>Sorbitol</li>\\n<li>Triethanolamine</li>\\n<li>Titanium Dioxide</li>\\n<li>Methyl Paraben</li>\\n<li>Propyl Paraben</li>\\n<li>Sodium lauryl sulphate</li>\\n<li>Perfume</li>\\n</ul>\\n\\n<h3>Direction for use</h3>\\n\\n<ul>\\n<li>Apply liberally 15 minutes before sun exposure and as needed.</li>\\n<li>Re-apply at least every 2 hours or after towel drying, swimming  or perspiring.</li>\\n</ul>\\n\\n\\n\",\n" +
            "      \"picture\": \"hagan_sun_screen_lotion.png\",\n" +
            "      \"picture_name\": \"hagan_pn.png\",\n" +
            "      \"category\": \"CREAMS\",\n" +
            "      \"strength\": \"NA\",\n" +
            "      \"pack\": \"50g\",\n" +
            "      \"custom_pack\": \"NA\",\n" +
            "      \"minimum_quantity\": \"5\",\n" +
            "      \"sizes\": \"NA\",\n" +
            "      \"price\": \"360\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"product_id\": 26,\n" +
            "      \"product_title\": \"Galan - Night cream\",\n" +
            "      \"tag\": \"skin rejuvenation\",\n" +
            "      \"product_desc\": \"<h3>Description</h3>\\n\\n<p>Apart from keeping skin well-hydrated, they are also said to contain excellent anti-ageing properties. Night creams contain ingredients like vitamin E and AHA acids that fight wrinkles and fine lines. Thus it is advisable to use a night cream on a daily basis.</p>\\n\\n<h3>Ingredients</h3>\\n\\n<ul>\\n<li>Alpha hydroxyl acid </li>\\n<li>Vitamin E in cream base</li>\\n</ul>\\n\\n<h3>Application</h3>\\n\\n<ul>\\n<li>Skin rejuvenation</li>\\n<li>Fine lines</li>\\n<li>wrinkles &amp; laxity</li>\\n</ul>\\n\",\n" +
            "      \"picture\": \"galan_night_cream.png\",\n" +
            "      \"picture_name\": \"galan_pn.png\",\n" +
            "      \"category\": \"CREAMS\",\n" +
            "      \"strength\": \"NA\",\n" +
            "      \"pack\": \"50g\",\n" +
            "      \"custom_pack\": \"NA\",\n" +
            "      \"minimum_quantity\": \"5\",\n" +
            "      \"sizes\": \"NA\",\n" +
            "      \"price\": \"380\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"product_id\": 27,\n" +
            "      \"product_title\": \"Glutathione Cream\",\n" +
            "      \"tag\": \"instant brightening\",\n" +
            "      \"product_desc\": \"<h3>Description</h3>\\n<p>Inhibits melanin synthesis in the reaction of tyrosinase; neutralize free radicals and maintain exogenous antioxidants such as vitamins C and E in their reduced (active) forms (Glutathione)  Anti-bacterial, anti-acne (Myrtus), Prevents bacterial and fungal conditions (Allium Cepa) Anti-infammatory, antiseptic, healing (Commiphora Myrrha)</p>\\n<h3>Ingredients</h3>\\n<p>Aqua (Water), Glutathione, Polyacrylamide, C13-14 isoparaffin, Laureth-7, Propylene Glycol, Myrtus, Communis, Allium Cepa, Commiphora Myrrha, Ammonium Hydroxide, Apricot Kernel Oil Peg-6 Esters, Tocopheryl Acetate, Glyceryl Linoleate, Glyceryl Linolenate Glyceril Arachidonate, Isopropyl myristate, Phenoxyethanol, Bht, Methylchloroisothiazolinone and Methylisothiazolinone</p>\\n<h3>Recommended for</h3>\\n<ul>\\n<li>All skin types</li>\\n<li>Sun Spots, photo-aging</li>\\n<li>Essential after facial peelings or other treatments</li>\\n</ul>\\n<p><strong>Usage</strong></p>\\n<ul>\\n<li>Apply on the face and neck twice a day morning and evening.</li>\\n</ul>\\n<p><strong>Effects</strong></p>\\n<ul>\\n<li>Reduces darkening of the skin</li>\\n<li>Helps maintain youthful vibrancy</li>\\n<li>Restores and regenerates damaged skin and prevents dryness of the skin, providing smoothness</li>\\n<li>Prevents sun blemishes.</li>\\n</ul>\",\n" +
            "      \"picture\": \"glutathione_cream.png\",\n" +
            "      \"picture_name\": \"glutathione_cream_pn.png\",\n" +
            "      \"category\": \"CREAMS\",\n" +
            "      \"strength\": \"NA\",\n" +
            "      \"pack\": [\n" +
            "        \"10g\",\n" +
            "        \"30g\",\n" +
            "        \"50g\"\n" +
            "      ],\n" +
            "      \"custom_pack\": \"NA\",\n" +
            "      \"minimum_quantity\": \"NA\",\n" +
            "      \"sizes\": \"NA\",\n" +
            "      \"price\": [\n" +
            "        \"950\",\n" +
            "        \"2500\",\n" +
            "        \"4500\"\n" +
            "      ]\n" +
            "    },\n" +
            "    {\n" +
            "      \"product_id\": 28,\n" +
            "      \"product_title\": \"Hydra Cream\",\n" +
            "      \"tag\": \"hydration,anti ageing\",\n" +
            "      \"product_desc\": \"<h3>Description</h3>\\n\\n<p>Its unmatched hydrating properties result in smoother, softer and younger looking skin while\\nhelping to diminish the appearance of fine lines and wrinkles.  This exclusive formula is a blend of this remarkable ingredient (Hyaluronic  Acid) with Aloevera, Olive oil, Jojoba oil and vegetable butters for a maximum skin benefits.</p>\\n\\n\",\n" +
            "      \"picture\": \"hydra_ha_cream.png\",\n" +
            "      \"picture_name\": \"hydra_ha_cream_pn.png\",\n" +
            "      \"category\": \"CREAMS\",\n" +
            "      \"strength\": \"NA\",\n" +
            "      \"pack\": \"50g\",\n" +
            "      \"custom_pack\": \"NA\",\n" +
            "      \"minimum_quantity\": \"5\",\n" +
            "      \"sizes\": \"NA\",\n" +
            "      \"price\": \"750\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"product_id\": 29,\n" +
            "      \"product_title\": \"Zaida - Anti-Wrinkle Cream\",\n" +
            "      \"tag\": \"wrinkle,fine lines\",\n" +
            "      \"product_desc\": \"<h3>Description</h3>\\n\\n<h3>Ingredients</h3>\\n\\n<ul>\\n<li>Eurol  BT</li>\\n<li>Vitamin E</li>\\n<li>Aloe vera along with lactic acid</li>\\n</ul>\\n\\n<h3>Application</h3>\\n\\n<p>Visibly reduce the depth of fine lines and protects against the effects of premature, light-induced aging giving the skin a smoother and firmer appearance</p>\",\n" +
            "      \"picture\": \"zaida_anti_wrinkle_cream.png\",\n" +
            "      \"picture_name\": \"zaida_pn.png\",\n" +
            "      \"category\": \"CREAMS\",\n" +
            "      \"strength\": \"NA\",\n" +
            "      \"pack\": \"50g\",\n" +
            "      \"custom_pack\": \"NA\",\n" +
            "      \"minimum_quantity\": \"5\",\n" +
            "      \"sizes\": \"NA\",\n" +
            "      \"price\": \"475\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"product_id\": 30,\n" +
            "      \"product_title\": \"Dr.Pen\",\n" +
            "      \"tag\": \"scar removal,stretch marks removal,anti ageing,hair restoration,tatooing,vitiligo surgery\",\n" +
            "      \"product_desc\": \"<h3>Description</h3>\\n<ul>\\n<li>6 levels of speed</li>\\n<li>Needle length adjustment from 0.25mm-3.0mm</li>\\n<li>The max speed can be 8000-18000 times/min</li>\\n<li>Metal body</li>\\n</ul>\",\n" +
            "      \"picture\": \"dr_pen.png\",\n" +
            "      \"picture_name\": \"dr_pen_pn.png\",\n" +
            "      \"category\": \"ROLLERS\",\n" +
            "      \"strength\": \"NA\",\n" +
            "      \"pack\": \"NA\",\n" +
            "      \"custom_pack\": \"NA\",\n" +
            "      \"minimum_quantity\": \"NA\",\n" +
            "      \"sizes\": \"NA\",\n" +
            "      \"price\": \"NA\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"product_id\": 31,\n" +
            "      \"product_title\": \"Dermaroller-T\",\n" +
            "      \"tag\": \"acne scar removal,hair loss treatment\",\n" +
            "      \"product_desc\": \"<h3>Description</h3>\\n<p>Titanium Plated &amp; Latest Low Torque Technology</p>\\n<h3>Applications</h3>\\n<ul>\\n<li>Scar removal including acne scar removal or treatment</li>\\n<li>Stretch mark removal</li>\\n<li>Anti aging</li>\\n<li>Anti wrinkle</li>\\n<li>Cellulite treatment / cellulite reduction or removal</li>\\n<li>Hair loss treatment/ hair restoration</li>\\n<li>Hyper pigmentation treatment.</li>\\n</ul>\\n<h2></a>Specification</h2>\\n<ul>\\n<li>Total numper of needles 192</li>\\n<li>Sizes Available :  0.5mm, 1.0mm, 1.5mm, 2.0mm</li>\\n</ul>\",\n" +
            "      \"picture\": \"derma_roller_t.png\",\n" +
            "      \"picture_name\": \"dermaroller_t_pn.png\",\n" +
            "      \"category\": \"ROLLERS\",\n" +
            "      \"strength\": \"NA\",\n" +
            "      \"pack\": \"NA\",\n" +
            "      \"custom_pack\": \"NA\",\n" +
            "      \"minimum_quantity\": \"NA\",\n" +
            "      \"sizes\": [\n" +
            "        \".5mm\",\n" +
            "        \"1mm\",\n" +
            "        \"1.5mm\",\n" +
            "        \"2mm\",\n" +
            "        \"2.5mm\"\n" +
            "      ],\n" +
            "      \"price\": [\n" +
            "        \"1500\",\n" +
            "        \"1500\",\n" +
            "        \"1500\",\n" +
            "        \"1500\",\n" +
            "        \"NA\"\n" +
            "      ]\n" +
            "    },\n" +
            "    {\n" +
            "      \"product_id\": 32,\n" +
            "      \"product_title\": \"Dermaroller-G\",\n" +
            "      \"tag\": \"acne scar removal,hair loss treatment\",\n" +
            "      \"product_desc\": \"<h3>Description</h3>\\n<p>Gold Plated &amp; Latest Low Torque Technology</p>\\n<strong>Specification:</strong>\\n<ul>\\n<li>Total numper of needles 192</li>\\n<li>Sizes Available : 0.5mm, 1.0mm, 1.5mm, 2.0mm</li>\\n</ul>\\n\",\n" +
            "      \"picture\": \"derma_roller_g.png\",\n" +
            "      \"picture_name\": \"dermaroller_g_pn.png\",\n" +
            "      \"category\": \"ROLLERS\",\n" +
            "      \"strength\": \"NA\",\n" +
            "      \"pack\": \"NA\",\n" +
            "      \"custom_pack\": \"NA\",\n" +
            "      \"minimum_quantity\": \"NA\",\n" +
            "      \"sizes\": [\n" +
            "        \".5mm\",\n" +
            "        \"1mm\",\n" +
            "        \"1.5mm\",\n" +
            "        \"2mm\",\n" +
            "        \"2.5mm\"\n" +
            "      ],\n" +
            "      \"price\": \"1200\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"product_id\": 33,\n" +
            "      \"product_title\": \"Epicool Roller\",\n" +
            "      \"tag\": \"for pre,post procedures\",\n" +
            "      \"product_desc\": \"<h3>Description</h3>\\n\\n<p>For Pre/Post Procedures</p>\",\n" +
            "      \"picture\": \"epicool_roller.png\",\n" +
            "      \"picture_name\": \"epicool_roller_pn.png\",\n" +
            "      \"category\": \"ROLLERS\",\n" +
            "      \"strength\": \"NA\",\n" +
            "      \"pack\": \"NA\",\n" +
            "      \"custom_pack\": \"NA\",\n" +
            "      \"minimum_quantity\": \"NA\",\n" +
            "      \"sizes\": \".5mm\",\n" +
            "      \"price\": \"1000\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"product_id\": 34,\n" +
            "      \"product_title\": \"DermaRoller-s\",\n" +
            "      \"tag\": \"under eye\",\n" +
            "      \"product_desc\": \"<h3>Description</h3>\\n\\n<h3>Specification</h3>\\n\\n<ul>\\n<li>Total number of needles 180</li>\\n<li>Sizes Available 0.3mm ,0.75mm</li>\\n</ul>\\n\",\n" +
            "      \"picture\": \"derma_roller_s.png\",\n" +
            "      \"picture_name\": \"dermaroller_s_pn.png\",\n" +
            "      \"category\": \"ROLLERS\",\n" +
            "      \"strength\": \"NA\",\n" +
            "      \"pack\": \"NA\",\n" +
            "      \"custom_pack\": \"NA\",\n" +
            "      \"minimum_quantity\": \"NA\",\n" +
            "      \"sizes\": [\n" +
            "        \".3mm\",\n" +
            "        \".75mm\"\n" +
            "      ],\n" +
            "      \"price\": [\n" +
            "        \"1000\",\n" +
            "        \"1000\"\n" +
            "      ]\n" +
            "    },\n" +
            "    {\n" +
            "      \"product_id\": 35,\n" +
            "      \"product_title\": \"DermaRoller-B\",\n" +
            "      \"tag\": \"roller with replaceable head\",\n" +
            "      \"product_desc\": \"<h3>Description</h3>\\n<h3>Specification</h3>\\n<ul>\\n<li>Total number of needles : 600</li>\\n<li>Head roller : Replaceable</li>\\n<li>Sizes Available : 0.5mm, 1.0mm, 1.5mm, 2.0mm, 2.5mm.</li>\\n</ul>\\n\",\n" +
            "      \"picture\": \"derma_roller_b.png\",\n" +
            "      \"picture_name\": \"dermaroller_b_pn.png\",\n" +
            "      \"category\": \"ROLLERS\",\n" +
            "      \"strength\": \"NA\",\n" +
            "      \"pack\": \"NA\",\n" +
            "      \"custom_pack\": \"NA\",\n" +
            "      \"minimum_quantity\": \"NA\",\n" +
            "      \"sizes\": [\n" +
            "        \".5mm\",\n" +
            "        \"1mm\",\n" +
            "        \"1.5mm\",\n" +
            "        \"2mm\",\n" +
            "        \"2.5mm\"\n" +
            "      ],\n" +
            "      \"price\": \"1200\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"product_id\": 36,\n" +
            "      \"product_title\": \"DermaRoller-BR\",\n" +
            "      \"tag\": \"body roller\",\n" +
            "      \"product_desc\": \"<h3>Description</h3>\\n<h3>Specification</h3>\\n\\n<li>Total number of needles:1200</li>\\n<li>Sizes Available: 1.0mm, 1.5mm</li>\",\n" +
            "      \"picture\": \"derma_roller_br.png\",\n" +
            "      \"picture_name\": \"dermaroller_br_pn.png\",\n" +
            "      \"category\": \"ROLLERS\",\n" +
            "      \"strength\": \"NA\",\n" +
            "      \"pack\": \"NA\",\n" +
            "      \"custom_pack\": \"NA\",\n" +
            "      \"minimum_quantity\": \"NA\",\n" +
            "      \"sizes\": [\n" +
            "        \".5mm\",\n" +
            "        \"1mm\",\n" +
            "        \"1.5mm\",\n" +
            "        \"2mm\",\n" +
            "        \"2.5mm\"\n" +
            "      ],\n" +
            "      \"price\": [\n" +
            "        \"NA\",\n" +
            "        \"1500\",\n" +
            "        \"1500\",\n" +
            "        \"NA\",\n" +
            "        \"NA\"\n" +
            "      ]\n" +
            "    },\n" +
            "    {\n" +
            "      \"product_id\": 37,\n" +
            "      \"product_title\": \"DermaRoller-LED\",\n" +
            "      \"tag\": \"photon, acne & pigmentation\",\n" +
            "      \"product_desc\": \"<h3>Description</h3>\\n<h3>Specification</h3>\\n<ul>\\n<li>Total number of needles: 540</li>\\n<li>Head roller: Replaceable</li>\\n<li>Sizes: 1.0mm,1.5mm.</li>\\n</ul>\",\n" +
            "      \"picture\": \"derma_roller_led.png\",\n" +
            "      \"picture_name\": \"dermaroller_led_pn.png\",\n" +
            "      \"category\": \"ROLLERS\",\n" +
            "      \"strength\": \"NA\",\n" +
            "      \"pack\": \"NA\",\n" +
            "      \"custom_pack\": \"NA\",\n" +
            "      \"minimum_quantity\": \"NA\",\n" +
            "      \"sizes\": [\n" +
            "        \".5mm\",\n" +
            "        \"1mm\",\n" +
            "        \"1.5mm\",\n" +
            "        \"2mm\",\n" +
            "        \"2.5mm\"\n" +
            "      ],\n" +
            "      \"price\": [\n" +
            "        \"NA\",\n" +
            "        \"2000\",\n" +
            "        \"2000\",\n" +
            "        \"NA\",\n" +
            "        \"NA\"\n" +
            "      ]\n" +
            "    },\n" +
            "    {\n" +
            "      \"product_id\": 38,\n" +
            "      \"product_title\": \"Aloe mask\",\n" +
            "      \"tag\": \"hydrating\",\n" +
            "      \"product_desc\": \"<h3>Description</h3>\\n\\n<p>An Aloe face mask is very effective to refresh,hydrate, moisturize, soothe and heal the skin.It is used in all skin types but is especially useful when you have dry skinIt ensures relaxing and satisfying natural fresh experience.</p>\",\n" +
            "      \"picture\": \"hydrating_aloe_mask.png\",\n" +
            "      \"picture_name\": \"aloe_mask_pn.png\",\n" +
            "      \"category\": \"MASK\",\n" +
            "      \"strength\": \"NA\",\n" +
            "      \"pack\": \"NA\",\n" +
            "      \"custom_pack\": \"NA\",\n" +
            "      \"minimum_quantity\": \"NA\",\n" +
            "      \"sizes\": \"NA\",\n" +
            "      \"price\": \"NA\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"product_id\": 39,\n" +
            "      \"product_title\": \"Arbutin Mask\",\n" +
            "      \"tag\": \"hyper pigmentation\",\n" +
            "      \"product_desc\": \"<h3>Description</h3>\\n\\n<p>Arbutin has been used in the treatment of disorders of hyper-pigmentation such as post inflammatory hyperpigmentation caused by acne, trauma, allergic reactions, infections, injuries, phototoxic reactions, reactions to medications, reactions to cosmetics, and inflammatory diseases.</p>\",\n" +
            "      \"picture\": \"hyper_pigmentation_arbutin_mask.png\",\n" +
            "      \"picture_name\": \"arbutin_mask_pn.png\",\n" +
            "      \"category\": \"MASK\",\n" +
            "      \"strength\": \"NA\",\n" +
            "      \"pack\": \"NA\",\n" +
            "      \"custom_pack\": \"NA\",\n" +
            "      \"minimum_quantity\": \"NA\",\n" +
            "      \"sizes\": \"NA\",\n" +
            "      \"price\": \"NA\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"product_id\": 40,\n" +
            "      \"product_title\": \"Collagen Mask\",\n" +
            "      \"tag\": \"anti ageing\",\n" +
            "      \"product_desc\": \"<h3>Description</h3>\\n\\n<p>A collagen mask helps to reduce the signs of aging in the skin and is intended for use on the face.As the skin ages, it loses collagen, which is a kind of protein that makes up a large percentage of connective tissue and accounts for somewhere between a quarter and 35 percent of all of the protein in the body. As such, collagen has been referred to as \\\"the glue\\\" for the human structure. As people age, the collagen in their bodies begins to deplete.</p>\",\n" +
            "      \"picture\": \"anti_ageing_collagen_mask.png\",\n" +
            "      \"picture_name\": \"collagen_mask_pn.png\",\n" +
            "      \"category\": \"MASK\",\n" +
            "      \"strength\": \"NA\",\n" +
            "      \"pack\": \"NA\",\n" +
            "      \"custom_pack\": \"NA\",\n" +
            "      \"minimum_quantity\": \"NA\",\n" +
            "      \"sizes\": \"NA\",\n" +
            "      \"price\": \"NA\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"product_id\": 41,\n" +
            "      \"product_title\": \"Cooling Gel\",\n" +
            "      \"tag\": \"for cooling effect,post procedures\",\n" +
            "      \"product_desc\": \"<h3>Description</h3>\\n\\n<ul>\\n<li>Carbopol Gel with Menthol<br><t><p>Can be used after any Dermatology,   Cosmetology procedures as a \\nsoothing agent</p></t></li>\\n</ul>\",\n" +
            "      \"picture\": \"cooling_gel.png\",\n" +
            "      \"picture_name\": \"cooling_gel_pn.png\",\n" +
            "      \"category\": \"OTHER\",\n" +
            "      \"strength\": \"NA\",\n" +
            "      \"pack\": \"100g\",\n" +
            "      \"custom_pack\": \"NA\",\n" +
            "      \"minimum_quantity\": \"5\",\n" +
            "      \"sizes\": \"NA\",\n" +
            "      \"price\": \"500\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"product_id\": 42,\n" +
            "      \"product_title\": \"Helina - Cleanser\",\n" +
            "      \"tag\": \"cleansing lotion\",\n" +
            "      \"product_desc\": \"<h3>Description</h3>\\n\\n<ul>\\n<li>Cetyl alcohol</li>\\n<li>Stearyl alcohol</li>\\n<li>Useful for sensitive skin line acne prone skin,eczema etc as a cleansing \\nagent. It is also used as a normal cleanser to remove dirt make up etc</li>\\n</ul>\",\n" +
            "      \"picture\": \"helina_cleansing_lotion.png\",\n" +
            "      \"picture_name\": \"helina_pn.png\",\n" +
            "      \"category\": \"OTHER\",\n" +
            "      \"strength\": \"NA\",\n" +
            "      \"pack\": \"100ml\",\n" +
            "      \"custom_pack\": \"NA\",\n" +
            "      \"minimum_quantity\": \"5\",\n" +
            "      \"sizes\": \"NA\",\n" +
            "      \"price\": \"240\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"product_id\": 43,\n" +
            "      \"product_title\": \"Myung Face Wash\",\n" +
            "      \"tag\": \"acne, salicylic acid\",\n" +
            "      \"product_desc\": \"<h3>Description</h3>\\n\\n<p>A salicylic acid face wash is a rinse off product that will typically have a concentration 1% to 2%. The amount of salicylic acid is effectively reduced if it is used on a wet face because it becomes diluted. Read the directions of your product. Medicated facial cleansers will often direct users to apply it to dry skin and wait a few minutes. This way the pH and concentration is not affected by water and the medication has some time to work. A glycolic cleanser is often used in the same way and can also benefit acne prone skin.</p>\\n\",\n" +
            "      \"picture\": \"myung_face_wash_cream.png\",\n" +
            "      \"picture_name\": \"myung_pn.png\",\n" +
            "      \"category\": \"OTHER\",\n" +
            "      \"strength\": \"NA\",\n" +
            "      \"pack\": \"100ml\",\n" +
            "      \"custom_pack\": \"NA\",\n" +
            "      \"minimum_quantity\": \"5\",\n" +
            "      \"sizes\": \"NA\",\n" +
            "      \"price\": \"350\"\n" +
            "    }\n" +
            "  ]\n" +
            "}";

    //   static String html = "<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p><table width=\"658\"><tbody><tr><td width=\"234\"><p><strong>Fusion Peels</strong></p></td><td width=\"90\"><p><strong>pH</strong></p></td><td width=\"219\"><p><strong>Application time</strong></p></td><td width=\"115\"><p><strong>Sessions</strong></p></td></tr><tr><td width=\"234\"><p>FUSION -A</p></td><td rowspan=\"2\" width=\"90\"><p>2.7</p></td><td rowspan=\"10\" width=\"219\"><p>5-8 Minutes</p></td><td width=\"115\"><p>Repeated</p></td></tr><tr><td width=\"234\"><p>(under Eye Peel)</p></td><td width=\"115\"><p>every 3</p></td></tr><tr><td width=\"234\"><p>FUSION -B</p></td><td rowspan=\"2\" width=\"90\"><p>2.5</p></td><td width=\"115\"><p>weeks.4-6</p></td></tr><tr><td width=\"234\"><p>(Melasma Peel)</p></td><td width=\"115\"><p>sessions are</p></td></tr><tr><td width=\"234\"><p>FUSION -C</p></td><td rowspan=\"2\" width=\"90\"><p>2</p></td><td width=\"115\"><p>suggested.</p></td></tr><tr><td width=\"234\"><p>(Acne)</p></td><td width=\"115\"><p>maintenance</p></td></tr><tr><td width=\"234\"><p>FUSION -D</p></td><td rowspan=\"2\" width=\"90\"><p>2.35</p></td><td width=\"115\"><p>peel can be</p></td></tr><tr><td width=\"234\"><p>(Acne Scar)</p></td><td width=\"115\"><p>done once</p></td></tr><tr><td width=\"234\"><p>FUSION -E</p></td><td rowspan=\"2\" width=\"90\"><p>2.35</p></td><td width=\"115\"><p>in 6 months</p></td></tr><tr><td width=\"234\"><p>(Brighten Peel)</p></td><td width=\"115\"><p>&nbsp;</p></td></tr><tr><td colspan=\"4\" width=\"658\"><p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Peels can be custom formulated according to the pH requirement</p></td></tr></tbody></table>";
    static String html = "<table style=\"border-collapse: collapse;border: 1px solid black;\">\n" +
            "<thead>\n" +
            "<tr>\n" +
            "<th style=\"text-align:center;border: 1px solid black;\">PEELS</th>\n" +
            "<th style=\"text-align:center;border: 1px solid black;\">INGREDIENTS</th>\n" +
            "<th style=\"text-align:center;border: 1px solid black;\">INDICATIONS</th>\n" +
            "</tr>\n" +
            "</thead>\n" +
            "<tbody>\n" +
            "<tr>\n" +
            "<td style=\"text-align:center;border: 1px solid black;\">FUSION PEEL -A</td>\n" +
            "<td style=\"text-align:left;border: 1px solid black;\">Lactic 45%+Citric 5%+Pyruvic 5%+Arginine 5%(Hydroalchol Base)</td>\n" +
            "<td style=\"text-align:left;border: 1px solid black;\">Wrinckles,Hyper Pigmentation, Dark Circle,Aging skin</td>\n" +
            "</tr>\n" +
            "<tr>\n" +
            "<td style=\"text-align:center;border: 1px solid black;\">FUSION PEEL -B</td>\n" +
            "<td style=\"text-align:left;border: 1px solid black;\">Lactic 15% + Kojic 5% + Citric 5%+HQ 2% + Sal 2%</td>\n" +
            "<td style=\"text-align:left;border: 1px solid black;\">Skin Lightening,Melasma,Photo Damaged skin</td>\n" +
            "</tr>\n" +
            "<tr>\n" +
            "<td style=\"text-align:center;border: 1px solid black;\">FUSION PEEL -C</td>\n" +
            "<td style=\"text-align:left;border: 1px solid black;\">Sal 17%+Lactic 17%+citric 8% + Gly 15%</td>\n" +
            "<td style=\"text-align:left;border: 1px solid black;\">Anti acne,Hyper Pigmentation,Anti Aging,Freckles</td>\n" +
            "</tr>\n" +
            "</tbody>\n" +
            "</table>";
    static String phtml = html.replace("\"", "'");
}
