package lazhora.ui.utils;

import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import lazhora.R;
import lazhora.ui.data.Item;

import static android.content.ContentValues.TAG;

/**
 * The type Custom adapter.
 */
public class CustomAdapter extends BaseAdapter {

    private final Context context;
    private final List<Item> productlist;
    private Item singleproduct;

    /**
     * Instantiates a new Custom adapter.
     *
     * @param context     the context
     * @param productlist the productlist
     */
    public CustomAdapter(Context context, List<Item> productlist) {
        this.context = context;
        this.productlist = productlist;

    }

    @Override
    public int getCount() {
        return productlist.size();
    }

    @Override
    public Object getItem(int position) {
        return productlist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return productlist.indexOf(getItem(position));
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.item_row, null);

            holder = new ViewHolder();

            holder.itemName = (TextView) convertView.findViewById(R.id.itemname);
            holder.desc = (TextView) convertView.findViewById(R.id.itemdesc);
            holder.imageid = (ImageView) convertView.findViewById(R.id.itemimage);

            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        singleproduct = productlist.get(position);

//        Resources res = context.getResources();
//        String proName = String.format(res.getString(R.string.title_product), singleproduct.name);

        //holder.itemName.setText(Html.fromHtml(proName));
        holder.itemName.setText(Html.fromHtml(singleproduct.name.trim()));
        holder.desc.setText(singleproduct.desc);
        try {
//            Log.e(TAG, "getView: " + singleproduct.imageid);
            String pic = singleproduct.imageid;

            int resID = context.getResources().getIdentifier(pic.substring(0, pic.lastIndexOf(".")),
                    "drawable", context.getPackageName());

            Glide.with(context).load("").thumbnail(0.5f)
                    .crossFade()
                    .placeholder(context.getResources().getDrawable(resID))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.imageid);
         //   holder.imageid.setImageDrawable(context.getResources().getDrawable(resID));
        } catch (Exception e) {
            Log.e(TAG, singleproduct.imageid + "getView: " + e.toString());
        }

        return convertView;
    }

    /**
     * The type View holder.
     */
    static class ViewHolder {

        /**
         * The Item name.
         */
        public TextView itemName, /**
         * The Desc.
         */
        desc;
        /**
         * The Imageid.
         */
        public ImageView imageid;

    }
}
