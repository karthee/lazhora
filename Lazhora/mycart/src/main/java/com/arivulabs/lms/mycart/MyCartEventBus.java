package com.arivulabs.lms.mycart;

/**
 * Created by arivuventures on 2/2/17.
 */

public class MyCartEventBus {
    private int price;

    public MyCartEventBus(int Price) {
        this.price = price;
    }

    public int getPrice() {
        return this.price;
    }
}
