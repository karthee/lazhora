package lazhora.ui.Screens.presenter;

import android.util.Log;

import org.json.JSONObject;

import lazhora.ui.utils.JsonparserObject;
import lazhora.ui.utils.SessionMaintainence;

import static lazhora.ui.api.EnquireApi.enquire;

/**
 * Created by Karthee on 19/01/17.
 */
public class ProductDetailsPresenterImplementation implements ProductDetailsPresenter {

    /**
     * The Details listener.
     */
    ProductDetailsListener detailsListener;
    private SessionMaintainence session;
    private JSONObject json;
    /**
     * The constant KEY_OUTPUT_STATUS.
     */
    public static final String KEY_OUTPUT_STATUS = "status";
    /**
     * The constant KEY_OUTPUT_DATA.
     */
    public static final String KEY_OUTPUT_DATA = "message";
    private boolean status;
    private String datav;

    /**
     * Instantiates a new Product details presenter implementation.
     *
     * @param listener the listener
     */
    public ProductDetailsPresenterImplementation(ProductDetailsListener listener) {
        this.detailsListener = listener;
    }

    @Override
    public void enquire(String proName) {
        JsonparserObject jparser = JsonparserObject.getInstance();
        session = SessionMaintainence.getInstance();

        detailsListener.showProgress();
        try {
            JSONObject jobj = new JSONObject();

            jobj.put("product", proName);
            jobj.put("name", session.getCustomerInfo().get(SessionMaintainence.KEY_CUS_NAME));
            jobj.put("phone_one", session.getCustomerInfo().get(SessionMaintainence.KEY_PHNUM1));
            jobj.put("phone_teo", session.getCustomerInfo().get(SessionMaintainence.KEY_PHNUM2));

            json = jparser.makeHttpRequest(session.getIp() + enquire,
                    "POST",
                    jobj.toString());

            status = json.getBoolean(KEY_OUTPUT_STATUS);
            datav = json.getString(KEY_OUTPUT_DATA);

        } catch (Exception e) {
            Log.e("OnEnquire", e.toString());

        }
        if (status) {
            detailsListener.hideProgress();
            detailsListener.success(json);



        } else {
            detailsListener.hideProgress();

            detailsListener.failure();

        }
    }

    /**
     * The interface Product details listener.
     */
    public interface ProductDetailsListener {
        /**
         * Success.
         *
         * @param json the json
         */
        void success(JSONObject json);

        /**
         * Failure.
         */
        void failure();

        /**
         * Show progress.
         */
        void showProgress();

        /**
         * Hide progress.
         */
        void hideProgress();
    }
}
