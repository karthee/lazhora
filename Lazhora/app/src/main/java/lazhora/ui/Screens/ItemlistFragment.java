package lazhora.ui.Screens;

import android.content.Intent;
import android.database.SQLException;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import lazhora.R;
import lazhora.ui.data.Item;
import lazhora.ui.data.JsonData;
import lazhora.ui.data.JsonDataCopy;
import lazhora.ui.db.DBManager;
import lazhora.ui.utils.CustomAdapter;
import lazhora.ui.utils.SessionMaintainence;

/**
 * A placeholder fragment containing a simple view.
 */
public class ItemlistFragment extends Fragment {

    private List<Item> itemlist;
    /**
     * The Session.
     */
    SessionMaintainence session;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);

        GridView rv = (GridView) rootView.findViewById(R.id.itemlist);

        itemlist = new ArrayList<>();
        session = SessionMaintainence.getInstance();
        createList();

        CustomAdapter ca = new CustomAdapter(getContext(), itemlist);
        rv.setAdapter(ca);

        rv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try {
                    String product_title = ((TextView) view.findViewById(R.id.itemname)).getText().toString();

                    Log.e("gridview", "onItemClick: " + product_title);

                    Intent i = new Intent(getActivity(), ProductDetails.class);
                    i.putExtra("productname", product_title);
                    startActivity(i);
//                    getActivity().finish();
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("Ex_clicking_grid", e.toString());
                }
            }
        });
        return rootView;
    }

    private void createList() {
        try {
            final DBManager myDbHelper = new DBManager(getActivity());

            myDbHelper.openDataBase();
            try {
                JSONObject products = new JSONObject(JsonDataCopy.jsondata);
                JSONArray productsjsonarray = products.getJSONArray("data");
                for (int i = 0; i < productsjsonarray.length(); i++) {
                    JSONObject jobj = productsjsonarray.getJSONObject(i);

                    Object priceObj = jobj.get("price");
                    String price = null;

                    if (priceObj instanceof String) {
                        price = (String) priceObj;
                        Log.e("price@adding_String", jobj.getString("product_title") + ", " + price);
                    } else if (priceObj instanceof JSONArray) {
                        StringBuilder priceBuilder = new StringBuilder();
                        JSONArray priceArr = (JSONArray) priceObj;

                        for (int j = 0; j < priceArr.length(); j++) {
                            if (j == 0) {
                                priceBuilder.append(priceArr.getString(j));
                            } else {
                                priceBuilder.append("," + priceArr.getString(j));
                            }
                        }
                        price = String.valueOf(priceBuilder);

                        Log.e("price@adding_Array", jobj.getString("product_title") + ", " + price);
                    }

                    myDbHelper.addProducts(String.valueOf(
                            jobj.getInt("product_id")),
                            jobj.getString("product_title"),
                            jobj.getString("product_desc"),
                            price.trim(),
                            jobj.getString("tag"),
                            jobj.getString("picture"),
                            jobj.getString("category"),
                            jobj.getString("picture_name"));
                }
            } catch (JSONException e) {
                Log.e("json", "createList: " + e.toString());
            }

            itemlist = myDbHelper.getProductsByCategory(session.getCategory());
            myDbHelper.close();
        } catch (SQLException e) {
            Log.e("sql", "createList: " + e.toString());
        }


    }
}
