package lazhora.presenter;

import lazhora.views.ProductsPresenter;

/**
 * Created by Karthee on 17/03/17.
 */

public class ProductsListImplementation implements ProductsPresenter {

    ProductsListListener listener;

    public ProductsListImplementation(ProductsListListener listener) {
        this.listener = listener;
    }

    @Override
    public void getFilteredResult(CharSequence constraint) {

    }

    public interface ProductsListListener {
    }
}
