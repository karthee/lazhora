package lazhora.ui.Screens;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.AppBarLayout.OnOffsetChangedListener;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.ButterKnife;
import lazhora.R;
import lazhora.R.color;
import lazhora.R.drawable;
import lazhora.R.id;
import lazhora.R.layout;
import lazhora.R.string;
import lazhora.R.style;
import lazhora.ui.Screens.presenter.ProductDetailsPresenterImplementation.ProductDetailsListener;
import lazhora.ui.api.EnquireApi;
import lazhora.ui.data.Item;
import lazhora.ui.data.JsonDataCopy;
import lazhora.ui.db.DBManager;
import lazhora.ui.utils.CustomProgressDialog;
import lazhora.ui.utils.Font;
import lazhora.ui.utils.NetworkConnectionVerification;
import lazhora.ui.utils.SessionMaintainence;

import static android.content.ContentValues.TAG;

/**
 * Created by Arivu on 26-09-2016.
 */

public class ProductDetails extends AppCompatActivity implements
        ProductDetailsListener {

    static String fileName = "mycart.json";
    final ArrayList<String> s = new ArrayList<String>();
    final ArrayList<String> w = new ArrayList<String>();
    // TextView productTag;
    // TextView productTags;
    final ArrayList<Integer> q = new ArrayList<Integer>();
    Font font;
    //    @BindView(R.id.product_title)
    TextView productTitle;
    //    @BindView(R.id.product_price)
    TextView productPrice;
    //    @BindView(R.id.product_desc_title)
    TextView productDescTitle;
    //    @BindView(R.id.product_desc)
    WebView productDesc;
    //    @BindView(R.id.appbar)
    AppBarLayout appbar;
    //    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbarLayout;
    //    @BindView(R.id.imageView)
    ImageView productImage;
    //    @BindView(R.id.imageViewname)
    ImageView productimagename;
    //    @BindView(R.id.imagePack)
    ImageView imagePack;
    //    @BindView(R.id.taglayout)
    LinearLayout taglayout;
    //    @BindView(R.id.buying_options)
//    Button buyingOptions;
    TextView buyingOptions;

    RelativeLayout buyingOptionsLO;

    LinearLayout items;
    //    @BindView(R.id.proDescLayout)
    LinearLayout proDescLayout;
    //    @BindView(R.id.proDescLayout2)
    LinearLayout proDescLayout2;
    List<HashMap<String, Object>> row = new ArrayList<HashMap<String, Object>>();
    List<HashMap<String, Object>> row2 = new ArrayList<HashMap<String, Object>>();
    //    @BindView(R.id.default_content2)
    LinearLayout default_content2;
    LinearLayout dynamicContent;
    //    @BindView(R.id.toolbar)
    Toolbar toolbarview;
    ImageView removeItem;
    String category = "";
    int resID;
    int quantity;
    int proPrice;

    String[] prices;
    StringBuilder strengths, packs, custom_packs, sizes;
    RelativeLayout relativeLayout;
    private String proName;
    private NetworkConnectionVerification ncv;
    private CustomProgressDialog progressBar;

    SessionMaintainence sm;

    public static void saveData(Context context, String mJsonResponse) {
        try {
            FileWriter file = new FileWriter(context.getFilesDir().getPath() + "/" +
                    ProductDetails.fileName);
            file.write(mJsonResponse);
            file.flush();
            file.close();
            Log.e("Write Success", String.valueOf(file));

        } catch (IOException e) {
            Log.e("Error in Writing", e.getLocalizedMessage());
        }
    }

    public static String getData(Context context) {
        try {
            File f = new File(context.getFilesDir().getPath() + "/" + ProductDetails.fileName);
            //check whether file exists
            FileInputStream is = new FileInputStream(f);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();

            Log.e("Read Success", new String(buffer, "UTF-8"));
            return new String(buffer, "UTF-8");
        } catch (IOException e) {
            Log.e("File not exist", e.getLocalizedMessage());
            return null;
        }
    }

    public static int getScreenWidth(Activity activity) {
        Point size = new Point();
        activity.getWindowManager().getDefaultDisplay().getSize(size);
        return size.x;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layout.activity_product_details);

//        ButterKnife.bind(this);

        sm = new SessionMaintainence(this);

        productTitle = (TextView) findViewById(id.product_title);
        productPrice = (TextView) findViewById(id.product_price);

        productDesc = (WebView) findViewById(id.product_desc);
        productDescTitle = (TextView) findViewById(id.product_desc_title);

        appbar = (AppBarLayout) findViewById(id.appbar);
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(id.collapsing_toolbar);

        productImage = (ImageView) findViewById(id.imageView);
        productimagename = (ImageView) findViewById(id.imageViewname);
        imagePack = (ImageView) findViewById(id.imagePack);

        taglayout = (LinearLayout) findViewById(id.taglayout);
//        buyingOptions = (Button) findViewById(id.buying_options);
        buyingOptions = (TextView) findViewById(id.buying_options);
        buyingOptionsLO = (RelativeLayout) findViewById(id.buying_optionsLO);
        proDescLayout = (LinearLayout) findViewById(id.proDescLayout);
        proDescLayout2 = (LinearLayout) findViewById(id.proDescLayout2);

        default_content2 = (LinearLayout) findViewById(id.default_content2);

        toolbarview = (Toolbar) findViewById(id.toolbar);

        setSupportActionBar(toolbarview);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        toolbarTextAppearance();

        font = new Font(this);

        SpannableString pageTitle = new SpannableString(getSupportActionBar().getTitle());
        pageTitle.setSpan(font.robotoMedium, 0, pageTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        getSupportActionBar().setTitle(pageTitle);

        collapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(android.R.color.transparent)); //
        collapsingToolbarLayout.setCollapsedTitleTextColor(getResources().getColor(color.colorAccent)); //Color of your

        //enable JavaScript ***
        productDesc.getSettings().setJavaScriptEnabled(true);
        productDesc.getSettings().setLoadWithOverviewMode(true);

        String product_title = getIntent().getStringExtra("productname");
        Log.e("product_details", "onCreate: " + product_title);

        ncv = new NetworkConnectionVerification(this);

        /*try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/

        new OnLoad().execute(product_title);

        try {
            if (product_title.contains("Mask")) {
                imagePack.setVisibility(View.VISIBLE);
            } else {
                imagePack.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            Log.e("onCreate: ", e.toString());
        }

        productPrice.setTypeface(font.robotoMedium);
        productDescTitle.setTypeface(font.robotoMedium);

    }

    private void toolbarTextAppearance() {
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(style.collapsedappbar);
        collapsingToolbarLayout.setExpandedTitleTextAppearance(style.expandedappbar);
        // colloapsing toolbar listener
        appbar.addOnOffsetChangedListener(new OnOffsetChangedListener
                () {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (verticalOffset == -collapsingToolbarLayout.getHeight() +
                        appBarLayout.getHeight()) {

                    collapsingToolbarLayout.setExpandedTitleTextAppearance(style.ExpandedAppBarPlus1);

                    Drawable upArrow = getResources().getDrawable(drawable.back_black);
                    getSupportActionBar().setHomeAsUpIndicator(upArrow);
                } else if (verticalOffset < -collapsingToolbarLayout.getHeight() +
                        appBarLayout.getHeight()) {

                    Drawable upArrow = getResources().getDrawable(drawable.back_white);
                    getSupportActionBar().setHomeAsUpIndicator(upArrow);
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setMessage("Not interested in buying this product?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
//                startActivity(new Intent(ProductDetails.this, HomeActivity.class));
                finish();

                dialogInterface.dismiss();
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                dialogInterface.dismiss();
            }
        });


        AlertDialog dialog = builder.create();
        dialog.show();
        dialog.getButton(dialog.BUTTON_NEGATIVE).setTextColor(
                getResources().getColor(color.colorDelete));
        dialog.getButton(dialog.BUTTON_POSITIVE).setTextColor(
                getResources().getColor(color.colorPrimary));
    }

    public void onEnquire(View view) {
        try {
            /*ProductDetailsPresenter presenter = new ProductDetailsPresenterImplementation(this);
            presenter.enquire(proName);*/

            if (ncv.isNetworkAvailable()) {
                new EnquireApi(this).execute(proName);
            } else {
                Toast.makeText(this, "Please check for internet connection.",
                        Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onBuyingOptions(View view) {

        String[] sArray = new String[0];
        String[] wArray = new String[0];
        int[] qArray = new int[0];

        String buyText = buyingOptions.getText().toString();

        if (buyText.equalsIgnoreCase("BUYING OPTIONS")) {

            buyingOptionDialog();

        } else if (buyText.equalsIgnoreCase("ADD TO CART")) {

            confirmDialog();


        } else if (buyText.equalsIgnoreCase("MY CART")) {

            this.getSupportActionBar().getTitle();
            Log.e("sArray", sArray.toString());
            this.startActivity(new Intent(this, MyCart.class));
            this.finish();
        }
    }

    private void confirmDialog() {
        Builder builder = new Builder(this);
        builder.setCancelable(false);
        builder.setMessage("Want to add this product into your cart?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                Log.e("yes", "clicked");
                String[] sArray = new String[0];
                String[] wArray = new String[0];
                int[] qArray = new int[0];

//                String string = "";
                String string = getData(ProductDetails.this);
                Log.e("string", "content is... " + string);

                if (quantity != 0) {
                    JSONArray resultSet;
                    try {
                        if (string != null) {
                            resultSet = new JSONArray(string);
                        } else {
                            resultSet = new JSONArray();
                        }

                        JSONObject values = new JSONObject();

                        values.put("quantity", quantity);
                        values.put("price", proPrice);
                        int overallPrice = proPrice * quantity;
                        values.put("overall_price", overallPrice);
                        values.put("type", 3);
                        values.put("picture", resID);
                        values.put("category", category);
                        values.put("title", getSupportActionBar().getTitle());
                        values.put("viewtype", 1);

                        resultSet.put(values);

                        Log.e("resultSet_myCart", String.valueOf(resultSet));

                        saveData(ProductDetails.this, resultSet.toString());
                        row.clear();

                        sm.setCartListCount(resultSet.length());

//                        startActivity(new Intent(ProductDetails.this, HomeActivity.class));
                        finish();

                        dialogInterface.dismiss();
                    } catch (JSONException e) {
                        Log.e("Exception", e.toString());
                    }
                } else {

                    ArrayList<String> sArr;
                    ArrayList<String> wArr = null;

                    if (category.equals("CHEMICAL PEEL")) {
                        sArr = new ArrayList<>();
                        wArr = new ArrayList<>();

                        for (String s1 : s) {
                            sArr.add(s1);
                        }

                        sArray = new String[sArr.size()];
                        for (int a = 0; a < sArray.length; a++) {
                            sArray[a] = sArr.get(a);
                            Log.e("sArray[i]", sArray[a]);
                        }

                        for (String w1 : w) {
                            wArr.add(w1);
                        }

                        wArray = new String[wArr.size()];
                        for (int j = 0; j < wArray.length; j++) {
                            wArray[j] = wArr.get(j);
                        }
                    } else if (category.equals("SERUMS - SKIN CARE")
                            || category.equals("SERUMS - HAIR CARE")
                            || category.equals("CREAMS")
                            || category.equals("ROLLERS")) {
                        wArr = new ArrayList<>();

                        for (String w1 : w) {
                            wArr.add(w1);
                        }

                        wArray = new String[wArr.size()];
                        for (int k = 0; k < wArray.length; k++) {
                            wArray[k] = wArr.get(k);
                        }
                    } /*else if (category.equals("Other")) {

            }*/

                    qArray = new int[q.size()];
                    for (int l = 0; l < qArray.length; l++) {
                        qArray[l] = q.get(l).intValue();
                    }

                    JSONArray resultSet;
                    try {
                        if (string != null) {
                            resultSet = new JSONArray(string);
                        } else {
                            resultSet = new JSONArray();
                        }

                        JSONObject values = new JSONObject();

                        JSONArray content = new JSONArray();

                        int netPrice = 0, overallPrice = 0;

                        for (int m = 0; m < row.size(); m++) {
                            HashMap<String, Object> roww = row.get(m);

                            Log.e("strength()", String.valueOf(roww.get("strength")));
                            JSONObject contentValue = new JSONObject();

                            if (category.equals("CHEMICAL PEEL")) {
                                contentValue.put("strength", roww.get("strength"));
                                contentValue.put("weight", roww.get("weight"));
                                contentValue.put("quantity", roww.get("quantity"));
                                contentValue.put("price", proPrice);
                                overallPrice += proPrice;
                            } else if (category.equals("SERUMS - SKIN CARE")
                                    || category.equals("SERUMS - HAIR CARE")
                                    || category.equals("CREAMS")
                                    || category.equals("ROLLERS")) {
                                contentValue.put("weight", roww.get("weight"));
                                contentValue.put("quantity", roww.get("quantity"));
                                contentValue.put("price",
                                        Integer.parseInt(roww.get("price").toString()));

                                int quantity = Integer.parseInt(roww.get("quantity").toString());
//                                netPrice = proPrice * quantity;
                                netPrice = Integer.parseInt(roww.get("price").toString()) * quantity;
                                contentValue.put("net_price", netPrice);

                                overallPrice += netPrice;
                            }
                            content.put(contentValue);
                        }
                        values.put("requirements", content);
                        values.put("overall_price", overallPrice);

                        if (category.equals("CHEMICAL PEEL")) {
                            values.put("type", 1);
                        } else if (category.equals("SERUMS - SKIN CARE")
                                || category.equals("SERUMS - HAIR CARE")
                                || category.equals("CREAMS")
                                || category.equals("ROLLERS")) {
                            values.put("type", 2);
                        }

                        values.put("picture", resID);
                        values.put("category", category);
                        values.put("title", getSupportActionBar().getTitle());
                        values.put("viewtype", 0);

                        resultSet.put(values);

                        Log.e("resultSet", String.valueOf(resultSet));

                        saveData(ProductDetails.this, resultSet.toString());
                        row.clear();


                        sm.setCartListCount(resultSet.length());

//                            startActivity(new Intent(ProductDetails.this, HomeActivity.class));
                        finish();

                        dialogInterface.dismiss();

                    } catch (JSONException e) {
                        Log.e("Exception", e.toString());
                    }
                }
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
        dialog.getButton(dialog.BUTTON_NEGATIVE).setTextColor(
                getResources().getColor(color.colorDelete));
        dialog.getButton(dialog.BUTTON_POSITIVE).setTextColor(
                getResources().getColor(color.colorPrimary));

    }

    public void buyingOptionDialog() {

        final Dialog dialog = new Dialog(this/*, android.R.style.Theme_Holo_Light_Dialog*/);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(layout.dialog_buying_options);

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int screenWidth = (int) (metrics.widthPixels * 0.99);

        dialog.getWindow().setLayout(screenWidth, ViewGroup.LayoutParams.WRAP_CONTENT);

        //---------------
        ButterKnife.bind(this, dialog);
        items = (LinearLayout) dialog.findViewById(id.items);
        Button done = (Button) dialog.findViewById(id.done);
        done.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
//                dynamicContent.removeAllViews();

                dialog.dismiss();

                TextView selectedRequirements = (TextView) findViewById(id.selected_requirements);
                TextView change = (TextView) findViewById(id.change);

                LinearLayout default_content = (LinearLayout) findViewById(id.default_content);

                selectedRequirements.setText("Selected Requirements");
                selectedRequirements.setCompoundDrawablesRelativeWithIntrinsicBounds(
                        null, null, null, null
                );
                change.setVisibility(View.VISIBLE);
                change.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dynamicContent.removeAllViews();

                        buyingOptionDialog();
                    }
                });

                default_content.setVisibility(View.GONE);

                LinearLayout defaultTitle = (LinearLayout) findViewById(id.default_title);
                defaultTitle.setVisibility(View.VISIBLE);

                TextView titleStrength = (TextView) defaultTitle.findViewById(id.title_strength);
                TextView titlePack = (TextView) defaultTitle.findViewById(id.title_pack);

                dynamicContent = (LinearLayout) findViewById(id.dynamic_content);
                dynamicContent.setVisibility(View.VISIBLE);

                for (int i = 0; i < row.size(); i++) {
                    HashMap<String, Object> roww = row.get(i);

                    Log.e("rowLayoutIdMap: ", String.valueOf(roww.get("layout_id")));
                    int id = (int) roww.get("layout_id");

                    LinearLayout linearLayout = (LinearLayout) View.inflate(ProductDetails.this,
                            layout.item_product_specification, null);

                    TextView strengthItem = (TextView) linearLayout.findViewById(R.id.item_strength);

                    if (category.equals("CHEMICAL PEEL")) {
                        titleStrength.setVisibility(View.VISIBLE);

                        strengthItem.setVisibility(View.VISIBLE);
                        String strength = roww.get("strength").toString();
                        strengthItem.setText(strength);

                        s.add(strength);
                        Log.e("s", String.valueOf(s));

                        TextView wealthItem = (TextView) linearLayout.findViewById(R.id.item_weight);
                        String weight = roww.get("weight").toString();
                        wealthItem.setText(weight);

                        w.add(weight);

                        TextView quantityItem = (TextView) linearLayout.findViewById(R.id.item_quantity);
                        String quantity = (String) roww.get("quantity");
                        quantityItem.setText(quantity);

                        q.add(Integer.valueOf(quantity));
                    } else if (category.equals("SERUMS - SKIN CARE")
                            || category.equals("SERUMS - HAIR CARE")
                            || category.equals("CREAMS")
                            || category.equals("ROLLERS")) {

                        titleStrength.setVisibility(View.GONE);

                        if (category.equals("ROLLERS")) {
                            titlePack.setText("Size");
                        } else {
                            titlePack.setText("Pack");
                        }

                        strengthItem.setVisibility(View.GONE);

                        TextView wealthItem = (TextView) linearLayout.findViewById(R.id.item_weight);
                        String weight = roww.get("weight").toString();
                        wealthItem.setText(weight);

                        w.add(weight);

                        TextView quantityItem = (TextView) linearLayout.findViewById(R.id.item_quantity);
                        String quantity = (String) roww.get("quantity");
                        quantityItem.setText(quantity);

                        q.add(Integer.valueOf(quantity));
                    }

                    dynamicContent.addView(linearLayout);
                }

                buyingOptions.setText("ADD TO CART");

                buyingOptions.setCompoundDrawablesWithIntrinsicBounds(
                        getResources().getDrawable(drawable.cart_add),
                        null, null, null);
                buyingOptions.setCompoundDrawablePadding(4);
            }
        });

        Button cancel = (Button) dialog.findViewById(id.cancel);
        cancel.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                row.clear();

               /* buyingOptions.setText("BUYING OPTIONS");
                buyingOptions.setCompoundDrawablesWithIntrinsicBounds(
                        getResources().getDrawable(R.drawable.cart_white),
                        null, null, null);*/
            }
        });

        if (row.size() != 0) {
            Log.e("row_size", String.valueOf(row.size()));

            for (HashMap<String, Object> roww : row) {
                row2.add(roww);
            }
            Log.e("lRowBefore", String.valueOf(row2.size()));
            row.clear();
            Log.e("lRowAfter", String.valueOf(row2.size()));
            createRow(row2);
            addButtonView();

        } else {
            createRow();
            addButtonView();
        }

        dialog.show();
    }

    public void addButtonView() {
        LayoutParams params = new LayoutParams(
                130, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.CENTER_HORIZONTAL;
        params.setMargins(0, 4, 0, 8);

        final TextView addButton = new TextView(this);
        addButton.setTag("dialog_item_add");
        addButton.setLayoutParams(params);
        addButton.setBackground(getResources().getDrawable(drawable.add_bg));
        addButton.setGravity(Gravity.CENTER);
        addButton.setPadding(8, 8, 8, 8);
        addButton.setText("ADD");
        addButton.setTextColor(getResources().getColor(color.colorPrimary));
        addButton.setTextSize(14);
        addButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                addButton.setVisibility(View.GONE);
                Log.e("row_size@add", String.valueOf(row.size()));
                removeItem.setVisibility(View.VISIBLE);

                Log.e("createRow", String.valueOf(row));


                createRow();
                addButtonView();
            }
        });

        items.addView(addButton);
    }

    public void createRow(List<HashMap<String, Object>> rowItems) {

        int lastItem = rowItems.size() - 1;
        Log.e("lastItem", String.valueOf(lastItem));

        int curItem = 0;

        for (HashMap<String, Object> roww : rowItems) {
            final RelativeLayout itemDialogLO = (RelativeLayout) View.inflate(this,
                    layout.item_dialog, null);
            itemDialogLO.setId(View.generateViewId());

            LinearLayout topLO = (LinearLayout) itemDialogLO.findViewById(id.top);
            topLO.setId(View.generateViewId());

            LinearLayout strenghtLO = (LinearLayout) itemDialogLO.findViewById(id.strength_layout);
            LinearLayout weightLO = (LinearLayout) itemDialogLO.findViewById(id.weight_layout);
            LinearLayout quantityLO = (LinearLayout) itemDialogLO.findViewById(id.quantity_layout);
            final LinearLayout priceLO = (LinearLayout) itemDialogLO.findViewById(id.price_layout);

            final Spinner strengthSpinner = (Spinner) strenghtLO.findViewById(id.strength_spinner);
            final Spinner weightSpinner = (Spinner) weightLO.findViewById(id.weight_spinner);
            final TextView quantityText = (TextView) quantityLO.findViewById(id.quantity_text);
            final TextView priceText = (TextView) priceLO.findViewById(id.price_text);
            final TextView netPriceText = (TextView) priceLO.findViewById(id.net_price_text);

            ImageView increaseQuantity = (ImageView) quantityLO.findViewById(id.increase);
            ImageView decreaseQuantity = (ImageView) quantityLO.findViewById(id.decrease);

            if (category.equals("CHEMICAL PEEL")) {
                strenghtLO.setVisibility(View.VISIBLE);
                weightLO.setVisibility(View.VISIBLE);
                priceLO.setVisibility(View.GONE);

                String[] strArray = strengths.toString().split(",");

                List<String> strengths = new ArrayList<String>();
                for (String s : strArray) {
                    strengths.add(s);
                }

                ArrayAdapter<String> strengthAdapter = new ArrayAdapter<String>(this,
                        android.R.layout.simple_spinner_item, strengths);

                strengthAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                strengthSpinner.setDropDownWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
                strengthSpinner.setAdapter(strengthAdapter);
                Log.e("change_strength", String.valueOf(roww.get("strength")));

                int i = 0;
                for (String strength : strengths) {
                    if (roww.get("strength").equals(strength)) {
                        strengthSpinner.setSelection(i);

                        break;
                    }
                    i++;
                }

                final List<String> weights = new ArrayList<String>();

                strengthSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                        weights.clear();

                        if (strengthSpinner.getSelectedItem().equals("Custom")) {
                            Log.e("custom_packs", custom_packs.toString());

                            if (custom_packs.toString().contains(",")) {
                                String[] pacArray = custom_packs.toString().split(",");

                                for (String s : pacArray) {
                                    weights.add(s);
                                }
                            } else {
                                weights.add(custom_packs.toString());
                            }
                        } else {

                            Log.e("packs", packs.toString());

                            if (packs.toString().contains(",")) {
                                String[] pacArray = packs.toString().split(",");

                                for (String s : pacArray) {
                                    weights.add(s);
                                }
                            } else {
                                weights.add(packs.toString());
                            }
                        }

                        for (int i = 0; i < row.size(); i++) {

                            HashMap<String, Object> roww = row.get(i);

                            int id = (int) roww.get("layout_id");

                            if (id == itemDialogLO.getId()) {
                                roww.put("strength", strengthSpinner.getSelectedItem());
                                roww.put("weight", weightSpinner.getSelectedItem());
                                roww.put("quantity", quantityText.getText());
                            }
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

                Log.e("packs", packs.toString());

                if (packs.toString().contains(",")) {
                    String[] pacArray = packs.toString().split(",");

                    for (String s : pacArray) {
                        weights.add(s);
                    }
                } else {
                    weights.add(packs.toString());
                }

                ArrayAdapter<String> weightAdapter = new ArrayAdapter<String>(this,
                        android.R.layout.simple_spinner_item, weights);

                weightAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                weightSpinner.setDropDownWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
                weightSpinner.setAdapter(weightAdapter);
                Log.e("change_weight: ", String.valueOf(roww.get("weight")));

                int j = 0;
                for (String weight : weights) {
                    if (roww.get("weight").equals(weight)) {
                        int spinnerPosition = strengthAdapter.getPosition(weight);
//                        weightSpinner.setPrompt(strengthAdapter.getItem(j));

                        Log.e(TAG, "spinner pos: " + (j));
                        Log.e(TAG, "spinner pos: " + weight);

                        weightSpinner.setSelection(j);

                        break;
                    }
                    j++;
                }

                weightSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                        for (int i = 0; i < row.size(); i++) {

                            HashMap<String, Object> roww = row.get(i);

                            int id = (int) roww.get("layout_id");

                            if (id == itemDialogLO.getId()) {
                                roww.put("strength", strengthSpinner.getSelectedItem());
                                roww.put("weight", weightSpinner.getSelectedItem());
                                roww.put("quantity", quantityText.getText());
                            }
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

                HashMap<String, Object> rowData = new HashMap<String, Object>();

                rowData.put("layout_id", itemDialogLO.getId());
//                Log.e("rowLayoutId: ", String.valueOf(rowLayout.getId()));

                Log.e("onClick: ", strengthSpinner.getSelectedItem().toString());

                rowData.put("strength", strengthSpinner.getSelectedItem());
                rowData.put("weight", weightSpinner.getSelectedItem());
                rowData.put("quantity", quantityText.getText());

                row.add(rowData);
                Log.e("rowData", String.valueOf(row));

            } else if (category.equals("SERUMS - SKIN CARE")
                    || category.equals("SERUMS - HAIR CARE")
                    || category.equals("CREAMS")
                    || category.equals("OTHER")) {
                strenghtLO.setVisibility(View.GONE);
                weightLO.setVisibility(View.VISIBLE);
//              priceLO.setVisibility(View.VISIBLE);

                String[] pacArray = packs.toString().split(",");

                List<String> weights = new ArrayList<String>();
                for (String s : pacArray) {
                    weights.add(s);
                }

                ArrayAdapter<String> weightAdapter = new ArrayAdapter<String>(this,
                        android.R.layout.simple_spinner_item, weights);

                weightAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                weightSpinner.setDropDownWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
                weightSpinner.setAdapter(weightAdapter);
                weightSpinner.setPrompt((CharSequence) roww.get("weight"));

                int j = 0;
                for (String weight : weights) {
                    if (roww.get("weight").equals(weight)) {
                        int spinnerPosition = weightAdapter.getPosition(weight);
//                        weightSpinner.setPrompt(strengthAdapter.getItem(j));
                        Log.e(TAG, "spinner pos: " + (j - 1));
                        Log.e(TAG, "spinner pos: " + weight);

                        weightSpinner.setSelection(j);

                        break;

                    }
                    j++;
                }

                weightSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {

                        if (prices[position].equals("NA")) {
                            priceLO.setVisibility(View.VISIBLE);
                            priceText.setText("--");
                            netPriceText.setText("--");
                            proPrice = 0;
                        } else {
                            priceLO.setVisibility(View.VISIBLE);
                            priceText.setText(prices[position]);
                            int quantity = Integer.parseInt(quantityText.getText().toString());
                            int unitPrice = Integer.parseInt(priceText.getText().toString());

                            unitPrice *= quantity;
                            netPriceText.setText(String.valueOf(unitPrice));
                            proPrice = Integer.parseInt(prices[position]);
                        }

                        for (int i = 0; i < row.size(); i++) {

                            HashMap<String, Object> roww = row.get(i);

                            int id = (int) roww.get("layout_id");

                            if (id == itemDialogLO.getId()) {
                                roww.put("strength", strengthSpinner.getSelectedItem());
                                roww.put("weight", weightSpinner.getSelectedItem());
                                roww.put("quantity", quantityText.getText());
                                if (prices[position].equals("NA")) {
                                    roww.put("price", proPrice);//
                                } else {
//                                    roww.put("price", prices[position]);
                                    roww.put("price",
                                            Integer.parseInt(netPriceText.getText().toString()) /
                                                    Integer.parseInt(quantityText.getText().toString()));
                                    roww.put("net_price",
                                            Integer.parseInt(netPriceText.getText().toString()));
                                }
                            }
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

                HashMap<String, Object> rowData = new HashMap<String, Object>();

                rowData.put("layout_id", itemDialogLO.getId());
//                Log.e("rowLayoutId: ", String.valueOf(rowLayout.getId()));

                Log.e("onClick: ", weightSpinner.getSelectedItem().toString());

                rowData.put("weight", weightSpinner.getSelectedItem());
                rowData.put("quantity", quantityText.getText());

                row.add(rowData);
            } else if (category.equals("ROLLERS")) {
                strenghtLO.setVisibility(View.GONE);
                weightLO.setVisibility(View.VISIBLE);
//              priceLO.setVisibility(View.VISIBLE);

                String[] pacArray = sizes.toString().split(",");

                List<String> weights = new ArrayList<String>();
                for (String s : pacArray) {
                    weights.add(s);
                }

                ArrayAdapter<String> weightAdapter = new ArrayAdapter<String>(this,
                        android.R.layout.simple_spinner_item, weights);

                weightAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                weightSpinner.setDropDownWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
                weightSpinner.setAdapter(weightAdapter);
                weightSpinner.setPrompt((CharSequence) roww.get("weight"));

                int j = 0;
                for (String weight : weights) {
                    if (roww.get("weight").equals(weight)) {
                        int spinnerPosition = weightAdapter.getPosition(weight);
//                        weightSpinner.setPrompt(strengthAdapter.getItem(j));
                        Log.e(TAG, "spinner pos: " + (j));
                        Log.e(TAG, "spinner pos: " + weight);

                        weightSpinner.setSelection(j);

                        break;

                    }
                    j++;
                }

                weightSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int position,
                                               long l) {

                        if (prices[position].equals("NA")) {
                            priceLO.setVisibility(View.VISIBLE);
                            priceText.setText("--");
                            netPriceText.setText("--");
                            proPrice = 0;
                        } else {
                            priceLO.setVisibility(View.VISIBLE);
                            priceText.setText(prices[position]);
                            int quantity = Integer.parseInt(quantityText.getText().toString());
                            int unitPrice = Integer.parseInt(priceText.getText().toString());

                            unitPrice *= quantity;
                            netPriceText.setText(String.valueOf(unitPrice));
                            proPrice = Integer.parseInt(prices[position]);

                        }

                        for (int i = 0; i < row.size(); i++) {

                            HashMap<String, Object> roww = row.get(i);

                            int id = (int) roww.get("layout_id");

                            if (id == itemDialogLO.getId()) {
                                roww.put("strength", strengthSpinner.getSelectedItem());
                                roww.put("weight", weightSpinner.getSelectedItem());
                                roww.put("quantity", quantityText.getText());
                                if (prices[position].equals("NA")) {
                                    roww.put("price", proPrice);
                                } else {
                                    roww.put("price",
                                            Integer.parseInt(netPriceText.getText().toString()) /
                                                    Integer.parseInt(quantityText.getText().toString()));
                                    roww.put("net_price",
                                            Integer.parseInt(netPriceText.getText().toString()));
                                }
                            }
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

                HashMap<String, Object> rowData = new HashMap<String, Object>();

                rowData.put("layout_id", itemDialogLO.getId());
//                Log.e("rowLayoutId: ", String.valueOf(rowLayout.getId()));

                rowData.put("weight", weightSpinner.getSelectedItem());
                rowData.put("quantity", quantityText.getText());

                row.add(rowData);
            }

            quantityText.setText((CharSequence) roww.get("quantity"));

            increaseQuantity.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    int quantity = Integer.parseInt(quantityText.getText().toString());

                    quantity++;
                    quantityText.setText(String.valueOf(quantity));

                    for (int i = 0; i < row.size(); i++) {

                        HashMap<String, Object> roww = row.get(i);

                        int id = (int) roww.get("layout_id");

                        if (id == itemDialogLO.getId()) {
                            roww.put("strength", strengthSpinner.getSelectedItem());
                            roww.put("weight", weightSpinner.getSelectedItem());
                            roww.put("quantity", quantityText.getText());

                        }
                    }

                    if (proPrice != 0) {
                        int unitPrice = Integer.parseInt(priceText.getText().toString());

                        unitPrice *= quantity;
                        netPriceText.setText(String.valueOf(unitPrice));
                    }
                }
            });

            decreaseQuantity.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    int quantity = Integer.parseInt(quantityText.getText().toString());

                    if (quantity > 0) {
                        quantity--;
                    }
                    quantityText.setText(String.valueOf(quantity));

                    for (int i = 0; i < row.size(); i++) {

                        HashMap<String, Object> roww = row.get(i);

                        int id = (int) roww.get("layout_id");

                        if (id == itemDialogLO.getId()) {
                            roww.put("strength", strengthSpinner.getSelectedItem());
                            roww.put("weight", weightSpinner.getSelectedItem());
                            roww.put("quantity", quantityText.getText());

                            /*int price = (int) roww.get("price");
                            roww.put("net_price", price * quantity);*/
                        }
                    }

                    if (proPrice != 0) {
                        int unitPrice = Integer.parseInt(priceText.getText().toString());

                        unitPrice *= quantity;
                        netPriceText.setText(String.valueOf(unitPrice));
                    }
                }
            });

//            addItem = (Button) itemDialogLO.findViewById(R.id.dialog_item_add);
            RelativeLayout removeItemLO = (RelativeLayout) itemDialogLO.findViewById(id.remove_layout);
            removeItem = (ImageView) itemDialogLO.findViewById(id.dialog_item_remove);

            verifyDialogListSize();

            if (curItem == lastItem) {
                removeItem.setVisibility(View.GONE);

            } else {
                Log.e("curItem", String.valueOf(curItem));
                removeItem.setVisibility(View.VISIBLE);
                curItem++;
            }

            removeItem.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {

                    for (int i = 0; i < row.size(); i++) {
                        if (row.size() == 1) {
                            removeItem.setVisibility(View.GONE);
                        } else {
                            removeItem.setVisibility(View.VISIBLE);
                            HashMap<String, Object> roww = row.get(i);

                            Log.e("rowLayoutIdMap: ", String.valueOf(roww.get("layout_id")));
                            int id = (int) roww.get("layout_id");

                            if (id == itemDialogLO.getId()) {
                                row.remove(i);

                                items.removeView(itemDialogLO);
                            }

                            verifyDialogListSize();
                        }
                    }
                    Log.e("row_size@remove", String.valueOf(row.size()));
                }
            });

            items.addView(itemDialogLO);

            /*LinearLayout.LayoutParams addParams = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
            );

            Button add = new Button(this);
            add.setLayoutParams(addParams);*/
        }
    }

    public void createRow() {
        final RelativeLayout itemDialogLO = (RelativeLayout) View.inflate(this,
                layout.item_dialog, null);
        itemDialogLO.setId(View.generateViewId());

        LinearLayout topLO = (LinearLayout) itemDialogLO.findViewById(id.top);
        topLO.setId(View.generateViewId());

        LinearLayout strenghtLO = (LinearLayout) itemDialogLO.findViewById(id.strength_layout);
        LinearLayout weightLO = (LinearLayout) itemDialogLO.findViewById(id.weight_layout);
        LinearLayout quantityLO = (LinearLayout) itemDialogLO.findViewById(id.quantity_layout);
        final LinearLayout priceLO = (LinearLayout) itemDialogLO.findViewById(id.price_layout);

        final Spinner strengthSpinner = (Spinner) strenghtLO.findViewById(id.strength_spinner);
        final Spinner weightSpinner = (Spinner) weightLO.findViewById(id.weight_spinner);
        final TextView quantityText = (TextView) quantityLO.findViewById(id.quantity_text);
        final TextView priceText = (TextView) priceLO.findViewById(id.price_text);
        final TextView netPriceText = (TextView) priceLO.findViewById(id.net_price_text);

        ImageView increaseQuantity = (ImageView) quantityLO.findViewById(id.increase);
        ImageView decreaseQuantity = (ImageView) quantityLO.findViewById(id.decrease);

        if (category.equals("CHEMICAL PEEL")) {
            strenghtLO.setVisibility(View.VISIBLE);
            weightLO.setVisibility(View.VISIBLE);
            priceLO.setVisibility(View.GONE);

            String[] strArray = strengths.toString().split(",");

            List<String> strengths = new ArrayList<String>();
            for (String s : strArray) {
                strengths.add(s);
            }

            ArrayAdapter<String> strengthAdapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_spinner_item, strengths);

            strengthAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            strengthSpinner.setDropDownWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
            strengthSpinner.setAdapter(strengthAdapter);

            final List<String> weights = new ArrayList<String>();

            strengthSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                    weights.clear();

                    if (strengthSpinner.getSelectedItem().equals("Custom")) {
                        Log.e("custom_packs", custom_packs.toString());

                        if (custom_packs.toString().contains(",")) {
                            String[] pacArray = custom_packs.toString().split(",");

                            for (String s : pacArray) {
                                weights.add(s);
                            }
                        } else {
                            weights.add(custom_packs.toString());
                        }
                    } else {

                        if (packs.toString().contains(",")) {
                            String[] pacArray = packs.toString().split(",");

                            for (String s : pacArray) {
                                weights.add(s);
                            }
                        } else {
                            weights.add(packs.toString());
                        }
                    }

                    for (int i = 0; i < row.size(); i++) {

                        HashMap<String, Object> roww = row.get(i);

                        int id = (int) roww.get("layout_id");

                        if (id == itemDialogLO.getId()) {
                            roww.put("strength", adapterView.getItemAtPosition(position));
                            roww.put("weight", weightSpinner.getSelectedItem());
                            roww.put("quantity", quantityText.getText());
                        }
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            Log.e("packs", packs.toString());

            if (packs.toString().contains(",")) {
                String[] pacArray = packs.toString().split(",");

                for (String s : pacArray) {
                    weights.add(s);
                }
            } else {
                weights.add(packs.toString());
            }

            ArrayAdapter<String> weightAdapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_spinner_item, weights);

            weightAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            weightSpinner.setDropDownWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
            weightSpinner.setAdapter(weightAdapter);
//            weightSpinner.setPrompt(weightAdapter.getItem(0));
            weightSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                    weightSpinner.setPrompt((CharSequence) weightSpinner.getSelectedItem());

                    for (int i = 0; i < row.size(); i++) {

                        HashMap<String, Object> roww = row.get(i);

                        int id = (int) roww.get("layout_id");

                        if (id == itemDialogLO.getId()) {
                            roww.put("strength", strengthSpinner.getSelectedItem());
                            roww.put("weight", adapterView.getItemAtPosition(position));
                            roww.put("quantity", quantityText.getText());
                        }
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            HashMap<String, Object> rowData = new HashMap<String, Object>();

            rowData.put("layout_id", itemDialogLO.getId());
//                Log.e("rowLayoutId: ", String.valueOf(rowLayout.getId()));

            Log.e("onClick: ", strengthSpinner.getSelectedItem().toString());

            rowData.put("strength", strengthSpinner.getSelectedItem());
            rowData.put("weight", weightSpinner.getSelectedItem());
            rowData.put("quantity", quantityText.getText());

            row.add(rowData);
            Log.e("rowData", String.valueOf(row));

        } else if (category.equals("SERUMS - SKIN CARE")
                || category.equals("SERUMS - HAIR CARE")
                || category.equals("CREAMS")) {
            strenghtLO.setVisibility(View.GONE);
            weightLO.setVisibility(View.VISIBLE);

            String[] pacArray = packs.toString().split(",");

            List<String> weights = new ArrayList<String>();
            for (String s : pacArray) {
                weights.add(s);
            }

            ArrayAdapter<String> weightAdapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_spinner_item, weights);

            weightAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            weightSpinner.setDropDownWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
            weightSpinner.setAdapter(weightAdapter);
            weightSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int position,
                                           long l) {

                    if (prices[position].equals("NA")) {
                        priceLO.setVisibility(View.VISIBLE);
                        priceText.setText("--");
                        netPriceText.setText("--");
                        proPrice = 0;
                    } else {
                        priceLO.setVisibility(View.VISIBLE);
                        priceText.setText(prices[position]);
                        int quantity = Integer.parseInt(quantityText.getText().toString());
                        int unitPrice = Integer.parseInt(priceText.getText().toString());

                        unitPrice *= quantity;
                        netPriceText.setText(String.valueOf(unitPrice));
                        proPrice = Integer.parseInt(prices[position]);
                    }

                    for (int i = 0; i < row.size(); i++) {

                        HashMap<String, Object> roww = row.get(i);

                        int id = (int) roww.get("layout_id");

                        if (id == itemDialogLO.getId()) {
                            roww.put("strength", strengthSpinner.getSelectedItem());
                            roww.put("weight", weightSpinner.getSelectedItem());
                            roww.put("quantity", quantityText.getText());

                            if (prices[position].equals("NA")) {
                                roww.put("price", proPrice);//
                            } else {
//                                    roww.put("price", prices[position]);
                                roww.put("price",
                                        Integer.parseInt(netPriceText.getText().toString()) /
                                                Integer.parseInt(quantityText.getText().toString()));
                                roww.put("net_price",
                                        Integer.parseInt(netPriceText.getText().toString()));
                            }
                        }
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            HashMap<String, Object> rowData = new HashMap<String, Object>();

            rowData.put("layout_id", itemDialogLO.getId());
//                Log.e("rowLayoutId: ", String.valueOf(rowLayout.getId()));

            Log.e("onClick: ", weightSpinner.getSelectedItem().toString());

            rowData.put("weight", weightSpinner.getSelectedItem());
            rowData.put("quantity", quantityText.getText());

            row.add(rowData);
        } else if (category.equals("ROLLERS")) {
            strenghtLO.setVisibility(View.GONE);
            weightLO.setVisibility(View.VISIBLE);

            String[] pacArray = sizes.toString().split(",");

            List<String> weights = new ArrayList<String>();
            for (String s : pacArray) {
                weights.add(s);
            }

            ArrayAdapter<String> weightAdapter = new ArrayAdapter<String>(this,
                    android.R.layout.simple_spinner_item, weights);

            weightAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            weightSpinner.setDropDownWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
            weightSpinner.setAdapter(weightAdapter);
            weightSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int position,
                                           long l) {
                    try {

                        if (prices[position].equals("NA")) {
                            priceLO.setVisibility(View.VISIBLE);
                            priceText.setText("--");
                            netPriceText.setText("--");
                            proPrice = 0;
                        } else {
                            priceLO.setVisibility(View.VISIBLE);
                            priceText.setText(prices[position]);
                            int quantity = Integer.parseInt(quantityText.getText().toString());
                            int unitPrice = Integer.parseInt(priceText.getText().toString());

                            unitPrice *= quantity;
                            netPriceText.setText(String.valueOf(unitPrice));
                            proPrice = Integer.parseInt(prices[position]);
                        }

                        for (int i = 0; i < row.size(); i++) {

                            HashMap<String, Object> roww = row.get(i);

                            int id = (int) roww.get("layout_id");

                            if (id == itemDialogLO.getId()) {
                                roww.put("strength", strengthSpinner.getSelectedItem());
                                roww.put("weight", weightSpinner.getSelectedItem());
                                roww.put("quantity", quantityText.getText());
                                if (prices[position].equals("NA")) {
                                    roww.put("price", proPrice);//
                                } else {
//                                    roww.put("price", prices[position]);
                                    roww.put("price",
                                            Integer.parseInt(netPriceText.getText().toString()) /
                                                    Integer.parseInt(quantityText.getText().toString()));
                                    roww.put("net_price",
                                            Integer.parseInt(netPriceText.getText().toString()));
                                }
                            }
                        }
                    } catch (Exception e) {
                        Log.e("onItemSelected_weight", e.toString());
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            HashMap<String, Object> rowData = new HashMap<String, Object>();

            rowData.put("layout_id", itemDialogLO.getId());
//                Log.e("rowLayoutId: ", String.valueOf(rowLayout.getId()));

            Log.e("onClick: ", weightSpinner.getSelectedItem().toString());

            rowData.put("weight", weightSpinner.getSelectedItem());
            rowData.put("quantity", quantityText.getText());

            row.add(rowData);
        }

        quantityText.setText("1");

        increaseQuantity.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                int quantity = Integer.parseInt(quantityText.getText().toString());
                quantity++;

                quantityText.setText(String.valueOf(quantity));

                for (int i = 0; i < row.size(); i++) {

                    HashMap<String, Object> roww = row.get(i);

                    int id = (int) roww.get("layout_id");

                    if (id == itemDialogLO.getId()) {
                        roww.put("strength", strengthSpinner.getSelectedItem());
                        roww.put("weight", weightSpinner.getSelectedItem());
                        roww.put("quantity", quantityText.getText());

                        /*int price = (int) roww.get("price");
                        roww.put("net_price", price * quantity);*/
                    }
                }

                Log.e("increase_click", String.valueOf(proPrice));
                if (proPrice != 0) {
                    int unitPrice = Integer.parseInt(priceText.getText().toString());

                    unitPrice *= quantity;
                    netPriceText.setText(String.valueOf(unitPrice));

                }
            }
        });

        decreaseQuantity.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                int quantity = Integer.parseInt(quantityText.getText().toString());

                if (quantity > 0) {
                    quantity--;
                }

                quantityText.setText(String.valueOf(quantity));

                for (int i = 0; i < row.size(); i++) {

                    HashMap<String, Object> roww = row.get(i);

                    int id = (int) roww.get("layout_id");

                    if (id == itemDialogLO.getId()) {
                        roww.put("strength", strengthSpinner.getSelectedItem());
                        roww.put("weight", weightSpinner.getSelectedItem());
                        roww.put("quantity", quantityText.getText());

                    }
                }

                Log.e("decrease_click", String.valueOf(proPrice));
                if (proPrice != 0) {
                    int unitPrice = Integer.parseInt(priceText.getText().toString());

                    unitPrice *= quantity;
                    netPriceText.setText(String.valueOf(unitPrice));
                }
            }
        });

//        addItem = (Button) itemDialogLO.findViewById(R.id.dialog_item_add);
        RelativeLayout removeItemLO = (RelativeLayout) itemDialogLO.findViewById(id.remove_layout);
        removeItem = (ImageView) itemDialogLO.findViewById(id.dialog_item_remove);

        verifyDialogListSize();

        removeItem.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                int lastItem = row.size() - 1;

                for (int i = 0; i < row.size(); i++) {
                    if (row.size() == 1) {
                        removeItem.setVisibility(View.GONE);
                    } else {
                        removeItem.setVisibility(View.VISIBLE);
                        HashMap<String, Object> roww = row.get(i);

                        int id = (int) roww.get("layout_id");

                        if (id == itemDialogLO.getId()) {
                            row.remove(i);

                            items.removeView(itemDialogLO);
                        }

                        verifyDialogListSize();
                    }
                }
                Log.e("row_size@remove", String.valueOf(row.size()));
            }
        });

        items.addView(itemDialogLO);
    }

    void verifyDialogListSize() {

        if (row.size() > 1) {
            removeItem.setVisibility(View.VISIBLE);
        } else {
            removeItem.setVisibility(View.GONE);
        }
    }

    @Override
    public void success(JSONObject json) {
        showAlertDialog("Your feedback has been submitted");

    }

    @Override
    public void failure() {
        Toast.makeText(this, "Network Error. Please try again", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void showProgress() {
        progressBar = new CustomProgressDialog(this, "Your request is being sent...");

    }

    @Override
    public void hideProgress() {
        progressBar.dismiss();
    }

    void layoutWithoutSpecification() {
        relativeLayout = (RelativeLayout) View.inflate(this,
                layout.layout_without_specifications, null);

        LinearLayout topLO = (LinearLayout) relativeLayout.findViewById(id.top);
        final TextView priceText = (TextView) topLO.findViewById(id.price_text);
        final TextView netPriceText = (TextView) topLO.findViewById(id.net_price_text);

        if (proPrice == 0) {
            priceText.setText("--");
            netPriceText.setText("--");
        } else {
            priceText.setText(Integer.toString(proPrice));
        }

        ImageView increase = (ImageView) relativeLayout.findViewById(id.increase);
        ImageView decrease = (ImageView) relativeLayout.findViewById(id.decrease);
        final TextView quantityText = (TextView) relativeLayout.findViewById(id.quantity);

        quantityText.setText("1");

        quantity = Integer.parseInt(quantityText.getText().toString());

        increase.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                quantity = Integer.parseInt(quantityText.getText().toString());
                quantity++;

                quantityText.setAlpha((float) .99);
                quantityText.setText(String.valueOf(quantity));

                if (proPrice != 0) {
                    int unitPrice = Integer.parseInt(priceText.getText().toString());

                    unitPrice *= quantity;
                    netPriceText.setText(String.valueOf(unitPrice));
                }
            }
        });

        decrease.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                quantity = Integer.parseInt(quantityText.getText().toString());

                if (quantity != 1) {
                    quantity--;
                    if (quantity == 1) {
                        quantityText.setAlpha((float) .54);
                    }

                    quantityText.setText(String.valueOf(quantity));
                }

                if (proPrice != 0) {
                    int unitPrice = Integer.parseInt(priceText.getText().toString());

                    unitPrice *= quantity;
                    netPriceText.setText(String.valueOf(unitPrice));
                }
            }
        });

        proDescLayout2.addView(relativeLayout);
        buyingOptions.setText("ADD TO CART");
    }

    void layoutWithoutSpecification5() {
        relativeLayout = (RelativeLayout) View.inflate(this,
                layout.layout_without_specifications, null);

        LinearLayout topLO = (LinearLayout) relativeLayout.findViewById(id.top);
        final TextView priceText = (TextView) topLO.findViewById(id.price_text);
        final TextView netPriceText = (TextView) topLO.findViewById(id.net_price_text);

        if (proPrice == 0) {
            priceText.setText("--");
            netPriceText.setText("--");
        } else {
            int unitPrice = proPrice;

            priceText.setText(Integer.toString(unitPrice));
            netPriceText.setText(Integer.toString(unitPrice * 5));

        }

        ImageView increase = (ImageView) relativeLayout.findViewById(id.increase);
        final ImageView decrease = (ImageView) relativeLayout.findViewById(id.decrease);
        final TextView quantityText = (TextView) relativeLayout.findViewById(id.quantity);

        decrease.setAlpha((float) .54);
        quantityText.setAlpha((float) .54);
        decrease.setClickable(false);
        quantityText.setText("5");

        quantity = Integer.parseInt(quantityText.getText().toString());

        increase.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                quantity = Integer.parseInt(quantityText.getText().toString());
                quantity++;

                quantityText.setAlpha((float) .99);
                quantityText.setText(String.valueOf(quantity));

                decrease.setAlpha((float) .99);
                decrease.setClickable(true);

                if (proPrice != 0) {
                    int unitPrice = Integer.parseInt(priceText.getText().toString());

                    unitPrice *= quantity;
                    netPriceText.setText(String.valueOf(unitPrice));
                }
            }
        });

        decrease.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                quantity = Integer.parseInt(quantityText.getText().toString());

                if (quantity > 5) {
                    quantity--;

                    if (quantity == 5) {
                        decrease.setAlpha((float) .54);
                        quantityText.setAlpha((float) .54);
                        decrease.setClickable(false);
                    }
                    quantityText.setText(String.valueOf(quantity));
                }

                if (proPrice != 0) {
                    int unitPrice = Integer.parseInt(priceText.getText().toString());

                    unitPrice *= quantity;
                    netPriceText.setText(String.valueOf(unitPrice));
                }
            }
        });

        proDescLayout2.addView(relativeLayout);
        buyingOptions.setText("ADD TO CART");
    }

    public void showAlertDialog(String message) {
        AlertDialog alert = null;
        Builder alertDialog = new Builder(this);
        LayoutInflater inflater = getLayoutInflater();

        View dialogView = inflater.inflate(layout.gift_layout, null);
        alertDialog.setView(dialogView);
        alert = alertDialog.create();

        TextView title = (TextView) dialogView.findViewById(id.dialogtitle);
        TextView msg = (TextView) dialogView.findViewById(id.dialogmessage);

        ImageView gift = (ImageView) dialogView.findViewById(id.gift);
        gift.setVisibility(View.GONE);

        title.setText("Thanks for your interest!");
        title.setTypeface(font.robotoMedium);
        title.setAlpha((float) .87);
        msg.setText("We will get in touch with you shortly.");
        msg.setTypeface(font.robotoRegular);

        Button gotit = (Button) dialogView.findViewById(id.gotit);

        final AlertDialog finalAlert = alert;
        gotit.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                finalAlert.dismiss();
            }
        });

        // Showing Alert Message
        alert.show();
    }

    class OnLoad extends AsyncTask<String, String, String> {

        Item product_details;
        CustomProgressDialog pdialog;

        String[] tags;

        @Override
        protected void onPreExecute() {

            pdialog = new CustomProgressDialog(ProductDetails.this, "Fetching " +
                    "Details...");

            pdialog.show();

            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                DBManager db = new DBManager(ProductDetails.this);
                db.openDataBase();
                product_details = db.getProductDetails(params[0]);
                Log.e(TAG, "doInBackground: " + product_details);
                db.close();

                category = product_details.category;

                Log.e("product_category", "category is... " + product_details.category);
                Log.e("product_price", "value is... " + product_details.price);

                if (product_details.price.contains(",")) {
                    prices = product_details.price.split(",");
                } else if (product_details.price.equals("NA")) {
                    proPrice = 0;
                } else {
                    proPrice = Integer.parseInt(product_details.price);
                }

                Log.e("proNameOnPost", "name is... " + product_details.name);
                proName = product_details.name;
                Log.e("proNameOnPost1", "name is... " + proName);

                // productDesc.setText(product_details.desc);
                // productTags.setText(product_details.tag);
                Log.e("tags", product_details.tag);
                tags = product_details.tag.split(",");
                Log.e("tags", String.valueOf(tags));

            } catch (Exception e) {
                Log.e("doInBackground", e.toString());
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            pdialog.dismiss();
            super.onPostExecute(s);
            productTitle.setText("");
            //      productDesc.setText(product_details.desc);
            try {
                String pic = product_details.imageid;
                resID = getResources().getIdentifier(pic.substring(0, pic.lastIndexOf(".")),
                        "drawable", getPackageName());
                Log.e("resID_ProductDetails", String.valueOf(resID));
                productImage.setImageDrawable(getResources().getDrawable(resID));

            } catch (Exception e) {
                Log.e("imageid", e.toString());
            }
            try {

                String pic = product_details.pic_name;
                int res_ID = getResources().getIdentifier(pic.substring(0, pic.lastIndexOf(".")),
                        "drawable", getPackageName());
                productimagename.setImageDrawable(getResources().getDrawable(res_ID));

            } catch (Exception e) {
                Log.e("pic_name", e.toString());
            }

            Log.e("opeCategory: ", category);
            Log.e("opeproName", "name is ... " + proName);

            if (category.equals("CHEMICAL PEEL")) {
                Log.e("ope_proName", "name is ... " + proName);
                Log.e("ope_category", "category is ... " + category);

                try {
                    JSONObject products = new JSONObject(JsonDataCopy.jsondata);
                    JSONArray productsjsonarray = products.getJSONArray("data");
                    for (int i = 0; i < productsjsonarray.length(); i++) {
                        JSONObject jobj = productsjsonarray.getJSONObject(i);

                        if (jobj.getString("product_title").equals(proName)) {
                            Log.e("ope_product_title", jobj.getString("product_title"));

                            Object strengthObj = jobj.get("strength");

                            if (strengthObj instanceof String) {
                                strengths = new StringBuilder();

                                strengths.append(strengthObj);
                            } else if (strengthObj instanceof JSONArray) {
                                JSONArray strength = (JSONArray) strengthObj;

                                strengths = new StringBuilder();

                                for (int j = 0; j < strength.length(); j++) {
                                    Log.e("strength_items", String.valueOf(strength.get(j)));
                                    if (j == 0) {
                                        strengths.append(strength.get(0));
                                    } else {
                                        strengths.append("," + strength.get(j));
                                    }
                                }
                            }

                            Object packObj = jobj.get("pack");

                            if (packObj instanceof String) {
                                packs = new StringBuilder();

                                packs.append(packObj);
                            } else if (packObj instanceof JSONArray) {
                                JSONArray pack = (JSONArray) packObj;

                                packs = new StringBuilder();

                                for (int j = 0; j < pack.length(); j++) {
                                    if (j == 0) {
                                        packs.append(pack.get(0));
                                    } else {
                                        packs.append("," + pack.get(j));
                                    }
                                }
                            }

                            Log.e("ope_strengths&packs", strengths + " ; " + packs);

                            if (strengths.toString().equals("NA") && packs.toString().equals("NA")) {
                                layoutWithoutSpecification();
                            } else {
                                relativeLayout = (RelativeLayout) View.inflate(ProductDetails.this,
                                        layout.layout_with_specification, null);
                                LinearLayout middleLayout = (LinearLayout) relativeLayout.findViewById(id.middleLayout);

                                proDescLayout2.addView(relativeLayout);

                                buyingOptions.setText("BUYING OPTIONS");
                            }

                            Object cusPackObj = jobj.get("custom_pack");

                            if (cusPackObj instanceof JSONArray) {
                                JSONArray custom_pack = (JSONArray) cusPackObj;

                                custom_packs = new StringBuilder();

                                for (int j = 0; j < custom_pack.length(); j++) {
                                    if (j == 0) {
                                        custom_packs.append(custom_pack.get(0));
                                    } else {
                                        custom_packs.append("," + custom_pack.get(j));
                                    }
                                }
                            }
                        }
                    }
                } catch (JSONException e) {
                    Log.e("chemical_peel excep", e.toString());
                }
            } else if (category.equals("SERUMS - SKIN CARE") || category.equals("SERUMS - HAIR CARE")) {
                Log.e("ope_category", category);

                try {
                    JSONObject products = new JSONObject(JsonDataCopy.jsondata);
                    JSONArray productsjsonarray = products.getJSONArray("data");
                    for (int i = 0; i < productsjsonarray.length(); i++) {
                        JSONObject jobj = productsjsonarray.getJSONObject(i);

                        if (jobj.getString("product_title").equals(proName)) {
                            Log.e("ope_product_title", jobj.getString("product_title"));
                            Object pack = jobj.get("pack");

                            if (pack instanceof String) {
                                layoutWithoutSpecification();
                            } else if (pack instanceof JSONArray) {

                                relativeLayout = (RelativeLayout) View.inflate(ProductDetails.this,
                                        layout.layout_with_specification, null);
                                proDescLayout2.addView(relativeLayout);

                                buyingOptions.setText("BUYING OPTIONS");

                                packs = new StringBuilder();

                                JSONArray weight = (JSONArray) pack;

                                for (int j = 0; j < weight.length(); j++) {
                                    if (j == 0) {
                                        packs.append(weight.get(0));
                                    } else {
                                        packs.append("," + weight.get(j));
                                    }
                                }
                            }
                        }
                    }
                } catch (JSONException e) {
                    Log.e("skin & hair excep", e.toString());
                }
            } else if (category.equals("CREAMS") || category.equals("OTHER")) {
                Log.e("ope_category", category);
                try {
                    JSONObject products = new JSONObject(JsonDataCopy.jsondata);
                    JSONArray productsjsonarray = products.getJSONArray("data");
                    for (int i = 0; i < productsjsonarray.length(); i++) {
                        JSONObject jobj = productsjsonarray.getJSONObject(i);

//                        Log.e("proTitle", jobj.getString("product_title"));
                        if (jobj.getString("product_title").equals(proName)) {
                            Log.e("proTitle", jobj.getString("product_title"));

                            Object pack = jobj.get("pack");

                            if (!pack.toString().equals("NA")) {
                                if (pack instanceof String) {
                                    layoutWithoutSpecification5();
                                } else if (pack instanceof JSONArray) {

                                    relativeLayout = (RelativeLayout) View.inflate(ProductDetails.this,
                                            layout.layout_with_specification, null);
                                    proDescLayout2.addView(relativeLayout);

                                    buyingOptions.setText("BUYING OPTIONS");

                                    packs = new StringBuilder();

                                    JSONArray weight = (JSONArray) pack;

                                    for (int j = 0; j < weight.length(); j++) {
                                        if (j == 0) {
                                            packs.append(weight.get(0));
                                        } else {
                                            packs.append("," + weight.get(j));
                                        }
                                    }
                                }
                            } else {
                                layoutWithoutSpecification();
                            }
                        }
                    }
                } catch (JSONException e) {
                    Log.e("creams and others excep", e.toString());
                }
            } else if (category.equals("ROLLERS")) {
                Log.e("ope_category", category);
                try {
                    JSONObject products = new JSONObject(JsonDataCopy.jsondata);
                    JSONArray productsjsonarray = products.getJSONArray("data");
                    for (int i = 0; i < productsjsonarray.length(); i++) {
                        JSONObject jobj = productsjsonarray.getJSONObject(i);

                        if (jobj.getString("product_title").equals(proName)) {

                            Object sizeObj = jobj.get("sizes");

                            if (!sizeObj.toString().equals("NA")) {
                                if (sizeObj instanceof String) {
                                    layoutWithoutSpecification();

                                    sizes = new StringBuilder();
                                    sizes.append(sizeObj);
                                } else if (sizeObj instanceof JSONArray) {
                                    JSONArray size = (JSONArray) sizeObj;

                                    relativeLayout = (RelativeLayout) View.inflate(ProductDetails.this,
                                            layout.layout_with_specification, null);
                                    LinearLayout middleLayout = (LinearLayout) relativeLayout.findViewById(id.middleLayout);
                                    proDescLayout2.addView(relativeLayout);

                                    buyingOptions.setText("BUYING OPTIONS");

                                    sizes = new StringBuilder();

                                    for (int j = 0; j < size.length(); j++) {
                                        if (j == 0) {
                                            sizes.append(size.get(0));
                                        } else {
                                            sizes.append("," + size.get(j));
                                        }
                                    }
                                }
                            } else {
                                layoutWithoutSpecification();
                            }
                        }
                    }
                } catch (JSONException e) {
                    Log.e("rollers excep", e.toString());
                }
            } else if (category.equals("MASK")) {
                Log.e("ope_category", category);
                layoutWithoutSpecification();
            }

            //productPrice.setText(product_details.price);
            //productDesc.setText(Html.fromHtml(product_details.desc));
            productDesc.clearView();
            try {
                productDesc.loadDataWithBaseURL(null, product_details.desc, "text/html", "utf-8", null);
            } catch (Exception e) {
                e.printStackTrace();
            }
            productDesc.reload();

            try {
                productDesc.loadData(product_details.desc, "text/html; charset=utf-8", "UTF-8");
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                for (String tag : tags) {
                    Log.e("tag", tag);
                    TextView t = new TextView(getApplicationContext());
                    if (VERSION.SDK_INT >= VERSION_CODES.JELLY_BEAN) {
                        t.setBackground(getResources().getDrawable(drawable.tag_bg));
                        //t.setTextAppearance(Typeface.ITALIC);
                    }
                    LayoutParams llp = new LayoutParams(
                            LayoutParams.WRAP_CONTENT,
                            LayoutParams.WRAP_CONTENT);
                    llp.setMargins(16, 0, 0, 0); // llp.setMargins(left, top, right, bottom);
                    t.setLayoutParams(llp);
                    t.setAlpha((float) .87);
                    t.setText(tag.trim());
                    t.setTextColor(Color.WHITE);
                    t.setTextSize(14);

                    //t.setTextAppearance(Typeface.ITALIC);

                    taglayout.addView(t);
                }
            } catch (Exception e) {
                Log.e("onPostExecute", e.toString());
            }

            Resources res = getResources();
            proName = String.format(res.getString(string.title_product), product_details.name).trim();

            getSupportActionBar().setTitle(proName);
            collapsingToolbarLayout.setTitle(proName);


        }
    }
}
