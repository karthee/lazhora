package lazhora.ui.utils;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by Arivu on 28-09-2016.
 */
public class Font {

    /**
     * The Roboto regular.
     */
    public Typeface robotoRegular, /**
     * The Roboto medium.
     */
    robotoMedium;
    /**
     * The Context.
     */
    Context context;

    /**
     * Instantiates a new Font.
     *
     * @param context the context
     */
    public Font(Context context) {
        this.context = context;
        robotoRegular = Typeface.createFromAsset(context.getAssets(), "Roboto-Regular.ttf");
        robotoMedium = Typeface.createFromAsset(context.getAssets(), "Roboto-Medium.ttf");
    }
}
