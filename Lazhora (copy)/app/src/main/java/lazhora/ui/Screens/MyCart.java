package lazhora.ui.Screens;

import android.R;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import lazhora.presenter.MyCartListImplementation;
import lazhora.presenter.MyCartListPresenter;
import lazhora.ui.utils.SessionMaintainence;
import mycart.MyCartListActivity;
import mycart.MyCartPOJO;

import static android.view.View.VISIBLE;

/**
 * Created by arivuventures on 11/1/17.
 */

public class MyCart extends AppCompatActivity implements
        MyCartListImplementation.MyCartListListener {

    final ArrayList<String> s = new ArrayList<String>();
    final ArrayList<String> w = new ArrayList<String>();
    final ArrayList<Integer> q = new ArrayList<Integer>();
    private List<MyCartPOJO> cartList = new ArrayList<>();
    MyCartPOJO myCartPOJO;
    String[] sArray, wArray;
    int[] qArray;
    int resID;
    SessionMaintainence sm;

    private RelativeLayout layoutMsg;
    private RelativeLayout emptyListViewMsg;

    public static String getData(Context context) {
        try {
            File f = new File(context.getFilesDir().getPath() + "/" + "mycart.json");
            //check whether file exists
            FileInputStream is = new FileInputStream(f);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();

            Log.e("Read Success@mycart", new String(buffer, "UTF-8"));
            return new String(buffer, "UTF-8");
        } catch (IOException e) {
            Log.e("File not exist@mycart", e.getLocalizedMessage());
            return null;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sm = SessionMaintainence.getInstance();

        MyCartListPresenter presenter = new MyCartListImplementation(this);
        presenter.getMyCartList();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
//        startActivity(new Intent(this, HomeActivity.class));
        finish();
    }

    @Override
    public void getCartProductList(List<MyCartPOJO> listasString) {

        MyCartListActivity myCartListActivity = new MyCartListActivity(this, listasString,
                sm.getCusName(), sm.getCusMobile());
        setContentView(myCartListActivity);

        layoutMsg = (RelativeLayout) findViewById(lazhora.R.id.msg);
        emptyListViewMsg = (RelativeLayout) findViewById(android.R.id.empty);
    }

    @Override
    public void emptyList() {
        layoutMsg.setVisibility(View.GONE);
        emptyListViewMsg.setVisibility(View.VISIBLE);
    }
}
