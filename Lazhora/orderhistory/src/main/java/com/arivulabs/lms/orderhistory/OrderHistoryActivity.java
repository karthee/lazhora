package com.arivulabs.lms.orderhistory;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class OrderHistoryActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private List<OrderHistoryPOJO> orderList = new ArrayList<>();
    OrderHistoryPOJO orderHistoryPOJO;
    private OrderHistoryAdapter orderHistoryAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_history);

        getSupportActionBar().setTitle("Order History");

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        orderHistoryAdapter = new OrderHistoryAdapter(this, orderList, new OnItemClickListener() {
            @Override
            public void onItemClick(OrderHistoryPOJO orderHistoryPOJO) {
                startActivity(new Intent(OrderHistoryActivity.this, OrderDetailsActivity.class)/*.
                        putExtra("order_item", orderHistoryPOJO)*/);
                finish();
            }
        });

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(orderHistoryAdapter);

//        loadData();

    }

    private void loadData() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Log.e("sdf", simpleDateFormat.format(calendar.getTime()));

        orderHistoryPOJO = new OrderHistoryPOJO("#12345634",
                simpleDateFormat.format(calendar.getTime()),
                2410);
        orderList.add(orderHistoryPOJO);

        orderHistoryPOJO = new OrderHistoryPOJO("#21545623",
                simpleDateFormat.format(calendar.getTime()), 2010);
        orderList.add(orderHistoryPOJO);

        orderHistoryPOJO = new OrderHistoryPOJO("#12445654",
                simpleDateFormat.format(calendar.getTime()), 1010);
        orderList.add(orderHistoryPOJO);

        orderHistoryPOJO = new OrderHistoryPOJO("#10545656",
                simpleDateFormat.format(calendar.getTime()), 2500);
        orderList.add(orderHistoryPOJO);

        orderHistoryPOJO = new OrderHistoryPOJO("#21345645",
                simpleDateFormat.format(calendar.getTime()), 2480);
        orderList.add(orderHistoryPOJO);

        orderHistoryPOJO = new OrderHistoryPOJO("#28795645",
                simpleDateFormat.format(calendar.getTime()), 3888);
        orderList.add(orderHistoryPOJO);

        orderHistoryPOJO = new OrderHistoryPOJO("#21321545",
                simpleDateFormat.format(calendar.getTime()), 4450);
        orderList.add(orderHistoryPOJO);

        orderHistoryPOJO = new OrderHistoryPOJO("#31345645",
                simpleDateFormat.format(calendar.getTime()), 500);
        orderList.add(orderHistoryPOJO);

        orderHistoryPOJO = new OrderHistoryPOJO("#15345645",
                simpleDateFormat.format(calendar.getTime()), 1590);
        orderList.add(orderHistoryPOJO);

        orderHistoryPOJO = new OrderHistoryPOJO("#44345645",
                simpleDateFormat.format(calendar.getTime()), 2410);
        orderList.add(orderHistoryPOJO);

        orderHistoryAdapter.notifyDataSetChanged();
    }
}
