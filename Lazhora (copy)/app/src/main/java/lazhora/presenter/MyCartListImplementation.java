package lazhora.presenter;

import java.util.List;

import lazhora.Data.CartRepository;
import lazhora.Data.MyCartRepository;
import mycart.MyCartPOJO;

import static java.util.Collections.EMPTY_LIST;

/**
 * Created by Karthee on 17/03/17.
 */

public class MyCartListImplementation implements MyCartListPresenter {

    MyCartListListener listener;
    MyCartRepository myCartRepository;

    public MyCartListImplementation(MyCartListListener listener) {
        this.listener = listener;
        myCartRepository = new CartRepository();
    }

    @Override
    public void getMyCartList() {

        List<MyCartPOJO> list = myCartRepository.getMyCartListfromModel();

        try {
            if (list.size() > 0) {
                listener.getCartProductList(list);
            } else {
                listener.getCartProductList(EMPTY_LIST);
                listener.emptyList();
            }
        } catch (Exception e) {
            listener.getCartProductList(EMPTY_LIST);
            listener.emptyList();

        }

    }


    public interface MyCartListListener {

        void getCartProductList(List<MyCartPOJO> listasString);

        void emptyList();
    }
}
