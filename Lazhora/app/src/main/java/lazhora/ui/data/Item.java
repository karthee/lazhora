package lazhora.ui.data;

/**
 * The type Item.
 */
public class Item {
    /**
     * The Name.
     */
    public String name;
    /**
     * The Imageid.
     */
    public String imageid;
    /**
     * The Desc.
     */
    public String desc;
    /**
     * The Category.
     */
    public String category;
    /**
     * The Price.
     */
    public String price;
    /**
     * The Tag.
     */
    public String tag;
    /**
     * The Pic name.
     */
    public String pic_name;

    /**
     * Instantiates a new Item.
     *
     * @param name     the name
     * @param desc     the desc
     * @param imageid  the imageid
     * @param price    the price
     * @param tag      the tag
     * @param pic_name the pic name
     */
    public Item(String name, String desc, String imageid, String price, String tag, String pic_name) {
        this.name = name;
        this.imageid = imageid;
        this.desc = desc;
        this.price = price;
        this.tag = tag;
        this.pic_name = pic_name;
    }

    /**
     * Instantiates a new Item.
     *
     * @param name     the name
     * @param desc     the desc
     * @param category the category
     * @param imageid  the imageid
     * @param price    the price
     * @param tag      the tag
     * @param pic_name the pic name
     */
    public Item(String name, String desc, String category, String imageid, String price, String tag, String pic_name) {
        this.name = name;
        this.imageid = imageid;
        this.desc = desc;
        this.category = category;
        this.price = price;
        this.tag = tag;
        this.pic_name = pic_name;
    }
}