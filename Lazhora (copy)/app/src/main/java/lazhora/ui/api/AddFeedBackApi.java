package lazhora.ui.api;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import org.json.JSONObject;

import lazhora.BuildConfig;
import lazhora.R;
import lazhora.ui.Screens.HomeActivity;
import lazhora.ui.utils.CustomProgressDialog;
import lazhora.ui.utils.JsonparserObject;
import lazhora.ui.utils.SessionMaintainence;

/**
 * Created by Karthee on 25/01/16.
 */
public class AddFeedBackApi extends AsyncTask<String, String, JSONObject> {

    // output keys for feedback api
    public static final String KEY_OUTPUT_STATUS = "status";
    public static final String KEY_OUTPUT_DATA = "data";
    public static final String KEY_OUTPUT_MESSAGE = "message";
    public static String add_feedback = "add_feedback";
    JSONObject json;
    private CustomProgressDialog progressBar;
    private boolean status;
    private String data;
    private Context context;
    private SessionMaintainence session;

    public AddFeedBackApi(Context context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        /*
         *    do things before doInBackground() code runs
         *    such as preparing and showing a Dialog or ProgressBar
        */
        progressBar = new CustomProgressDialog(context, "Feedback is being sent...");
        session = SessionMaintainence.getInstance();

    }

    @Override
    protected JSONObject doInBackground(String... f_url) {
        JsonparserObject jparser = JsonparserObject.getInstance();

        try {
            JSONObject jobj = new JSONObject();

            jobj.put("title", f_url[0]);
            jobj.put("description", f_url[1]);
            jobj.put("name", session.getCustomerInfo().get(SessionMaintainence.KEY_CUS_NAME));
            jobj.put("rating", f_url[2]);

//            json = jparser.makeHttpRequest(session.getIp() + add_feedback,
            json = jparser.makeHttpRequest(BuildConfig.url + add_feedback,
                    "POST",
                    jobj.toString());

        } catch (Exception e) {
            Log.e("OnAddfeedback", e.toString());

        }

        return json;
    }

    @Override
    protected void onPostExecute(JSONObject json) {
        /*
         *    do something with data here
         *    display it or send to mainactivity
         *    close any dialogs/ProgressBars/etc...
        */

        // dismiss the dialog after the file was downloaded
        try {
            progressBar.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            status = json.getBoolean(KEY_OUTPUT_STATUS);
            data = json.getString(KEY_OUTPUT_DATA);

        } catch (Exception e) {
            Log.e("OnFeedbackStatus", e.toString());
            Toast.makeText(context, "Network Error. Please try again", Toast.LENGTH_SHORT).show();

        }
        if (status) {
            showAlertDialog("Your feedback has been submitted");

        } else {
            Toast.makeText(context, "Network Error. Please try again", Toast.LENGTH_SHORT).show();

        }
    }

    public void showAlertDialog(String message) {
        AlertDialog alertDialog = new AlertDialog.Builder(context)
                                          .create();

        // Setting Dialog Title
//        alertDialog.setTitle(title);

        // Setting Dialog Message
        alertDialog.setMessage(message);

        // Setting alert dialog icon
        //	alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);

        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                if (session.getScreenId().equals("HomeActivity")) {
                    session.setScreenId("");
                    ((Activity) context).finish();
                    System.exit(0);
                } else {
                    Intent i = new Intent(context, HomeActivity.class);
                    context.startActivity(i);
                    ((Activity) context).finish();
                }
            }
        });

        // Showing Alert Message
        alertDialog.show();

        Button pbutton = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        pbutton.setTextColor(context.getResources().getColor(R.color.colorPrimary));
    }

}
