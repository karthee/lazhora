package lazhora.Data;

import java.util.List;

import mycart.MyCartPOJO;

/**
 * Created by Karthee on 17/03/17.
 */

public interface MyCartRepository {
    List<MyCartPOJO> getMyCartListfromModel();
}
