package lazhora.ui;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import lazhora.ui.utils.JsonparserObject;
import lazhora.ui.utils.SessionMaintainence;

/**
 * Created by Karthee on 19/01/17.
 */
public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        JsonparserObject.init(this);
        SessionMaintainence.init(this);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
