package lazhora.ui.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import lazhora.ui.data.Item;


/**
 * Created by Arivu on 12-09-2016.
 */
public class DBManager extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "lazhora";

    //product table
    private static final String TABLE_PRODUCTS = "products";

    private static final String KEY_ID = "_id";
    private static final String KEY_PRODUCT_ID = "product_id";
    private static final String KEY_PRODUCT_TITLE = "product_title";
    private static final String KEY_PRODUCT_DESC = "product_desc";
    private static final String KEY_PRODUCT_PRICE = "product_price";
    /**
     * The constant KEY_PRODUCT_TAG.
     */
    public static final String KEY_PRODUCT_TAG = "product_tag";
    private static final String KEY_PRODUCT_IMAGE = "product_image";

    // category table
    private static final String TABLE_CATEGORY = "categories";

    private static final String KEY_CATEGORY_ID = "category_id";
    private static final String KEY_CATEGORY_NAME = "category_name";

    // category products mappinng table
    private static final String PRODUCTS_CATEGORY = "products_categories";

    // strength table
    private static final String TABLE_STRENGTH = "strength";

    private static final String KEY_STRENGTH_ID = "strength_id";
    private static final String KEY_STRENGTH_NAME = "strength_name";

    // strength products mappinng table
    private static final String PRODUCTS_STRENGTH = "products_STRENGTH";
    private static final String KEY_PRODUCT_CATEGORY = "product_category";
    private static final String KEY_PRODUCT_IMAGE_NAME = "product_image_name";
    private static String DB_PATH;
    private final Context myContext;
    private SQLiteDatabase myDataBase;

    /**
     * Instantiates a new Db manager.
     *
     * @param context the context
     */
    public DBManager(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.myContext = context;
        DB_PATH = "/data/data/" + context.getPackageName() + '/' + "databases/";
    }

    /**
     * Open data base.
     *
     * @throws SQLException the sql exception
     */
    public void openDataBase() throws SQLException {
        try {
            this.getReadableDatabase();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Open the database
        String myPath = DB_PATH + DATABASE_NAME;
        myDataBase = SQLiteDatabase.openDatabase(myPath, null,
                SQLiteDatabase.CREATE_IF_NECESSARY | SQLiteDatabase.OPEN_READWRITE);
    }

    @Override
    public synchronized void close() {

        if (myDataBase != null)
            myDataBase.close();

        super.close();
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        Log.e("DATABASE", "tables created");
        String CREATE_PRODUCT_TABLE = "CREATE TABLE " + TABLE_PRODUCTS + "("
                + KEY_ID + " INTEGER primary key,"
                + KEY_PRODUCT_ID + " TEXT,"
                + KEY_PRODUCT_TITLE + " TEXT,"
                + KEY_PRODUCT_CATEGORY + " TEXT,"
                + KEY_PRODUCT_DESC + " TEXT,"
                + KEY_PRODUCT_PRICE + " TEXT,"
                + KEY_PRODUCT_TAG + " TEXT,"
                + KEY_PRODUCT_IMAGE_NAME + " TEXT,"
                + KEY_PRODUCT_IMAGE + " TEXT)";

        String CREATE_CATEGORIES_TABLE = "CREATE TABLE " + TABLE_CATEGORY + "("
                + KEY_ID + " INTEGER primary key,"
                + KEY_CATEGORY_ID + " TEXT,"
                + KEY_CATEGORY_NAME + " TEXT)";

        String CREATE_STRENGTH_TABLE = "CREATE TABLE " +
                TABLE_STRENGTH + "("
                + KEY_ID + " INTEGER primary key,"
                + KEY_STRENGTH_ID + " TEXT,"
                + KEY_STRENGTH_NAME + " TEXT)";

        String CREATE_PRODUCT_STRENGTH_TABLE = "CREATE TABLE " +
                PRODUCTS_STRENGTH + "("
                + KEY_ID + " INTEGER primary key,"
                + KEY_STRENGTH_ID + " TEXT,"
                + KEY_PRODUCT_ID + " TEXT)";

        String CREATE_PRODUCT_CATEGORY_TABLE = "CREATE TABLE " +
                PRODUCTS_CATEGORY + "("
                + KEY_ID + " INTEGER primary key,"
                + KEY_PRODUCT_ID + " TEXT,"
                + KEY_CATEGORY_ID + " TEXT)";

        sqLiteDatabase.execSQL(CREATE_PRODUCT_TABLE);
        sqLiteDatabase.execSQL(CREATE_CATEGORIES_TABLE);
        sqLiteDatabase.execSQL(CREATE_STRENGTH_TABLE);
        sqLiteDatabase.execSQL(CREATE_PRODUCT_CATEGORY_TABLE);
        sqLiteDatabase.execSQL(CREATE_PRODUCT_STRENGTH_TABLE);

        Log.e("DATABASE", "tables created");
    }

//    public String getSigninPwd(String userName) {
//        SQLiteDatabase db = this.getWritableDatabase();
//
//        Cursor cursor = db.query(TABLE_USER_CRED, null, KEY_USERNAME + " =?",
//                new String[]{userName}, null, null, null);
//        if (cursor.getCount() < 1) {
//            cursor.close();
//            return "NOT EXIST";
//        }
//        cursor.moveToFirst();
//        String password = cursor.getString(cursor.getColumnIndex(KEY_PASSWORD));
//        cursor.close();
//        return password;
//    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        // Drop older table if existed
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_PRODUCTS);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_CATEGORY);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_STRENGTH);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + PRODUCTS_CATEGORY);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + PRODUCTS_STRENGTH);

        // Create tables again
        onCreate(sqLiteDatabase);
    }

    /**
     * Add products.
     *
     * @param product_id    the product id
     * @param product_title the product title
     * @param product_desc  the product desc
     * @param product_price the product price
     * @param product_tag   the product tag
     * @param product_image the product image
     * @param category      the category
     * @param picture_name  the picture name
     */
// Adding new PRODUCT
    public void addProducts(String product_id,
                            String product_title,
                            String product_desc,
                            String product_price,
                            String product_tag,
                            String product_image,
                            String category,
                            String picture_name) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_PRODUCT_ID, product_id);
        values.put(KEY_PRODUCT_TITLE, product_title);
        values.put(KEY_PRODUCT_DESC, product_desc);
        values.put(KEY_PRODUCT_PRICE, product_price);
        values.put(KEY_PRODUCT_TAG, product_tag);
        values.put(KEY_PRODUCT_IMAGE, product_image);
        values.put(KEY_PRODUCT_CATEGORY, category);
        values.put(KEY_PRODUCT_IMAGE_NAME, picture_name);

        int ispresent = db.query(TABLE_PRODUCTS, null, KEY_PRODUCT_ID + "= " +
                        "'" + product_id + "'",
                null, null, null, null, null).getCount();

        if (ispresent > 0) {
            db.update(TABLE_PRODUCTS, values, KEY_PRODUCT_ID + " = '" + product_id + "'",
                    null);
        } else {
            // Inserting Row
            db.insert(TABLE_PRODUCTS, null, values);
        }

        db.close(); // Closing database connection
    }

    /**
     * Add strength.
     *
     * @param strength_id   the strength id
     * @param strength_name the strength name
     */
// Adding new STRENGTH
    public void addStrength(String strength_id, String strength_name) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_STRENGTH_ID, strength_id);
        values.put(KEY_STRENGTH_NAME, strength_name);


        // Inserting Row
        db.insert(TABLE_STRENGTH, null, values);
        db.close(); // Closing database connection
    }

    /**
     * Add categories.
     *
     * @param categories_id the categories id
     * @param category_name the category name
     */
// Adding new Categories
    public void addCategories(String categories_id, String category_name) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_CATEGORY_ID, categories_id);
        values.put(KEY_CATEGORY_NAME, category_name);


        // Inserting Row
        db.insert(TABLE_CATEGORY, null, values);
        db.close(); // Closing database connection
    }

//    // Adding new USER
//    public void addNewUser(UserCredentials userCredentials) {
//        SQLiteDatabase db = this.getWritableDatabase();
//
//        ContentValues values = new ContentValues();
//        values.put(KEY_USERNAME, userCredentials.getUserName());
//        values.put(KEY_PASSWORD, userCredentials.getPassWord());
//
//        // Inserting Row
//        db.insert(TABLE_USER_CRED, null, values);
//        db.close(); // Closing database connection
//    }

    //    // Getting single USER Detail
//    public UserCredentials getUser(int id) {
//        SQLiteDatabase db = this.getReadableDatabase();
//
//        Cursor cursor = db.query(TABLE_USER_CRED, new String[]{KEY_ID,
//                        KEY_USERNAME, KEY_PASSWORD}, KEY_ID + "=?",
//                new String[]{String.valueOf(id)}, null, null, null, null);
//        if (cursor != null)
//            cursor.moveToFirst();
//
//        UserCredentials userCredentials =
//                new UserCredentials(Integer.valueOf(cursor.getString(0)),
//                                           cursor.getString(1), cursor.getString(2));
//        // return contact
//        return userCredentials;
//    }
//

    /**
     * Gets search products.
     *
     * @param prdouct_title the prdouct title
     * @return the search products
     */
// Getting Search Content
    public Cursor getSearchProducts(String prdouct_title) {
        SQLiteDatabase db = this.getReadableDatabase();


        Cursor cursor = db.rawQuery("select * from products where (product_title like '%" + prdouct_title + "%') or  (product_tag like '%" + prdouct_title + "%')",
                null);


        // return contact
        return cursor;
    }

    /**
     * Gets product details.
     *
     * @param prdouct_title the prdouct title
     * @return the product details
     */
// Getting Product Details
    public Item getProductDetails(String prdouct_title) {
        SQLiteDatabase db = this.getReadableDatabase();
        Item product_details = null;
        try {
            Cursor cursor = db.rawQuery("select * from products where " +
                    "product_title like '" +
                    prdouct_title + "'", null);
        /*Cursor cursor = db.rawQuery("select * from products where " +
                "product_title like '" + "%" +
                prdouct_title + "%" + "'", null);*/

            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    product_details = new Item(
                            cursor.getString(cursor.getColumnIndex(KEY_PRODUCT_TITLE)),
                            cursor.getString(cursor.getColumnIndex(KEY_PRODUCT_DESC)),
                            cursor.getString(cursor.getColumnIndex(KEY_PRODUCT_CATEGORY)),
                            cursor.getString(cursor.getColumnIndex(KEY_PRODUCT_IMAGE)),
                            cursor.getString(cursor.getColumnIndex(KEY_PRODUCT_PRICE)),
                            cursor.getString(cursor.getColumnIndex(KEY_PRODUCT_TAG)),
                            cursor.getString(cursor.getColumnIndex(KEY_PRODUCT_IMAGE_NAME)));

                    Log.e("getProDet_Category: ", cursor.getString(cursor.getColumnIndex(KEY_CATEGORY_NAME)));
                    Log.e("getProDet_Price: ", cursor.getString(cursor.getColumnIndex(KEY_PRODUCT_PRICE)));

                } while (cursor.moveToNext());
            }
            cursor.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // return contact
        return product_details;
    }

    /**
     * Gets sorted products.
     *
     * @return the sorted products
     */
// Getting Products as cursor
    public Cursor getSortedProducts() {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery("select * from products group by product_title order by product_title",
                null);


        // return contact
        return cursor;
    }

    /**
     * Gets all products.
     *
     * @return the all products
     */
// Getting All Products
    public List<Item> getAllProducts() {
        List<Item> productList = new ArrayList<Item>();
        // Select All Query
        //String selectQuery = "select * from products group by product_title order by product_title";
        String selectQuery = "select * from products";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Item item = new Item(cursor.getString(cursor.getColumnIndex(KEY_PRODUCT_TITLE)),
                        cursor.getString(cursor.getColumnIndex(KEY_PRODUCT_DESC))
                        , cursor.getString(cursor.getColumnIndex(KEY_PRODUCT_IMAGE)),
                        cursor.getString(cursor.getColumnIndex(KEY_PRODUCT_PRICE)),
                        cursor.getString(cursor.getColumnIndex(KEY_PRODUCT_TAG)),
                        cursor.getString(cursor.getColumnIndex(KEY_PRODUCT_IMAGE_NAME)));

                // Adding product to list
                productList.add(item);
            } while (cursor.moveToNext());
        }
        cursor.close();
        // return product list
        return productList;
    }// Getting All Products

    /**
     * Gets products by category.
     *
     * @param category the category
     * @return the products by category
     */
    public List<Item> getProductsByCategory(String category) {
        List<Item> productList = new ArrayList<Item>();
        // Select All Query
        //String selectQuery = "select * from products group by product_title order by product_title";
        String selectQuery;
        if (!category.equals(""))
            selectQuery = "select * from products where product_category = '" + category + "'";
        else
            selectQuery = "select * from products";


        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Item item = new Item(cursor.getString(cursor.getColumnIndex(KEY_PRODUCT_TITLE)),
                        cursor.getString(cursor.getColumnIndex(KEY_PRODUCT_DESC))
                        , cursor.getString(cursor.getColumnIndex(KEY_PRODUCT_IMAGE)),
                        cursor.getString(cursor.getColumnIndex(KEY_PRODUCT_PRICE)),
                        cursor.getString(cursor.getColumnIndex(KEY_PRODUCT_TAG)),
                        cursor.getString(cursor.getColumnIndex(KEY_PRODUCT_IMAGE_NAME)));

                // Adding product to list
                productList.add(item);
            } while (cursor.moveToNext());
        }
        cursor.close();
        // return product list
        return productList;
    }

    /**
     * Gets categories.
     *
     * @return the categories
     */
    public List<String> getCategories() {
        List<String> categoryList = new ArrayList<String>();
        // Select All Query
        //String selectQuery = "select * from products group by product_title order by product_title";


        String selectQuery = "select Distinct(product_category) from products";


        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {

                // Adding category to list
                categoryList.add(cursor.getString(0));
                // Log.e("db", "getCategories: "+cursor.getString(0) );
            } while (cursor.moveToNext());
        }
        cursor.close();
        // return product list
        return categoryList;
    }
//
//    //get Ranking list
//    public List<HashMap<String, String>> getRanking() {
//        List<HashMap<String, String>> contactList = new ArrayList<>();
//        // Select All Query
//        String selectQuery = "SELECT  * FROM " + TABLE_USER_CRED;
//
//        SQLiteDatabase db = this.getWritableDatabase();
//        Cursor cursor = db.rawQuery(selectQuery, null);
//
//        // looping through all rows and adding to list
//        if (cursor.moveToFirst()) {
//            do {
//                HashMap<String, String> map = new HashMap<>();
//                map.put("name", cursor.getString(1));
//                map.put("time", cursor.getString(5));
//                // Adding contact to list
//                contactList.add(map);
//            } while (cursor.moveToNext());
//        }
//
//        // return contact list
//        return contactList;
//    }
//
//    // Getting Overall USERS Count
//    public int getUsersCount() {
//        String countQuery = "SELECT  * FROM " + TABLE_USER_CRED;
//        SQLiteDatabase db = this.getReadableDatabase();
//        Cursor cursor = db.rawQuery(countQuery, null);
//        cursor.close();
//
//        // return count
//        return cursor.getCount();
//    }
//
//    // Updating single USER
//    public int updateContact(UserCredentials userCredentials) {
//        SQLiteDatabase db = this.getWritableDatabase();
//
//        ContentValues values = new ContentValues();
//        values.put(KEY_USERNAME, userCredentials.getUserName());
//        values.put(KEY_PASSWORD, userCredentials.getPassWord());
//
//        // updating row
//        return db.update(TABLE_USER_CRED, values, KEY_ID + " = ?",
//                new String[]{String.valueOf(userCredentials.getId())});
//    }
//
//    public int updateTime(String name, String timing) {
//        SQLiteDatabase db = this.getWritableDatabase();
//
//        ContentValues values = new ContentValues();
//        values.put(KEY_OVERALL, timing);
//
//        // updating row
//        return db.update(TABLE_USER_CRED, values, KEY_USERNAME + " = ?",
//                new String[]{name});
//    }
//
//    // Deleting single USER
//    public void deleteContact(UserCredentials userCredentials) {
//        SQLiteDatabase db = this.getWritableDatabase();
//        db.delete(TABLE_USER_CRED, KEY_ID + " = ?",
//                new String[]{String.valueOf(userCredentials.getId())});
//        db.close();
//    }
}
