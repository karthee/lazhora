package lazhora.ui.utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;

/**
 * Created by Arivu on 10-12-2015.
 */
public class SessionMaintainence {

    /**
     * The constant PREF_NAME.
     */
    public static final String PREF_NAME = "lazhora";
    /**
     * The constant KEY_CUS_NAME.
     */
    public static final String KEY_CUS_NAME = "cl_cus_name";
    /**
     * The constant KEY_CUS_CONTACT.
     */
    public static final String KEY_CUS_CONTACT = "cl_cus_mobile";
    private static final String KEY_IP = "ip";
    /**
     * The constant KEY_PHNUM1.
     */
    public static final String KEY_PHNUM1 = "phnum1";
    /**
     * The constant KEY_PHNUM2.
     */
    public static final String KEY_PHNUM2 = "phnum2";
    private static final String KEY_ADDRESS = "address";
    private static final String KEY_EMPLOYMENT = "employment";
    private static final String KEY_STATUS = "status";
    private static final String KEY_SCREEN_ID = "screen_id";
    private static final String KEY_CATEGORY = "category";
    /**
     * The Preferences.
     */
    SharedPreferences preferences;
    /**
     * The Editor.
     */
    SharedPreferences.Editor editor;
    /**
     * The Context.
     */
    Context context;
    /**
     * The Private mode.
     */
    int PRIVATE_MODE = 0;
    private static SessionMaintainence sessionMaintainance;

    private SessionMaintainence(Context context) {
        this.context = context;
        preferences = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = preferences.edit();
    }

    /**
     * Init.
     *
     * @param context the context
     */
    public static void init(Context context) {
        if (sessionMaintainance == null) {
            sessionMaintainance = new SessionMaintainence(context);
        }
    }

    /**
     * Gets instance.
     *
     * @return the instance
     */
    public static SessionMaintainence getInstance() {
        return sessionMaintainance;
    }

    /**
     * Gets ip.
     *
     * @return the ip
     */
    public String getIp() {

        String userdetail1 = "http://52.201.218.175:93/api/App/";
        return userdetail1;
    }

    /**
     * Gets cus name.
     *
     * @return the cus name
     */
    public String getCusName() {
        return preferences.getString(KEY_CUS_NAME, "");
    }

    /**
     * Sets cus name.
     *
     * @param screenId the screen id
     */
    public void setCusName(String screenId) {
        editor.putString(KEY_CUS_NAME, screenId);

        editor.commit();
    }

    /**
     * Gets cus mobile.
     *
     * @return the cus mobile
     */
    public String getCusMobile() {
        return preferences.getString(KEY_PHNUM1, "");
    }

    /**
     * Sets cus mobile.
     *
     * @param screenId the screen id
     */
    public void setCusMobile(String screenId) {
        editor.putString(KEY_PHNUM1, screenId);

        editor.commit();
    }

    /**
     * Gets status.
     *
     * @return the status
     */
    public boolean getStatus() {
        return preferences.getBoolean(KEY_STATUS, false);
    }

    /**
     * Sets status.
     *
     * @param status the status
     */
    public void setStatus(boolean status) {
        editor.putBoolean(KEY_STATUS, status);

        editor.commit();
    }

    /**
     * Gets screen id.
     *
     * @return the screen id
     */
    public String getScreenId() {
        return preferences.getString(KEY_SCREEN_ID, "");
    }

    /**
     * Sets screen id.
     *
     * @param screenId the screen id
     */
    public void setScreenId(String screenId) {
        editor.putString(KEY_SCREEN_ID, screenId);

        editor.commit();
    }

    /**
     * Gets category.
     *
     * @return the category
     */
    public String getCategory() {
        return preferences.getString(KEY_CATEGORY, "");
    }

    /**
     * Sets category.
     *
     * @param category the category
     */
    public void setCategory(String category) {
        editor.putString(KEY_CATEGORY, category);

        editor.commit();
    }

    /**
     * Sets customer info.
     *
     * @param name       the name
     * @param phnum1     the phnum 1
     * @param phnum2     the phnum 2
     * @param address    the address
     * @param employment the employment
     */
    public void setCustomerInfo(String name,
                                String phnum1,
                                String phnum2,
                                String address,
                                String employment) {
        editor.putString(KEY_CUS_NAME, name);
        editor.putString(KEY_PHNUM1, phnum1);
        editor.putString(KEY_PHNUM2, phnum2);
        editor.putString(KEY_ADDRESS, address);
        editor.putString(KEY_EMPLOYMENT, employment);

        editor.commit();
    }

    /**
     * Gets customer info.
     *
     * @return the customer info
     */
    public HashMap<String, String> getCustomerInfo() {
        HashMap<String, String> cus_datas = new HashMap<>();
        cus_datas.put(KEY_CUS_NAME, preferences.getString(KEY_CUS_NAME, ""));
        cus_datas.put(KEY_PHNUM1, preferences.getString(KEY_PHNUM1, ""));
        cus_datas.put(KEY_PHNUM2, preferences.getString(KEY_PHNUM2, ""));
        cus_datas.put(KEY_ADDRESS, preferences.getString(KEY_ADDRESS, ""));
        cus_datas.put(KEY_EMPLOYMENT, preferences.getString(KEY_EMPLOYMENT, ""));

        return cus_datas;
    }

    /**
     * Clear session.
     */
    public void clearSession() {
        editor.clear();
        editor.commit();
    }
}
