package lazhora.ui.Screens;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;
import lazhora.R;
import lazhora.ui.api.AddUserinfoApi;
import lazhora.ui.utils.Font;
import lazhora.ui.utils.NetworkConnectionVerification;
import lazhora.ui.utils.SessionMaintainence;

/**
 * Created by Arivu on 29-09-2016.
 */

public class UserInformation extends AppCompatActivity {

    TextView userName, userPhone1, userPhone2, userLocation;
    Button submit;

    Font font;
    Spinner userEmployStatus;
    SessionMaintainence sessionmanager;

    boolean doubleBackToExitPressedOnce = false,
            nameEmpty = false,
            phoneEmpty = false,
            employStateEmpty = false,
            locationEmpty = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_user_info);

        font = new Font(UserInformation.this);

//        SpannableString pageTitle = new SpannableString(getSupportActionBar().getTitle());
//        pageTitle.setSpan(font.robotoMedium, 0, pageTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
//        getSupportActionBar().setTitle(pageTitle);

        final NetworkConnectionVerification ncv = new NetworkConnectionVerification(UserInformation.this);

        userName = (TextView) findViewById(R.id.user_name);
        userPhone1 = (TextView) findViewById(R.id.user_phone1);
        userPhone2 = (TextView) findViewById(R.id.user_phone2);
        userEmployStatus = (Spinner) findViewById(R.id.user_employment_status);
        userLocation = (TextView) findViewById(R.id.user_location);

        submit = (Button) findViewById(R.id.user_submit);

        userName.setTypeface(font.robotoRegular);
        userPhone1.setTypeface(font.robotoRegular);
        userPhone2.setTypeface(font.robotoRegular);
        // userEmployStatus.setTypeface(font.robotoRegular);
        userLocation.setTypeface(font.robotoRegular);

        //initializing session
        sessionmanager = SessionMaintainence.getInstance();

        if (!sessionmanager.getCustomerInfo().get(SessionMaintainence
                .KEY_CUS_NAME).equals("")) {
            Intent i = new Intent(UserInformation.this, HomeActivity.class);
            startActivity(i);
            finish();
            Log.e("userinfo", "onCreate: home " + sessionmanager.getCustomerInfo().
                    get(SessionMaintainence.KEY_CUS_NAME));
        } else {
            Log.e("userinfo", "onCreate: " + sessionmanager.getCustomerInfo().
                    get(SessionMaintainence.KEY_CUS_NAME));
        }

        /*userName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                isNameEmpty(String.valueOf(charSequence));
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        userPhone1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                isPhoneEmpty(String.valueOf(charSequence));
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        userEmployStatus.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                isEmployStateEmpty(String.valueOf(charSequence));
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        userLocation.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                isLocationEmpty(String.valueOf(charSequence));
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });*/

        submit.setTypeface(font.robotoMedium);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isNameEmpty(userName.getText().toString().trim());
                isPhoneEmpty(userPhone1.getText().toString().trim());
                isEmployStateEmpty(userEmployStatus.getSelectedItem().toString().trim());
                isLocationEmpty(userLocation.getText().toString().trim());

                String username = userName.getText().toString().trim();
                String phone1 = userPhone1.getText().toString().trim();
                String phone2 = userPhone2.getText().toString().trim();
                String employment = userEmployStatus.getSelectedItem().toString();
                String Location = userLocation.getText().toString().trim();

                if (!phone1.matches("\\d{10}")) {
                    userPhone1.setError("Enter valid mobile number!");
                } else {

                    if (!nameEmpty && !phoneEmpty && !employStateEmpty && !locationEmpty) {
                        try {
                            if (ncv.isNetworkAvailable()) {
                                new AddUserinfoApi(UserInformation.this).execute(
                                        username, phone1, phone2, Location, employment);
                            } else {
                                Toast.makeText(UserInformation.this,
                                        "Please check for internet connection.",
                                        Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
//                    sessionmanager.setCustomerInfo(username, Phone1, Phone2,
//                            Location, employment);
//                    sessionmanager.setStatus(true);
//
//                    Intent i = new Intent(UserInformation.this, HomeActivity.class);
//                    startActivity(i);
//                    finish();
                    } else {
                        Toast.makeText(UserInformation.this,
                                "Please assure that mandatory fields are entered.",
                                Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    public void isNameEmpty(String name) {
        if (name.equals(""))
            nameEmpty = true;
        else
            nameEmpty = false;
    }

    public void isPhoneEmpty(String phone) {
        if (phone.equals(""))
            phoneEmpty = true;
        else
            phoneEmpty = false;
    }

    public void isEmployStateEmpty(String employState) {
        if (employState.equals(getResources().getStringArray(R.array.employment)[0]))
            employStateEmpty = true;
        else
            employStateEmpty = false;
    }

    public void isLocationEmpty(String location) {
        if (location.equals(""))
            locationEmpty = true;
        else
            locationEmpty = false;
    }

    @Override
    public void onBackPressed() {
//        if (doubleBackToExitPressedOnce) {
//            super.onBackPressed();
//            return;
//        }
//
//        this.doubleBackToExitPressedOnce = true;
//        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
//
//        new Handler().postDelayed(new Runnable() {
//
//            @Override
//            public void run() {
//                doubleBackToExitPressedOnce = false;
//            }
//        }, 2000);
        showExitDialog("Do you want to exit?");

    }

    private void showExitDialog(String s) {
//        AlertDialog alertDialog = new AlertDialog.Builder(this)
//                                          .create();
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        // Setting Dialog Title
//        alertDialog.setTitle(title);

        // Setting Dialog Message
        alertDialog.setMessage(s);

        //  Setting alert dialog icon
        //	alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);

        // Setting Yes Button
        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                finish();
                System.exit(0);
            }
        });

        // Setting No Button
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        // Showing Alert Message
        AlertDialog alert = alertDialog.create();
        alert.show();
        Button pbutton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        pbutton.setTextColor(getResources().getColor(R.color.colorPrimary));
        Button nbutton = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
        nbutton.setTextColor(getResources().getColor(R.color.colorPrimary));

    }

}
