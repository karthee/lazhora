package mycart;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.List;

import lazhora.R.id;
import lazhora.R.layout;
import lazhora.presenter.MyCartListImplementation;
import lazhora.presenter.MyCartListPresenter;
import lazhora.ui.utils.SessionMaintainence;
import mycart.adapter.MyCartRecAdapter;
import mycart.adapter.listener.DeleteProductListener;
import mycart.adapter.listener.FooterListener;


/**
 * Created by arivuventures on 21/12/16.
 */

public class MyCartListActivity extends RelativeLayout implements
        DeleteProductListener, MyCartListImplementation.MyCartListListener, FooterListener {

    private static final String TAG = "MyCartListActivity";

    private static String name, phone;
    private final Context context;

    MyCartAdapter myCartAdapter;
    private RelativeLayout layoutMsg;
    private RelativeLayout emptyListViewMsg;
    private RecyclerView cartListView;
    private List<MyCartPOJO> list = new ArrayList<>();
    private MyCartRecAdapter myCartRecAdapter;

    public MyCartListActivity(Context context) {
        super(context);
        this.context = context;

        init(context);

    }

    public MyCartListActivity(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init(context);
    }

    public MyCartListActivity(Context context, List<MyCartPOJO> lists) {
        super(context);
        this.context = context;
        list = lists;
        init(context, list);
    }

    public MyCartListActivity(Context context, List<MyCartPOJO> lists,
                              String name, String phone) {
        super(context);
        this.context = context;
        list = lists;
        init(context, list);
        MyCartListActivity.name = name;
        MyCartListActivity.phone = phone;
    }

    private void init(Context context) {
        View.inflate(context, layout.activity_my_cart, this);

    }

    private void init(final Context context, List<MyCartPOJO> items) {
        View.inflate(context, layout.activity_my_cart, this);

        cartListView = (RecyclerView) findViewById(id.mycart_list);
        cartListView.setLayoutManager(new LinearLayoutManager(getContext()));

        layoutMsg = (RelativeLayout) findViewById(id.msg);

        emptyListViewMsg = (RelativeLayout) findViewById(android.R.id.empty);

        Button browseProducts = (Button) emptyListViewMsg.findViewById(id.browse_products);

        browseProducts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((Activity) getContext()).finish();
            }
        });

        myCartRecAdapter = new MyCartRecAdapter(this, this);

        cartListView.setAdapter(myCartRecAdapter);

        myCartRecAdapter.setCartList(list);

    }

    @Override
    public void deleteProduct(MyCartPOJO MyCartModel) {
        Log.e(TAG, "deleteProduct");
        MyCartListPresenter presenter = new MyCartListImplementation(this);
        presenter.getMyCartList();
    }

    @Override
    public void getCartProductList(List<MyCartPOJO> listasString) {
        myCartRecAdapter.setCartList(listasString);

    }

    @Override
    public void emptyList() {
        Log.e("emptylist: ", "called");
        this.layoutMsg.setVisibility(View.GONE);
        emptyListViewMsg.setVisibility(VISIBLE);
    }

    @Override
    public void emptylist() {
        Log.e("emptylist: ", "called");
        this.layoutMsg.setVisibility(View.GONE);
        emptyListViewMsg.setVisibility(VISIBLE);
    }
}
