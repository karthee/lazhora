package com.arivulabs.lms.mycart;

import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

import static android.R.attr.category;

/**
 * Created by arivuventures on 6/1/17.
 */

public class MyCartPOJO implements Parcelable {

    private String productTitle;
    //private String productImagePath;
    Drawable productImagePath;
    private int productPrice;
    private String category;
    private String[] strength;
    private String[] weight;
    private int[] quantity;
    private int quantity2 = 0;

    public MyCartPOJO(String proTitle,
                      Drawable proImagePath,
                      int proPrice,
                      String category,
                      int quantity) {
        setProductTitle(proTitle);
        setProductImagePath(proImagePath);
        setProductPrice(proPrice);
        setProductCategory(category);
        setQuantity2(quantity);
        /*setStrength(null);
        setWeight(null);
        setQuantity(null);*/
    }

    public MyCartPOJO(String proTitle,
                      Drawable proImagePath,
                      int proPrice,
                      String category,
                      String[] weight,
                      int[] quantity) {
        setProductTitle(proTitle);
        setProductImagePath(proImagePath);
        setProductPrice(proPrice);
        setProductCategory(category);
//        setQuantity2(0);
        setWeight(weight);
        setQuantity(quantity);
    }

    public MyCartPOJO(String proTitle,
                      Drawable proImagePath,
                      int proPrice,
                      String category,
                      String[] strength,
                      String[] weight,
                      int[] quantity) {
        setProductTitle(proTitle);
        setProductImagePath(proImagePath);
        setProductPrice(proPrice);
        setProductCategory(category);
//        setQuantity2(0);
        setStrength(strength);
        setWeight(weight);
        setQuantity(quantity);
    }

    public MyCartPOJO() {
        productTitle = "";
        productPrice = 0;
        category = "";
        productImagePath = null;
        strength = null;
        weight = null;
        quantity = null;
        quantity2 = 0;
    }

    protected MyCartPOJO(Parcel in) {
        productTitle = in.readString();
        //productImagePath = in.readParcelable(Drawable.class.getClassLoader());
        productPrice = in.readInt();
        category = in.readString();
        strength = in.createStringArray();
        weight = in.createStringArray();
        quantity = in.createIntArray();
        quantity2 = in.readInt();
    }

    public static final Creator<MyCartPOJO> CREATOR = new Creator<MyCartPOJO>() {
        @Override
        public MyCartPOJO createFromParcel(Parcel in) {
            return new MyCartPOJO(in);
        }

        @Override
        public MyCartPOJO[] newArray(int size) {
            return new MyCartPOJO[size];
        }
    };

    public String getProductTitle() {
        return productTitle;
    }

    public void setProductTitle(String productTitle) {
        this.productTitle = productTitle;
    }

    public Drawable getProductImagePath() {
        return productImagePath;
    }

    public void setProductImagePath(Drawable productImagePath) {
        this.productImagePath = productImagePath;
    }

    public int getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(int productPrice) {
        this.productPrice = productPrice;
    }

    public String getProductCategory() {
        return category;
    }

    public void setProductCategory(String category) {
        this.category = category;
    }

    public String[] getStrength() {
        return strength;
    }

    public void setStrength(String[] strength) {
        this.strength = strength;
    }

    public String[] getWeight() {
        return weight;
    }

    public void setWeight(String[] weight) {
        this.weight = weight;
    }

    public int[] getQuantity() {
        return quantity;
    }

    public void setQuantity(int[] quantity) {
        this.quantity = quantity;
    }

    public int getQuantity2() {
        return quantity2;
    }

    public void setQuantity2(int quantity2) {
        this.quantity2 = quantity2;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(productTitle);
        //parcel.writeParcelable((Parcelable) productImagePath, i);
        parcel.writeInt(productPrice);
        parcel.writeString(category);
        parcel.writeStringArray(strength);
        parcel.writeStringArray(weight);
        parcel.writeIntArray(quantity);
        parcel.writeInt(quantity2);
    }
}
