package com.arivulabs.lms.orderhistory;

/**
 * Created by arivuventures on 11/1/17.
 */

public interface OnItemClickListener {

    void onItemClick(OrderHistoryPOJO orderHistoryPOJO);
}
